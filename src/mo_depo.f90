!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_depo

  USE mo_constants,    ONLY: wp, pi, mp, z1_3, pi_6, z3_4pi
  USE mo_sp_types,     ONLY: t_sp, sp_stat
  USE mo_atmo_types,   ONLY: Rv, rholi, rhoii, T_3, c_pa, c_pw, rhoi
  USE mo_atmo,         ONLY: t_atmo, diffusivity_vapour, &
                             latentHeat_depo, latentHeat_evap, thermalCond_dry, &
                             psat_ice, psat_water
  USE mo_atmo_nml,     ONLY: IGF_id, tab, IGFdim, get_IGF
  USE mo_sp_nml,       ONLY: sp_habit
#ifndef WDROPS  
  USE mo_mass2diam,    ONLY: m2d_particle, m2d_particle_solid, spheroid_geometry
  USE mo_velocity,     ONLY: vterm_snow
#endif  

  IMPLICIT NONE
  PRIVATE

  INTEGER, PARAMETER :: subcycles = 1

#ifndef WDROPS
  PUBLIC  :: sp_depo, sp_depo_iteration, ventilation_coef_IGF
  PRIVATE :: vapourDiffusion,        &
             vapourDiffusionMelting, &
             ventilation_coef_i,     & ! TODO unify ventilation coefs for smooth transitions
             ventilation_coef_w,     &
             ventilation_coef_m,     &
             capacitance
             
#endif
  

CONTAINS


#ifndef WDROPS
!
! condensation/evaporation of super droplets
!
SUBROUTINE sp_depo( sp, atmo, dt, q_heat, l_delnode, dep_rate, dep_mono, heat_rate )
  TYPE(t_sp),   INTENT(inout)  :: sp
  TYPE(t_atmo), INTENT(in)     :: atmo
  REAL(wp),     INTENT(in)     :: dt
  REAL(wp),     INTENT(inout)  :: q_heat
  LOGICAL,      INTENT(out)    :: l_delnode
  REAL(wp),     INTENT(inout)  :: dep_rate, dep_mono, heat_rate

  INTEGER               :: inewton
  REAL(wp)              :: m_t0, dtg_f, &
                           mtot, mdiff, qdiff, latentHeatCoeff
  REAL(wp)              :: temp_M, vt, V_new, phi_tend

  !REAL(wp), PARAMETER :: rho_rdep = 500.0_wp
                    
  dtg_f = dt / REAL(subcycles,wp)

  l_delnode = .FALSE.
  mtot = sp%m_i + sp%m_f + sp%m_r + sp%m_w
  m_t0 = mtot

  DO inewton = 1, subcycles
    CALL m2d_particle( sp )
    vt = vterm_snow( atmo, sp )  ! modify heat and vapour transfer by ventilation
    !IF( .NOT. BTEST(sp%statusb,sp_stat%liquidOut) ) THEN ! no liquid water outside
    IF( sp%m_w < TINY(sp%m_w) ) THEN ! no liquid water outside
      CALL vapourDiffusion( sp, vt, dtg_f, atmo, mdiff, qdiff, latentHeatCoeff, V_new, phi_tend )
      q_heat= q_heat + dtg_f*qdiff
      heat_rate = heat_rate + dtg_f*mdiff*latentHeatCoeff*sp%xi
      mdiff =          dtg_f*mdiff
      IF( mdiff > 0.0_wp ) THEN                          ! deposition
        IF ( ( sp_habit == 1 .AND. sp%mm < 1.01 ) .OR. (sp_habit == 2) ) THEN
          temp_M = 1._wp
          ! Update of habit specific variables
          sp%phi = sp%phi * phi_tend    
          sp%V_i = V_new
        ELSE
          temp_M = sp%m_i / (sp%m_i+sp%m_r+sp%m_f)
        END IF       
        sp%m_i = sp%m_i + temp_M*mdiff
        sp%m_f = sp%m_f + (1.0_wp-temp_M)*mdiff
        !mdiff = mdiff/(sp%m_i + sp%m_r)
        !sp%m_i = sp%m_i + sp%m_i * mdiff 
        !sp%m_r = sp%m_r + sp%m_r * mdiff
        !sp%v_r = sp%v_r + sp%m_r * mdiff / max(rho_rdep,MERGE(sp%m_r/sp%v_r, 0._wp, sp%v_r > 0)) 
      ELSE
        ! mdiff = 1.0_wp + mdiff/(sp%m_i + sp%m_f)
        mdiff = 1.0_wp + mdiff/(sp%m_i + sp%m_f + sp%m_r)
        IF( mdiff < TINY(mdiff) ) THEN                   ! if ice crystal and rime mass is gone remove SP
          IF( sp%m_w < TINY(sp%m_w) ) THEN
            l_delnode = .TRUE. 
            EXIT
          ELSE
            mdiff = 0.0_wp
          ENDIF
        ENDIF                                            ! sublimation
        sp%m_i = sp%m_i*mdiff
        sp%m_f = sp%m_f*mdiff
        sp%m_r = sp%m_r*mdiff
        sp%V_r = sp%V_r*mdiff ! keep rho_r the same
        sp%V_i = sp%V_i * mdiff
      END IF
    ELSE 
      IF( (sp%m_i + sp%m_f + sp%m_r) < TINY(sp%m_i) ) THEN        ! only liquid water
          CALL vapourDiffusion( sp, vt, dtg_f, atmo, mdiff, qdiff, latentHeatCoeff, V_new, phi_tend )
          q_heat = q_heat   + dtg_f*qdiff
          heat_rate = heat_rate + dtg_f*mdiff*latentHeatCoeff*sp%xi
          sp%m_w = sp%m_w   + dtg_f*mdiff
          IF( sp%m_w < TINY(sp%m_w) ) THEN               ! if water mass is evaporated, remove SP
            l_delnode = .TRUE.
            EXIT
          END IF
      ELSE                                               ! melting
        CALL vapourDiffusionMelting( sp, vt, atmo, mdiff, qdiff, latentHeatCoeff )
        q_heat = q_heat + dtg_f*qdiff
        heat_rate = heat_rate + dtg_f*mdiff*latentHeatCoeff*sp%xi
        mdiff =           dtg_f*mdiff
        sp%m_w = MAX( 0.0_wp, sp%m_w + mdiff )
      END IF
    END IF
  END DO

  IF (.not.l_delnode) THEN
     mtot = sp%m_i + sp%m_f + sp%m_r + sp%m_w
  ELSE
     mtot = 0.0_wp
  ENDIF
  dep_rate = dep_rate +  sp%xi*(mtot-m_t0)/dt
  IF (sp%mm < 1.01_wp) THEN
    dep_mono = dep_mono + sp%xi*(mtot-m_t0)/dt
  END IF
END SUBROUTINE sp_depo

!
! implicit euler for particle temperature (explicit is unstable as equation is stiff for small thermalInteria
!
SUBROUTINE sp_depo_iteration( sp, atmo, dt, q_heat, l_delnode, dep_rate, dep_mono, heat_rate )
  USE mo_atmo_types, ONLY: c_pw, c_pi
  TYPE(t_sp),   INTENT(inout)  :: sp
  TYPE(t_atmo), INTENT(in)     :: atmo
  REAL(wp),     INTENT(in)     :: dt
  REAL(wp),     INTENT(inout)  :: q_heat
  LOGICAL,      INTENT(out)    :: l_delnode
  REAL(wp),     INTENT(inout)  :: dep_rate, dep_mono, heat_rate

  REAL(wp) :: thermalInertia, f2, f1, fn, xn_heat, x1_heat, x2_heat
  TYPE(t_sp) :: x2, x1, xn
  INTEGER  :: counter
  INTEGER, PARAMETER :: max_iterations = 100
  INTEGER, SAVE :: max_counter = -1
  REAL(wp), PARAMETER :: eps = T_3*1.0e-7_wp
  REAL(wp) :: dMdt, dQdt, factor, vt, L, T_old, V_new, phi_tend
  LOGICAL :: l_melting

  T_old = sp%T
  thermalInertia = c_pw*sp%m_w + c_pi*(sp%m_i+sp%m_f+sp%m_r)
  l_melting = .FALSE.
  IF( (sp%m_w > 0.0_wp) .AND. (sp%m_i + sp%m_f + sp%m_r > 0.0_wp) ) THEN
    l_melting = .TRUE.
    sp%T = T_3
    counter = 0
  ELSE
    CALL m2d_particle( sp )
    vt = vterm_snow( atmo, sp )

    x2 = sp
    !CALL vapourDiffusionTemperature( x2, vt, dt, atmo, dMdt, dQdt, V_new, phi_tend )
    !x2_heat = q_heat + dt*dQdt
    !x2%T = MIN( MAX( atmo%T - 50.0_wp,  sp%T - x2_heat/thermalInertia ), atmo%T + 50.0_wp ) ! restrict values to avoid NAN
    CALL vapourDiffusionTemperature( x2, vt, dt, atmo, dMdt, dQdt, V_new, phi_tend )
    x2_heat = q_heat + dt*dQdt
    f2 = (x2%T - sp%T)*thermalInertia + x2_heat

    x1 = sp
    CALL vapourDiffusion( x1, vt, dt, atmo, dMdt, dQdt, L, V_new, phi_tend, factor )
    x1%T = atmo%T + factor*dMdt
    CALL vapourDiffusionTemperature( x1, vt, dt, atmo, dMdt, dQdt, V_new, phi_tend )
    x1_heat = q_heat + dt*dQdt
    f1 = (x1%T - sp%T)*thermalInertia + x1_heat

    IF( ABS(f1) < ABS(f2) ) THEN
    ELSE
      fn = f2
      f2 = f1
      f1 = fn
      xn = x2
      x2 = x1
      x1 = xn
    ENDIF
    xn = x1
    fn = f1

    l_delnode = .FALSE.
    DO counter=1,max_iterations
      IF( (ABS(f1-f2) < eps*thermalInertia) .OR. (ABS(fn) < eps*thermalInertia) ) THEN
        EXIT
      ENDIF

      l_melting = .FALSE.
      xn = sp
      xn%T = (x2%T*f1-x1%T*f2)/(f1-f2)
      IF( (xn%T > T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) THEN
        l_melting = .TRUE. ! ice cannot be superwarmed
        xn%T = T_3
      ENDIF
      CALL vapourDiffusionTemperature( xn, vt, dt, atmo, dMdt, dQdt, V_new, phi_tend )
      xn_heat = q_heat + dt*dQdt
      fn = (xn%T - sp%T)*thermalInertia + xn_heat

      f2 = f1
      x2 = x1
      f1 = fn
      x1 = xn
    ENDDO
    IF( counter > max_iterations ) THEN
      WRITE(*,*) "no conv", sp%z, sp%m_w+sp%m_i+sp%m_f+sp%m_r, q_heat, sp%T, xn%T
      l_delnode = .TRUE.
      RETURN
    ENDIF
    max_counter = MAX(counter, max_counter)
    sp%T = xn%T
  ENDIF
  CALL sp_depo_temp( sp, atmo, dt, q_heat, l_delnode, dep_rate, dep_mono, heat_rate )
  IF( .NOT. l_melting ) THEN 
    q_heat = 0.0_wp
  ELSE
    q_heat = q_heat + (sp%T-T_old)*thermalInertia
  ENDIF
END SUBROUTINE sp_depo_iteration


!
! condensation/evaporation of super droplets with (surface temperature)
!
SUBROUTINE sp_depo_temp( sp, atmo, dt, q_heat, l_delnode, dep_rate, dep_mono, heat_rate )
  TYPE(t_sp),   INTENT(inout)  :: sp
  TYPE(t_atmo), INTENT(in)     :: atmo
  REAL(wp),     INTENT(in)     :: dt
  REAL(wp),     INTENT(inout)  :: q_heat
  LOGICAL,      INTENT(out)    :: l_delnode
  REAL(wp),     INTENT(inout)  :: dep_rate, dep_mono, heat_rate

  ! TODO heat_rate
  INTEGER               :: inewton
  REAL(wp)              :: m_t0, dtg_f, temp_M,     &
                           mtot, mdiff, qdiff, vt,  &
                           L, V_new, phi_tend

  dtg_f = dt / REAL(subcycles,wp)

  l_delnode = .FALSE.
  mtot = sp%m_i + sp%m_f + sp%m_r + sp%m_w
  m_t0 = mtot

  DO inewton = 1, subcycles
    CALL m2d_particle( sp )
    vt = vterm_snow( atmo, sp )  ! modify heat and vapour transfer by ventilation
    CALL vapourDiffusionTemperature( sp, vt, dtg_f, atmo, mdiff, qdiff, V_new, phi_tend, L )

    q_heat = q_heat + dtg_f*qdiff
    mdiff  =          dtg_f*mdiff
    heat_rate = heat_rate - dtg_f*qdiff*sp%xi
    IF(       sp%m_w <            TINY(sp%m_w) ) THEN ! pure ice
      IF( mdiff > 0.0_wp ) THEN                       ! deposition
        IF ( ( sp_habit == 1 .AND. sp%mm < 1.01 ) .OR. (sp_habit == 2) ) THEN
          temp_M = 1._wp
          ! Update of habit specific variables
          sp%phi = sp%phi * phi_tend    
          sp%V_i = V_new
        ELSE
          temp_M = sp%m_i / (sp%m_i+sp%m_r+sp%m_f)
        END IF 
        sp%m_i = sp%m_i + mdiff*temp_M
        sp%m_f = sp%m_f + mdiff*(1.0_wp-temp_M)
      ELSE                                            ! sublimation
        mdiff  = 1.0_wp + mdiff/(sp%m_i+sp%m_f+sp%m_r)
        sp%m_i = MAX( 0.0_wp, sp%m_i*mdiff )
        sp%m_f = MAX( 0.0_wp, sp%m_f*mdiff )
        sp%m_r = MAX( 0.0_wp, sp%m_r*mdiff )
        sp%v_r = MAX( 0.0_wp, sp%v_r*mdiff ) ! keep rho_r the same
        sp%V_i = MERGE(sp%V_i * mdiff, 0._wp, sp%m_i > TINY(sp%m_i))
      ENDIF
    ELSE                                              ! pure liquid or melting
        sp%m_w = MAX( 0.0_wp, sp%m_w + mdiff)         ! ToDo evaporate also ice in this case?
    ENDIF
    mtot = sp%m_i + sp%m_f + sp%m_r + sp%m_w
    IF( mtot < TINY(sp%m_i) ) THEN
      l_delnode = .TRUE.                              ! remove SP if mass is gone
      EXIT
    ENDIF
  ENDDO

  dep_rate = dep_rate +  sp%xi*(mtot-m_t0)/dt
  IF (sp%mm < 1.01_wp) THEN
    dep_mono = dep_mono + sp%xi*(mtot-m_t0)/dt
  END IF
END SUBROUTINE sp_depo_temp

SUBROUTINE vapourDiffusionTemperature( sp, vt, dtg_f, atmo, dMdt, dQdt, V_new, phi_tend, L )
  TYPE(t_sp),    INTENT(in)    :: sp
  REAL(wp),      INTENT(in)    :: vt 
  REAL(wp),      INTENT(in)    :: dtg_f
  TYPE(t_atmo),  INTENT(in)    :: atmo
  REAL(wp),      INTENT(out)   :: dMdt
  REAL(wp),      INTENT(out)   :: dQdt
  REAL(wp),      INTENT(out)  :: V_new
  REAL(wp),      INTENT(out)  :: phi_tend
  REAL(wp), OPTIONAL, INTENT(out)  :: L

  REAL(wp) :: Re, sqrtRe, Dv, Ka, cubrPr, cubrSc, C, es, fv

  Re = atmo%rho * vt * sp%d / atmo%eta
  sqrtRe = SQRT( Re )
  Dv = diffusivity_vapour( atmo%T, atmo%p )
  Ka = thermalCond_dry( atmo%T )
  cubrPr = ( atmo%eta*c_pa/Ka )**z1_3
  cubrSc = ( atmo%eta/(atmo%rho*Dv) )**z1_3
  C = capacitance( sp, Re )

  IF(       sp%m_w                     < TINY(sp%m_w) ) THEN   ! pure ice
    Dv = Dv*ventilation_coef_i( sqrtRe*cubrSc, sp%phi ) 
    Ka = Ka*ventilation_coef_i( sqrtRe*cubrPr, sp%phi )
    es = psat_ice(sp%T)
    L  = latentHeat_depo( sp%T )
  ELSE IF( (sp%m_i + sp%m_f + sp%m_r)  < TINY(sp%m_i) ) THEN   ! pure water
    Dv = Dv*ventilation_coef_w( sqrtRe*cubrSc )
    Ka = Ka*ventilation_coef_w( sqrtRe*cubrPr )
    es = psat_water(sp%T)
    L  = latentHeat_evap( sp%T ) !TODO at which temperature, also for Dv and Ka, for the moment assume sp%T ~ atmo%T
  ELSE                                                         ! melting 
    fv = ventilation_coef_m( Re )
    Dv = fv*cubrSc*Dv
    Ka = Ka*cubrPr*Ka

    es = psat_water( T_3 )
    L  = latentHeat_evap( T_3 )
  ENDIF

  ! neglecting curvature effects relevant for small particles
  dMdt =  4.0_wp * pi * C * Dv * (atmo%pv/(Rv*atmo%T) - es/(Rv*sp%T))
  ! neglect heat from riming/accreation
  dQdt = -4.0_wp * pi * C * Ka * (atmo%T - sp%T) - L * dMdt  ! TODO add riming, attention sign is reverse for melting

  ! habit prediction only when ice exists
  IF ( dMdt > 0._wp ) THEN
    IF ( ( sp_habit == 1 .AND. sp%mm < 1.01 ) .OR. (sp_habit == 2) ) THEN
      CALL habit_predict(sp, dMdt, dtg_f, atmo%T, Dv, vt, sqrtRe*cubrSc, V_new, phi_tend)
    END IF
  END IF
END SUBROUTINE vapourDiffusionTemperature


!
! vapour diffusion results to particle mass change
!
SUBROUTINE vapourDiffusion( sp, vt, dtg_f, atmo, dMdt, dQdt, latentHeatCoeff, V_new, phi_tend, factor )
  TYPE(t_sp),    INTENT(in)    :: sp
  REAL(wp),      INTENT(in)    :: vt
  REAL(wp),      INTENT(in)    :: dtg_f
  TYPE(t_atmo),  INTENT(in)    :: atmo
  REAL(wp),      INTENT(out)   :: dMdt
  REAL(wp),      INTENT(out)   :: dQdt
  REAL(wp),      INTENT(out)   :: latentHeatCoeff
  REAL(wp),      INTENT(out)   :: V_new
  REAL(wp),      INTENT(out)   :: phi_tend
  REAL(wp), OPTIONAL, INTENT(out) :: factor

  REAL(wp) :: es, L, Dv, Ka, C, sqrtRe, cubrPr, cubrSc, Hi, Re

  !CALL m2d_particle( sp ) ! update particle dimensions before velocity and capacitance computation (already done in sp_depo)
  !vt = vterm_snow( atmo, sp )  ! modify heat and vapour transfer by ventilation
  Re = atmo%rho * vt * sp%d / atmo%eta              ! we can use d_max since we re-scale in ventilation for habit particles
  sqrtRe = SQRT( Re )
  Dv = diffusivity_vapour( atmo%T, atmo%p )
  Ka = thermalCond_dry( atmo%T )
  cubrPr = ( atmo%eta*c_pa/Ka )**z1_3
  cubrSc = ( atmo%eta/(atmo%rho*Dv) )**z1_3
  C = capacitance( sp, Re )

  IF( (sp%m_i + sp%m_f + sp%m_r) < TINY(sp%m_i ) ) THEN ! only water -> condensation/evaporation
    Dv = Dv*ventilation_coef_w( sqrtRe*cubrSc )
    Ka = Ka*ventilation_coef_w( sqrtRe*cubrPr )

    es = atmo%psatw
    L  = latentHeat_evap( atmo%T )
  ELSE                                         ! no water outside -> deposition/sublimation
    Dv = Dv*ventilation_coef_i( sqrtRe*cubrSc, sp%phi )  ! CHRS for old bugged version (reproduce paper) comment these two lines
    Ka = Ka*ventilation_coef_i( sqrtRe*cubrPr, sp%phi )

    es = atmo%psati
    L  = latentHeat_depo( atmo%T )
  ENDIF
  Hi = (L/(Rv*atmo%T) - 1.0_wp)*(L*Dv*es)/(Ka*Rv*atmo%T**2)

  ! neglecting curvature effects relevant for small particles
  dMdt =  4.0_wp * pi * C *   Dv * (atmo%pv - es)                         / (Rv * atmo%T * (1._wp+Hi))
  ! sum heat for possible melting at surface temperature T_3 ( psat_ice(T_3) = psat_water(T_3), and assumed L ~ const )
  dQdt = -4.0_wp * pi * C * ( Dv * (atmo%pv - (atmo%T/T_3)*psat_ice(T_3)) / (Rv * atmo%T) * L + Ka * (atmo%T - T_3) ) ! sum heat for possible melting
  latentHeatCoeff = L
  IF( PRESENT(factor) ) factor = L/(4.0_wp*pi*C*Ka)

  ! habit prediction only when ice exists
  IF ( dMdt > 0._wp ) THEN
    IF ( ( sp_habit == 1 .AND. sp%mm < 1.01 ) .OR. (sp_habit == 2) ) THEN
      CALL habit_predict(sp, dMdt, dtg_f, atmo%T, Dv, vt, sqrtRe*cubrSc, V_new, phi_tend)
    END IF
  END IF
END SUBROUTINE vapourDiffusion

!
! vapour diffusion during melting, i.e., coexistance of water and ice
! Thereby the particle surface temperature is fixed to T_3
! instead the convective and latent heat will lead to melting/freezing
!
SUBROUTINE vapourDiffusionMelting( sp, vt, atmo, dMdt, dQdt, latentHeatCoeff )
  TYPE(t_sp),    INTENT(in)    :: sp
  REAL(wp),      INTENT(in)    :: vt
  TYPE(t_atmo),  INTENT(in)    :: atmo
  REAL(wp),      INTENT(out)   :: dMdt
  REAL(wp),      INTENT(out)   :: dQdt
  REAL(wp),      INTENT(out)   :: latentHeatCoeff

  REAL(wp) :: Re, Dv, Ka, cubrPr, cubrSc, fv, C, es, L

  !CALL m2d_particle( sp )     ! update particle dimensions before velocity and capacitance computation (already done in sp_depo)
  !vt = vterm_snow( atmo, sp ) ! modify heat and vapour transfer by ventilation
  Re = atmo%rho * vt * sp%d / atmo%eta
  Dv = diffusivity_vapour( atmo%T, atmo%p )
  Ka = thermalCond_dry( atmo%T )
  cubrPr = ( atmo%eta*c_pa/Ka )**z1_3
  cubrSc = ( atmo%eta/(atmo%rho*Dv) )**z1_3
  fv = ventilation_coef_m( Re ) 

  C  = capacitance( sp, Re )
  es = psat_water( T_3 )
  L  = latentHeat_evap( T_3 )

  dMdt =  4.0_wp * pi * C * fv * (   cubrSc*Dv/(Rv*atmo%T) * (atmo%pv - (atmo%T/T_3)*es) )
  dQdt = -4.0_wp * pi * C * fv * ( L*cubrSc*Dv/(Rv*atmo%T) * (atmo%pv - (atmo%T/T_3)*es) + Ka*cubrPr*(atmo%T - T_3) )
  latentHeatCoeff = L
END SUBROUTINE vapourDiffusionMelting

!
! Perform temperature-dependent habit prediction due to deposition/sublimation. Deposition
! density (rho_depo) is used to determine eventual branching/hollowing.
!
SUBROUTINE habit_predict(sp, dMdt, dtg_f, T, Dv, vt, ReSc, V_new, phi_tend)
  TYPE(t_sp),    INTENT(in)    :: sp
  REAL(wp),      INTENT(in)    :: dMdt              ! ice mass added by deposition
  REAL(wp),      INTENT(in)    :: dtg_f             ! time step
  REAL(wp),      INTENT(in)    :: T                 ! Temperature
  REAL(wp),      INTENT(in)    :: Dv                ! Diffusivity Coefficient
  REAL(wp),      INTENT(in)    :: vt                ! term. velocity
  REAL(wp),      INTENT(in)    :: ReSc              ! sqrt(Re) * cubr(Sc)
  REAL(wp),      INTENT(out)   :: V_new             ! new ice volume
  REAL(wp),      INTENT(out)   :: phi_tend          ! change in phi, update in sp_depo

  REAL(wp) :: rho_depo, dVidt, IGF, IGF_vent, expo, r_a, r_c, V_tot, V_tot_new
  LOGICAL  :: branch_crit
  TYPE(t_sp) :: sp_new

  IF ( sp%m_i + sp%m_r + sp%m_f < TINY(sp%V_i)) RETURN

  ! small crystals (D < 10 um) are assumed to grow spherical (for original IGF)
  IF ( (sp%d .GE. 1.e-5) .OR. (IGF_id > 1) ) THEN
    IGF  = get_IGF(T)
  ELSE
    IGF  = 1._wp
  END IF

  CALL spheroid_geometry(sp, r_a, r_c, V_tot)

  ! deposition density 
  ! consider eventual branching/hollowing following Jensen&Harrington '15 p.4 (2572)
  IF (IGF_id == 1) THEN
    branch_crit = ( ( vt * r_a**2 ) > (pi * Dv * r_c) )  ! JH15, p.4: Branching criterion (a ~ 100 um)
  ELSE
    branch_crit = ( (0.5_wp * sp%d) .GE. 200e-6_wp)
  END IF

  IF (IGF < 1._wp) THEN
    IF ( branch_crit ) THEN  
      rho_depo = rhoi * IGF
    ELSE
      rho_depo = rhoi
    END IF
  ELSE                                            
    rho_depo = rhoi / IGF                           ! columns are assumed to hollow immediately
  END IF

  dVidt    = dMdt / rho_depo                        ! C&L 94 Eq. 41

  ! ventilation correction depending on particle shape (C&L 94, Eq. 33) relative to vol. equiv. radius
  IGF_vent = IGF * ventilation_coef_IGF( ReSc, r_c, V_tot**z1_3, sp%phi ) /                       &
                   ventilation_coef_IGF( ReSc, r_a, V_tot**z1_3, sp%phi ) 
  
  expo     = (IGF_vent - 1._wp) / (IGF_vent + 2._wp)  ! C&L94, Eq. 37. Exponent for update of Ice Volume      
  V_new    = sp%V_i + dVidt * dtg_f                   ! New ice volume, V_new >= m_i / rho_i
  sp_new   = sp
  sp_new%m_i = sp%m_i + dMdt  * dtg_f
  sp_new%V_i = V_new
  CALL spheroid_geometry(sp_new, r_a, r_c, V_tot_new)
  phi_tend = ( V_tot_new / V_tot )**expo           
END SUBROUTINE habit_predict

!
! ventilation coefficient of ice based on formulation of Hall & Pruppacher
! including phi-fit of ventilation coefficient for habits
! fit is based on volume equivalent diameter so transformation is needed for habit particles
!
PURE ELEMENTAL FUNCTION ventilation_coef_i( X, phi ) result ( fv )
  REAL(wp), INTENT(in) :: X
  REAL(wp), INTENT(in) :: phi
  REAL(wp)             :: fv, X_equiv
  REAL(wp), PARAMETER  :: c1 = 0.0280_wp
  REAL(wp), PARAMETER  :: c2 = 0.0028_wp

  IF (phi >= 1._wp) THEN
    X_equiv = X * phi**(-1./3.)
  ELSE IF (phi < 1._wp) THEN
    X_equiv = X * phi**(1./6.)
  END IF

! spheres:  fv = 1.00 + 0.14*X^2,         for X < 1   
!           fv = 0.86 + 0.28*X,           for X > 1  
  IF( X_equiv < 1.0_wp ) THEN
    fv = 1.0_wp  + 0.14_wp*X_equiv**2
  ELSE
    fv = 0.86_wp + 0.28_wp*X_equiv
  END IF

! prolates: fv = fv + c1 * phi * X,     for phi > 1
! oblates:  fv = fv + c2 / phi * X^3/2, for phi < 1 
  IF (phi > 1._wp) THEN
  ! sqrt(Re_equiv) = sqrt(Re_c / phi * phi^(1/3) )= sqrt(Re_c * phi^(-2/3)) = sqrt(Re_c) * phi^(-1/3) 
    fv      = fv + c1 * phi * X_equiv
  ELSE IF (phi < 1._wp) THEN
  ! sqrt(Re_Dequiv) = sqrt(Re_a * phi^(1/3)) = X_a * phi^(1/6)
    fv      = fv + c2 / phi * X_equiv**(3./2.)
  END IF
END FUNCTION ventilation_coef_i


!
! ventilation coefficient of water 
! could be extended by a shape-dependent term but effect should be small because oblate flattening
!
PURE ELEMENTAL FUNCTION ventilation_coef_w( X ) result ( fv )
  REAL(wp), INTENT(in) :: X
  REAL(wp)             :: fv

  IF( X < 1.4_wp ) THEN
    fv = 1.0_wp  + 0.108*X**2
  ELSE
    fv = 0.78_wp + 0.308*X
  END IF
END FUNCTION ventilation_coef_w

!
! ventilation coefficient during melting
! Vivec eq 19
!
PURE ELEMENTAL FUNCTION ventilation_coef_m( Re ) result( fv )
  REAL(wp), INTENT(in) :: Re
  REAL(wp)             :: fv
  REAL(wp) :: sqrtRe
  sqrtRe = SQRT(Re)
  fv = 2.1_wp*sqrtRe**0.38_wp + 1e-3_wp*sqrtRe**2.13
END FUNCTION ventilation_coef_m

!
! ventilation coefficient for inherent growth function w.r.t. a spherical particle/vol. equiv. radius
!
PURE ELEMENTAL FUNCTION ventilation_coef_IGF( X, s, r, phi ) result ( fv )
  REAL(wp), INTENT(in) :: X                ! X = sqrt(Re) * cubr(Sc),
  REAL(wp), INTENT(in) :: s, r             ! s = spheroid axis [a,c], r = vol. equiv. radius 
  REAL(wp), INTENT(in) :: phi
  REAL(wp)             :: fv

  REAL(wp) :: X_equiv

  IF (phi >= 1._wp) THEN
    X_equiv = X * phi**(-1./3.)
  ELSE IF (phi < 1._wp) THEN
    X_equiv = X * phi**(1./6.)
  END IF

  IF( X_equiv < 1.0_wp ) THEN
    fv = 1.0_wp  + 0.14_wp*X_equiv**2 * SQRT( s / r )
  ELSE
    fv = 0.86_wp + 0.28_wp*X_equiv * SQRT( s / r )
  END IF
END FUNCTION ventilation_coef_IGF

!
! general capacitance function
! for melting particles and rain it is assumed that an aspherical shape lowers the capacitance as for ice
! for melting particles in the range 3000 < Re < 6000 internal circulation is not strong enough to 
!  have the surface temperature at T3 (see Rasmussen and Heymsfield 87). To avoid solving iteratively
!  for the surface temperature Vivek suggested to not use the max dimension but the solid diamter instead 
!  to account for most of the error made by using T3 as surface temperature.
! TODO however, that should lead to jump of capacitance at complete melting
!
PURE FUNCTION capacitance( sp, Re ) result ( C )
  TYPE(t_sp),   INTENT(in)    :: sp
  REAL(wp),     INTENT(in)    :: Re
  REAL(wp)                    :: C

  REAL(wp), PARAMETER :: ReLimit(2) = (/ 3000.0_wp, 6000.0_wp /)

  REAL(wp) :: v_crit, Fr, C_unr, d, d_solid, a_solid, part, eps, r_a, r_c, V_tot
  REAL(wp), PARAMETER ::  C_sph = 0.5_wp ! graupel (spherical ice)

  ! capacitance of explicitly evolving particles/spheroids, following Chen&Lamb '94 (p.9)
  ! TODO: there is no dependency of capacitance on porosity. This might lead to an overestimation
  ! TODO: of capacitance (cf. Westbrook et al. 2008).
  IF ( ( (sp_habit == 1 .AND. sp%mm < 1.01_wp) .OR. (sp_habit ==2 ) )                             &
    .AND. ( sp%m_i + sp%m_r + sp%m_f > 0.0_wp) ) THEN
    CALL spheroid_geometry(sp, r_a, r_c, V_tot)

    IF (sp%phi < 1._wp) THEN                ! Chen&Lamb '94 Eq. 39
      eps = SQRT(1._wp - sp%phi**2)
      C   = r_a * eps / ASIN(eps)
    ELSE IF (sp%phi > 1._wp) THEN           ! Chen&Lamb '94 Eq. 40
      eps = SQRT( 1._wp - sp%phi**(-2) )
      C   = r_c * eps / LOG( (1._wp + eps) * sp%phi )
    ELSE
      C   = (z3_4pi * V_tot)**z1_3          ! spherical particle C = r
    END IF
  ELSE
    C_unr = 0.25_wp                        ! aggregates ( eq. A9 of Soelch and Kaercher 2010) after Westbrook 2008
    IF(sp%mm < 1.01_wp) C_unr = 1.0_wp / pi     ! spherical plate PK 13-77
    v_crit  = pi_6 * sp%d**3 - sp%m_i * rhoii                                                 ! v_crit based on sp%d, hence Fr <=1
    Fr = MERGE( (sp%m_f*rhoii + MAX(sp%v_r, sp%m_r*rhoii + sp%m_w * rholi)) / v_crit,              &
                1.0_wp, v_crit > 0.0_wp )  ! v_crit = 0 for small ice 
    d  = sp%d
    IF( BTEST(sp%statusb, sp_stat%liquidOut) .AND. (sp%m_i+sp%m_f+sp%m_r > 0.0_wp)                 &
        &.AND. (.NOT. BTEST(sp%statusb, sp_stat%liquidFillin)) ) THEN ! 8 Torus, 9 Torus to rain
      part = MIN( 1.0_wp, (Re - ReLimit(1)) / (ReLimit(2) - ReLimit(1)) )
      IF( part > 0.0_wp ) THEN
        CALL m2d_particle_solid( sp, d_solid, a_solid )
        d = (1.0_wp - part) * sp%d + part * d_solid
      END IF
    ENDIF
    C  = d * ( (1.0_wp - Fr) * C_unr + Fr * C_sph )
  END IF
END FUNCTION capacitance

! end ndef WDROPS
#endif

END MODULE mo_depo
