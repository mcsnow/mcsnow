!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_rime_stoc

#ifndef WDROPS

  USE mo_constants,    ONLY: pi, wp, i8, z1_3, pi_4, z4pi_3
  USE mo_sp_types,     ONLY: t_sp
  USE mo_sp_nml,       ONLY: rm, Vm, diffusion_type, sp_habit, coll_kern_type
  USE mo_atmo_types,   ONLY: rhol, T_3, r2m, c_pw, rhoii
  USE mo_atmo,         ONLY: t_atmo, latentHeat_melt
  USE mo_mass2diam,    ONLY: m2d_particle, habit_predict_riming
  USE mo_velocity,     ONLY: vterm_snow, vterm_beard
  USE mo_colleffi,     ONLY: impact_vel, Ecbohm_riming, rho_rime_CL
  USE mo_ice_multiplication, ONLY: ice_multiplication_radius, init_ice_multiplication

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: sp_rime, sp_rime_new
  

CONTAINS

!
! riming process after Cober and List '92
!
SUBROUTINE sp_rime_new( sp, atmo, dt, q_heat, multiplication_flag, n_splinter_tot, multiplication_n_colls, rime_rate, heat_rate )
  TYPE(t_sp),      INTENT(inout) :: sp
  TYPE(t_atmo),    INTENT(in)    :: atmo
  REAL(wp),        INTENT(in)    :: dt
  REAL(wp),        INTENT(inout) :: q_heat
  INTEGER,         INTENT(in)    :: multiplication_flag
  INTEGER(i8),     INTENT(inout) :: n_splinter_tot, multiplication_n_colls
  REAL(wp),        INTENT(inout) :: rime_rate
  REAL(wp),        INTENT(inout) :: heat_rate

  INTEGER :: ir, nr_int, xi_c_int, gama, gama_t
  REAL(wp) :: m_0, lwc, nr, &
              rand, K, Vgain, mgain, rd, rho_rime, prob, d, d_geo, &
              vd, Ed, xi_c, v_rel, q_factor, nr_real, xi_c_real, shape
  REAL(wp) :: multiplication_threshold

  INTEGER, PARAMETER   :: idsd  = 1    ! 0:monodispers, 1:exponential_radius, 2:exponential_mass, 3:gamma_radius, 4:gamma_mass

  IF (atmo%qc < 1e-100_wp) RETURN
  lwc = atmo%rho * atmo%qc

  CALL m2d_particle( sp )
  sp%v = vterm_snow( atmo, sp )
  v_rel = ABS(Vm - sp%v)

  ! distinguish between A- and D-kernel
  IF (coll_kern_type > 0 ) THEN
   d_geo = sp%d 
  ELSE
   d_geo = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )     ! correction in case of prolate particles
  END IF
  ! avg. number of droplets in the sweep-out volume with the avg. droplet
  nr = pi * (0.5_wp * d_geo + rm)**2 * v_rel * dt * lwc / (r2m*rm**3)

  if (idsd == 0) then
     ! monodisperse distribution with rd=rm, no change in nr
  elseif (idsd == 1 .or. idsd == 11) then
     ! divide by 3!=2*3=6 to yield same lwc as monodisperse distribution
     nr = nr / 6.
  elseif (idsd == 2) then
     ! no change necessary. Note that this will give the correct mass, but a slightly lower rm 
     ! compared to the radius distribution. The reason is that we sample rm**3 instead of rm.
     ! This could in principle be corrected by adjusting N and rm. It is not possible to change 
     ! only N as for the radius distribution, because this will lead to a bias in lwc.
  elseif (idsd == 4) then
     ! no change necessary, but rm has a different meaning. For shape > 2 the difference in rm
     ! between mass and radius distribution is smaller then 5 %.
     shape = 2.0
  else
     shape = 6.0
     ! take into account gamma distribution to get correct lwc
     nr = nr / ( gamma(shape+3.)/gamma(shape) / shape**3 )
  end if

  ! multiplicity of cloud droplets
  xi_c = 0.1_wp*nr+0.9_wp
  nr_real = nr / xi_c
  ! sample fractional nr to get integer value
  CALL random_number( rand )
  nr_int = INT(nr_real)+INT(rand+(nr_real-INT(nr_real)))
  IF(nr_int < 1) RETURN
  xi_c_real = nr / nr_int

  IF( diffusion_type > 1 ) THEN
    q_factor = c_pw*(atmo%T - sp%T)
  ELSE
    q_factor = c_pw*(atmo%T - T_3)
  ENDIF
  IF( (atmo%T < T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) THEN
    q_factor = q_factor + latentHeat_melt(atmo%T)

    multiplication_threshold = init_ice_multiplication( atmo%T )

    ! correction in case of prolate particles, we define non-dimensional number regarding d_equiv
    d = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )    
    ! Stokes number w.r.t. average droplet (eq. 4, Rasmussen and Heymsfield '85)
    K = 4_wp * rhol * v_rel * rm**2 / (9._wp * atmo%eta * d)
    rho_rime = rho_rime_CL( sp, atmo, K, rm, v_rel)
  ENDIF
  m_0 = sp%m_r + sp%m_w

  DO ir = 1, nr_int

    if (idsd == 0) then
       ! monodispers
       rd = rm
    elseif (idsd == 1) then
       ! exponential in radius
       do 
          CALL random_number( rand )
          rd = -rm * LOG(1._wp - rand)
          if (rd.gt.1e-6_wp.and.rd.lt.40e-6_wp) exit
       end do
    elseif (idsd == 2) then
       ! exponential in mass
       CALL random_number( rand )
       rd = ( -rm**3 * LOG(1._wp - rand) )**(1./3.)
       rd = max(min(rd,min(2.0*rm,40e-6_wp)),1e-6_wp)
    elseif (idsd == 3) then
       ! gamma distribution in radius 
       rd = rm * random_gamma(shape) / shape
       rd = max(min(rd,40e-6_wp),1e-6_wp)
    elseif (idsd == 4) then
       ! gamma distribution in mass
       rd = ( rm**3 * random_gamma(shape) / shape )**(1./3.)
       rd = max(min(rd,min(2.0*rm,40e-6_wp)),1e-6_wp)
    else
       ! exponential in radius
       CALL random_number( rand )
       rd = -rm * LOG(1._wp - rand)
       rd = max(min(rd,40e-6_wp),1e-6_wp)
    end if

    if (idsd > 0) then
       vd = vterm_beard( rd )
    else
       vd = vm
    end if

    ! Here we ignore cloud droplets that fall faster than the ice
    ! consistent with continuous riming
    IF (sp%v < vd) CYCLE

    Ed = Ecbohm_riming( atmo, sp, 2.0_wp*rd)

    ! sample fractional to get integer multiplicity
    CALL random_number( rand )
    xi_c_int  = INT(xi_c_real)+INT(rand+(xi_c_real-INT(xi_c_real)))
    ! collision probality, also fractional sampled
    prob = Ed * xi_c_int
    CALL random_number( rand )
    gama = FLOOR(prob) + FLOOR(rand+(prob-FLOOR(prob)))
    IF( gama < 1 ) CYCLE ! no collision
    ! limit number of (multiple) collision to cloud drop multiplicity (not necesarry if Ed<=1)
    gama_t = MIN( gama, xi_c_int )

    IF( (atmo%T < T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) THEN       ! T < T_3 necessary for Ri > 0
      IF( multiplication_flag == 1 ) CALL ice_multiplication_radius( sp%xi, multiplication_threshold, &
                               & rd, multiplication_n_colls, n_splinter_tot ) ! rd is reducedd if splintering is happening
      ! mass gain by riming, possibly multiple collisions
      mgain = r2m*rd**3 * gama_t
      Vgain = mgain / rho_rime
      IF ( (sp_habit == 1 .AND. sp%mm < 1.01_wp) .OR. (sp_habit == 2) ) THEN
        sp%phi = habit_predict_riming(sp, Vgain, rd)
      ENDIF
      sp%m_r = sp%m_r + mgain
      sp%V_r = sp%V_r + Vgain
    ELSE
      mgain = r2m*rd**3 * gama_t
      sp%m_w = sp%m_w + mgain
    END IF
    q_heat   = q_heat - mgain * q_factor ! sum heat for eventual melting process

    CALL m2d_particle( sp )
    sp%v = vterm_snow( atmo, sp )
  END DO

  rime_rate = rime_rate + sp%xi * (sp%m_r + sp%m_w - m_0) / dt
  heat_rate = heat_rate + sp%xi * (sp%m_r + sp%m_w - m_0) * q_factor
END SUBROUTINE sp_rime_new

!
! riming process after Cober and List '92
!
SUBROUTINE sp_rime( sp, atmo, dt, q_heat, multiplication_flag, n_splinter_tot, multiplication_n_colls, rime_rate, heat_rate )
  TYPE(t_sp),      INTENT(inout) :: sp
  TYPE(t_atmo),    INTENT(in)    :: atmo
  REAL(wp),        INTENT(in)    :: dt
  REAL(wp),        INTENT(inout) :: q_heat
  INTEGER,         INTENT(in)    :: multiplication_flag
  INTEGER(i8),     INTENT(inout) :: n_splinter_tot, multiplication_n_colls
  REAL(wp),        INTENT(inout) :: rime_rate
  REAL(wp),        INTENT(inout) :: heat_rate
 
  INTEGER :: ir, nr_int
  REAL(wp) :: m_r0, rimerate, lwc, nr, &
              Eb, rand, K, mgain, rd, rho_rime, d, d_geo, &
              vd, Ed, xi_c, v_rel, q_factor, Vgain, shape
  REAL(wp) :: multiplication_threshold
  
  INTEGER, PARAMETER   :: idsd  = 1    ! 0:monodispers, 1:exponential_radius, 2:exponential_mass, 3:gamma_radius, 4:gamma_mass
  LOGICAL, PARAMETER   :: bulk_efficiency  = .false.


  multiplication_threshold = init_ice_multiplication( atmo%T )

  m_r0 = sp%m_r
  
  IF (atmo%qc < 1e-100_wp) RETURN
  lwc = atmo%rho * atmo%qc
  IF( diffusion_type > 1 ) THEN
    q_factor = c_pw*(atmo%T - sp%T)
  ELSE
    q_factor = c_pw*(atmo%T - T_3)
  ENDIF
  IF( (atmo%T < T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) q_factor = q_factor + latentHeat_melt(atmo%T)

  CALL m2d_particle( sp )
  sp%v = vterm_snow( atmo, sp )
  v_rel = ABS(Vm - sp%v)

  ! correction in case of prolate particles, we define non-dimensional number regarding d_equiv
  d = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )    
  ! Stokes number w.r.t. average droplet (eq. 4, Rasmussen and Heymsfield '85)
  K = 4_wp * rhol * v_rel * rm**2 / (9._wp * atmo%eta * d)
  IF (bulk_efficiency) THEN
     ! bulk collision efficiency w.r.t. average droplet (Cober and List '93)
     Eb = MIN(MAX( 0.55_wp * LOG10(2.51_wp * K), 0._wp ), 1.0_wp)
  ELSE
     Eb = 1.0_wp
  END IF
  ! distinguish between A- and D-kernel  
  IF (coll_kern_type > 0 ) THEN
   d_geo = sp%d 
  ELSE
   d_geo = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )     ! correction in case of prolate particles
  END IF
  ! avg. number of droplets in the sweep-out volume with the avg. droplet
  nr = pi * (0.5_wp * d_geo + rm)**2 * dt * &
       v_rel * Eb * lwc / (r2m * rm**3) 

  if (idsd == 0) then
     ! monodisperse distribution with rd=rm, no change in nr
  elseif (idsd == 1 .or. idsd == 11) then
     ! divide by 3!=2*3=6 to yield same lwc as monodisperse distribution
     nr = nr / 6.
  elseif (idsd == 2) then
     ! no change necessary. Note that this will give the correct mass, but a slightly lower rm 
     ! compared to the radius distribution. The reason is that we sample rm**3 instead of rm.
     ! This could in principle be corrected by adjusting N and rm. It is not possible to change 
     ! only N as for the radius distribution, because this will lead to a bias in lwc.
  elseif (idsd == 4) then
     ! no change necessary, but rm has a different meaning. For shape > 2 the difference in rm
     ! between mass and radius distribution is smaller then 5 %.
     shape = 2.0
  else
     shape = 6.0
     ! take into account gamma distribution to get correct lwc
     nr = nr / ( gamma(shape+3.)/gamma(shape) / shape**3 )
  end if

  ! multiplicity of cloud droplets
  xi_c = 0.1_wp*nr+1.0_wp

  ! rescale nr with multiplicity
  nr = nr / xi_c

  ! sample fractional nr to get integer value
  CALL random_number( rand )
  nr_int = INT(nr)+INT(rand+(nr-INT(nr)))
  IF(nr_int < 1) RETURN
  rho_rime = rho_rime_CL(sp, atmo, K, rm, v_rel)

  DO ir = 1, nr_int

    if (idsd == 0) then
       ! monodispers
       rd = rm
    elseif (idsd == 1) then
       ! exponential in radius
       do 
          CALL random_number( rand )
          rd = -rm * LOG(1._wp - rand)
          if (rd.gt.1e-6_wp.and.rd.lt.40e-6_wp) exit
       end do
    elseif (idsd == 2) then
       ! exponential in mass
       CALL random_number( rand )
       rd = ( -rm**3 * LOG(1._wp - rand) )**(1./3.)
       rd = max(min(rd,min(2.0*rm,40e-6_wp)),1e-6_wp)
    elseif (idsd == 3) then
       ! gamma distribution in radius 
       rd = rm * random_gamma(shape) / shape
       rd = max(min(rd,40e-6_wp),1e-6_wp)
    elseif (idsd == 4) then
       ! gamma distribution in mass
       rd = ( rm**3 * random_gamma(shape) / shape )**(1./3.)
       rd = max(min(rd,min(2.0*rm,40e-6_wp)),1e-6_wp)
    else
       ! exponential in radius
       CALL random_number( rand )
       rd = -rm * LOG(1._wp - rand)
       rd = max(min(rd,40e-6_wp),1e-6_wp)
    end if

    if (idsd > 0) then
       vd = vterm_beard( rd )
    else
       vd = vm
    end if

    ! Here we ignore cloud droplets that fall faster than the ice
    ! consistent with continuous riming
    IF (sp%v .gt. vd) THEN
       Ed = Ecbohm_riming( atmo, sp, 2.0_wp*rd )
    ELSE
       Ed = 0.0_wp
    END IF

    IF (bulk_efficiency) THEN
       Ed = Ed/Eb
    END IF
    
    IF( (atmo%T < T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) THEN       ! T < T_3 necessary for Ri > 0
      IF( multiplication_flag == 1 ) CALL ice_multiplication_radius( sp%xi, multiplication_threshold, &
                               & rd, multiplication_n_colls, n_splinter_tot ) ! rd is reducedd if splintering is happening
      ! mass gain by riming, rescaled with multiplicity
      mgain = r2m*rd**3 * Ed * xi_c
      Vgain = mgain / rho_rime
      IF ( (sp_habit == 1 .AND. sp%mm < 1.01_wp) .OR. (sp_habit == 2) ) THEN
        sp%phi = habit_predict_riming(sp, Vgain, rd)
      ENDIF  
      sp%m_r = sp%m_r + mgain
      sp%V_r = sp%V_r + Vgain
    ELSE
      mgain = r2m*rd**3 * Ed * xi_c
      sp%m_w = sp%m_w + mgain
    END IF
    q_heat   = q_heat - mgain * q_factor ! sum heat for eventual melting process

    CALL m2d_particle( sp )
    sp%v = vterm_snow( atmo, sp )
  END DO 

  rime_rate = rime_rate + sp%xi * (sp%m_r - m_r0)/dt
  IF( (atmo%T < T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) heat_rate = heat_rate + sp%xi * (sp%m_r - m_r0) * latentHeat_melt(atmo%T)

#ifndef NDEBUG
  rimerate = 100._wp*(sp%m_r - m_r0) / sp%m_i
  IF ( rimerate < 0._wp ) STOP "sp_rime: rimerate < 0"
#endif
END SUBROUTINE sp_rime


REAL(wp) FUNCTION random_gamma(s)
  !
  ! Function generates a random variate in [0,infinity) for a gamma distribution
  ! with P(x) ~ x**(s-1)*exp(-bet*x) using Best's T distribution method
  ! (Dagpunar, John 'Principles of random variate generation', Oxford, 1988.)
  !
  IMPLICIT NONE
  REAL(wp), INTENT(IN) :: s
  REAL(wp)             :: b, h, d, r, g, f, x
  REAL(wp), PARAMETER  :: zero = 0.0, half = 0.5, one = 1.0, two = 2.0

  IF (s <= one) THEN
     PRINT *, 'ERROR only for shape parameters > 1'
     STOP
  END IF

  b = s - one
  h = SQRT(3.0_wp*s - 0.75_wp)
  DO
     CALL RANDOM_NUMBER(r)
     g = r - r*r
     IF (g <= zero) CYCLE
     f = (r - half)*h/SQRT(g)
     x = b + f
     IF (x <= zero) CYCLE
     CALL RANDOM_NUMBER(r)
     d = 64.0_wp*g*(r*g)**2
     IF (d <= zero) EXIT
     IF (d*x < x - two*f*f) EXIT
     IF (LOG(d) < two*(b*LOG(x/b) - f)) EXIT
  END DO
  random_gamma = x
  RETURN
END FUNCTION random_gamma

! end ndef WDROPS
#endif

END MODULE mo_rime_stoc
