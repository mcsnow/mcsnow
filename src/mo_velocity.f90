!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_velocity

  USE mo_constants,     ONLY: pi, wp, mp, z1_6, pi_4, pi_6, z4_pi
  USE mo_sp_nml,        ONLY: sp_vterm_id, rm, Vm, sp_habit, prolate_intp
  USE mo_sp_types,      ONLY: t_sp, sp_stat
  USE mo_atmo_types,    ONLY: grav, t_atmo, rhoii, rholi
#ifndef WDROPS 
  USE mo_mass2diam,     ONLY: m2d_particle_solid, m2d_particle_liquid, spheroid_geometry
#endif

  IMPLICIT NONE
  PRIVATE

#ifndef WDROPS 
  PUBLIC ::  t_CE_boehm, boehmVel, Reynolds_Boehm, hydrodynamicValuesBoehm
  PUBLIC ::              &
    vterm_snow,          &
    vterm_hw10,          &
    vterm_kc05,          &
    vterm_beard,         &
    init_part_geom
  PRIVATE ::             &
    X2Cd_kc05rough,      &
    X2Cd_kc05smooth
#else
  PUBLIC ::              &
    vterm_beard,         &
    init_part_geom
#endif

  TYPE t_CE_boehm
    REAL(wp) :: &
             &  r,        & 
             &  r_e,      & 
             &  v,        & 
             &  m,        & 
             &  alpha,    &
             &  alpha_e,  & 
             &  q_e,      &
             &  k,        & 
             &  DELS,    &
             &  CDI
    REAL(wp) :: SIG = 0.25_wp
    INTEGER  :: j   = 2
  END TYPE t_CE_boehm

CONTAINS


!
! set terminal velocity
!
SUBROUTINE init_part_geom()

  ! compute fall velocity of an average droplet
  Vm = vterm_beard( rm )
END SUBROUTINE init_part_geom


#ifndef WDROPS

!
! terminal fall velocity 
!   from Heymsfield and Westbrook (2010)
!
FUNCTION vterm_hw10( atmo, sp ) RESULT( vt )
  TYPE(t_atmo), INTENT(in)    :: atmo
  TYPE(t_sp),   INTENT(in)    :: sp    ! super-droplet
  REAL(mp)                    :: vt

  REAL(wp), PARAMETER :: do_i = 8.0_wp
  REAL(wp), PARAMETER :: co_i = 0.35_wp

  REAL(wp) :: mtot, Re, c1, c2, Xbest, bracket, Ar

  mtot = sp%m_r + sp%m_f + sp%m_i + sp%m_w

  ! modified Best number eq. on p. 2478
  Ar = sp%p_area * z4_pi
  Xbest = atmo%rho * 8._wp * mtot * grav * sp%d / (atmo%eta**2 * pi * SQRT(Ar))

  ! Re-X eq. on p. 2478
  c1 = 4.0_wp / ( do_i**2 * SQRT(co_i) )
  c2 = 0.25_wp * do_i**2
  bracket = SQRT(1.0 + c1*SQRT(Xbest)) - 1.0_wp
  Re = c2*bracket**2

  vt = atmo%eta * Re / (atmo%rho * sp%d)
END FUNCTION vterm_hw10


!
! terminal fall velocity of ice from Khvorostyanov and Curry (2005/2002)
! optional parameter to switch to smooth surface parameters
! 
! Note: Rassmussen and Heymsfield JAS 44, 2754pp, 1987 interpolate to Cd 0.6 at 
! Re > 2.5e4 on wet particles. This is not done here to avoid a transition to pure water drops
!
FUNCTION vterm_kc05( atmo, sp, smooth ) RESULT( vt )
  TYPE(t_atmo), INTENT(in)    :: atmo  ! background atmosphere
  TYPE(t_sp),   INTENT(in)    :: sp    ! super-droplet
  LOGICAL,      INTENT(in), OPTIONAL :: smooth
  REAL(mp)                    :: vt

  REAL(wp) :: mtot, Xbest, Cd, Vb, Fb

  mtot = sp%m_r + sp%m_f + sp%m_i + sp%m_w

  ! Best number eq. (2.4b) with buoyancy
  Vb = (sp%m_i + sp%m_f)*rhoii + MAX(sp%v_r, sp%m_r*rhoii + sp%m_w*rholi)
  Fb = atmo%rho * Vb * grav
  Xbest = 2._wp * ABS(mtot*grav-Fb) * atmo%rho * sp%d**2 / (sp%p_area * atmo%eta**2)

  IF( PRESENT(smooth) ) THEN
    IF( smooth ) THEN
      Cd  = X2Cd_kc05smooth( Xbest )
    ELSE
      Cd  = X2Cd_kc05rough( Xbest )
    ENDIF
  ELSE
      Cd  = X2Cd_kc05rough( Xbest )
  ENDIF
  vt = SQRT( 2*ABS(mtot*grav - Fb)/(atmo%rho * sp%p_area * Cd) )
END FUNCTION vterm_kc05

FUNCTION X2Cd_kc05rough( Xbest ) RESULT( Cd )
  REAL(wp), INTENT(in)  :: Xbest
  REAL(wp)              :: Cd

  REAL(wp), PARAMETER :: do_i = 5.83_wp
  REAL(wp), PARAMETER :: co_i = 0.6_wp
  REAL(wp), PARAMETER :: Ct = 1.6_wp
  REAL(wp), PARAMETER :: X0_i = .35714285714285714285e-6 !1.0_wp/2.8e6_wp
  ! derived constants
  REAL(wp), PARAMETER :: c1 = 4.0 / ( do_i**2 * SQRT(co_i) )
  REAL(wp), PARAMETER :: c2 = 0.25_wp * do_i**2

  REAL(wp) :: bracket, psi, Re

  ! Re-X eq. (2.5)
  bracket = SQRT(1.0 + c1*SQRT(Xbest)) - 1.0
  ! turbulent Reynold's number, eq (3.3)
  psi = (1+(Xbest*X0_i)**2) / (1+Ct*(Xbest*X0_i)**2)
  Re  = c2*bracket**2 ! * SQRT(psi) ! TODO remove psi in Re?
  ! eq. (2.1) from KC05 with (3.2)
  Cd = co_i * (1._wp + do_i/SQRT(Re))**2 / psi
END FUNCTION X2Cd_kc05rough

FUNCTION X2Cd_kc05smooth( Xbest ) RESULT( Cd )
  REAL(wp), INTENT(in)  :: Xbest
  REAL(wp)              :: Cd

  REAL(wp), PARAMETER :: do_i = 9.06_wp
  REAL(wp), PARAMETER :: co_i = 0.292_wp
  REAL(wp), PARAMETER :: Ct = 1.6_wp
  REAL(wp), PARAMETER :: X0_i = 1.0_wp/6.7e6_wp
  ! derived constants
  REAL(wp), PARAMETER :: c1 = 4.0_wp / ( do_i**2 * SQRT(co_i) )
  REAL(wp), PARAMETER :: c2 = 0.25_wp * do_i**2

  REAL(wp) :: bracket, psi, Re

  ! Re-X eq. (2.5)
  bracket = SQRT(1.0 + c1*SQRT(Xbest)) - 1.0
  ! turbulent Reynold's number, eq (3.3)
  psi = (1+(Xbest*X0_i)**2) / (1+Ct*(Xbest*X0_i)**2)
  Re  = c2*bracket**2 !* SQRT(psi) ! TODO remove psi in Re?
  ! eq. (2.1) from KC05 with (3.2)
  Cd = co_i * (1._wp + do_i/SQRT(Re))**2 / psi
END FUNCTION X2Cd_kc05smooth

! set terminal velocity
!
FUNCTION vterm_snow( atmo, sp ) RESULT( vt )
  TYPE(t_atmo), INTENT(in)    :: atmo
  TYPE(t_sp),   INTENT(inout) :: sp    ! super-droplet
  REAL(mp)                    :: vt

  REAL(wp) :: d_solid, a_solid, smoothFraction, v_smooth

  IF( sp_vterm_id > 2 ) THEN
    vt = boehmVel( atmo, sp )
  ELSE
    IF( BTEST(sp%statusb,sp_stat%liquidSurf) ) THEN   ! smooth
      vt = vterm_kc05( atmo, sp, .TRUE. )
    ELSE                                              ! could be either rough or partially smooth
      IF     ( sp_vterm_id == 1 ) THEN
        vt = vterm_hw10( atmo, sp )
      ELSE IF( sp_vterm_id == 2 ) THEN
        vt = vterm_kc05( atmo, sp )
      END IF
      IF( BTEST(sp%statusb,sp_stat%liquidOut) ) THEN  ! liquidOut but not the whole surface -> waterFillin or torus development
        CALL m2d_particle_solid( sp, d_solid, a_solid )
        CALL m2d_particle_liquid(      sp, d_solid, a_solid, smoothFraction )
        v_smooth = vterm_kc05( atmo, sp, .TRUE. )
        vt = smoothFraction*v_smooth + (1.0_wp - smoothFraction)*vt
      END IF
    ENDIF
  ENDIF
END FUNCTION vterm_snow

! ifdef WDROPS
#endif


! 
! calculate terminal velocity of water droplets
! based on Table1 in Beard (1976), p.853
!
FUNCTION vterm_beard( rad ) RESULT(vt)
  REAL(wp), INTENT(in) :: rad   ![m]
  REAL(mp) :: vT                ![m/s]

  REAL(wp) :: b2(7),b3(6)
  data b2 /-0.318657e1,0.992696,-0.153193e-2,-0.987059e-3, &
           -0.578878e-3,0.855176e-4,-0.327815e-5/
  data b3 /-0.500015e1,0.523778e1,-0.204914e1, &
           0.475294,-0.542819e-1,0.238449e-2/
  REAL(wp), PARAMETER :: & ! CGS-units
    g     = 980.665_wp,  & ![cm/s2]
    eta0  = 1.818e-4_wp, & ![g/cm/s]
    l0    = 6.62e-6_wp,  & ![cm]
    rhow  = 1._wp,       & ![g/cm3]
    rhoa  = 1.225e-3_wp, & ![g/cm3]
    t0    = 273.15    
  REAL(wp) :: sigma, x, y, c1, c_sc, Bo, Np, N_Da, N_Re
  REAL(wp) :: d ! diameter [cm]

  d = 2e2_wp*rad
  IF( d <= 1.9e-3 ) THEN
    c1 = g * (rhow-rhoa) / (18.*eta0)
    c_sc = 1 + 2.51*l0/d
    vt = c1 * c_sc * d**2
  ELSEIF( d>1.9e-3 .AND. d<=0.107 ) THEN
    N_Da = 4.*rhoa * (rhow-rhoa) * g / (3.*eta0**2)
    x = LOG(N_Da * d**3)
    y = (((((b2(7)*x+b2(6))*x+b2(5))*x+b2(4))*x+b2(3))*x+b2(2))*x+b2(1)
    c_sc = 1_wp + 2.51*l0/d
    N_Re = c_sc*EXP(y)
    vt = eta0 * N_Re / (rhoa * d)
  ELSE IF( d > 0.107 )THEN
    d = MIN(d, 0.7_wp) !limit
    sigma = 76.1 - 0.155*(293.15 - t0) ! a better one?
    Bo = 4.*(rhow-rhoa) * g / (3.*sigma)*d*d
    Np = sigma**3 * rhoa**2 / ((eta0**4)*(rhow-rhoa)*g)
    x = LOG(Bo * Np**z1_6)
    y = ((((b3(6)*x+b3(5))*x+b3(4))*x+b3(3))*x+b3(2))*x+b3(1)
    N_Re = Np**z1_6 * EXP(y)
    vt = eta0 * N_Re / (rhoa * d)
  ENDIF

  vt = 0.01_wp*vt
END FUNCTION vterm_beard

#ifndef WDROPS

!
! velocity after Boehm89 and 92 I, see also Boehm99
! and if partInfo is provided, additional parameters to calculate the collision efficiency
!
REAL(wp) FUNCTION boehmVel( atmo, sp, partInfo ) RESULT( vterm_bohm )
  TYPE(t_atmo),                     INTENT(IN)    :: atmo
  TYPE(t_sp),                       INTENT(IN)    :: sp
  TYPE(t_CE_boehm), OPTIONAL, INTENT(OUT)   :: partInfo

  !REAL(wp) :: ALPHA 
  !REAL(wp) :: ZAEHLER, NENNER, XXX, CDP, CDPRIM, BETA, NRE0, CDO, GAM, NRE, DEL0, X0
  !REAL(wp) :: alf, beta, logm
  REAL(wp) :: fx
  REAL(wp) :: q, k, gama_big, N_Re, DEL0, d, mtot, X, X_0, V_tot, r_a, r_c
  REAL(wp) :: k_pro, gama_big_pro, N_Re_pro, vterm_bohm_pro, X_pro
  
  mtot = sp%m_w + sp%m_r + sp%m_f + sp%m_i

  IF( BTEST(sp%statusb,sp_stat%liquidOut)  ) THEN
  !IF( BTEST(sp%statusb,sp_stat%liquidSurf) ) THEN   ! smooth
    X_0 = 6.7D6
  ELSE                                              ! rough
    X_0 = 2.8D6
  ENDIF

  ! characteristic length is defined regarding the radius/a-axis, transformation needed for prolates
  IF (sp%phi .GT. 1._wp) THEN   
    IF (sp%mm < 1.01_wp) THEN
      q = z4_pi
      IF ( (sp_habit == 1 .AND. sp%mm < 1.01_wp) .OR. (sp_habit == 2) ) THEN
        CALL spheroid_geometry(sp, r_a, r_c, V_tot)
      ELSE
        V_tot = pi_6 * sp%d**3 ! TODO what about melting particles, they might not be spherical
      ENDIF
      d = (4.0_wp * V_tot / pi / sp%phi)**(1.0_wp/3.0_wp)
    ELSE
      q = sp%p_area / (pi_4 * sp%d**2 / sp%phi)
      d = sp%d / sp%phi
    END IF                                  
  ELSE
    q = sp%p_area / (pi_4 * sp%d**2)
    d = sp%d
  END IF

  ! cylindrical assumption q = 4/pi
  N_Re       = Reynolds_Boehm(atmo, sp%phi, mtot, q, X_0, k, gama_big, X)
  vterm_bohm = N_Re * atmo%eta / (d * atmo%rho)         ! I (22)
  ! possible interpolation between prolate and cylindrical geometry.
  ! TRAIL data (McCorquodale & Westbrook 2020) show transition instead of sudden jump
  IF ( (sp%phi .GT. 1._wp) .AND. (prolate_intp) ) THEN
    ! prolate assumption q = A / A_ce 
    N_Re_pro       = Reynolds_Boehm(atmo, sp%phi, mtot, sp%p_area / (pi_4 * sp%d**2 / sp%phi),    &
                              X_0, k_pro, gama_big_pro, X_pro)
    vterm_bohm_pro = N_Re_pro * atmo%eta / ( sp%d / sp%phi * atmo%rho)

    !logm        = LOG(mtot)
    ! alf         = 8.60e-2_wp * logm + 1.722 
    ! beta        = 3.08e-2_wp * logm + 1.691  

    ! fx          = MIN(1._wp, beta * exp(-alf * sp%phi))         ! limiter to prevent overestimation
    fx          =  exp( -0.3_wp * (sp%phi-1.0))                   ! [0.16, 0.0094] complex fit
    vterm_bohm  = fx * vterm_bohm_pro + (1._wp - fx) * vterm_bohm
  END IF


  IF( PRESENT( partInfo ) ) THEN

    IF( BTEST(sp%statusb,sp_stat%liquidSurf) ) THEN   ! smooth
      partInfo%SIG       = 0.0_wp
    ELSE                                              ! rough
      partInfo%SIG       = 0.25_wp
    ENDIF
    IF( sp%phi > 1.0_wp ) THEN     ! flow is not axisymmetric
      partInfo%j = 1
    ELSE
      partInfo%j = 2
    ENDIF

    partInfo%r       = 0.5_wp * d                                         ! 92c, p.106 "radius of drop or cylinder"
    IF (sp%phi .GT. 1._wp) THEN
      partInfo%r_e     = partInfo%r * SQRT(z4_pi * sp%phi )               ! B99 p. 170 definition in text
    ELSE
      partInfo%r_e     = SQRT( sp%p_area / pi )                           ! 99 eq. 3
    END IF
    partInfo%v       = vterm_bohm
    partInfo%m       = mtot
    partInfo%alpha   = sp%phi
    partInfo%alpha_e = MIN(sp%phi, 1.0_wp) * partInfo%r/partInfo%r_e      ! 99 eq. 4
    partInfo%q_e     = q
    IF( q > 1.0_wp ) THEN                                                 ! 99 eq. 5
      partInfo%q_e    = q * pi / 4.0_wp
    END IF
    partInfo%k       = k

    ! Berechnung weiterer Groessen fuer die Bestimmung der Effizienz
    IF( sp%phi .LE. 1.0_wp ) THEN ! 99 chapter 2.2
      DEL0=3.60_wp
    ELSE
      DEL0=4.54_wp
    ENDIF
    partInfo%DELS   = DEL0*partInfo%r/SQRT(N_Re*gama_big)                 ! 99 eq. 1
    partInfo%CDI    = X/N_Re**2 - 24.0_Wp*partInfo%k/N_Re                 ! 99 eq. 18

  ENDIF
END FUNCTION boehmVel

! inversion of the Re-Cd relation assumed by Boehm '92 including turbulence correction and low
! Reynolds number correction
REAL(wp) FUNCTION Reynolds_Boehm(atmo, alpha, mtot, q, X_0, k, gama_big, X) RESULT(N_Re)
  TYPE(t_atmo), INTENT(IN)    :: atmo
  REAL(wp), INTENT(IN)        :: alpha      ! aspect ratio, original naming of Boehm
  REAL(wp), INTENT(IN)        :: mtot
  REAL(wp), INTENT(IN)        :: q
  REAL(wp), INTENT(IN)        :: X_0
  REAL(wp), INTENT(OUT)       :: k
  REAL(wp), INTENT(OUT)       :: gama_big
  REAL(wp), INTENT(OUT)       :: X

  REAL(wp) :: C_DP, C_DP_prim, beta, gama_small, C_DO, N_Re0

  CALL hydrodynamicValuesBoehm( alpha, q, k, gama_big, C_DP )

  ! 99 (7) or I (17)/(18)
  X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(alpha,1.0_wp)*q**(1.0_wp/4.0_wp))                 ! porosity correction also for q > 1
  !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(alpha,1.0_wp)*MAX(q**(1.0_wp/4.0_wp),q))         ! original Version, Boehm Paper Version
  !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(alpha,1.0_wp)*MIN(q**(1.0_wp/4.0_wp),1.0_wp))    ! Best fit to B92Fig3

  ! I (23) turbulence correction
  C_DP_prim = C_DP*((1.0_wp+1.6_wp*(X/X_0)**2)/(1.0_wp+(X/X_0)**2))

  ! I (21)
  beta = SQRT(1.0_wp+C_DP_prim/6.0_wp/k*SQRT(X/C_DP_prim))-1
  ! I (20)
  N_Re0 = 6.0_wp*k/C_DP_prim*beta**2

  ! low Reynolds number
  ! I (16)
  C_DO = 4.5_wp*k**2*MAX(alpha,1.0_wp)
  ! I (26)
  gama_small = (C_DO - C_DP) / (4.0_wp * C_DP)
  ! I (27)
  N_Re  = N_Re0*(1.0_wp + (2.0_wp*beta*EXP(-beta*gama_small))/((2.0_wp+beta)*(1.0_wp+beta)) )

END FUNCTION Reynolds_Boehm

!
! helper function for Boehm efficiency
!
SUBROUTINE hydrodynamicValuesBoehm( phi, q, k, gama_big, C_DP )
  REAL(wp), INTENT(in)  :: phi
  REAL(wp), INTENT(in)  :: q
  REAL(wp), INTENT(out) :: k
  REAL(wp), INTENT(out) :: gama_big
  REAL(wp), INTENT(out) :: C_DP

  ! 99 (9) attention sqrt(phi) should be just alpha, see I (11)/(12)/(13)/(14) | alpha = phi
  k = min(max(0.82_wp+0.18_wp*phi,0.85_wp),0.37_wp+0.63_wp/phi, &
    & 1.33_wp/(max(log(phi),0.0_wp)+1.19_wp))
  ! 99 (11) or I (5)
  gama_big = max(1.0_wp, min(1.98_wp,3.76_wp-8.41_wp*phi+9.18*phi**2-3.53*phi**3))
  ! 99 (10) or I (7)/(8)
  C_DP = max(0.292_wp*k*gama_big,0.492_wp-0.2_wp/sqrt(phi))
  C_DP = max(1.0_wp,q*(1.46_wp*q-0.46_wp))*C_DP
END SUBROUTINE hydrodynamicValuesBoehm

! endif ndef WDROPS
#endif

END MODULE mo_velocity
