!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_rime_cont

#ifndef WDROPS

  USE mo_constants,    ONLY: wp, pi, i8, z4pi_3, z1_3
  USE mo_sp_types,     ONLY: t_sp
  USE mo_sp_nml,       ONLY: rm, Vm, diffusion_type, sp_habit, coll_kern_type
  USE mo_atmo_types,   ONLY: rhol, T_3, c_pw
  USE mo_atmo,         ONLY: t_atmo, latentHeat_melt
  USE mo_mass2diam,    ONLY: m2d_particle, habit_predict_riming
  USE mo_velocity,     ONLY: vterm_snow
  USE mo_colleffi,     ONLY: impact_vel, rho_rime_CL
  USE mo_ice_multiplication, ONLY: ice_multiplication_mass

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: sp_rime
  

CONTAINS

!
! riming process after Cober and List '93
!
SUBROUTINE sp_rime( sp, atmo, dt, q_heat, multiplication_flag, n_splinter_tot, rime_rate, heat_rate )
  TYPE(t_sp),   INTENT(inout) :: sp
  TYPE(t_atmo), INTENT(in)    :: atmo
  REAL(wp),     INTENT(in)    :: dt
  REAL(wp),     INTENT(inout) :: q_heat
  INTEGER,      INTENT(in)    :: multiplication_flag 
  INTEGER(i8),  INTENT(inout) :: n_splinter_tot
  REAL(wp),     INTENT(inout) :: rime_rate
  REAL(wp),     INTENT(inout) :: heat_rate
  
  REAL(wp) :: m_r0, rimerate, dv, lwc, m_rime, K_Ns, EB, rho_rime, Vgain, d, d_geo

  m_r0 = sp%m_r
  IF (atmo%qc < 1e-20) RETURN
  lwc = atmo%rho * atmo%qc

  CALL m2d_particle( sp )
  sp%v = vterm_snow( atmo, sp )

  ! cycle if v is smaller than vm, otherwise we would have to switch
  ! collector and collected particle
  IF (sp%v.lt.vm) RETURN

  ! the initial relative velocity between the droplet and the ice particle
  dv = ABS(Vm - sp%v)

  ! correction in case of prolate particles, we define non-dimensional number regarding d_equiv
  d = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )    
  ! Stokes number (eq. 4, Rasmussen and Heymsfield '85) 
  K_Ns = 4_wp * rhol * dv * rm**2 / (9._wp * atmo%eta * d)

  ! bulk collision efficiency
  Eb = MIN(MAX( 0.55_wp * LOG10(2.51_wp * K_Ns), 0._wp), 1.0_wp)

  !m_rime = pi / 3.0_wp   *lwc * Eb * sp%d**2 * sp%v * dt ! CHRS old bugged version to reproduce paper
  ! distinguish between A- and D-kernel
  IF (coll_kern_type > 0 ) THEN
    d_geo = sp%d 
  ELSE
    d_geo = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )     ! correction in case of prolate particles
  END IF
  m_rime = pi * 0.25_wp * lwc * Eb * d_geo**2 * dv * dt
  
  IF( (atmo%T < T_3) .AND. (sp%m_w < TINY(sp%m_w)) ) THEN       ! T < T_3 necessary for Ri > 0
    IF( multiplication_flag == 1 ) CALL ice_multiplication_mass( atmo%T, sp%xi,  m_rime,  n_splinter_tot ) ! m_rime is reducedd if splintering is happening

    IF( diffusion_type > 1 ) THEN
      q_heat = q_heat - m_rime * c_pw * (atmo%T - sp%T) - latentHeat_melt(atmo%T) * m_rime  
    ELSE 
      q_heat = q_heat - m_rime * c_pw * (atmo%T -  T_3) - latentHeat_melt(atmo%T) * m_rime  ! sum heat for eventual melting process
    ENDIF
    heat_rate = heat_rate + latentHeat_melt(atmo%T) * m_rime * sp%xi

    rho_rime = rho_rime_CL(sp, atmo, K_Ns, rm, dv)
    Vgain  = m_rime / rho_rime

    IF ( (sp_habit == 1 .AND. sp%mm < 1.01_wp) .OR. (sp_habit == 2) ) THEN
      sp%phi = habit_predict_riming(sp, Vgain, rm)
    END IF
    sp%m_r = sp%m_r + m_rime
    sp%V_r = sp%V_r + Vgain

  ELSE                                   ! melting or pure liquid water
    sp%m_w = sp%m_w + m_rime
    IF( diffusion_type > 1 ) THEN
      q_heat = q_heat - m_rime * c_pw * (atmo%T - sp%T) 
    ELSE
      q_heat = q_heat - m_rime * c_pw * (atmo%T - T_3) ! sum heat for eventual melting process
    ENDIF
  END IF

  rime_rate = rime_rate + sp%xi * m_rime/dt
      
#ifndef NDEBUG
  ! just diagnostics
  rimerate = 100._wp*(sp%m_r - m_r0) / (sp%m_i + sp%m_r + sp%m_w + sp%m_f)
  IF ( rimerate < 0._wp ) STOP "sp_rime: rimerate < 0"
#endif
END SUBROUTINE sp_rime

! end ndef WDROPS
#endif

END MODULE mo_rime_cont
