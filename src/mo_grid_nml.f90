!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_grid_nml

  USE mo_constants,     ONLY: wp

  IMPLICIT NONE

  INTEGER  :: nz  ! number of grid z-elements
  REAL(wp) ::    &
    dom_top,     &  ! [m]  domain top
    dom_bottom,  &  ! [m]  domain bottom
    box_area        ! [m2] box area (to form a quasi 3d model)

  PUBLIC :: read_grid_nml, nz, dom_top, dom_bottom, box_area


CONTAINS

!
! read grid parameters from input.nml
!
SUBROUTINE read_grid_nml()
  NAMELIST /grid_nml/ nz, box_area, dom_top, dom_bottom
  
  ! default settings
  nz = 1
  dom_top = 5000._wp
  dom_bottom = 0._wp
  box_area = 200._wp

  OPEN(UNIT=74, FILE="input.nml") !, STATUS=istat)
  READ(74, grid_nml)
  CLOSE(74)

  ! print non-derived parameters
  PRINT *, "grid_nml::nz         : ", nz
  PRINT *, "grid_nml::dom_top    : ", dom_top
  PRINT *, "grid_nml::dom_bottom : ", dom_bottom
  PRINT *, "grid_nml::box_area   : ", box_area
END SUBROUTINE read_grid_nml

END MODULE mo_grid_nml
