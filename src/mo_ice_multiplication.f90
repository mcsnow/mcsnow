!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_ice_multiplication
! see Pruppbacher und Klett 16.1.6: Snow crystal multiplication by riming
#ifndef WDROPS

  USE mo_constants,    ONLY: wp, i8, z1_3
  USE mo_mass2diam,    ONLY: mth
  USE mo_atmo_types,   ONLY: m2r, r2m, rhoi
  USE mo_atmo,         ONLY: t_atmo, get_atmo
  USE mo_sp_types,     ONLY: t_sp, sp_stat, p_addSp
  USE mo_sp_nml,       ONLY: agg_hist, xi0

  IMPLICIT NONE
  PRIVATE

  PUBLIC  :: ice_multiplication_mass, ice_multiplication_radius, init_ice_multiplication, add_splinter
  PRIVATE :: temperature_factor

CONTAINS

!
! Ice Multiplication: Rimed mass creates splinter
!  ignoring that drops might have to be larger than 24um to create splinters
!  no data about mass of created splinters, assume they have m_th (small ice)
!
SUBROUTINE ice_multiplication_mass( Tp, xi,  m_rime,  n_splinter_tot )
  REAL(wp),    INTENT(in)    :: Tp
  INTEGER(i8), INTENT(in)    :: xi
  REAL(wp),    INTENT(inout) :: m_rime
  INTEGER(i8), INTENT(inout) :: n_splinter_tot

  REAL(wp)     :: n_splinter
  REAL(wp)     :: factor

  factor = temperature_factor( Tp )

  n_splinter = ( 350.e6_wp * m_rime * factor) ! eq 16-70 dm/dt in g
  n_splinter_tot = n_splinter_tot + FLOOR(xi * n_splinter, i8)
  m_rime = m_rime - n_splinter * ( mth - EPSILON( mth ) )
#ifndef NDEBUG
      IF( m_rime < 0 ) STOP "sp_rime: m_rime < 0"
#endif
END SUBROUTINE ice_multiplication_mass

!
! Ice Multiplication: Rimed drop creates splinter
!  no data about mass of created splinters, assume they have m_th (small ice)
!
SUBROUTINE ice_multiplication_radius( xi, gen_threshold, rd, n_colls, n_splinter_tot )
  INTEGER(i8), INTENT(in)    :: xi
  REAL(wp),    INTENT(in)    :: gen_threshold
  REAL(wp),    INTENT(inout) :: rd
  INTEGER(i8), INTENT(inout) :: n_colls
  INTEGER(i8), INTENT(inout) :: n_splinter_tot

  INTEGER  :: noDrops
  
  IF ( rd < 12e-6_wp ) RETURN ! process only efficient for drops larger 24um in diameter
  !IF ( r2m*rd**3 < mth ) RETURN  ! selects slightly larger drops than 24um but so rd does not become negative if the splinter mass is reduced

  n_colls = n_colls + xi
  noDrops = FLOOR(xi/ gen_threshold) 
  IF ( noDrops > 0 ) THEN
    n_splinter_tot = n_splinter_tot + noDrops
    n_colls = n_colls - NINT(noDrops*gen_threshold)
    rd = (m2r*( xi*r2m*rd**3 - noDrops*mth )/xi)**z1_3 ! reduce mass by splinter mass, mth small ice
  END IF
END SUBROUTINE ice_multiplication_radius

!
! Setup threshold every x riming drops a splinter is created
!  at -5 C one splinter is generated every 250 drops, see Pruppbacher + Klett
!
PURE REAL(wp) FUNCTION init_ice_multiplication( Tp ) result ( gen_threshold )
  REAL(wp), INTENT(in)  :: Tp

  REAL(wp) :: factor
 
  gen_threshold = HUGE( gen_threshold )
 
  factor = temperature_factor( Tp )
  IF ( factor > 0.0_wp ) THEN
    gen_threshold = FLOOR( 250 / factor ) ! at -5 C one splinter every 250 drops
  END IF
END FUNCTION init_ice_multiplication

!
!  Temperature interpolation according to Pruppbacher + Klett, Microphysics of Clouds and Precipitation, 16-71
!   splintering measured by Hallet-Mossop to be most efficient at -5 Celsius
!
PURE REAL(wp) FUNCTION temperature_factor( Tp ) 
  REAL(wp), INTENT(in)  :: Tp

  REAL(wp) :: temp

  temperature_factor = 0 
  temp = Tp - 265.16_wp
  IF ( (temp > 0.0_wp) .AND. (Tp <= 268.16_wp) ) THEN
    temperature_factor = temp/3.0_wp
  ELSE
    temp = 270.16_wp - Tp
    IF ( (temp > 0_wp) .AND. (Tp > 268.16_wp) ) THEN
       temperature_factor = temp*0.5_wp
    END IF
  END IF
END FUNCTION temperature_factor

!
! add splinters 
!
SUBROUTINE add_splinter( addSp, z, dz, no_splinter )
   procedure(p_addSp)          :: addSp
   REAL(wp),     INTENT(in)    :: z, dz
   INTEGER(i8),  INTENT(in)    :: no_splinter

   TYPE(t_sp)  :: sp
   REAL(wp)    :: randf, prob
   INTEGER(i8) :: splinter_count
   TYPE(t_atmo)          :: atmo

   splinter_count = no_splinter

   prob = no_splinter/REAL( xi0, wp )
   IF( prob < 1.0_wp ) THEN
     CALL random_number( randf )
     IF( randf < prob ) THEN
        splinter_count = xi0 ! FLOOR(no_splinter/prob)
     ELSE
        RETURN
     END IF
   END IF

   CALL random_number( randf )
   sp%z   = z + dz*(randf-0.5_wp)
   sp%m_i = mth - EPSILON( mth )
   sp%V_i = sp%m_i / rhoi                           ! TODO: what shape & density do splinter have
   sp%phi = 1._wp
   sp%xi  = splinter_count
   sp%mm  = 1.0
   CALL get_atmo( sp%z, atmo )
   sp%T = atmo%T
   IF ( agg_hist >= 0 ) THEN
      sp%init_temp = atmo%T
      sp%init_ssat = atmo%ssat
   END IF
   sp%statusb = IBSET(sp%statusb, sp_stat%iceMulti)
   CALL addSp( sp )
END SUBROUTINE add_splinter


! end ndef WDROPS
#endif

END MODULE mo_ice_multiplication
