!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_atmo_nml

  USE mo_constants,      ONLY: wp
  USE mo_grid_nml,       ONLY: dom_top, dom_bottom

  IMPLICIT NONE

  INTEGER ::  &
    atmo_type,    & ! 1: idealized profile, 2: ecmwf profiles
    IGF_id          ! 1: IGF of CL92, 2: updated IGF + all modifications

  REAL(wp) ::     &
    area_factor,  & ! [1] coefficient for qv in 1d model with ecmwf profiles
    ssat,         & ! [1] supersaturation in the upper part of troposphere
    h2,           & ! [m] upper border of the 'riming zone'
    h1,           & ! [m] lower border of the 'riming zone'
    h0,           & ! [m] melting layer height, i.e. where T = T3
    nh2,          & ! [m] upper border of the 'nucleation zone'
    nh1             ! [m] lower border of the 'nucleation zone'

  CHARACTER(len=50) :: atmofile  ! name of a file containing atmospheric profiles
  CHARACTER(len=50) :: IGFfile  ! name of a file containing atmospheric profiles

  INTEGER                     :: IGFdim
  REAL(wp), DIMENSION(:,:), ALLOCATABLE  :: tab

  PUBLIC :: read_atmo_nml, IGF_id, tab, IGFdim, get_IGF


CONTAINS

!
! read grid parameters from input.nml
!
SUBROUTINE read_atmo_nml()
  NAMELIST /atmo_nml/ atmo_type, ssat, h0, h1, h2, nh1, nh2, area_factor, atmofile, IGF_id
  
  ! default settings
  atmo_type = 1
  ssat = 0.05_wp
  h0  = 0._wp
  h1  = 0._wp
  h2  = 0._wp
  nh1  = 3500._wp
  nh2  = 4500._wp
  area_factor = 1.1_wp
  atmofile = 'ecmwf_profile.txt'
  IGF_id     = 1

  OPEN(UNIT=74, FILE="input.nml") !, STATUS=istat)
  READ(74, atmo_nml)
  CLOSE(74)

  ! print non-derived parameters
  IF( (atmo_type == 1) .OR. (atmo_type == 11) ) THEN
    PRINT *, "atmo_nml::ssat: ", ssat
    PRINT *, "atmo_nml::h0: ", h0
    PRINT *, "atmo_nml::h1: ", h1
    PRINT *, "atmo_nml::h2: ", h2
    PRINT *, "atmo_nml::nh1: ", nh1
    PRINT *, "atmo_nml::nh2: ", nh2
    IF ( h1>h2 .OR. h2>dom_top .OR. h1<dom_bottom) &
      PRINT *, "WARNING: h1>h2 .OR. h2>dom_top .OR. h1<dom_bottom"
    IF ( nh1>nh2 .OR. nh2>dom_top .OR. nh1<dom_bottom) &
      PRINT *, "WARNING: nh1>nh2 .OR. nh2>dom_top .OR. nh1<dom_bottom"
  ELSE
    PRINT *, "atmo_nml::area_factor: ", area_factor
    PRINT *, "atmo_nml::atmofile: ", atmofile
  END IF
  PRINT *, "atmo_nml::IGF_id  : ", IGF_id
  CALL read_IGF()
END SUBROUTINE read_atmo_nml

!
! reads the inherent growth function (IGF) specified in 'curve_inherent.txt'.
! IGF is used for habit prediction
!
SUBROUTINE read_IGF()
  INTEGER :: i

  IF (IGF_id == 1) THEN
    IGFfile = 'curve_inherent.txt'
    IGFdim     = 117
  ELSE
    IGFfile = 'IGF_new_poly.txt'
    IGFdim     = 293
  END IF

  ALLOCATE( tab(2, IGFdim) )
  OPEN(74, FILE=IGFfile, STATUS='old', ACTION='read')

  DO i = 1, IGFdim
    READ(74,*) tab(1,i), tab(2,i)
  END DO

  CLOSE(74)

  ! values have to be converted to Kelvin since fit is for °C
  tab(1,:) = tab(1,:) + 273.15

END SUBROUTINE read_IGF

! 
! Function to get value of inherent growth function (IGF) for given temperature. The values are
! based on a reconstructed fit from Chen&Lamb (1994, Fig. 3). 
PURE FUNCTION get_IGF(T) result (IGF)
  REAL(wp), INTENT(in)        :: T
  INTEGER                     :: i
  REAL(wp)                    :: IGF, interpolation

  ! If temperature is higher than largest lookup value, take that one.
  ! Same for lowest temperature. This could be extended by a wider IGF fit
  IF (T > tab(1,IGFdim)) THEN
      IGF =  tab(2,IGFdim)
  ELSE IF ((T .LE. tab(1,1))) THEN 
      IGF = tab(2,1)
  ELSE
      DO i = 2, IGFdim
          IF (tab(1,i) .GT. T) THEN
              interpolation = (T - tab(1,i-1)) / (tab(1,i) - tab(1,i-1))
              IGF           = tab(2,i-1) + interpolation * (tab(2,i) - tab(2,i-1))
              EXIT
          END IF
      END DO
  END IF
END FUNCTION get_IGF

END MODULE mo_atmo_nml
