!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_outputArray

#ifdef HAVE_OPENMP
  USE omp_lib
#endif
  USE mo_constants,    ONLY: wp, i8, pi, z1_3, z3_4pi, zs1_2pi, pi_6
  USE mo_sp_types,     ONLY: t_sp!, sp_stat
  USE mo_grid,         ONLY: t_grid, coord2index
  USE mo_sp_nml,       ONLY: dtc, lwc0, dt_1dprof, dt_2dprof, &
                             mean_mass0, sigma0, has_asol,  &
                             perf_test, ini_type, time_end, agg_hist, sp_habit
  USE mo_atmo_nml,     ONLY: h1, h2
  USE mo_atmo_types,   ONLY: rholi, rhoii
  USE mo_atmo,         ONLY: t_atmo, get_atmo, diag
  USE mo_check,        ONLY: plot_atmo
  USE mo_sp_array,     ONLY: t_cont
#ifndef WDROPS
  USE mo_mass2diam,    ONLY: m2d_particle, z3_bet, unr_alf_i, spheroid_geometry
  USE mo_velocity,     ONLY: vterm_snow
#else
  USE mo_velocity,     ONLY: vterm_beard
#endif

  IMPLICIT NONE
  PRIVATE

  INTEGER, PARAMETER ::  &
    n_out1d = 200                          ! # sample points
  INTEGER, PARAMETER ::  &
    n_out2d = 100                          ! # sample points

  REAL(wp), PARAMETER ::         &
    dres = 50000._wp**(1/REAL(n_out1d-1,wp)) ! sample end at 50 mm hydrometeor's size

  REAL(wp), DIMENSION(n_out1d) :: &
    masspens, rmasspens                    ! sampled num. solution and ref. solution

  REAL(wp) :: &
    last_1dprof, &                         ! time of last write to 1d profiles
    last_2dprof                            ! time of last write to 2d profiles

#ifndef WDROPS
  PUBLIC :: write_solution, init_output !, write_riming_2dprofile
#else
  PUBLIC :: write_solution, init_output
#endif

  INTERFACE
    INTEGER FUNCTION func( sp )
      USE mo_sp_types, ONLY: t_sp
      TYPE(t_sp), INTENT(in) :: sp
    END FUNCTION func
  END INTERFACE


  CONTAINS

SUBROUTINE init_output( grid )
  TYPE(t_grid), INTENT(in) :: grid

  last_1dprof = -dt_1dprof
  last_2dprof = -dt_2dprof
  CALL plot_atmo( grid )
END SUBROUTINE

!
! write solution for files, compute error
!
SUBROUTINE write_solution( cont, tstep, grid, twomom_d )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: twomom_d(:,:,:)

  REAL(wp) :: h0a,h0b,h1a,h1b,h2a,h2b,h3a,h3b,dzout
  REAL(wp) :: g, ctime
  INTEGER  :: i
  LOGICAL  :: l1dmodel

  IF ( perf_test ) RETURN
  l1dmodel = ( ini_type /= 1 )
  ctime = REAL(tstep,wp) * dtc

#ifndef WDROPS
  ! 2d-profile stuff
  !
  IF ( l1dmodel .AND. ctime - last_2dprof >= dt_2dprof ) THEN
    CALL write_riming_2dprofile( cont, tstep, grid )
    last_2dprof = ctime
  END IF
#endif

  ! 1d stuff
  !
  IF ( ctime - last_1dprof < dt_1dprof ) RETURN

  ! 1d mass-to-Fr ratio
  ! IF ( l1dmodel ) CALL write_icefraction( cont, tstep, grid ) 
  IF ( ( l1dmodel ) .AND. (ctime == time_end) ) CALL write_icefraction( cont, tstep, grid ) 

  ! write to files and update sampled solution arrays
  IF ( l1dmodel ) THEN

    ! define layers for output of size distributions

    IF (h1.gt.2900.and.h2.gt.4000) THEN ! 1d_melt_and_break
       h0a = 0.
       h0b = 200.
       h1a = 3000.
       h1b = 3200.
       h2a = 6001.
       h2b = 6200.
       h3a = h2
       h3b = (h2+grid%dom_top)/2.0_wp
    ELSE
       h0a = 0._wp 
       h0b = h1 / 2.0_wp
       h1a = 700.
       h1b = 900.
       h2a = h1
       h2b = (h1+h2)/2.0_wp
       h3a = h2
       h3b = (h2+grid%dom_top)/2.0_wp
    END IF

    if (.true.) then
       CALL write_distributions( cont, tstep, grid, h0a, h0b )
       CALL write_distributions( cont, tstep, grid, h1a, h1b )
       CALL write_distributions( cont, tstep, grid, h2a, h2b )
       CALL write_distributions( cont, tstep, grid, h3a, h3b )

#ifndef WDROPS
       CALL write_distributions_rimedegree( cont, tstep, grid, h0a, h0b )
       CALL write_distributions_rimedegree( cont, tstep, grid, h1a, h1b )
       CALL write_distributions_rimedegree( cont, tstep, grid, h2a, h2b )
       CALL write_distributions_rimedegree( cont, tstep, grid, h3a, h3b )
       
       CALL write_distributions_meltdegree( cont, tstep, grid, h0a, h0b )
       CALL write_distributions_meltdegree( cont, tstep, grid, h1a, h1b )
       CALL write_distributions_meltdegree( cont, tstep, grid, h2a, h2b )
       CALL write_distributions_meltdegree( cont, tstep, grid, h3a, h3b )
       
       CALL write_distributions_monomers( cont, tstep, grid, h0a, h0b )
       CALL write_distributions_monomers( cont, tstep, grid, h1a, h1b )
       CALL write_distributions_monomers( cont, tstep, grid, h2a, h2b )
       CALL write_distributions_monomers( cont, tstep, grid, h3a, h3b )

       CALL write_distributions_rhorime( cont, tstep, grid, h0a, h0b )
       CALL write_distributions_rhorime( cont, tstep, grid, h1a, h1b )
       CALL write_distributions_rhorime( cont, tstep, grid, h2a, h2b )
       CALL write_distributions_rhorime( cont, tstep, grid, h3a, h3b )

       CALL write_distributions_temp( cont, tstep, grid, h0a, h0b )
       CALL write_distributions_temp( cont, tstep, grid, h1a, h1b )
       CALL write_distributions_temp( cont, tstep, grid, h2a, h2b )
       CALL write_distributions_temp( cont, tstep, grid, h3a, h3b )

       IF (sp_habit == 1) THEN
        CALL write_distributions_velocity( cont, tstep, grid, h0a, h0b )
        CALL write_distributions_velocity( cont, tstep, grid, h1a, h1b )
        CALL write_distributions_velocity( cont, tstep, grid, h2a, h2b )
        CALL write_distributions_velocity( cont, tstep, grid, h3a, h3b )
       END IF
#endif

    else
       
       dzout = 200.0_wp
       h0a = 0.0_wp
       h0b = dzout
       do while (h0b.le.h2+dzout) 
          CALL write_distributions( cont, tstep, grid, h0a, h0b )
#ifndef WDROPS
          CALL write_distributions_rhorime( cont, tstep, grid, h0a, h0b )
          CALL write_distributions_rimedegree( cont, tstep, grid, h0a, h0b )
          CALL write_distributions_meltdegree( cont, tstep, grid, h0a, h0b )
          CALL write_distributions_monomers( cont, tstep, grid, h0a, h0b )
#endif
          h0a = h0a + dzout
          h0b = h0b + dzout
       end do
       CALL write_distributions( cont, tstep, grid, h3a, h3b )

#ifndef WDROPS
       CALL write_distributions_rimedegree( cont, tstep, grid, h3a, h3b )
       CALL write_distributions_meltdegree( cont, tstep, grid, h3a, h3b )
       CALL write_distributions_monomers( cont, tstep, grid, h3a, h3b )
#endif

    end if


    CALL write_hprofiles( cont, tstep, grid )
    CALL write_hprofiles_diag( tstep, grid )
    IF( PRESENT(twomom_d) ) THEN
      CALL write_twomom_d( twomom_d, tstep )
    END IF

#ifndef WDROPS
    !IF ( agg_hist >= 0 .AND. ABS(ctime - time_end) < dtc ) &
    !  CALL write_history( sp_list_cell, grid )
#endif
  ELSE
    CALL write_distributions( cont, tstep, grid )
  END IF

  last_1dprof = ctime

  ! analytical solution for 1d-profiles
  !
  IF ( .NOT.has_asol ) RETURN

  CALL write_ana_mass_dens( tstep )

  ! we do not have reference solution in general
  ! just compute l2 norm of num. solution
  g = 0._wp

  DO i=1, n_out1d
    rmasspens(i) = 0._wp
    g = g + (masspens(i)-rmasspens(i))**2
  END DO

  PRINT *, ""
  WRITE(*,'(a,f8.1,es10.1)') " -+-+-+-    time, l2-error: ", dtc*tstep, SQRT(g)
  PRINT *, ""
END SUBROUTINE 

!
! core function of write distribution
!
SUBROUTINE write_distributions_core( cont, tstep, grid, f, n_cats, fname_ext, l_extraOutput, h1_opt, h2_opt, thres_opt )
  TYPE(t_cont), TARGET, INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  procedure(func) :: f
  INTEGER, INTENT(in) :: n_cats
  CHARACTER(LEN=*), INTENT(in) :: fname_ext
  LOGICAL, INTENT(in) :: l_extraOutput
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt
  REAL(wp), OPTIONAL, INTENT(in) :: thres_opt(:)

  REAL(wp), ALLOCATABLE :: Nd(:,:), Md(:,:), Fn(:,:), Fm(:,:), Fmono(:,:)

  TYPE(t_sp), POINTER :: sp
  REAL(wp) :: r, m, rad, sigmai, expdiff, percube, h1, h2
  INTEGER :: i, isp, ir, ih1, ih2, n, i1, i2, jc, jb, startPos
  INTEGER :: nsp
  CHARACTER(len=50) :: fname_rad
  LOGICAL :: file_exists
  TYPE(t_atmo) :: atmo

  IF ( PRESENT(h1_opt) ) THEN
    IF ( .NOT. PRESENT(h2_opt) ) STOP "write_distributions:: argument h2 missing"
    h1 = h1_opt
    h2 = h2_opt
  ELSE
    h1 = grid%dom_bottom
    h2 = grid%dom_top
  END IF

  WRITE(fname_rad, '(a,a,i4.4,a,i4.4,a)') "distribution_", fname_ext, INT(h1), "_", INT(h2), ".dat"

  i1 = coord2index( MAX(h1,grid%dom_bottom) )
  i2 = coord2index( MIN(h2,grid%dom_top) )
  ih1 = MIN(i1,i2)
  ih2 = MAX(i1,i2)
  jc = 1
  jb = 1
  IF( (h2-h1) < 1._wp) RETURN
  percube = 1.0_wp/( (h2-h1)*grid%box_area )

  ! total number of SPs in layer between h1 and h2 -> N_s for kernel estimate
  nsp = SUM( cont%cellSParray(ih1:ih2,jc,jb)%noParts )

  ! sigma for kernel estimate, sigma = sigma0/N_s**(1/5), see Shima Sec 5.1.4
  sigmai = REAL(nsp,wp)**0.2_wp / sigma0

  INQUIRE(FILE=fname_rad, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname_rad, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname_rad, status="new", action="write")
  END IF

  WRITE(74,*) ""
  WRITE(74,*) ""
  IF( PRESENT(thres_opt) ) THEN
    WRITE(74,"(a,f8.1,a,20f10.2)") "#", tstep*dtc/60, " min ", thres_opt
  ELSE
    WRITE(74,"(a,f8.1,a)") "#""", tstep*dtc/60, " min """
  ENDIF

  ALLOCATE( Nd(0:n_cats,n_out1d), Md(0:n_cats,n_out1d), Fn(0:n_cats,n_out1d), Fm(0:n_cats,n_out1d), Fmono(0:n_cats,n_out1d) )
  Nd(:,:) = 0._wp  ! [1/kg] number to radius
  Md(:,:) = 0._wp  ! [kg] mass to radius
  Fn(:,:) = 0._wp  ! [-] number flux
  Fm(:,:) = 0._wp  ! [kg] mass flux
  Fmono(:,:) = 0._wp  ! [-] monomer flux

  ! compute mass density distribution for various radii
  ! run over all cells
  !$OMP PARALLEL DO DEFAULT(none) &
  !$OMP SHARED(ih1,ih2,cont,jc,jb,h1,h2,atmo,sigmai) &
  !$OMP PRIVATE(rad,startPos,ir, nsp, isp, sp, m, r, expdiff, n) &
  !$OMP REDUCTION(+:Nd,Md,Fn,Fm,Fmono) &
  !$OMP SCHEDULE(static,1)
  DO i = ih1, ih2 !height loop
    !rad = 1e-6_wp ! sample start at 1 um of hydrometeor's radius
    !DO ir = 1, n_out1d ! radius loop
      nsp = cont%cellSParray(i,jc,jb)%noParts
      startPos = cont%cellSParray(i,jc,jb)%startPos
      DO isp = 1, nsp !particle loop
        sp => cont%sp_array(startPos+isp-1)
        IF( sp%cellId < 1 ) CYCLE ! inactive particle
        IF( (sp%z < h1) .OR. (sp%z > h2) ) CYCLE
#ifndef WDROPS
        CALL get_atmo( sp%z, atmo )
        CALL m2d_particle( sp )
        sp%v = vterm_snow( atmo, sp )
        m = sp%m_i + sp%m_f + sp%m_r + sp%m_w
        r = 0.5_wp * sp%d
        !r = ( z3_4pi * ( sp%V_i + sp%V_r + (sp%m_f * rhoii) + (sp%m_w * rholi) ) )**z1_3      ! volume equivalent radius
#else
        m = sp%m_w
        r = ( z3_4pi * rholi * m )**z1_3
        sp%v = vterm_beard( r )
#endif
        n = f( sp )
        rad = 1e-6_wp ! sample start at 1 um of hydrometeor's radius
        DO ir = 1, n_out1d ! radius loop
          expdiff = zs1_2pi*sigmai * EXP(-0.5_wp*((LOG(rad)-LOG(r))*sigmai)**2)
          Nd(0,ir) = Nd(0,ir) + sp%xi * expdiff
          Md(0,ir) = Md(0,ir) + sp%xi * m * expdiff
          Fn(0,ir) = Fn(0,ir) + sp%xi * sp%v * expdiff
          Fm(0,ir) = Fm(0,ir) + sp%xi * m * sp%v * expdiff
          Fmono(0,ir) = Fmono(0,ir) + sp%xi * sp%mm * sp%v * expdiff
          IF( n > 0 ) THEN
            Nd(n,ir) = Nd(n,ir) + sp%xi * expdiff
            Md(n,ir) = Md(n,ir) + sp%xi * m * expdiff
            Fn(n,ir) = Fn(n,ir) + sp%xi * sp%v * expdiff
            Fm(n,ir) = Fm(n,ir) + sp%xi * m * sp%v * expdiff
            Fmono(n,ir) = Fmono(n,ir) + sp%xi * sp%mm * sp%v * expdiff
          ENDIF
          rad = rad*dres
        END DO ! radius loop
      END DO !particle loop
      !rad = rad*dres
    !END DO ! radius loop
  END DO !height loop
  !$OMP END PARALLEL DO


  Nd(:,:) = Nd(:,:) * percube
  Md(:,:) = Md(:,:) * percube
  Fn(:,:) = Fn(:,:) * percube
  Fm(:,:) = Fm(:,:) * percube
  Fmono(:,:) = Fmono(:,:) * percube
  ! output
  rad = 1e-6_wp ! sample start at 1 um of hydrometeor's radius
  DO ir = 1, n_out1d ! radius loop
    WRITE(74, '(es12.2e3)',advance='no') rad
    DO i=0,n_cats
        WRITE(74, '(es12.2e3)',advance='no') Nd(i,ir)
        WRITE(74, '(es12.2e3)',advance='no') Md(i,ir)
      IF( l_extraOutput ) THEN
        WRITE(74, '(es12.2e3)',advance='no') Fn(i,ir)
      ENDIF
        WRITE(74, '(es12.2e3)',advance='no') Fm(i,ir)
      IF( l_extraOutput ) THEN
        WRITE(74, '(es12.2e3)',advance='no') Fmono(i,ir)
      END IF
    ENDDO
    WRITE(74, *)
    rad = rad*dres
  END DO ! radius loop

  CLOSE(74)

  DEALLOCATE( Nd, Md, Fn, Fm, Fmono )
END SUBROUTINE write_distributions_core


!
! categorized particle into classes
!
INTEGER FUNCTION categorizeSp( sp ) RESULT( returnIndex )
  TYPE(t_sp), INTENT(in) :: sp        
#ifndef WDROPS
  REAL(wp), PARAMETER :: unr_agg_thres = 0.01_wp
  REAL(wp), PARAMETER :: graupel_thres = 0.95_wp
  REAL(wp) :: Fr
  !REAL(wp) :: v_crit              
#endif                                  
  returnIndex = -1                    
#ifndef WDROPS                          
  ! classification of unrimed and graupel depends on spherical fill-in
  !v_crit = pi_6*sp%d**3 - sp%m_i*rhoii ! v_crit might be 0 for small ice
  !Fr = MIN(1.0_wp, MERGE( sp%v_r/v_crit, 1.0_wp , v_crit > 0.0_wp) ) 
  Fr = sp%m_r / (sp%m_i + sp%m_f + sp%m_r + sp%m_w)
  IF( (sp%m_i+sp%m_f+sp%m_r) < TINY(sp%m_i) ) THEN
      returnIndex = 1 ! liquid drop     
  ELSEIF( sp%mm < 1.1_wp ) THEN
      IF ( Fr < unr_agg_thres )  THEN
        returnIndex = 2 ! unrimed pristine ice
      ELSE
        returnIndex = 5 ! rimed pristine ice
      END IF
  ELSEIF( Fr < unr_agg_thres ) THEN   
      returnIndex = 3 ! unrimed aggregate
  ELSEIF( Fr > graupel_thres ) THEN   
      returnIndex = 4 ! graupel         
  !ELSE !       There is not ELSE, because 'rimed snow' is calculated as residuum 
  !  returnIndex = 5 ! rimed aggregate
  ENDIF                               
#endif                                  
END FUNCTION categorizeSp


!
! write distributions of ice particles located between the h1 and h2 heights
!
SUBROUTINE write_distributions( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  CALL write_distributions_core( cont, tstep, grid, categorizeSp, 5, "", .TRUE., h1_opt, h2_opt )
END SUBROUTINE write_distributions


#ifndef WDROPS
!
! write distributions of ice particles located between the h1 and h2 heights
!
SUBROUTINE write_distributions_rimedegree( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  INTEGER,  PARAMETER :: noCases = 10
  REAL(wp), PARAMETER :: rthres(noCases-1) = (/0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9/)

  CALL write_distributions_core( cont, tstep, grid, returnIndex, noCases, "rime_", .FALSE., h1_opt, h2_opt, rthres )
CONTAINS

  INTEGER FUNCTION returnIndex( sp )
    TYPE(t_sp), INTENT(in) :: sp

    REAL(wp) :: Frime
    
    ! stratified by degree of riming
    Frime = sp%m_r/(sp%m_i+sp%m_f+sp%m_r+sp%m_w)
    IF (Frime < rthres(1)) THEN
           returnIndex = 1
    ELSEIF (Frime < rthres(2)) THEN
           returnIndex = 2
    ELSEIF (Frime < rthres(3)) THEN
           returnIndex = 3
    ELSEIF (Frime < rthres(4)) THEN
           returnIndex = 4
    ELSEIF (Frime < rthres(5)) THEN
           returnIndex = 5
    ELSEIF (Frime < rthres(6)) THEN
           returnIndex = 6
    ELSEIF (Frime < rthres(7)) THEN
           returnIndex = 7
    ELSEIF (Frime < rthres(8)) THEN
           returnIndex = 8
    ELSEIF (Frime < rthres(9)) THEN
           returnIndex = 9
    ELSE
           returnIndex = 10
    ENDIF
  END FUNCTION returnIndex
END SUBROUTINE write_distributions_rimedegree

!
! write distributions of ice particles located between the h1 and h2 heights
!
SUBROUTINE write_distributions_rhorime( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  INTEGER,  PARAMETER :: noCases = 10
  REAL(wp), PARAMETER :: rthres(noCases-1) = (/0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6/) * 1e3

  CALL write_distributions_core( cont, tstep, grid, returnIndex, noCases, "rhorime_", .FALSE., h1_opt, h2_opt, rthres )
CONTAINS

  INTEGER FUNCTION returnIndex( sp )
    TYPE(t_sp), INTENT(in) :: sp

    REAL(wp) :: Frime, rrime
    ! stratified by rime density
    Frime = sp%m_r/(sp%m_i+sp%m_f+sp%m_r+sp%m_w)
    rrime = MERGE(sp%m_r/sp%v_r , -1.0_wp, sp%v_r > 0.0_wp)
    IF (Frime < 0.1) THEN
           returnIndex = 1
    ELSEIF (rrime < rthres(1)) THEN
           returnIndex = 2
    ELSEIF (rrime < rthres(2)) THEN
           returnIndex = 3
    ELSEIF (rrime < rthres(3)) THEN
           returnIndex = 4
    ELSEIF (rrime < rthres(4)) THEN
           returnIndex = 5
    ELSEIF (rrime < rthres(5)) THEN
           returnIndex = 6
    ELSEIF (rrime < rthres(6)) THEN
           returnIndex = 7
    ELSEIF (rrime < rthres(7)) THEN
           returnIndex = 8
    ELSEIF (rrime < rthres(8)) THEN
           returnIndex = 9
    ELSE
           returnIndex = 10
    END IF
  END FUNCTION returnIndex
END SUBROUTINE write_distributions_rhorime

!
! write distributions of ice particle temperatures located between the h1 and h2 heights
!
SUBROUTINE write_distributions_temp( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  INTEGER,  PARAMETER :: noCases = 10
  REAL(wp), PARAMETER :: rthres(noCases-1) = (/-2.0,-1.5,-1.0,-0.5,0.0,0.5,1.0,1.5,2.0/)

  CALL write_distributions_core( cont, tstep, grid, returnIndex, noCases, "temp_", .FALSE., h1_opt, h2_opt, rthres )
CONTAINS

  INTEGER FUNCTION returnIndex( sp )
    TYPE(t_sp), INTENT(in) :: sp

    TYPE(t_atmo) :: atmo
    REAL(wp)     :: Fmelt
    CALL get_atmo(sp%z, atmo)  
    Fmelt = atmo%T - sp%T

    IF (Fmelt < rthres(1)) THEN
           returnIndex = 1
    ELSEIF (Fmelt < rthres(2)) THEN
           returnIndex = 2
    ELSEIF (Fmelt < rthres(3)) THEN
           returnIndex = 3
    ELSEIF (Fmelt < rthres(4)) THEN
           returnIndex = 4
    ELSEIF (Fmelt < rthres(5)) THEN
           returnIndex = 5
    ELSEIF (Fmelt < rthres(6)) THEN
           returnIndex = 6
    ELSEIF (Fmelt < rthres(7)) THEN
           returnIndex = 7
    ELSEIF (Fmelt < rthres(8)) THEN
           returnIndex = 8
    ELSEIF (Fmelt < rthres(9)) THEN
           returnIndex = 9
    ELSE
           returnIndex = 10
    END IF
  END FUNCTION returnIndex
END SUBROUTINE write_distributions_temp


!
! write distributions of ice particles located between the h1 and h2 heights
!
SUBROUTINE write_distributions_meltdegree( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  INTEGER,  PARAMETER :: noCases = 10
  REAL(wp), PARAMETER :: rthres(noCases-1) = (/0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9/)

  CALL write_distributions_core( cont, tstep, grid, returnIndex, noCases, "melt_", .FALSE., h1_opt, h2_opt, rthres )
CONTAINS

  INTEGER FUNCTION returnIndex( sp )
    TYPE(t_sp), INTENT(in) :: sp
    REAL(wp) :: Fmelt

    ! stratified by degree of melting
    Fmelt = sp%m_w/(sp%m_i+sp%m_f+sp%m_r+sp%m_w)
    IF (Fmelt < rthres(1)) THEN
           returnIndex = 1
    ELSEIF (Fmelt < rthres(2)) THEN
           returnIndex = 2
    ELSEIF (Fmelt < rthres(3)) THEN
           returnIndex = 3
    ELSEIF (Fmelt < rthres(4)) THEN
           returnIndex = 4
    ELSEIF (Fmelt < rthres(5)) THEN
           returnIndex = 5
    ELSEIF (Fmelt < rthres(6)) THEN
           returnIndex = 6
    ELSEIF (Fmelt < rthres(7)) THEN
           returnIndex = 7
    ELSEIF (Fmelt < rthres(8)) THEN
           returnIndex = 8
    ELSEIF (Fmelt < rthres(9)) THEN
           returnIndex = 9
    ELSE
           returnIndex = 10
    END IF
  END FUNCTION returnIndex
END SUBROUTINE write_distributions_meltdegree

!
! write distributions of ice particles for different monomer counts
!
SUBROUTINE write_distributions_monomers( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  INTEGER,  PARAMETER :: noCases = 7 
  REAL(wp), PARAMETER :: rthres(noCases-1) = (/2,10,30,100,300,600/)

  CALL write_distributions_core( cont, tstep, grid, returnIndex, noCases, "mono_", .FALSE., h1_opt, h2_opt, rthres )
CONTAINS

  INTEGER FUNCTION returnIndex( sp )
    TYPE(t_sp), INTENT(in) :: sp

    ! stratified by monomer number
    IF (sp%mm < rthres(1) ) THEN
      returnIndex = 1
    ELSEIF (sp%mm < rthres(2) ) THEN
      returnIndex = 2
    ELSEIF (sp%mm < rthres(3) ) THEN
      returnIndex = 3
    ELSEIF (sp%mm < rthres(4) ) THEN
      returnIndex = 4
    ELSEIF (sp%mm < rthres(5) ) THEN
      returnIndex = 5
    ELSEIF (sp%mm < rthres(6) ) THEN
      returnIndex = 6
    ELSE
      returnIndex = 7
    END IF
  END FUNCTION returnIndex
END SUBROUTINE write_distributions_monomers


!
! write distributions of ice particles located between the h1 and h2 heights
!
SUBROUTINE write_distributions_velocity( cont, tstep, grid, h1_opt, h2_opt )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid
  REAL(wp), OPTIONAL, INTENT(in) :: h1_opt, h2_opt

  INTEGER,  PARAMETER :: noCases = 10
  REAL(wp), PARAMETER :: rthres(noCases-1) = (/0.1,0.25,0.5,0.75,1.,1.25,1.5,1.75,2./)

  CALL write_distributions_core( cont, tstep, grid, returnIndex, noCases, "velo_", .FALSE., h1_opt, h2_opt, rthres )
CONTAINS

  INTEGER FUNCTION returnIndex( sp )
    TYPE(t_sp), INTENT(in) :: sp
    
    ! stratified by degree of velocity
    IF (sp%v < rthres(1)) THEN
           returnIndex = 1
    ELSEIF (sp%v < rthres(2)) THEN
           returnIndex = 2
    ELSEIF (sp%v < rthres(3)) THEN
           returnIndex = 3
    ELSEIF (sp%v < rthres(4)) THEN
           returnIndex = 4
    ELSEIF (sp%v < rthres(5)) THEN
           returnIndex = 5
    ELSEIF (sp%v < rthres(6)) THEN
           returnIndex = 6
    ELSEIF (sp%v < rthres(7)) THEN
           returnIndex = 7
    ELSEIF (sp%v < rthres(8)) THEN
           returnIndex = 8
    ELSEIF (sp%v < rthres(9)) THEN
           returnIndex = 9
    ELSE
           returnIndex = 10
    ENDIF
  END FUNCTION returnIndex
END SUBROUTINE write_distributions_velocity
#endif

SUBROUTINE write_twomom_d( twomom_d, tstep )
  REAL(wp), INTENT(IN) :: twomom_d(:,:,:)
  INTEGER,  INTENT(IN) :: tstep

  CHARACTER(len=50), PARAMETER :: fname_twomom = "twomom_d.dat"
  LOGICAL :: file_exists


  INQUIRE(FILE=fname_twomom, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname_twomom, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname_twomom, status="new", action="write")
  END IF
  WRITE(74,*) ""
  WRITE(74,*) ""
  WRITE(74,"(a,f8.1,a)") "#""", tstep*dtc/60, " min"""
  WRITE(74,*) twomom_d

  CLOSE(74)
END SUBROUTINE write_twomom_d

!
! write height profile
!
SUBROUTINE write_hprofiles( cont, tstep, grid )
  TYPE(t_cont), TARGET, INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid

  TYPE(t_sp), POINTER :: sp
  REAL(wp) :: m, sigmai, Ns, dz, z, weight, weightNs, presigma
  REAL(wp), DIMENSION(0:5) :: Nd, Md, Fn, Fm, Fmono

  INTEGER :: i, isp, ir, jb, jc, startPos
  INTEGER :: nprof
  INTEGER :: nsp
  LOGICAL :: file_exists
  CHARACTER(len=50), PARAMETER ::   &
    fname_hei = "hei2massdens.dat"
  LOGICAL, PARAMETER :: kestimator = .false.
  TYPE(t_atmo) :: atmo
  INTEGER      :: n

  ! when not a 1D model stop here
  IF ( ini_type /= 2 ) RETURN

  INQUIRE(FILE=fname_hei, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname_hei, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname_hei, status="new", action="write")
    WRITE(74,"(a,i8)") "#",grid%nz
  END IF
  WRITE(74,*) ""
  WRITE(74,*) ""
  WRITE(74,"(a,f8.1,a)") "#""", tstep*dtc/60, " min"""

  IF (kestimator) THEN
     nprof = n_out1d
  ELSE
     nprof = grid%nz+1
  END IF

  jc = 1
  jb = 1
  nsp = SUM( cont%cellSParray(:,jc,jb)%noParts )
  sigmai = REAL(nsp,wp)**0.2_wp / 500._wp
  presigma = zs1_2pi * sigmai !/ REAL(nsp,wp)
  dz = (grid%dom_top - grid%dom_bottom) / REAL(nprof-1,wp)
  z = grid%dom_bottom
  DO ir = 1, nprof ! z loop
    Ns    = 0._wp  ! [1/m3] number of SPs
    Nd(:) = 0._wp  ! [1/m3] number of particles
    Md(:) = 0._wp  ! [kg] mass
    Fn(:) = 0._wp  ! [kg] number flux
    Fm(:) = 0._wp  ! [kg] mass flux
    Fmono(:) = 0._wp  ! [kg] monomer flux

    !$OMP PARALLEL DO DEFAULT(none) &
    !$OMP SHARED(jc,jb,grid,atmo,cont,presigma,z,dz,sigmai) &
    !$OMP PRIVATE(nsp,startPos,isp,sp,m,weight,weightNs,n) &
    !$OMP REDUCTION(+:Ns,Nd,Md,Fn,Fm,Fmono) &
    !$OMP SCHEDULE(STATIC,1)
    DO i = 1, cont%noCells ! cell loop
      nsp = cont%cellSParray(i,jc,jb)%noParts
      startPos = cont%cellSParray(i,jc,jb)%startPos
      DO isp = 1, nsp ! particle loop
        sp => cont%sp_array(startPos+isp-1)
        IF( sp%cellId < 1 ) CYCLE ! inactive particle
#ifndef WDROPS
        CALL get_atmo( sp%z, atmo )
        IF (kestimator) THEN
           weight = presigma * EXP(-0.5_wp*((z-sp%z)*sigmai)**2)
           weightNs = weight
        ELSE
           IF (sp%z.ge.z.and.sp%z.lt.z+dz) THEN
              weight = 1.0_wp / dz
              weightNs = 1.0_wp
           ELSE
              CYCLE
           END IF
        END IF
        CALL m2d_particle( sp )
        sp%v = vterm_snow( atmo, sp )
        m = sp%m_i + sp%m_f + sp%m_r + sp%m_w
#else
        weight   = 1.0_wp
        weightNs = 1.0_wp        
        sp%v = vterm_beard( (z3_4pi*rholi*m)**z1_3 )
        m = sp%m_w
#endif
        Ns = Ns + weightNs
        Nd(0) = Nd(0) + sp%xi * weight
        Md(0) = Md(0) + sp%xi * m * weight
        Fn(0) = Fn(0) + sp%xi * sp%v * weight
        Fm(0) = Fm(0) + sp%xi * m * sp%v * weight
        Fmono = Fmono(0) + sp%xi * sp%mm * sp%v * weight
#ifndef WDROPS
        n = categorizeSp( sp )
        IF( n > 0 ) THEN
          Nd(n) = Nd(n) + sp%xi * weight
          Md(n) = Md(n) + sp%xi * m * weight
          Fn(n) = Fn(n) + sp%xi * sp%v * weight
          Fm(n) = Fm(n) + sp%xi * m * sp%v * weight
          Fmono(n) = Fmono(n) + sp%xi * sp%mm * sp%v * weight
        ENDIF
#endif
      END DO ! particle loop
    END DO ! cell loop
    !$OMP END PARALLEL DO

    Nd(:) = Nd(:) / grid%box_area
    Md(:) = Md(:) / grid%box_area
    Fn(:) = Fn(:) / grid%box_area
    Fm(:) = Fm(:) / grid%box_area
    Fmono(:) = Fmono(:) / grid%box_area
    WRITE(74,'(2es12.2e3)',advance='no') z, Ns
    i = 0
    WRITE(74,'(5es12.2e3)',advance='no') Nd(i), Md(i), Fn(i), Fm(i), Fmono(i)
#ifndef WDROPS
    i = 2
    WRITE(74,'(5es12.2e3)',advance='no') Nd(i), Md(i), Fn(i), Fm(i), Fmono(i)
    i = 3
    WRITE(74,'(5es12.2e3)',advance='no') Nd(i), Md(i), Fn(i), Fm(i), Fmono(i)
    i = 4
    WRITE(74,'(5es12.2e3)',advance='no') Nd(i), Md(i), Fn(i), Fm(i), Fmono(i)
    i = 1
    WRITE(74,'(5es12.2e3)',advance='no') Nd(i), Md(i), Fn(i), Fm(i), Fmono(i)
    i = 5
    WRITE(74,'(5es12.2e3)',advance='no') Nd(i), Md(i), Fn(i), Fm(i), Fmono(i)
#endif
    WRITE(74, *)
    z = z + dz
  END DO ! z loop

  CLOSE(74)
END SUBROUTINE write_hprofiles

!
! write height profile
!
SUBROUTINE write_hprofiles_diag( tstep, grid )
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid

  INTEGER  :: i
  LOGICAL  :: file_exists
  CHARACTER(len=50), PARAMETER :: fname = "hprofiles_diag.dat"

  ! when not a 1D model stop here
  IF ( ini_type /= 2 ) RETURN

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname, status="new", action="write")
  END IF
  WRITE(74,*) ""
  WRITE(74,*) ""
  WRITE(74,"(a,f8.1,a)") "#""", tstep*dtc/60, " min"""

  DO i = 1, grid%nz
    WRITE(74,'(5es12.2e3)') grid%z_f(i), diag(i)%dep_rate, diag(i)%dep_mono, diag(i)%rime_rate, diag(i)%heat_rate
  END DO

  CLOSE(74)
END SUBROUTINE write_hprofiles_diag

!
! write ice fraction
!
SUBROUTINE write_icefraction( cont, tstep, grid )
  TYPE(t_cont), INTENT(in) :: cont
  INTEGER, INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid

#ifndef WDROPS
  TYPE(t_atmo) :: atmo
#endif
  TYPE(t_sp) :: sp
  REAL(wp) :: mtot, vt, d, A, m_i, rho_app, rho_tot, V_tot
  INTEGER :: i, isp, jk, jc, jb, nsp, startPos
  LOGICAL :: file_exists
  LOGICAL :: first_time = .TRUE.
  CHARACTER(len=50), PARAMETER ::   &
    fname_fr = "mass2fr.dat"

  INQUIRE(FILE=fname_fr, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname_fr, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname_fr, status="new", action="write")
  END IF
  IF (first_time) THEN
    WRITE(74,*) ' time ', ',', ' mTot ',    ',', ' sHeight ',   ',', ' vel ', ',',    &
                ' dia ',    ',', ' area ',    ',', ' sMice ', ',', ' sVice ',',',    &
                ' sPhi ',  ',', ' sRhoIce ', ',',  ' sNmono ', ',',    &
                ' sMrime ',  ',', ' sVrime ',  ',', ' sMult ', ',', 'sRho_tot'
    first_time = .FALSE.
  ENDIF
  ! WRITE(74,*) ""
  WRITE(74,*) ""
  ! WRITE(74,"(a,f8.1,a)") "#""", tstep*dtc/60, " min"""

  DO i = 1, grid%nz
    jk = i
    jc = 1
    jb = 1
    nsp = cont%cellSParray(i,jc,jb)%noParts
    startPos = cont%cellSParray(i,jc,jb)%startPos
    DO isp = 1, nsp
      sp = cont%sp_array(startPos+isp-1)
#ifndef WDROPS
      mtot = sp%m_i + sp%m_f + sp%m_r + sp%m_w
      m_i  = sp%m_i
      CALL get_atmo( sp%z, atmo ) 
      CALL m2d_particle( sp )
      vt = vterm_snow( atmo, sp )
      IF ( (sp_habit == 1 .AND. sp%mm < 1.01_wp) .OR. (sp_habit == 2) ) THEN
        CALL spheroid_geometry( sp, d, A, V_tot) ! misuse of d and A as temp storage
      ELSE
        V_tot = pi_6 * sp%d**3
      ENDIF
      rho_tot = mtot / V_tot
      d = sp%d
      A = sp%p_area

      IF (sp%V_i .GT. 0._wp) THEN
        rho_app  = sp%m_i/sp%V_i
      ELSE 
        rho_app  = 0._wp
      END IF
      
      WRITE(74,*) tstep*dtc/60,',', mtot, ',', sp%z, ',',vt, ',',d, ',',A, ',',sp%m_i, ',',sp%V_i,    &
                   ',',sp%phi, ',',rho_app, ',', sp%mm, ',', sp%m_r, ',', sp%v_r, ',', sp%xi, ',',  rho_tot   
#else
      mtot = sp%m_w
      vt   = vterm_beard( (z3_4pi*rholi*mtot)**z1_3 )
      d      = 2.0_wp*(z3_4pi*rholi*mtot)**z1_3
      A      = pi/4.0_wp*d**2
! ifndef WDROPS
#endif
      ! WRITE(74,'(5es12.2e3,i10,5es12.2e3,i10,i10,es12.2e3)') mtot, m_rime/mtot, sp%z, d_rime, &
      !   vt, sp%xi, rhor, a_rime, mr_crit, d, A, sp%mm, sp%statusb, m_i
    END DO
  END DO

  CLOSE(74)
END SUBROUTINE write_icefraction

#ifndef WDROPS

!
! write 'riming' region values:
!   m_i, m_r, N, M
! at different height levels
!
SUBROUTINE write_riming_2dprofile( cont, tstep, grid )
  TYPE(t_cont), TARGET, INTENT(in) :: cont
  INTEGER,   INTENT(in) :: tstep
  TYPE(t_grid), INTENT(in) :: grid

  INTEGER, PARAMETER :: nz = 3  ! number of horizontal cross-sections
  TYPE(t_sp), POINTER :: sp
  REAL(wp) :: z, diff, sigmazi, logdiff, &
              xie, mxie, sigmai, sxi, smxi, mrce, &
              xie_mm1, mxie_mm1, xie_unr, mxie_unr
  REAL(wp), DIMENSION(n_out2d) :: mi, mr
  REAL(wp), DIMENSION(nz,n_out2d,n_out2d) :: xi, mxi, mrc, &
    xi_mm1, mxi_mm1, xi_unr, mxi_unr
  REAL(wp), PARAMETER :: &
    dmi = 1e12_wp**(1._wp/REAL(n_out2d-1,wp)), &
    dmr = 1e18_wp**(1._wp/REAL(n_out2d-1,wp))
  INTEGER :: isp, ic, mii, mri, iz, icmin, icmax, ns, startPos, iz_start
  CHARACTER(len=50) :: fname
  LOGICAL :: file_exists

  mi(1) = 1e-14_wp
  mr(1) = 1e-21_wp

  DO mii = 2, n_out2d
    mi(mii) = mi(mii-1) * dmi
    mr(mii) = mr(mii-1) * dmr
  END DO

  iz_start = CEILING( (grid%dom_bottom + 10_wp) / h2 * REAL(nz,wp) )

  !$OMP PARALLEL DO DEFAULT(none) &
  !$OMP SHARED(iz_start,h2,grid, cont,sigma0,mi,mr, &
  !$OMP   xi,mxi,xi_mm1,mxi_mm1,xi_unr,mxi_unr,mrc) &
  !$OMP PRIVATE(z,fname,file_exists,mii,mri,xie,mxie,&
  !$OMP   ns,startPos,sigmai,sigmazi,isp,sp,logdiff,diff,ic,icmin,&
  !$OMP   xie_mm1,xie_unr,mxie_mm1,mxie_unr,icmax,mrce) &
  !$OMP SCHEDULE(STATIC,1)
  DO iz = iz_start, nz
    z = iz * h2 / REAL(nz,wp)
    icmin = MIN( coord2index(z - 10_wp*grid%dz), coord2index(z + 10_wp*grid%dz) )
    icmax = MAX( coord2index(z - 10_wp*grid%dz), coord2index(z + 10_wp*grid%dz) )
    DO mii = 1, n_out2d
      DO mri = 1, n_out2d
        xie = 0._wp
        mxie = 0._wp
        mrce = 0._wp
        xie_mm1 = 0._wp
        mxie_mm1 = 0._wp
        xie_unr = 0._wp
        mxie_unr = 0._wp

        DO ic = icmin, icmax
          ns = cont%cellSParray(ic,1,1)%noParts
          startPos = cont%cellSParray(ic,1,1)%startPos
          sigmai = REAL(ns,wp)**0.2_wp / sigma0
          sigmazi = sigma0 * sigmai / (0.1_wp*(grid%dom_top-grid%dom_bottom))

          DO isp = 1, ns
            sp => cont%sp_array(startPos+isp-1)
            IF( sp%cellId < 1 ) CYCLE ! inactive particle
            logdiff = zs1_2pi**3 * sigmai**2 * sigmazi * &
              EXP(-0.5_wp * sigmai**2 * ((LOG(mi(mii))-LOG(MERGE(sp%m_i,1.0_wp,sp%m_i>1e-25_wp)))**2 + &
              (LOG(mr(mri))-LOG(MERGE(sp%m_r,1._wp,sp%m_r>1e-25_wp)))**2))
            diff = logdiff * EXP(-0.5_wp*sigmazi*(z-sp%z)**2 )
            xie = xie + sp%xi * diff
            mxie = mxie + sp%xi * (sp%m_i + sp%m_r) * diff
            IF( sp%mm < 1.1_wp ) THEN
              xie_mm1 = xie_mm1 + sp%xi * diff
              mxie_mm1 = mxie_mm1 + sp%xi * (sp%m_i + sp%m_r) * diff
            ELSE IF( sp%m_r < 0.25_wp*sp%m_i ) THEN
              xie_unr = xie_unr + sp%xi * diff
              mxie_unr = mxie_unr + sp%xi * (sp%m_i + sp%m_r) * diff
            END IF
            IF ( sp%v_r > 1e-20_wp ) &
              mrce = mrce + sp%m_r/sp%v_r*(pi_6*(unr_alf_i*sp%m_i)**z3_bet - sp%m_i*rhoii) * diff
          END DO
        END DO
        xi(iz,mii,mri) = xie
        mxi(iz,mii,mri) = mxie
        xi_mm1(iz,mii,mri) = xie_mm1
        mxi_mm1(iz,mii,mri) = mxie_mm1
        xi_unr(iz,mii,mri) = xie_unr
        mxi_unr(iz,mii,mri) = mxie_unr
        mrc(iz,mii,mri) = mrce
      END DO
    END DO
  END DO
  !$OMP END PARALLEL DO

  ! write 2d-riming files
  DO iz = 1, nz

    ! never write zero data
    sxi = SUM( xi(iz,:,:) )
    smxi = SUM( mxi(iz,:,:) )
    IF ( sxi < 1e-10_wp .AND. smxi < 1e-15_wp ) THEN
      PRINT *, "Riming data negligible. Not writing anything."
      CYCLE
    END IF

    z = iz * (h2 - grid%dom_bottom) / REAL(nz,wp)
    WRITE(fname,'(a,i4.4,a,i6.6,a)') "riming2d-z", INT(z), "-t", &
      FLOOR(REAL(tstep,wp)*dtc), ".dat"
    WRITE(*,'(a,a)') "Writing ", TRIM(fname)
    INQUIRE( FILE=fname, EXIST=file_exists )
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF

    WRITE(74,*) ""
    WRITE(74,*) ""
    WRITE(74,"(a,f10.1,a)") "#""", time_end/60, " min"""

    DO mii = 1, n_out2d
      DO mri = 1, n_out2d
        WRITE(74,'(9es12.2e3)') mi(mii), mr(mri), xi(iz,mii,mri), &
          mxi(iz,mii,mri), mrc(iz,mii,mri), xi_mm1(iz,mii,mri), &
          mxi_mm1(iz,mii,mri), xi_unr(iz,mii,mri), mxi_unr(iz,mii,mri)
      END DO
      WRITE(74,*) ""
    END DO
    CLOSE(74)

  END DO

END SUBROUTINE write_riming_2dprofile

! ifndef WDROPS
#endif


! 
! Analytical solution for mass density function
! for the Golovin kernel
!
SUBROUTINE write_ana_mass_dens( tstep )
  INTEGER, INTENT(in) :: tstep

  INTEGER :: ir
  REAL(wp) :: rad, s, tau, lbt
  CHARACTER(len=*), PARAMETER :: fname = "ana_mass_dens.dat"
  LOGICAL :: file_exists

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname, status="new", action="write")
  END IF

  WRITE(74,*) ""
  WRITE(74,"(a,i6,a,f6.1,a)") "#tstep, time", tstep, ", ", tstep*dtc, "s"

  ! compute mass density distribution for various radii
  rad = 1e-6_wp ! sample start at 1 um of hydrometeor's radius

  IF ( tstep == 0 ) THEN
    DO ir = 1, n_out1d
      s = rad**3/(z3_4pi*rholi*mean_mass0)
      lbt = lwc0*1.5_wp*tstep*dtc
      tau = 1._wp - EXP(-lbt)
      rmasspens(ir) = 3._wp*lwc0 * s**2 * EXP(-s)
      WRITE(74,*) rad, rmasspens(ir)
      rad = rad*dres
    END DO
  ELSE
    DO ir = 1, n_out1d
      s = rad**3/(z3_4pi*rholi*mean_mass0)
      lbt = lwc0*1.5_wp*tstep*dtc
      tau = 1._wp - EXP(-lbt)
      IF ( rad < 0.0002_wp ) THEN
        ! exact ana. solution
        rmasspens(ir) = 3._wp*lwc0 * s / SQRT(tau) * (1._wp-tau) * &
          EXP((-1._wp-tau)*s) * bessel_j1(2*SQRT(tau)*s)
      ELSE
        ! use asymptotic ana. solution
        rmasspens(ir) = 1.5*lwc0/SQRT(pi) * SQRT(s) * &
          EXP(-s*(SQRT(tau)-1)**2 - lbt)
      ENDIF
      WRITE(74,'(2es12.2e3)') rad, rmasspens(ir)
      rad = rad*dres
    END DO
  ENDIF

  CLOSE(74)

END SUBROUTINE write_ana_mass_dens

END MODULE mo_outputArray
