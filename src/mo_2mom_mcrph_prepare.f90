!
! Copyright (C) 2004-2024, DWD, MPI-M, DKRZ, KIT, ETH, MeteoSwiss
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!
!===============================================================================!
!
! This was originally part of mo_2mom_mcrph_driver, but ..
!
!  Work-around for Intel 14.0.3 optimizer bug.
!  Host-associated variables are incorrectly propagated at -O2.
!  (SVN Comment by Thomas Jahns in icon-hdcp2-20150604, rev 22867)
!
!===============================================================================!

MODULE mo_2mom_prepare

  USE mo_constants,       ONLY: wp
  USE mo_2mom_mcrph_main, ONLY: particle, particle_lwf, atmosphere
  IMPLICIT NONE
  PUBLIC :: prepare_twomoment, post_twomoment

CONTAINS

  SUBROUTINE prepare_twomoment(atmo, cloud, rain, ice, snow, graupel, hail, &
       rho, rhocorr, rhocld, pres, w, tk, &
       qv, qc, qnc, qr, qnr, qi, qni, qs, qns, qg, qng, qh, qnh, qgl, qhl)

    TYPE(atmosphere), INTENT(inout)   :: atmo
    CLASS(particle),  INTENT(inout)   :: cloud, rain, ice, snow
    CLASS(particle),  INTENT(inout)   :: graupel, hail
    REAL(wp), TARGET, DIMENSION(:, :), INTENT(in) :: &
         rho, rhocorr, rhocld, pres, w, tk
    REAL(wp), DIMENSION(:,:), INTENT(inout) , TARGET :: &
         &               qv, qc, qnc, qr, qnr, qi, qni, qs, qns, qg, qng, qh, qnh
    REAL(wp), DIMENSION(:,:), INTENT(INOUT), TARGET, OPTIONAL :: &
         &               qgl,qhl

    ! set pointers
    atmo%w   => w
    atmo%T   => tk
    atmo%p   => pres
    atmo%qv  => qv
    atmo%rho => rho

    cloud%rho_v   => rhocld
    rain%rho_v    => rhocorr
    ice%rho_v     => rhocorr
    graupel%rho_v => rhocorr
    snow%rho_v    => rhocorr
    hail%rho_v    => rhocorr

    cloud%q   => qc
    cloud%n   => qnc
    rain%q    => qr
    rain%n    => qnr
    ice%q     => qi
    ice%n     => qni
    snow%q    => qs
    snow%n    => qns
    graupel%q => qg
    graupel%n => qng
    hail%q    => qh
    hail%n    => qnh

    SELECT TYPE (graupel)
    CLASS IS (particle_lwf) 
       graupel%l => qgl
    END SELECT

    SELECT TYPE (hail)
    CLASS IS (particle_lwf) 
       hail%l    => qhl
    END SELECT

  END SUBROUTINE prepare_twomoment

  SUBROUTINE post_twomoment( atmo, cloud, rain, ice, snow, graupel, hail )
    TYPE(atmosphere), INTENT(inout)   :: atmo
    CLASS(particle), INTENT(inout)    :: cloud, rain, ice, snow
    CLASS(particle), INTENT(inout)    :: graupel, hail

    ! nullify pointers
    atmo%w   => null()
    atmo%T   => null()
    atmo%p   => null()
    atmo%qv  => null()
    atmo%rho => null()

    cloud%rho_v   => null()
    rain%rho_v    => null()
    ice%rho_v     => null()
    graupel%rho_v => null()
    snow%rho_v    => null()
    hail%rho_v    => null()

    cloud%q   => null()
    cloud%n   => null()
    rain%q    => null()
    rain%n    => null()
    ice%q     => null()
    ice%n     => null()
    snow%q    => null()
    snow%n    => null()
    graupel%q => null()
    graupel%n => null()
    hail%q    => null()
    hail%n    => null()

    SELECT TYPE (graupel)
    CLASS IS (particle_lwf) 
       graupel%l => null()
    END SELECT

    SELECT TYPE (hail)
    CLASS IS (particle_lwf) 
       hail%l    => null()
    END SELECT

  END SUBROUTINE post_twomoment

END MODULE mo_2mom_prepare
