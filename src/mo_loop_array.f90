!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_loop_array

#ifdef HAVE_OPENMP
  USE omp_lib
#endif 
  USE mo_constants,         ONLY: wp, i8
  USE mo_sp_types,          ONLY: t_sp, t_list_stats
  USE mo_atmo,              ONLY: t_atmo, get_atmo, diag
  USE mo_grid,              ONLY: grid, coord2index
  USE mo_sedi,              ONLY: sp_sedi
  USE mo_sp_nml,            ONLY: dtc, idtgm, tstep_end, ini_type, msg_level, xi0, nrp0, maxTotalNoSps, &
                                & diffusion_type, hydrobreakup_type, rime_type, melt_type, multiplication_type, bnd_type, &
                                & nucl_type, sp_kernel_id
  USE mo_breakup,           ONLY: breakup_hydro
  USE mo_sp_array,          ONLY: t_patch, t_cont, sp_sanity, gather_stats, mergeSPs
  USE mo_sp_array,          ONLY: addParticle, delParticle, moveSpsToNewCell, allocContainer, deallocContainer 
  USE mo_bnd_cond,          ONLY: t_inflow_bnd_data, bnd_condition_init, bnd_condition, bnd_condition_delete
  USE mo_outputArray,       ONLY: write_solution, init_output
  USE mo_nucleation,        ONLY: nucl_simple, nucl_meyers_a
  USE mo_coll,              ONLY: sp_coll_a
#ifndef WDROPS
  USE mo_coll,              ONLY: sp_coll_a_n2
  USE mo_depo,              ONLY: sp_depo, sp_depo_iteration
  USE mo_melt,              ONLY: sp_melt, add_shedded, sp_meltTemp
  USE mo_rime_cont,         ONLY: sp_rime_cont => sp_rime
  USE mo_rime_stoc,         ONLY: sp_rime_stoc => sp_rime, sp_rime_stoc_new => sp_rime_new
  USE mo_ice_multiplication,ONLY: add_splinter
#else
  USE mo_cond,              ONLY: sp_cond
#endif
  USE mo_mcrph_sb,          ONLY: two_moment_mcrph_init, two_moment_mcrph, two_moment_mcrph_init_mem, &
                                & i2m, twomom_data, two_moment_mcrph_init_values, two_moment_mcrph_end_mem

  IMPLICIT NONE
  PRIVATE
  SAVE

  TYPE t_timer
    INTEGER(i8) :: cur = 0
    REAL(wp)    :: tot = 0.0_wp
  END TYPE t_timer

  PUBLIC :: main_loop_array

CONTAINS

  SUBROUTINE main_loop_array
    TYPE(t_patch) :: p_patch
    TYPE(t_cont), TARGET  :: cont
    INTEGER :: itc, ic, nsp, isp, itg, startPos
    INTEGER :: jk, jc, jb
    INTEGER(i8)           :: n_splinter_tot, multiplication_n_colls
    REAL(wp)              :: m_shed_tot, dtg, q_heat
    LOGICAL               :: l_delnode, checkForHydroBreakup
    TYPE(t_sp),   POINTER :: sp
    TYPE(t_atmo)          :: atmo
    TYPE(t_list_stats)    :: stats           ! statistics on list
    TYPE(t_inflow_bnd_data), POINTER :: bnd_data
    INTEGER(i8)           :: cr, cm
    REAL(wp)              :: clockrate, noThreads, walltime, t1, t2
    TYPE(t_timer)         :: timers(0:16)
    REAL(wp)              :: times_t(16)
    INTEGER(i8)           :: times_c(16)

    ! init flux boundary condition
    CALL bnd_condition_init( grid, bnd_data )

    ! allocate sp_array
    p_patch%nlev = grid%nz
    CALL allocContainer( p_patch, cont, MAX(maxTotalNoSps, NINT(2*nrp0*grid%dom_vol/xi0,i8) ) )

    ! initialize output
    CALL init_output( grid )

    !
    ! distinguish between the box model and the 1d-Model
    !
    IF ( ini_type == 1 ) THEN ! **** BOX MODEL

      ic = coord2index(grid%dom_top)
      jk = ic
      jc = 1
      jb = 1

      ! init list once via boundary condition call
      CALL bnd_condition( addSp, grid, bnd_data, dtc )

      ! resort such that initial/bnd condition particles are active
      CALL moveSpsToNewCell(   cont, stats, grid%dom_bottom, grid%box_area )

      ! print initial mass density function
      CALL write_solution( cont, 0, grid)

      DO itc = 1, tstep_end

        nsp      = cont%cellSParray(jk,jc,jb)%noParts
        startPos = cont%cellSParray(jk,jc,jb)%startPos

        IF( nsp > 0 ) THEN
          ! do cellwise Monte-Carlo as in Shima et al. (2009)
          IF( sp_kernel_id > 0 ) THEN
            CALL sp_coll_a( addSp, cont%cellSParray(jk,jc,jb), cont%sp_array( startPos:(startPos+nsp-1) ), grid%dboxi*dtc )
          END IF

          CALL mergeSPs( cont%cellSParray(jk,jc,jb), cont%sp_array( startPos:(startPos+nsp-1) ) )
        END IF

        ! iterate particle list
        DO isp = 1, nsp ! loop particles in cell
          sp => cont%sp_array(startPos+isp-1)
          IF( sp%cellId < 1 ) CYCLE ! inactive particle, e.g. xi=0 in coll
          checkForHydroBreakup = (hydrobreakup_type > 0)
#ifndef WDROPS
          IF( (sp%m_i+sp%m_f+sp%m_r) > 0.0_wp ) checkForHydroBreakup = .FALSE.
#endif
          ! hydro break-up
          IF( checkForHydroBreakup ) CALL breakup_hydro(sp, dtc)
        END DO         ! loop particles in cell end

        CALL moveSpsToNewCell(   cont, stats, grid%dom_bottom, grid%box_area )

        ! print mass density function
        CALL write_solution( cont, itc, grid )

        if (mod(itc,20).eq.0) then
          CALL gather_stats( cont, cont%sp_array, stats, grid%dom_vol )
          WRITE (*,"(i8,a,f9.1,a,es15.7,a,es11.3,a,es11.3,a,i12)") itc,",  t_c [s]:",dtc*itc,&
                & ",  mass [kg]: ",stats%tot_mass,",  number [1/m3]: ",stats%nrp,",  prec [mm/h]: ",stats%tot_prec*3600,&
                & ", #SP:", nsp
        end if

      END DO

      ! final statistics on droplets
      IF( msg_level > 2 ) THEN
        PRINT *, ""
        CALL gather_stats( cont, cont%sp_array, stats, grid%dom_vol )
        WRITE(*,'(a,es15.1)') "mean RP mass           [kg]: ", stats%mean_mass
        WRITE(*,'(a,f 15.1)') "tot. RP mass in sys    [kg]: ", stats%tot_mass_sys
        WRITE(*,'(a,es15.1)') "tot. RP mass        [kg/m3]: ", stats%tot_mass
        WRITE(*,'(a,f 15.1)') "num. SPs             [1/m3]: ", stats%nsp
        WRITE(*,'(a,f 15.1)') "num. RPs             [1/m3]: ", stats%nrp
      END IF

    ELSE ! ini_type ! **** 1D-MODE

      ! print initial mass density function
      CALL write_solution( cont, 0, grid )

      CALL two_moment_mcrph_init(igscp=4, msg_level=16)
      CALL two_moment_mcrph_init_mem(grid)

      CALL system_clock(count_rate=cr)
      CALL system_clock(count_max=cm)
      clockrate = REAL(cr,wp)

      CALL tic(timers(0)%cur)
      CALL CPU_TIME(t1)
      DO itc = 1, tstep_end

        ! set diagnostic arrays to zero
        diag%rime_rate = 0.0_wp
        diag%dep_rate  = 0.0_wp
        diag%dep_mono  = 0.0_wp
        diag%heat_rate = 0.0_wp

        times_t(:) = 0.0_wp
        ! run over all cells
        !$OMP PARALLEL DO DEFAULT(none) &
        !$OMP SHARED(grid,cont,stats,diag,idtgm,dtc, nucl_type, sp_kernel_id, clockrate,&
        !$OMP        hydrobreakup_type,diffusion_type,rime_type,melt_type, multiplication_type) &
        !$OMP PRIVATE(jk,jc,jb,nsp,startPos,n_splinter_tot,multiplication_n_colls,isp,sp,q_heat, &
        !$OMP         atmo,checkForHydroBreakup,dtg,itg,l_delnode,m_shed_tot,times_c) &
        !$OMP REDUCTION(+:times_t) &
        !$OMP SCHEDULE(static,1)
        DO ic = 1, grid%nz
          jk = ic
          jc = 1
          jb = 1

          nsp      = cont%cellSParray(jk,jc,jb)%noParts
          startPos = cont%cellSParray(jk,jc,jb)%startPos

          IF( nsp > 0 ) THEN
#ifndef NDEBUG
            ! check if the list is sane
            CALL tic(times_c(1))
            IF( .NOT. sp_sanity( cont%sp_array( startPos:(startPos+nsp-1) ), "new timestep" ) ) STOP "SP list is not sane."
            CALL toc(times_c(1),clockrate,times_t(1))
#endif

            IF( sp_kernel_id > 0 ) THEN
                CALL tic(times_c(2))
#ifndef WDROPS
                IF( sp_kernel_id == 22 ) THEN   ! do cellwise Monte-Carlo as in Unterstrasser et al. 
                  CALL sp_coll_a_n2( addSp, cont%cellSParray(jk,jc,jb), cont%sp_array( startPos:(startPos+nsp-1) ), &
                                   & grid%dboxi*dtc )
                ELSE                            ! do cellwise Monte-Carlo as in Shima et al. (2009)
#endif
                  CALL sp_coll_a(    addSp, cont%cellSParray(jk,jc,jb), cont%sp_array( startPos:(startPos+nsp-1) ), & 
                                &    grid%dboxi*dtc )
#ifndef WDROPS
                ENDIF
#endif
                CALL toc(times_c(2),clockrate,times_t(2))
            END IF

            ! merge similiar-sized pure ice or water particles to reduce the number of super-particles
            CALL tic(times_c(3))
            CALL mergeSPs( cont%cellSParray(jk,jc,jb), cont%sp_array( startPos:(startPos+nsp-1) ) )
            CALL toc(times_c(3),clockrate,times_t(3))
          END IF

          n_splinter_tot = 0_i8
          m_shed_tot = 0.0_wp
          multiplication_n_colls = 0

          partloop: DO isp = 1, nsp ! loop particles in cell
            sp => cont%sp_array(startPos+isp-1)
            q_heat = 0.0_wp
            IF( sp%cellId < 1 ) CYCLE ! inactive particle, e.g. xi=0 in coll
            CALL get_atmo( sp%z, atmo )
            checkForHydroBreakup = (hydrobreakup_type > 0)

            dtg   = dtc / REAL(idtgm,wp)
            DO itg = 1, idtgm ! condensation loop
#ifdef WDROPS
              ! condensation/evaporation
              IF( diffusion_type /= 0 ) THEN
                CALL tic(times_c(4))
                CALL sp_cond( sp, atmo, dtg )
                CALL toc(times_c(4),clockrate,times_t(4))
              END IF
#else
              ! riming
              IF ( rime_type == 1 ) THEN
                CALL tic(times_c(5))
                CALL sp_rime_cont( sp, atmo, dtg, q_heat, multiplication_type, n_splinter_tot, & 
                                 & diag(ic)%rime_rate, diag(ic)%heat_rate )
                CALL toc(times_c(5),clockrate,times_t(5))
              ELSE IF ( rime_type == 2 ) THEN
                CALL tic(times_c(5))
                CALL sp_rime_stoc( sp, atmo, dtg, q_heat, multiplication_type, n_splinter_tot, multiplication_n_colls, &
                                 & diag(ic)%rime_rate, diag(ic)%heat_rate )
                CALL toc(times_c(5),clockrate,times_t(5))
              ELSE IF ( rime_type == 3 ) THEN
                CALL tic(times_c(5))
                CALL sp_rime_stoc_new( sp, atmo, dtg, q_heat, multiplication_type, n_splinter_tot, multiplication_n_colls, &
                                 & diag(ic)%rime_rate, diag(ic)%heat_rate )
                CALL toc(times_c(5),clockrate,times_t(5))
              END IF
  
              ! deposition/sublimation
              IF( diffusion_type > 0 ) THEN
                CALL tic(times_c(4))
                IF( diffusion_type == 1 ) THEN
                  CALL sp_depo( sp, atmo, dtg, q_heat, l_delnode, diag(ic)%dep_rate, diag(ic)%dep_mono, diag(ic)%heat_rate )
                ELSE
                  CALL sp_depo_iteration( sp, atmo, dtg, q_heat, l_delnode, diag(ic)%dep_rate, diag(ic)%dep_mono, &
                                        & diag(ic)%heat_rate )
                ENDIF
                CALL toc(times_c(4),clockrate,times_t(4))
                IF( l_delnode ) THEN
                  CALL delParticle( cont%cellSParray(jk,jc,jb), sp )
                  CYCLE partloop
                ENDIF
              ENDIF

              ! melting
              IF( diffusion_type > 1 ) THEN
                CALL tic(times_c(6))
                CALL sp_meltTemp( sp, atmo, dtg, q_heat, m_shed_tot, diag(ic)%heat_rate )
                CALL toc(times_c(6),clockrate,times_t(6))
              ELSE IF ( melt_type /= 0 ) THEN
                CALL tic(times_c(6))
                CALL sp_melt(     sp, atmo, dtg, q_heat, m_shed_tot, diag(ic)%heat_rate )
                CALL toc(times_c(6),clockrate,times_t(6))
              END IF

              IF( (sp%m_i+sp%m_f+sp%m_r) > 0.0_wp ) checkForHydroBreakup = .FALSE.
#endif
              ! hydro break-up
              IF( checkForHydroBreakup ) THEN 
                CALL tic(times_c(7))
                CALL breakup_hydro(sp, dtg)
                CALL toc(times_c(7),clockrate,times_t(7))
              END IF
            END DO ! conddepo_loop end

            ! motion of SPs ! TODO ensure that only adding is done after sedi as it breaks the cell order
            CALL tic(times_c(8))
            CALL sp_sedi( sp, atmo, dtc )
            CALL toc(times_c(8),clockrate,times_t(8))

          END DO partloop ! loop particles in cell end

          ! create new super-ice for ice splinters in the cell
#ifndef WDROPS
          IF( n_splinter_tot > 0_i8 ) THEN
            CALL tic(times_c(9))
            CALL add_splinter( addSp, grid%z_f(ic), grid%dz, n_splinter_tot )
            CALL toc(times_c(9),clockrate,times_t(9))
          END IF

          IF( m_shed_tot > 0.0_wp ) THEN
            CALL tic(times_c(10))
            CALL add_shedded( addSp, grid%z_f(ic), grid%dz, m_shed_tot )
            CALL toc(times_c(10),clockrate,times_t(10))
          END IF
#endif

          ! meyers nucleation
          IF (nucl_type == 2) THEN
            CALL tic(times_c(11))
            CALL nucl_meyers_a( addSp, grid%z_f(ic), grid%dz, grid%dbox, cont%sp_array(startPos:(startPos+nsp-1)) )
            CALL toc(times_c(11),clockrate,times_t(11))
          END IF

        END DO ! loop over all cells
        !$OMP END PARALLEL DO
        DO ic=1,SIZE(times_t)
            timers(ic)%tot = timers(ic)%tot + times_t(ic)
        END DO

        ! simple nucleation
        IF (nucl_type == 1) THEN
            CALL tic(timers(11)%cur)
            CALL nucl_simple( addSp, grid%box_area, dtc ) 
            CALL toc(timers(11)%cur,clockrate,timers(11)%tot)
        END IF

        ! sedimentation in the BND zone for 1d models
        IF (bnd_type > 0) THEN 
            CALL tic(timers(12)%cur) 
            CALL bnd_condition( addSp, grid, bnd_data, dtc )
            CALL toc(timers(12)%cur,clockrate,timers(12)%tot)
        END IF

        ! move super-particles to new cells
        CALL tic(timers(13)%cur)
        CALL moveSpsToNewCell(   cont, stats, grid%dom_bottom, grid%box_area )
        CALL toc(timers(13)%cur,clockrate,timers(13)%tot)

        IF( MOD( itc-1, tstep_end/100 ) == 0 ) THEN
            CALL tic(timers(14)%cur)
            WRITE(*,*) (itc-1)/(tstep_end/100), "% TimeStep: ", itc, "   #SP = ", cont%endOfSParray
            CALL gather_stats( cont, cont%sp_array, stats, grid%dom_vol )
            WRITE(*,'(a,es15.1)') "mean RP mass           [kg]: ", stats%mean_mass
            WRITE(*,'(a,f 15.1)') "tot. RP mass in sys    [kg]: ", stats%tot_mass_sys
            WRITE(*,'(a,es15.1)') "tot. RP mass        [kg/m3]: ", stats%tot_mass
            WRITE(*,'(a,f 15.1)') "num. SPs             [1/m3]: ", stats%nsp
            WRITE(*,'(a,f 15.1)') "num. RPs             [1/m3]: ", stats%nrp
            WRITE(*,'(a,es15.1)') "precipitation        [1/m2]: ", stats%nsedi
            WRITE(*,'(a,es15.1)') "precipitation mass  [kg/m2]: ", stats%msedi
            CALL toc(timers(14)%cur,clockrate,timers(14)%tot)
        ENDIF

        CALL tic(timers(16)%cur)
        CALL two_moment_mcrph_init_values( grid, bnd_data%number_flux, bnd_data%mass_flux )
        CALL two_moment_mcrph(                                    &
                        isize  = 1,                              &!in: array size
                        ke     = grid%nz,                        &!in: end level/array size
                        is     = 1,                              &!in: start index
                        ie     = 1,                              &!in: end index
                        ks     = 1,                              &!in: start level
                        dt     = dtc,                            &!in: time step
                        dz     = twomom_data%d(:,:,i2m%idz),     &!in: vertical layer thickness
                        rho    = twomom_data%d(:,:,i2m%irho),    &!in:  density
                        pres   = twomom_data%d(:,:,i2m%ipres),   &!in:  pressure
                        qv     = twomom_data%d(:,:,i2m%iqv),     &!inout:sp humidity
                        qc     = twomom_data%d(:,:,i2m%iqc),     &!inout:cloud water
                        qnc    = twomom_data%d(:,:,i2m%iqnc),    &!inout: cloud droplet number 
                        qr     = twomom_data%d(:,:,i2m%iqr),     &!inout:rain
                        qnr    = twomom_data%d(:,:,i2m%iqnr),    &!inout:rain droplet number 
                        qi     = twomom_data%d(:,:,i2m%iqi),     &!inout: ice
                        qni    = twomom_data%d(:,:,i2m%iqni),    &!inout: cloud ice number
                        qs     = twomom_data%d(:,:,i2m%iqs),     &!inout: snow 
                        qns    = twomom_data%d(:,:,i2m%iqns),    &!inout: snow number
                        qg     = twomom_data%d(:,:,i2m%iqg),     &!inout: graupel 
                        qng    = twomom_data%d(:,:,i2m%iqng),    &!inout: graupel number
                        qh     = twomom_data%d(:,:,i2m%iqh),     &!inout: hail 
                        qnh    = twomom_data%d(:,:,i2m%iqnh),    &!inout: hail number
                        ninact = twomom_data%d(:,:,i2m%ininact), &!inout: IN number
                        tk     = twomom_data%d(:,:,i2m%it),      &!inout: temp 
                        w      = twomom_data%d(:,:,i2m%iw),      &!inout: w
                        prec_nr = twomom_data%d(1,:,i2m%ifnr),   &!inout precp rate rain
                        prec_ni = twomom_data%d(1,:,i2m%ifni),   &!inout precp rate ice
                        prec_ns = twomom_data%d(1,:,i2m%ifns),   &!inout precp rate snow
                        prec_ng = twomom_data%d(1,:,i2m%ifng),   &!inout precp rate graupel
                        prec_nh = twomom_data%d(1,:,i2m%ifnh),   &!inout precp rate hail
                        prec_r = twomom_data%d(1,:,i2m%ifr),     &!inout precp rate rain
                        prec_i = twomom_data%d(1,:,i2m%ifi),     &!inout precp rate ice
                        prec_s = twomom_data%d(1,:,i2m%ifs),     &!inout precp rate snow
                        prec_g = twomom_data%d(1,:,i2m%ifg),     &!inout precp rate graupel
                        prec_h = twomom_data%d(1,:,i2m%ifh),     &!inout precp rate hail
                        msg_level = 16                ,    &
                        l_cv=.TRUE.          )
        CALL toc(timers(16)%cur,clockrate,timers(16)%tot)

        ! print mass density function
        CALL tic(timers(15)%cur)
        CALL write_solution( cont, itc, grid, twomom_data%d )
        CALL toc(timers(15)%cur,clockrate,timers(15)%tot)

      END DO ! end time step loop


      ! final statistics on droplets 
      PRINT *, ""
      CALL gather_stats( cont, cont%sp_array, stats, grid%dom_vol )
      WRITE(*,'(a,es15.1)') "mean RP mass           [kg]: ", stats%mean_mass
      WRITE(*,'(a,f 15.1)') "tot. RP mass in sys    [kg]: ", stats%tot_mass_sys
      WRITE(*,'(a,es15.1)') "tot. RP mass        [kg/m3]: ", stats%tot_mass
      WRITE(*,'(a,f 15.1)') "num. SPs             [1/m3]: ", stats%nsp
      WRITE(*,'(a,f 15.1)') "num. RPs             [1/m3]: ", stats%nrp
      WRITE(*,'(a,es15.1)') "precipitation         [1/m2]: ", stats%nsedi
      WRITE(*,'(a,es15.1)') "precipitation mass   [kg/m2]: ", stats%msedi

      CALL toc( timers(0)%cur, clockrate, timers(0)%tot )
      CALL CPU_TIME(t2)
      walltime = t2-t1
#ifdef HAVE_OPENMP
      noThreads = omp_get_max_threads()
#else
      noThreads = 1.0_wp
#endif
        
      WRITE(*,*) "system_clock: ", timers(0)%tot, walltime
      WRITE(*,*) "sanity      : ", timers(1)%tot, NINT(timers(1)%tot/timers(0)%tot*100/noThreads), NINT(timers(1)%tot/walltime*100)
      WRITE(*,*) "coll        : ", timers(2)%tot, NINT(timers(2)%tot/timers(0)%tot*100/noThreads), NINT(timers(2)%tot/walltime*100)
      WRITE(*,*) "merge       : ", timers(3)%tot, NINT(timers(3)%tot/timers(0)%tot*100/noThreads), NINT(timers(3)%tot/walltime*100)
      WRITE(*,*) "diffusion   : ", timers(4)%tot, NINT(timers(4)%tot/timers(0)%tot*100/noThreads), NINT(timers(4)%tot/walltime*100)
      WRITE(*,*) "riming      : ", timers(5)%tot, NINT(timers(5)%tot/timers(0)%tot*100/noThreads), NINT(timers(5)%tot/walltime*100)
      WRITE(*,*) "melting     : ", timers(6)%tot, NINT(timers(6)%tot/timers(0)%tot*100/noThreads), NINT(timers(6)%tot/walltime*100)
      WRITE(*,*) "hydro_break : ", timers(7)%tot, NINT(timers(7)%tot/timers(0)%tot*100/noThreads), NINT(timers(7)%tot/walltime*100)
      WRITE(*,*) "sedi        : ", timers(8)%tot, NINT(timers(8)%tot/timers(0)%tot*100/noThreads), NINT(timers(8)%tot/walltime*100)
      WRITE(*,*) "add_splinter: ", timers(9)%tot, NINT(timers(9)%tot/timers(0)%tot*100/noThreads), NINT(timers(9)%tot/walltime*100)
    WRITE(*,*) "add_shedded : ", timers(10)%tot, NINT(timers(10)%tot/timers(0)%tot*100/noThreads), NINT(timers(10)%tot/walltime*100)
    WRITE(*,*) "nucleation  : ", timers(11)%tot, NINT(timers(11)%tot/timers(0)%tot*100/noThreads), NINT(timers(11)%tot/walltime*100)
      WRITE(*,*) "bnd_cond    : ", timers(12)%tot, NINT(timers(12)%tot/timers(0)%tot*100), NINT(timers(12)%tot/walltime*100)
      WRITE(*,*) "move+sort   : ", timers(13)%tot, NINT(timers(13)%tot/timers(0)%tot*100), NINT(timers(13)%tot/walltime*100)
      WRITE(*,*) "stats       : ", timers(14)%tot, NINT(timers(14)%tot/timers(0)%tot*100), NINT(timers(14)%tot/walltime*100)
      WRITE(*,*) "output      : ", timers(15)%tot, NINT(timers(15)%tot/timers(0)%tot*100), NINT(timers(15)%tot/walltime*100)
      WRITE(*,*) "2mom        : ", timers(16)%tot, NINT(timers(16)%tot/timers(0)%tot*100), NINT(timers(16)%tot/walltime*100)

      CALL two_moment_mcrph_end_mem()

    END IF ! ini type

    CALL bnd_condition_delete( bnd_data )
    CALL deallocContainer( cont )

  CONTAINS
    
    SUBROUTINE tic( t1 )
      INTEGER(i8), INTENT(out) :: t1
      CALL system_clock(t1)
    END SUBROUTINE tic

    SUBROUTINE toc( t1, clockrate, tot )
      INTEGER(i8), INTENT(in)    :: t1
      REAL(wp),    INTENT(in)    :: clockrate
      REAL(wp),    INTENT(inout) :: tot

      INTEGER(i8) :: t2
      CALL system_clock(t2)
      tot = tot + (t2 - t1)/clockrate
    END SUBROUTINE toc

    SUBROUTINE addSp( sp )
      TYPE(t_sp),   INTENT(in) :: sp
      INTEGER :: jc, jk, jb
      jk = coord2index(sp%z)
      jc = 1
      jb = 1
      !$OMP CRITICAL (addParticle) 
        CALL addParticle( cont, sp, jc, jk, jb ) ! add function is not thread safe as it increments global counter
      !$OMP END CRITICAL (addParticle)
    END SUBROUTINE addSp

  END SUBROUTINE main_loop_array

END MODULE mo_loop_array
