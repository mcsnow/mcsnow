!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_sp_nml

  USE mo_constants,         ONLY: wp, i8

  IMPLICIT NONE

  ! namelist variables
  INTEGER     ::   &
    msg_level,     &
    out_type,      &  ! 1: ASCII, 2: NetCDF
    tcase_id,      &  ! unique test case id
    ini_type,      &  ! test type 
    bnd_type,      &  ! type of upper boundary condition (0) off (1) constant xi0 (2) xi from cdf (3) discrete particles
    rime_type,     &  ! rime type: 1) continuous model, 2) stochastic model
    nucl_type,     &  ! nucleation: 0) off 1) constant rate 2) Meyers
    melt_type,     &  ! melting process: 0) off 1) on 
    shedding_type, &  ! shedding process: 0) off 1) on
    diffusion_type,&  ! diffusion process: 0) off 1) on 2) solve for particle temperature
    hydrobreakup_type, &   ! hydrodynamic break-up of water drops: 0) off 1) on
    collbreakup_type,  &   ! collision induced break-up of water drops: 0) off 1) straub 2) mcfarquhar 3) simple
    multiplication_type, & ! ice multiplication 0) off 1) rime splintering
    sp_kernel_id,  &  ! 1:Golovin, 2:hydrodynamic, 22: hydrodynamic n2 coll
    sp_stick_effi, &  ! 0:=1, 1:Pruppacher and Klett, p. 691, 2: Fig. 14 of Connolly et al. ACP 2012, 3: upper limit of 2, 4: Karrer et al (2021), 8: Phillips CKE Scheme
    sp_vterm_id,   &  ! 1:Heymsfield, 2:Khvorostyanov, 3: Boehm
    idtgm,         &  ! [1] dtg = dtc/idtgm
    agg_hist,      &  ! <0: no history, 0: history with near surface sampling, >0: history with the whole domain sampling
    snowgeo_type,  &  ! 1: bulk, 2: fillin, 3: similarity
    agggeo_type,   &  ! 1: M96 mixed agg, 2: MK96 plates + M96 mixed agg, 3: monomer-number dependent fit to plates
    sp_habit,      &  ! 0: fixed m-D-relations, 1: habit prediction only for monomers, 2: habit prediction for all particles
    sp_frag,       &  ! 0: off, 1: Phillips Fragmentation, 2: updated Fragmentation formulation
    coll_kern_type    ! 0: A-kernel, 1: D-kernel
    
  INTEGER(i8) ::   &
    xi0,           &  ! [1] number of RPs per SP
    nrp0,          &  ! [RP/m3] number of real ice particles (RPs)
    maxTotalNoSps     ! [1] in array version allocate space for this number of super-particles, use list version for value < 0
  REAL(wp)    ::   &
    time_end,      &  ! [s] end time
    mergeNoTresh,  &  ! number of super-particles per cell, if threshold is exceeded, similar particle get merged
    mergeRatioLimit, &! mass ratio below which two similar pure ice or pure water drops get merged, below 1.0 it is switched off
    iwc,           &  ! [kg/m3] ice water content
    mu_gam,        &  ! [1] exponent of the initial/boundary generalized gamma distribution A*m**nu_gam*exp(-lambda*m**mu_gam)
    nu_gam,        &  ! [1] exponent of the initial/boundary generalized gamma distribution A*m**nu_gam*exp(-lambda*m**mu_gam)
    lwc0,          &  ! [kg/m3] LWC parameter for idealized atmosphere with riming height-belt
    rm,            &  ! [m] mean w. droplet size in riming of the 1d-model
    dtc,           &  ! [s] time step of collision
    dt_1dprof,     &  ! [s] time interval for writing 1d profiles
    dt_2dprof,     &  ! [s] time interval for writing 2d riming profiles
    sigma0,        &  ! kernel density estimation factor (see Shima, p.1313)
    nclrate,       &  ! [SP/m3s] num. new SPs in the 'nucleation zone'
    nclmass,       &  ! [kg] mean mass of particle after nucleation
    sp_kern_sig       ! added velocity variance
  LOGICAL     ::   &
    has_asol,      &  ! does the test case a reference solution
    perf_test,     &  ! switch off output, running a performance test
    interpol_atmos,&  ! interpolation of thermodynamic variables on particle position
    prolate_intp      ! switch on interpolation between prolate and cylinder for fall speed

  ! dependable variables
  INTEGER     ::   &
    tstep_end      
  REAL(wp)    ::   &
    mean_mass0,    &  ! [kg] initial mean mass of droplets
    Vm                ! [m/s] velocity of avg. particle in the riming yone

  PUBLIC :: msg_level, out_type, tcase_id, ini_type, bnd_type, rime_type, nucl_type, melt_type, diffusion_type, &
            xi0, sp_kernel_id, sp_stick_effi, sp_vterm_id, dt_1dprof, dt_2dprof, nrp0, multiplication_type, &
            time_end, iwc, nu_gam, mu_gam, lwc0, dtc, mergeRatioLimit, mergeNoTresh, &
            idtgm, rm, Vm, maxTotalNoSps, &
            read_sp_nml, print_sp_nml, tstep_end, sigma0, nclrate, nclmass, has_asol, interpol_atmos, &
            shedding_type, collbreakup_type, hydrobreakup_type, sp_habit, sp_frag, coll_kern_type, sp_kern_sig, &
            prolate_intp

CONTAINS          


!
! read from input.nml
!
SUBROUTINE read_sp_nml()

  NAMELIST /general_nml/ msg_level, out_type, time_end, tcase_id, &
  ini_type, bnd_type, rime_type, nucl_type, dt_1dprof, dt_2dprof, &
  snowgeo_type,agggeo_type, has_asol, perf_test, multiplication_type, &
  melt_type, diffusion_type, mergeRatioLimit, mergeNoTresh, &
  shedding_type, collbreakup_type, hydrobreakup_type, interpol_atmos

  NAMELIST /sp_nml/ iwc, nu_gam, mu_gam, lwc0, nrp0, xi0, maxTotalNoSps, dtc, idtgm,              &
  sp_kernel_id, sp_stick_effi, sp_vterm_id, sigma0, nclrate, nclmass, rm, agg_hist, sp_habit,     &
  sp_frag, coll_kern_type, sp_kern_sig, prolate_intp

  ! default settings for general_nml
  msg_level      = 10
  out_type       = 1
  time_end       = 1._wp     ! [s]
  mergeRatioLimit= 0.0_wp
  mergeNoTresh   = 1*1000*1000
  ini_type       = 1
  bnd_type       = 1         ! default: constant influx given by iwc and nrp0
  rime_type      = 2         ! default: stochastic model
  nucl_type      = 0         ! default: off
  melt_type      = 1         ! default: on
  diffusion_type = 1
  multiplication_type = 0
  shedding_type  = 1
  collbreakup_type = 3
  hydrobreakup_type = 1
  dt_1dprof      = 1200._wp
  dt_2dprof      = 1200._wp
  has_asol       = .FALSE.
  perf_test      = .FALSE.
  interpol_atmos = .TRUE.
  prolate_intp   = .FALSE.
  snowgeo_type   = 2         ! fill-in m-D relation
  agggeo_type    = 1         ! geometry of aggregates
  coll_kern_type = 0         ! Area kernel

  ! default settings for SP_nml
  iwc            = 1.e-4_wp    ! [kg]
  nu_gam         = 1           ! [1]
  mu_gam         = 1           ! [1]
  lwc0           = 1.e-3_wp    ! [kg]
  nrp0           = 8388608_i8  ! [RP/m3]
  xi0            = 1024_i8
  maxTotalNoSps  = 10000_i8
  dtc            = 1.0_wp      ! [s]
  idtgm          = 1           ! [1]
  sp_kernel_id   = 2
  sp_stick_effi  = 1
  sp_vterm_id    = 2
  sp_habit       = 0           ! m-D-relations 
  sp_frag        = 0
  sigma0         = 0.62_wp     ! Shima 2009, Sec. 5.1.4
  sp_kern_sig    = 0.0_wp
  nclrate        = 0
  nclmass        = 1e-11_wp
  rm             = 1e-5_wp
  agg_hist       = -1

  OPEN(UNIT=74, FILE="input.nml") !, STATUS=istat)
  READ(74, general_nml)

  ! prevent wrong "ice-particle" code running droplets scrpts
  ! and vice versa
#ifndef WDROPS
  IF( tcase_id < 100 .AND. tcase_id > 0 ) THEN
    STOP "tcase_id can not run with this code. Try:  ~SPM> make clean; make wd_release"
#else
  IF( tcase_id > 99 ) THEN
    STOP "tcase_id can not run with this code. Try ~SPM> make clean; make release"
#endif
  END IF

  ! ... now, read sp_nml
  READ(74, sp_nml)
  CLOSE(74)

  IF( (agg_hist >= 0) .AND. (maxTotalNoSps > 0) ) THEN
    STOP "Aggregation history is not implemented for array version, set maxTotalNoSps < 0"
  END IF

  IF( (agg_hist >= 0) .AND. (mergeRatioLimit > 1.0_wp) ) &
    STOP "SIP merging (mergeRatioLimit > 1.0) will break the aggregation history (agg_hist >= 0)" 

  ! fix some namelist variables
  dt_1dprof = MAX( dt_1dprof, dtc )
  dt_2dprof = MAX( dt_2dprof, dtc )

  ! dependable variable
  !Vm = vterm_beard( rm )
  tstep_end = CEILING(time_end/REAL(dtc,wp))
#ifndef WDROPS
  mean_mass0 = iwc / nrp0  ! [kg]
#else  
  mean_mass0 = lwc0 / nrp0  ! [kg]
#endif
END SUBROUTINE read_sp_nml


!
! print namelist parameters
! note: this subroutine should be called after init_part_geom so that derived variable are computed
! added 9 May 2017, S. Brdar (JSC)
SUBROUTINE print_sp_nml()

  IF( msg_level <= 4 ) RETURN

  ! print non-derived parameters
  PRINT *, ""
#ifndef WDROPS
  PRINT *, "IWC             [kg/m3]: ", iwc
  PRINT *, "snowgeo_type         []: ", snowgeo_type
  PRINT *, "agggeo_type          []: ", agggeo_type
#endif
  PRINT *, "nu_gamm             [1]: ", nu_gam
  PRINT *, "nu_gamm             [1]: ", mu_gam
  PRINT *, "LWC             [kg/m3]: ", lwc0    ! wrong, we have 1d profile of lwc now
  PRINT *, "sp_nml::nrp0     [1/m3]: ", nrp0
  PRINT *, "sp_nml::xi0         [1]: ", xi0
  PRINT *, "sp_nml::maxTotalNoSps  : ", maxTotalNoSps
  PRINT *, "sp_nml::time_end    [s]: ", time_end
  PRINT *, "sp_nml::mergeNoTresh   : ", mergeNoTresh
  PRINT *, "sp_nml::mergeRatioLimit: ", mergeRatioLimit
  PRINT *, "sp_nml::tcase_id       : ", tcase_id
  PRINT *, "sp_nml::ini_type       : ", ini_type
  PRINT *, "sp_nml::bnd_type       : ", bnd_type
  PRINT *, "sp_nml::nucl_type      : ", nucl_type
  PRINT *, "sp_nml::rime_type      : ", rime_type
  PRINT *, "sp_nml::melt_type      : ", melt_type
  PRINT *, "sp_nml::shedding_type  : ", shedding_type
  PRINT *, "sp_nml::diffusion_type : ", diffusion_type
  PRINT *, "sp_nml::multiplication : ", multiplication_type
  PRINT *, "sp_nml::hydrobreakup   : ", hydrobreakup_type
  PRINT *, "sp_nml::collbreakup    : ", collbreakup_type
  PRINT *, "sp_nml::collkernel     : ", coll_kern_type
  PRINT *, "sp_nml::stick_effi     : ", sp_stick_effi
  PRINT *, "sp_nml::habit_predict  : ", sp_habit
  PRINT *, "sp_nml::fragmentation  : ", sp_frag
  PRINT *, "sp_nml::dt_1dprof   [s]: ", dt_1dprof
  PRINT *, "sp_nml::dt_2dprof   [s]: ", dt_2dprof
  PRINT *, "sp_nml::msg_level      : ", msg_level
  PRINT *, "sp_nml::sigma0      [?]: ", sigma0
  PRINT *, "sp_nml::sp_kern_sig    : ", sp_kern_sig
  PRINT *, "sp_nml::has_asol       : ", has_asol
  PRINT *, "sp_nml::perf_test      : ", perf_test
  PRINT *, "sp_nml::interpol_atmos : ", interpol_atmos
  PRINT *, "sp_nml::prolate_intp   : ", prolate_intp
  PRINT *, "sp_nml::agg_hist       : ", agg_hist
  PRINT *, "sp_nml::rm          [m]: ", rm
  PRINT *, "sp_nml::nclrate[SP/sm3]: ", nclrate
  PRINT *, "sp_nml::nclmass    [kg]: ", nclmass
  ! print derived parameters
  PRINT *, ""
  PRINT *, "------ derived variables ----------"
  PRINT *, "sp_nml::tstep_end      : ", tstep_end
  PRINT *, "sp_nml::mean_mass  [kg]: ", mean_mass0
  PRINT *, "sp_nml::Vm        [m/s]: ", Vm

  IF( (msg_level>0).AND.(idtgm/=1) ) THEN
    PRINT *, "WARNING dtc = dtg not fullfilled."
  END IF

  IF( nucl_type > 2 .OR. nucl_type < 0) THEN
    STOP "nucl_type invalid"
  END IF

  IF( ABS(rime_type-1)>2 ) THEN
    STOP "rime_type invalid"
  ELSE IF ( rime_type == 0 ) THEN
    PRINT *, "Warning: Riming disabled"
  END IF
  
  IF( (multiplication_type < 0) .OR. (multiplication_type > 1 ) ) THEN
    STOP "multiplication_type invalid"
  END IF

  IF ( out_type/=1 .AND. out_type/=2 ) THEN
    STOP "mo_sp_nml: out_type not 1 or 2."
  END IF

  IF( snowgeo_type .eq. 1 ) THEN
    STOP "snowgeo_type = 1 not supported anymore, if you really need it, use an older McSnow version"
  END IF

  IF( out_type==2 ) THEN
#ifndef HAVE_NETCDF
    PRINT *, "WARNING: NetCDF not found. Setting out_type to 1."
    out_type = 1
#endif
  END IF
END SUBROUTINE print_sp_nml


END MODULE mo_sp_nml
