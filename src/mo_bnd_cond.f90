!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_bnd_cond

  USE mo_constants,    ONLY: wp, i8, pi, z1_3
  USE mo_grid,         ONLY: t_grid
  USE mo_sp_types,     ONLY: t_sp, t_node, t_list, sp_stat, p_addSp
  USE mo_sp_nml,       ONLY: agg_hist, nrp0, mean_mass0, xi0, nu_gam, mu_gam, ini_type, bnd_type
  USE mo_atmo,         ONLY: t_atmo, get_atmo
  USE mo_atmo_types,   ONLY: rhoi, m2r, rhol, rhoii
#ifndef WDROPS
  USE mo_mass2diam,    ONLY: m2d_particle, unr_alf, unr_bet, iceGeo_init 
  USE mo_velocity,     ONLY: vterm_snow
#else
  USE mo_velocity,     ONLY: vterm_beard
#endif
  USE mo_2mom_mcrph_types,     ONLY: particle, particle_frozen, particle_frozen_nonsphere
  USE mo_2mom_mcrph_main,      ONLY: init_2mom_scheme
  USE mo_2mom_mcrph_processes, ONLY: particle_velocity_nonsphere, particle_velocity
  USE mo_mcrph_sb,             ONLY: lnonsphere_snow_and_ice, rho_vel, rho0

  IMPLICIT NONE
  PRIVATE

  INTEGER, PARAMETER  :: n_samples = 10000
  REAL(wp), PARAMETER :: n_stds    = 50.0_wp

  TYPE t_inflow_bnd_data
    REAL(wp) :: x(0:n_samples), cdf(0:n_samples)
    REAL(wp) :: number_flux
    REAL(wp) :: mass_flux
  END TYPE t_inflow_bnd_data
  
  PUBLIC ::               &
    t_inflow_bnd_data,    &
    bnd_condition_init,   &
    bnd_condition_delete, &
    bnd_condition

CONTAINS


!
! init upper flux boundary condition
! numerically calculates the number and mass flux of 
! a generalized gamma distribution A*x^nu*exp(-lambda*x^mu)
! additionally, the cdf of the boundary flux is stored in bnd_data
! Remark: an analytical Re-X relationship was not accurate enough
!
SUBROUTINE bnd_condition_init( grid, bnd_data )
  TYPE(t_grid), INTENT(in)    :: grid
  TYPE(t_inflow_bnd_data), POINTER, INTENT(INOUT) :: bnd_data

  REAL(wp) :: Fn, Fm
  REAL(wp) :: lambda, A_gam

  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo
  REAL(wp)     :: gam_n1_m, gam_n2_m, gam_n3_m
  REAL(wp)     :: a_int, b_int, mean_x, std_x, h
  REAL(wp)     :: x_now, x_old, f_now, f_old, v_now, v_old
  INTEGER      :: i

  TYPE(particle)                  :: cloud, rain
  TYPE(particle_frozen)           :: ice, snow, graupel, hail
  TYPE(particle_frozen_nonsphere) :: ice_nonsphere, snow_nonsphere

  ALLOCATE( bnd_data )

  IF( (bnd_type==0) .OR. (bnd_type == 3) ) THEN
    bnd_data%number_flux = 0.0_wp
    bnd_data%mass_flux   = 0.0_wp
    RETURN  ! discrete particles for implementation tests
  ENDIF

  gam_n1_m = GAMMA((nu_gam+1.0_wp)/mu_gam)
  gam_n2_m = GAMMA((nu_gam+2.0_wp)/mu_gam)
  gam_n3_m = GAMMA((nu_gam+3.0_wp)/mu_gam)
  ! Axel Diss eq. (A.4)
  lambda = (gam_n1_m/gam_n2_m*mean_mass0)**(-mu_gam)
  A_gam = mu_gam*nrp0/gam_n1_m*lambda**((nu_gam+1.0_wp)/mu_gam)
  ! mean and standard deviation of the generalized gamma distribution
  mean_x = mean_mass0
  std_x = lambda**(-1.0_wp/mu_gam)*SQRT( gam_n3_m/gam_n1_m - (gam_n2_m/gam_n1_m)**2 )

  ! start and end of the numerical flux integration
  ! analytical values would be nicer, however inverting the gamma distribtion is too much work
  a_int  = MAX( mean_x-n_stds*std_x, EPSILON(mean_x) ) !  mth ) ! EPSILON(mean_x) )
  b_int  = MIN( mean_x+n_stds*std_x, 1.0e-5_wp )
  h      = (b_int-a_int)/REAL(n_samples-1,wp)

  ! trapez rule integration of int_(a_int)^(b_int) f*v*x^k dx
  ! Fm = 0.5_wp*h*( SUM(2.0_wp*v*f*x) - v(1)*f(1)*x(1) - v(n_samples)*f(n_samples)*x(n_samples) )
  Fn = 0.0_wp
  Fm = 0.0_wp
  ! store the x values and the cdf in the bnd_data 
  bnd_data%x(0)   = 0.0_wp
  bnd_data%cdf(0) = 0.0_wp
  sp%z   = grid%dom_top
  sp%xi  = 1
  CALL get_atmo( sp%z, atmo )
  DO i=1,n_samples
   x_now  = a_int + (i-1)*h
   bnd_data%x(i)   = x_now
   IF( ini_type == 1 ) THEN ! box model, no sedimentation
    v_now = 1.0_wp 
   ELSE                     ! 1d model, sedimentation
#ifndef WDROPS
    IF(bnd_type == 4) THEN !! 2mom
      IF(lnonsphere_snow_and_ice) THEN
        CALL init_2mom_scheme(cloud,rain,ice_nonsphere,snow_nonsphere,graupel,hail)
        v_now = particle_velocity_nonsphere( ice_nonsphere, (6.0_wp*x_now/pi/rhol)**(1.0_wp/3.0_wp) )
      ELSE
        CALL init_2mom_scheme(cloud,rain,ice,snow,graupel,hail)
        v_now = particle_velocity(ice, x_now)
      ENDIF
      v_now = v_now * exp(-rho_vel*log(atmo%rho/rho0))
    ELSE                   !! McSnow
      sp%m_i = x_now 
      call iceGeo_init(sp) ! set V_i and phi
      CALL m2d_particle( sp )
      v_now  = vterm_snow( atmo, sp )
    ENDIF
#else
    sp%m_w = x_now 
    v_now  = vterm_beard( (m2r*sp%m_w)**z1_3 )
#endif
   ENDIF
   f_now  = A_gam*x_now**nu_gam*EXP(-lambda*x_now**mu_gam)
   bnd_data%cdf(i) = bnd_data%cdf(i-1) + v_now*f_now
   IF( i > 1) THEN
    Fn = Fn + v_now*f_now +       v_old*f_old
    Fm = Fm + v_now*f_now*x_now + v_old*f_old*x_old
   ENDIF
   x_old = x_now
   f_old = f_now
   v_old = v_now
  END DO

  bnd_data%cdf(:) = bnd_data%cdf(:)/bnd_data%cdf(n_samples)
  bnd_data%number_flux = Fn*0.5_wp*h
  bnd_data%mass_flux   = Fm*0.5_wp*h

  WRITE(*,*) "Bnd Init: number_flux ", bnd_data%number_flux, " mass_flux ", bnd_data%mass_flux
END SUBROUTINE bnd_condition_init


!
! free memory of boundary condition
!
SUBROUTINE bnd_condition_delete( bnd_data )
  TYPE(t_inflow_bnd_data), POINTER :: bnd_data

  IF( ASSOCIATED(bnd_data) ) THEN
    DEALLOCATE( bnd_data )
    bnd_data => NULL()
  END IF
END SUBROUTINE bnd_condition_delete


!
! call the correct boundary condition
!
SUBROUTINE bnd_condition( addSp, grid, bnd_data, dt )
  procedure(p_addSp)                   :: addSp
  TYPE(t_grid),            INTENT(in)  :: grid
  TYPE(t_inflow_bnd_data), INTENT(in)  :: bnd_data
  REAL(wp),                INTENT(in)  :: dt

  REAL(wp) :: rand, new_number
  INTEGER  :: noNewSps

  IF( bnd_type < 1 ) RETURN ! switched off
  IF( bnd_type == 3 ) THEN  ! discrete particles for implementation tests
    CALL bnd_condition_discrete_particles( addSp , grid )
    RETURN
  ENDIF

  IF     ( ini_type == 1 ) THEN ! box model
    new_number = nrp0*grid%dom_vol
  ELSE IF( ini_type == 2 ) THEN ! 1d model
    new_number = bnd_data%number_flux*grid%box_area*dt
  END IF

  ! sample fractional to get integer value, for small dt simulations with large xi0
  CALL random_number( rand )
  noNewSps = INT(new_number/xi0)+INT(rand+(new_number/xi0-INT(new_number/xi0)))

  IF      ( bnd_type == 1 ) THEN ! constant xi0
    CALL bnd_condition_constant_xi( addSp, bnd_data, grid%dom_top, noNewSps )
  ELSE !IF ( bnd_type == 2 ) THEN ! xi according to bnd_data%cdf
    CALL bnd_condition_variable_xi( addSp, bnd_data, grid%dom_top, noNewSps, new_number )
  END IF
END SUBROUTINE bnd_condition


!
! flux boundary condition with constant xi=xi0
! the mass is chosen randomly by inverting the cdf
! the multiplicity is set to the constant xi0
!
! This is similar to the v_const-init of
! Unterstrasser et al. Geosci. Model Dev., 10, 1521–1548, 2017
!
SUBROUTINE bnd_condition_constant_xi( addSp, bnd_data, dom_top, noNewSps )
  procedure(p_addSp)                     :: addSp
  TYPE(t_inflow_bnd_data), INTENT(in)    :: bnd_data
  REAL(wp),                INTENT(in)    :: dom_top
  INTEGER,                 INTENT(in)    :: noNewSps

#ifndef WDROPS
  TYPE(t_atmo) :: atmo
#endif
  TYPE(t_sp) :: sp
  REAL(wp) :: rand, curMass
  INTEGER  :: i, ind

  DO i=1,noNewSps 
    sp%z  = dom_top
    sp%xi = xi0 ! init with constant xi0
    sp%statusb = IBSET(sp%statusb, sp_stat%topInflux)
    ! invert cdf => find index probability that equals the random number
    CALL random_number( rand ) ! [0,1)
    rand = 1.0_wp - rand
    ind = MIN(n_samples,MAX(1, binary_search_rightmost(bnd_data%cdf, rand ) ) )

    rand = (bnd_data%cdf(ind)-rand)/(bnd_data%cdf(ind) - bnd_data%cdf(ind-1))
    curMass = rand*bnd_data%x(ind-1) + (1.0_wp-rand)*bnd_data%x(ind)
#ifndef WDROPS
    CALL get_atmo( dom_top, atmo )
    sp%T = atmo%T
    IF ( agg_hist >= 0 ) THEN
      ! save temp. and supersaturation at nucleation point
      sp%init_temp = atmo%T
      sp%init_ssat = atmo%ssat
    END IF
    sp%m_i = curMass 
    call iceGeo_init(sp) ! set V_i and phi
#else
    sp%m_w = curMass 
#endif
    CALL addSp(sp)
  END DO

CONTAINS

!
! binary search that finds right-most index 
! such that array(lb-1) < key < array(lb)
!
PURE FUNCTION binary_search_rightmost(array, key)
  REAL(wp), INTENT(IN) :: array(:), key
  INTEGER :: binary_search_rightmost, lb, rb, m
  lb = 1
  rb = SIZE(array)+1
  DO WHILE( lb < rb )
    m = FLOOR( (lb+rb)/2.0_wp )
    IF( array(m) > key ) THEN
      rb = m
    ELSE
      lb = m + 1
    END IF
  END DO
  binary_search_rightmost = lb - 1
END FUNCTION  binary_search_rightmost

END SUBROUTINE bnd_condition_constant_xi


!
! flux boundary condition with sampled xi
! the m,cdf is further discretized into new_number/xi0 bins
! the mass is randomly chosen within each bin
! the multiplicity xi is calculated proportional to the cdf
!
! This is similar to the singleSIP-init of
! Unterstrasser et al. Geosci. Model Dev., 10, 1521–1548, 2017 
!
SUBROUTINE bnd_condition_variable_xi( addSp, bnd_data, dom_top, noNewSps, new_number )
  procedure(p_addSp)                     :: addSp
  TYPE(t_inflow_bnd_data), INTENT(in)    :: bnd_data
  REAL(wp),                INTENT(in)    :: dom_top
  INTEGER,                 INTENT(in)    :: noNewSps
  REAL(wp),                INTENT(in)    :: new_number

#ifndef WDROPS
  TYPE(t_atmo) :: atmo
#endif
  TYPE(t_sp) :: sp
  REAL(wp) :: rand, curMass, ind_step
  INTEGER  :: i, ind_begin, ind_end, noBins, j, noSpPerBin

  ! avoid supersampling, if noNewSps comes close to n_samples of the cfd, initialize several SPs per Bin
  IF( n_samples > 5*noNewSps ) THEN
    noBins = noNewSps
    noSpPerBin = 1
  ELSE
    noBins = n_samples/5
    noSpPerBin = CEILING(noNewSps/REAL(noBins,wp))
  END IF

  ! create noBins bins from finer m,cdf
  ind_step  = (n_samples-1.0_wp)/REAL(noBins,wp)
  ind_begin = 1
  DO i=1,noBins 
    ind_end   = 1 + NINT(ind_step*(i-0))
    ! set xi proportional to probability in the bin
    sp%xi = NINT(REAL(new_number,wp)*(bnd_data%cdf(ind_end) - bnd_data%cdf(ind_begin))/REAL(noSpPerBin,wp),i8)
    IF( sp%xi <= 0 ) THEN 
      ! do not increment ind_begin to not forget the skipped particles
      ! However, give warning, might be an overflow indicator
      CYCLE
    ENDIF  
    sp%z  = dom_top
    sp%statusb = IBSET(sp%statusb, sp_stat%topInflux)
    DO j=1,noSpPerBin
      ! random sample mass within the bin
      CALL random_number( rand )
      curMass = rand*bnd_data%x(ind_begin) + (1.0_wp-rand)*bnd_data%x(ind_end)
#ifndef WDROPS
      CALL get_atmo( dom_top, atmo )
      sp%T = atmo%T
      IF ( agg_hist >= 0 ) THEN
        ! save temp. and supersaturation at nucleation point
        sp%init_temp = atmo%T
        sp%init_ssat = atmo%ssat
      END IF
      sp%m_i = curMass
      call iceGeo_init(sp) ! set V_i and phi
#else
      sp%m_w = curMass
#endif
      CALL addSp(sp)
    ENDDO 

    ind_begin = ind_end
  ENDDO
END SUBROUTINE bnd_condition_variable_xi

!
! initialize just some discrete particles for implementation testing
!
SUBROUTINE bnd_condition_discrete_particles( addSp, grid )
  procedure(p_addSp)          :: addSp
  TYPE(t_grid), INTENT(in)    :: grid

#ifndef WDROPS
  REAL(wp), PARAMETER :: diameters(13) = (/0.6, 0.8, 1.0, 1.2, 1.5, 1.8, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5/)
  REAL(wp), PARAMETER :: rho_init = rhoi*0.5_wp

  INTEGER  :: iniPartType
  REAL(wp) :: mtot
  TYPE(t_atmo) :: atmo
#else
  REAL(wp)     :: rand
#endif

  TYPE(t_sp)   :: sp
  INTEGER      :: i, n_sps

  n_sps = INT( REAL(nrp0,wp) * grid%dom_vol / REAL(xi0,wp) )

  DO i = 1, n_sps
      sp%z = grid%dom_top
#ifndef WDROPS
      IF( ini_type == 1 ) THEN
        mtot        = mean_mass0*nrp0
        iniPartType = MODULO( i-1, 4 )
      ELSE
        mtot        = mean_mass0*rhoi*pi/6.0_wp*(diameters(MODULO(i-1, SIZE(diameters))+1)/100)**3
        iniPartType = MODULO( (i-1)/SIZE(diameters), 4)
      END IF
      SELECT CASE ( iniPartType )
          CASE(0)
            sp%m_i = ((6.0_wp*mtot/pi/rho_init)**(unr_bet/3.0_wp))*unr_alf
            sp%m_r = mtot - sp%m_i
            sp%v_r = pi/6.0_wp*(sp%m_i/unr_alf)**(3.0_wp/unr_bet) - sp%m_i/rhoi
            sp%V_i = sp%m_i / rhoi
            sp%m_w = 0.0_wp
          CASE(1)
            sp%m_i = 0.0_wp
            sp%m_r = mtot
            sp%v_r = sp%m_r/rhoi
            sp%V_i = 0.0_wp
            sp%m_w = 0.0_wp
          CASE(2)
            sp%m_i = mtot
            sp%m_r = 0.0_wp
            sp%v_r = 0.0_wp
            sp%V_i = sp%m_i / rhoi
            sp%m_w = 0.0_wp
          CASE(3)
            sp%m_i = 0.0_wp
            sp%m_r = 0.0_wp
            sp%v_r = 0.0_wp
            sp%V_i = 0.0_wp
            sp%m_w = mtot
          CASE DEFAULT
            STOP "Error occurred during initialization"
      END SELECT

      sp%mm  = i + 1
      sp%statusb = IBSET(sp%statusb,sp_stat%initialized)
      CALL get_atmo( grid%dom_top, atmo )
      sp%T   = atmo%T
#else
      CALL random_number( rand )
      sp%m_w = -mean_mass0 * LOG(1._wp - rand)
#endif
      sp%xi = xi0
      CALL addSp(sp)
    END DO

END SUBROUTINE bnd_condition_discrete_particles


END MODULE mo_bnd_cond
