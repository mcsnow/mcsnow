!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_sedi

  USE mo_constants,    ONLY: wp, z1_3
  USE mo_sp_types,     ONLY: t_sp, t_list, t_node, &
                             t_list_stats
  USE mo_grid,         ONLY: t_grid, coord2index
  USE mo_sp_nml,       ONLY: msg_level
  USE mo_atmo_types,   ONLY: m2r, t_atmo
#ifndef WDROPS
  USE mo_mass2diam,    ONLY: m2d_particle
  USE mo_velocity,     ONLY: vterm_snow 
#else
  USE mo_velocity,     ONLY: vterm_beard
#endif

  IMPLICIT NONE
  PRIVATE


  PUBLIC :: sp_moveToNewCell, sp_sedi

CONTAINS


!
! sedimentation of super droplets
!
SUBROUTINE sp_sedi( sp, atmo, dt )
  TYPE(t_sp),   INTENT(inout) :: sp
  TYPE(t_atmo), INTENT(in)    :: atmo
  REAL(wp),     INTENT(in)    :: dt

#ifndef WDROPS
  CALL m2d_particle( sp )
  sp%v = vterm_snow( atmo, sp )
#else
  sp%v = vterm_beard( (m2r*sp%m_w)**z1_3 )
#endif
  sp%z = sp%z - dt * sp%v
END SUBROUTINE sp_sedi

!
! due to sedimentation super particle can move to another cell or even to the ground
! this subroutine moves the sps to their new cell
! Important: this function cannot be paralellized by OpenMP
!
SUBROUTINE sp_moveToNewCell( sp_list, grid, stats )
  TYPE(t_list),        INTENT(inout) :: sp_list(:)
  TYPE(t_grid),        INTENT(in)    :: grid
  TYPE(t_list_stats),  INTENT(inout) :: stats

  INTEGER :: i, nsp, isp, icell
  TYPE(t_node), POINTER :: node, prev_node, next_node
  TYPE(t_sp), POINTER :: sp
  REAL(wp) :: mtot

  ! run over all cells
  DO i = 1, grid%nz
    nsp = sp_list(i)%length
    node => sp_list(i)%head
    prev_node => node
    ! run over all particles
    DO isp = 1, nsp

      next_node => node%next
      sp => node%sp

      ! h-meteor reached ground level
      IF ( sp%z - grid%dom_bottom < 1e-2_wp ) THEN
        IF( ASSOCIATED(prev_node, TARGET=node) ) THEN
          sp_list(i)%head => next_node
          prev_node => sp_list(i)%head
        ELSE
          prev_node%next => next_node
        ENDIF
        sp_list(i)%length = sp_list(i)%length - 1 
        stats%nsedi = stats%nsedi + REAL(sp%xi,wp)/grid%box_area
#ifndef WDROPS
        mtot = sp%m_i + sp%m_r + sp%m_w
#else
        mtot = sp%m_w
#endif
        stats%msedi = stats%msedi + REAL(sp%xi,wp)*mtot/grid%box_area
        DEALLOCATE( node )

      ELSE

        ! did the SP stay in the same cell?
        !
        icell = coord2index( node%sp%z )
        IF ( icell /= i ) THEN

#ifndef NDEBUG        
          IF ( msg_level > 10 ) &
            WRITE(*,'(a,i6,a,i6)') "SP moved to cell ", icell, " from cell ", i
#endif

          ! remove node from the i-list
          IF( ASSOCIATED(prev_node, TARGET=node) ) THEN
            sp_list(i)%head => next_node
            prev_node => sp_list(i)%head
          ELSE
            prev_node%next => next_node
          ENDIF
          sp_list(i)%length = sp_list(i)%length - 1 

          ! add the node to the icell-list
          node%next => sp_list(icell)%head
          sp_list(icell)%head => node
          sp_list(icell)%length = sp_list(icell)%length + 1

        ELSE

          prev_node => node
        END IF
      END IF

      node => next_node
    END DO
  END DO
END SUBROUTINE sp_moveToNewCell

END MODULE mo_sedi
