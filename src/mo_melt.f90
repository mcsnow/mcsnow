!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_melt

#ifndef WDROPS

  USE mo_constants,    ONLY: wp, pi, i8
  USE mo_sp_nml,       ONLY: shedding_type, agg_hist
  USE mo_sp_types,     ONLY: t_sp, sp_stat, p_addSp
  USE mo_atmo,         ONLY: t_atmo, latentHeat_melt, get_atmo
  USE mo_atmo_types,   ONLY: rhoii, rhol, rholi, T_3, c_pw, c_pi
  USE mo_mass2diam,    ONLY: m2d_particle

  IMPLICIT NONE
  PRIVATE

  REAL(wp), PARAMETER   :: m_shed = rhol*pi/6.0_wp*(1.5_wp/1000.0_wp)**3

  PUBLIC  :: sp_melt, add_shedded, sp_meltTemp

CONTAINS

!
! Temperature Integration and Melting
!
SUBROUTINE sp_meltTemp( sp, atmo, dt, q_heat, m_shedded, heat_rate )
  TYPE(t_sp),   INTENT(inout) :: sp
  TYPE(t_atmo), INTENT(in)    :: atmo
  REAL(wp),     INTENT(in)    :: dt
  REAL(wp),     INTENT(inout) :: q_heat
  REAL(wp),     INTENT(inout) :: m_shedded
  REAL(wp),     INTENT(inout) :: heat_rate

  REAL(wp), PARAMETER   :: a_het = 0.65_wp,     b_het = 2.0e-4_wp*(100.0_wp**3)/rhol
  
  REAL(wp) :: thermalInertia, L, deltaM, lambda, p0, rnd, V_air, F_f, q_heat_avail
  LOGICAL  :: l_melt, l_freeze

  !IF( ABS(q_heat) < TINY(q_heat) ) RETURN

  thermalInertia = c_pw*sp%m_w + c_pi*(sp%m_i+sp%m_f+sp%m_r)
  q_heat_avail   = q_heat + (T_3 - sp%T)*thermalInertia
  l_melt   = .FALSE.
  l_freeze = .FALSE.

  !sp%T = sp%T - q_heat/thermalInertia

  IF(     sp%m_w < TINY(sp%m_w) ) THEN                      ! pure ice
    IF( q_heat_avail < 0.0_wp ) l_melt = .TRUE. ! cannot be superheated =>  melting begin
    !IF( sp%T > T_3 ) l_melt   = .TRUE. ! cannot be superheated =>  melting begin
  ELSE IF( (sp%m_i + sp%m_f + sp%m_r) < TINY(sp%m_i) ) THEN ! pure water
    IF( sp%T < T_3 ) THEN ! stochastic freezing of supercooled water
      ! TODO Raymond said volume dependent freeze rate is outdated?
      lambda = MAX(0.0_wp,b_het*(exp(a_het*(T_3-sp%T))-1.0_wp))*sp%m_w
      p0 = 1 - exp(-lambda*dt)
      CALL random_number(rnd)
      IF( rnd < p0 ) l_freeze = .TRUE. ! freeze
    ENDIF
  ELSE                                                      ! mixed phase
    l_melt   = .TRUE. ! stays at T_3, melting or freezing due to heat
    l_freeze = .TRUE.
  END IF

  IF( l_melt .OR. l_freeze ) THEN
    sp%T   = T_3
    L      = latentHeat_melt(T_3)
    !q_heat = (T_3-sp%T)*thermalInertia
    q_heat = q_heat_avail ! TODO so far mass conservation that could further heat/cool the particle is neglected stability
    IF( q_heat < 0.0_wp ) THEN ! melting
      deltaM = MIN( -q_heat/L, sp%m_i + sp%m_f + sp%m_r ) ! mass conservation, intentional positive now
      !sp%T   = T_3 - (deltaM*L+q_heat)/thermalInertia     ! on mass conservation, deltaM*L < q_heat -> temperature increase
      q_heat = 0.0_wp
      heat_rate = heat_rate - deltaM*L*sp%xi
      sp%m_w = sp%m_w + deltaM
      deltaM = 1.0_wp - deltaM/(sp%m_i + sp%m_f + sp%m_r)
      sp%m_i = sp%m_i*deltaM
      sp%m_f = sp%m_f*deltaM
      sp%m_r = sp%m_r*deltaM
      sp%v_r = sp%v_r*deltaM ! keep rho_r the same
      sp%V_i = sp%V_i*deltaM
    ELSE                       ! freezing
      deltaM = MIN( q_heat/L, sp%m_w )                    ! mass conservation
      !sp%T   = T_3 - (q_heat-deltaM*L)/thermalInertia     ! on mass conservation, deltaM*L < q_heat ->  temperature decrease
      q_heat = 0.0_wp
      heat_rate = heat_rate + deltaM*L*sp%xi
      sp%m_w = sp%m_w - deltaM
      ! potentially convert water filled rime to frozen mass
      V_air = sp%v_r - sp%m_r*rhoii
      IF( V_air > 0.0_wp ) THEN
        F_f = MIN(1.0_wp, deltaM*rhoii/V_air)
        sp%m_f = sp%m_f + deltaM + sp%m_r*F_f
        sp%m_r = sp%m_r          - sp%m_r*F_f
        sp%v_r = sp%v_r          - sp%v_r*F_f
      ELSE
        sp%m_f = sp%m_f + deltaM
      ENDIF
    ENDIF
  END IF

  IF( (shedding_type > 0) .AND. (sp%m_w > 0.0_wp) ) THEN
    CALL shedWater( sp, m_shedded )
  END IF 
END SUBROUTINE sp_meltTemp
    

!
! Melting
!
SUBROUTINE sp_melt( sp, atmo, dt, q_heat, m_shedded, heat_rate )
  TYPE(t_sp),   INTENT(inout) :: sp
  TYPE(t_atmo), INTENT(in)    :: atmo
  REAL(wp),     INTENT(in)    :: dt
  REAL(wp),     INTENT(inout) :: q_heat
  REAL(wp),     INTENT(inout) :: m_shedded
  REAL(wp),     INTENT(inout) :: heat_rate

  REAL(wp), PARAMETER   :: a_het = 0.65_wp,     b_het = 2.0e-4_wp*(100.0_wp**3)/rhol

  REAL(wp)    :: deltaM, L 
  REAL(wp)    :: F_f, V_air
  REAL(wp)    :: lambda, p0, rnd

  L = latentHeat_melt( atmo%T )
  deltaM = q_heat / L
  q_heat = 0.0_wp                                    ! reset heat
  IF( deltaM < 0.0_wp ) THEN                         ! melting
    deltaM = MIN( -deltaM, sp%m_i + sp%m_f + sp%m_r ) ! mass conservation, intentional possitive now
    heat_rate = heat_rate - deltaM*L*sp%xi
    IF(deltaM > 0.0_wp) THEN ! avoid divison by zero in case the solid is zero
      sp%m_w = sp%m_w + deltaM
      deltaM = 1.0_wp - deltaM/(sp%m_i + sp%m_f + sp%m_r)
      sp%m_i = sp%m_i*deltaM
      sp%m_f = sp%m_f*deltaM
      sp%m_r = sp%m_r*deltaM
      sp%v_r = sp%v_r*deltaM ! keep rho_r the same
      sp%V_i = sp%V_i*deltaM
    END IF
  ELSE                                              ! freezing
    deltaM = MIN( deltaM, sp%m_w ) ! mass conservation
    IF( (sp%m_i + sp%m_f + sp%m_r) > 0.0_wp ) THEN
      heat_rate = heat_rate + deltaM*L*sp%xi
      sp%m_w = sp%m_w - deltaM
      ! potentially convert water filled rime to frozen mass
      V_air = sp%v_r - sp%m_r*rhoii
      IF( V_air > 0.0_wp ) THEN
        F_f = MIN(1.0_wp,deltaM*rhoii/V_air)
        sp%m_f = sp%m_f + deltaM + sp%m_r*F_f
        sp%m_r = sp%m_r          - sp%m_r*F_f
        sp%v_r = sp%v_r          - sp%v_r*F_f ! keep rho_r constant
      ELSE
        sp%m_f = sp%m_f + deltaM
      ENDIF
      !sp%m_r = sp%m_r + deltaM
      !sp%v_r = sp%v_r + deltaM*rhoii
    ELSE ! refreeze pure water drops with probability
      lambda = MAX(0.0_wp,b_het*(exp(a_het*(T_3-atmo%T))-1.0_wp))*sp%m_w
      p0 = 1 - exp(-lambda*dt)
      call random_number(rnd)
      IF( rnd < p0 ) THEN
        heat_rate = heat_rate + deltaM*L*sp%xi
        sp%m_w = sp%m_w - deltaM
        sp%m_f = sp%m_f + deltaM
      END IF
    END IF
  END IF
 
  IF( (shedding_type > 0) .AND. (sp%m_w > 0.0_wp) ) THEN
    CALL shedWater( sp, m_shedded )
  END IF
END SUBROUTINE sp_melt

!
! shedded water
!
SUBROUTINE shedWater( sp, m_shedded )
  TYPE(t_sp),   INTENT(inout) :: sp
  REAL(wp),     INTENT(inout) :: m_shedded

  REAL(wp), PARAMETER   :: acrit = 0.268e-3_wp, bcrit = 0.1389e0_wp
  REAL(wp)    :: m_crit, m_outside, m_inside
  INTEGER(i8) :: noDrops

  CALL m2d_particle( sp )                         ! prepare for shedding 
  IF( BTEST(sp%statusb,sp_stat%liquidOut) ) THEN  ! calculate water on the outside of the particle
    m_outside = rhol*(sp%m_r*rhoii + sp%m_w*rholi - sp%v_r) ! if liquidOut, m_outside > 0
    m_inside  = sp%m_i + sp%m_f + sp%m_r + sp%m_w - m_outside
    m_crit    = acrit + bcrit*m_inside            ! critical amount of water outside, see RH87a JAS 44, 2754
    noDrops = FLOOR( (m_outside - m_crit)/m_shed )
    IF( noDrops > 0_i8 ) THEN
      m_shedded = m_shedded + m_shed*noDrops*sp%xi
      sp%m_w    = sp%m_w    - m_shed*noDrops
    END IF
  END IF
END SUBROUTINE shedWater 

!
! add shedded drops
!
SUBROUTINE add_shedded( addSp, z, dz, m_shed_tot )
   procedure(p_addSp)          :: addSp
   REAL(wp),     INTENT(in)    :: z, dz
   REAL(wp),     INTENT(in)    :: m_shed_tot

   INTEGER(i8)   :: no_shed
   TYPE(t_sp)    :: sp
   REAL(wp)      :: randf
   REAL(wp)      :: d_rnd, m_rnd
   TYPE(t_atmo)  :: atmo

   ! Rasmussen, Levizzani, Pruppacher 84 Part 3, values in text
   REAL(wp), PARAMETER :: d_shed_min = 1.5_wp/1000.0_wp
   REAL(wp), PARAMETER :: d_shed_max = 4.5_wp/1000.0_wp

   ! simple random sampling of shedding size
   ! should maybe be gaussian and broader
   ! or dependent on the mode / particle Reynolds number as in RLP84
   CALL random_number( randf )
   d_rnd = d_shed_min + randf*(d_shed_max-d_shed_min)
   m_rnd = rhol*pi/6.0_wp*d_rnd**3
   no_shed = MAX(1_i8,NINT(m_shed_tot/m_rnd,i8))
   m_rnd = m_shed_tot/REAL(no_shed,wp) ! conserve mass 

   CALL random_number( randf )
   sp%z   = z + dz*(randf-0.5_wp)
   sp%m_w = m_rnd
   sp%xi  = no_shed
   CALL get_atmo( sp%z, atmo )
   sp%T = atmo%T
   IF ( agg_hist >= 0 ) THEN
      sp%init_temp = atmo%T
      sp%init_ssat = atmo%ssat
   END IF
   sp%statusb = IBSET(sp%statusb, sp_stat%shedded)
   CALL addSp( sp )
END SUBROUTINE add_shedded

! end ndef WDROPS
#endif

END MODULE mo_melt
