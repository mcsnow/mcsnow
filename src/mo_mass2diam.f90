!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_mass2diam

  USE mo_constants,     ONLY: pi, wp, i8, mp, z1_3, z1_6, pi_6, pi_4, pi_2, z4_pi, z6_pi, z4pi_3, z3_4pi
  USE mo_sp_nml,        ONLY: snowgeo_type, agggeo_type, sp_vterm_id, rm, Vm, sp_habit
  USE mo_sp_types,      ONLY: t_sp, sp_stat
  USE mo_atmo_types,    ONLY: grav, t_atmo, rhoi, rhoii, rhol, rholi

  IMPLICIT NONE
  PRIVATE

  ! m-D and A-D constants from 
  !   Morrison and Grabowski 'A Novel Approach...' (2007)
  !
  ! NOTE: coeffs in Table 1, p. 1532 are given in CGS, 
  REAL(wp), PARAMETER ::                       &
!    These are 'crystal with sector-like branches P1b' of M96
!    unr_bet = 2.02_wp,                         &
!    unr_alf = 0.00142_wp*10_wp**(2_wp*unr_bet-3_wp), & ! 10_wp**.. to scale to MKS
!    unr_gam = 1.97_wp,                         &
!    unr_sig = 0.55_wp*10_wp**(2_wp*unr_gam-4_wp),  &
!   Jussi's aggregates of dendrites
    ! unr_alf = 0.01243_wp,          &
    ! unr_bet = 2.00000_wp,          &
    ! unr_sig = 0.05625_wp,          &
    ! unr_gam = 1.81000_wp,          &
!   Aggregates of side planes, columns and bullets of M96
    unr_bet = 2.1_wp,                         &
    unr_alf = 0.0028_wp*10_wp**(2_wp*unr_bet-3_wp), & ! 10_wp**.. to scale to MKS
    unr_gam = 1.88_wp,                         &
    unr_sig = 0.2285_wp*10_wp**(2_wp*unr_gam-4_wp),  &
    sph_gam = 2.0_wp,                          &
    sph_sig = pi_4,                            &
    gra_gam = 2.0_wp,                          &
    gra_sig = pi_4,                            &
    ! derived m-D and A-D constants
    unr_alf_i   = 1/unr_alf,           &
    unr_bet_i   = 1/unr_bet,           &
    betm3       = unr_bet - 3._wp,     &
    betm3_i     = 1/betm3,             &
    Dth         = (rhoi * pi_6 * unr_alf_i)**betm3_i, &   ! Eq. (8)
    mth         = pi_6 * rhoi * Dth**3, &
    z1_6alf     = z1_6*unr_alf_i,      &
    pi_6alf     = pi*z1_6*unr_alf_i,   &
    z6alf_pi    = 6*unr_alf/pi,        &
    z3_bet      = 3*unr_bet_i

  ! m-D and A-D constants for m2d_dda
  !   by A. Seifert and J. Leinonen
  REAL(wp), PARAMETER ::               &
    unr_alf_dda = 0.01243_wp,          &
    unr_bet_dda = 2.00000_wp,          &
    unr_sig_dda = 0.05625_wp,          &
    unr_gam_dda = 1.81000_wp,          &
    a_m = 0.72_wp, &
    p_m = 0.52_wp, &
    ! derived m-D and A-D constants
    unr_alf_dda_i = 1._wp/unr_alf_dda, &
    unr_bet_dda_i = 1._wp/unr_bet_dda, &
    unr_sig_dda_i = 1._wp/unr_sig_dda, &
    unr_gam_dda_i = 1._wp/unr_gam_dda, &
    p_m_i = 1._wp / p_m, &
    a_m_i = 1._wp / a_m
  ! properties of plate in Jussis aggregation-model   
    REAL(wp), PARAMETER  :: &
    mono_bet_plates = 2.47,                            &
    mono_alf_plates = 0.7453,                          &
    mono_gam_plates = 2.00,                            &
    mono_sig_plates = 0.6495,                          &
    mono_epsilon_plates = 0.474-1.0,                   &
    mono_zeta_plates    = 0.00125058
#ifndef WDROPS 
  PUBLIC ::              &
    unr_alf, unr_bet, mth, pi_6alf, z3_bet, unr_bet_i, unr_alf_i, unr_gam, unr_sig,   &
    m2d_particle,        &
    m2d_particle_solid,  &
    m2d_particle_liquid, &
    porosity_xi,         &
    unrimed_geometry,    &
    iceGeo_init,         &
    spheroid_geometry,   &
    habit_predict_riming              
  PRIVATE ::             &
    agg_monomer_depend,  &
    habit_geometry,      &
    m2d_fillin,          &
    m2d_similarity

CONTAINS

!
! calculate d and p_area of the sp
!
SUBROUTINE m2d_particle( sp )
  TYPE(t_sp),   INTENT(inout) :: sp

  REAL(wp) :: d_solid, a_solid

  CALL m2d_particle_solid(  sp, d_solid, a_solid )
  IF ( (sp_habit == 0) .OR. ((sp_habit==1) .AND. (sp%mm>1)) ) call iceGeo_init(sp) ! If geometry not prognostic, reset V_i to meaningful value
  CALL m2d_particle_liquid( sp, d_solid, a_solid )
END SUBROUTINE m2d_particle

!
!  calculate the geometry of unrimed aggregates dependent on the monomer number 
!
PURE SUBROUTINE agg_monomer_depend( mm, mono_alf, mono_bet, mono_sig, mono_gam, unr_alf_agg, unr_bet_agg, unr_sig_agg, unr_gam_agg)
  REAL(wp), INTENT(in)  :: mm
  REAL(wp), INTENT(in)  :: mono_alf, mono_bet, mono_sig, mono_gam
  REAL(wp), INTENT(out) :: unr_alf_agg, unr_bet_agg, unr_sig_agg, unr_gam_agg
  REAL(wp), PARAMETER :: &
   alf_mod1 = -0.846, & !first parameter in rational function of a in m=aD**b
   alf_mod2 = 0.547, &  !second parameter in rational function of a in m=aD**b
   bet_mod1 = -0.146, & !first parameter in rational function of b in m=aD**b
   bet_mod2 = 0.338, &  !second parameter in rational function of b in m=aD**b
   sig_mod1 = -0.460, & !first parameter in rational function of c in A=cD**d
   sig_mod2 = 0.287, &  !second parameter in rational function of c in A=cD**d
   gam_mod1 = -0.022, & !first parameter in rational function of d in A=cD**d
   gam_mod2 = -0.165    !second parameter in rational function of d in A=cD**d

  REAL(wp) :: log10Ofmm

  log10Ofmm = log10(mm)
  
  !modify the mass
  unr_alf_agg = 10**((alf_mod1 *log10Ofmm)/(1+alf_mod2*log10Ofmm))*mono_alf
  unr_bet_agg = (bet_mod1*log10Ofmm)/(1+bet_mod2*log10Ofmm)+mono_bet
  !modify the area
  unr_sig_agg = 10**((sig_mod1 *log10Ofmm)/(1+sig_mod2*log10Ofmm))*mono_sig
  unr_gam_agg = (gam_mod1*log10Ofmm)/(1+gam_mod2*log10Ofmm)+mono_gam

END SUBROUTINE agg_monomer_depend

!
! Find max. dimension and area of a non-spherical crystal
!
PURE SUBROUTINE habit_geometry(sp,d_solid,a_solid)
  TYPE(t_sp), INTENT(in)   :: sp
  REAL(wp),   INTENT(out)  :: d_solid, a_solid

  REAL(wp) :: r_a, r_c, xi, V_tot

  IF( (sp%V_i + sp%V_r + (sp%m_f * rhoii)) < TINY(sp%m_i)) THEN
    d_solid = 0.0_wp
    a_solid = 0.0_wp
    RETURN
  ENDIF

  CALL spheroid_geometry(sp, r_a, r_c, V_tot)

  d_solid = 2._wp * MAX(r_a,r_c)
  
  ! A = xi * A_ellipsis 
  xi      = porosity_xi(sp)
  a_solid = xi * pi * r_a * max(r_a, r_c)
END SUBROUTINE habit_geometry

!
! Distribute rime mass along a- or c-axes according to actual shape. 
! Main assumption includes that rime is always added to the smaller 
! axis to draw shape toward graupel (phi = 0.8 or 1.25).
!
REAL(wp) FUNCTION habit_predict_riming(sp, Vgain, r_drop) RESULT(phi_new)
  TYPE(t_sp), INTENT(in)    :: sp
  REAL(wp),   INTENT(in)    :: Vgain
  REAL(wp),   INTENT(in)    :: r_drop

  REAL(wp)                  :: r_a, r_c, V_old, V_new
  LOGICAL                   :: spherical = .false.

  ! IF (sp%m_i > sp%m_r) RETURN    ! AXEL: first stage of riming at constant phi
  IF (sp%mm > 1._wp .AND. sp_habit > 1) THEN ! first fill rime into agg spheroid structure
    IF( (sp%V_i - sp%m_i*rhoii) > (sp%m_f*rhoii + sp%V_r + Vgain) ) THEN
      phi_new = sp%phi     ! if rime fits inside, keep aspect ratio constant
      RETURN
    END IF
  ENDIF

  !V_new  = sp%V_i + sp%V_r + (sp%m_f * rhoii)      ! total volume after riming
  !V_old  = V_new - Vgain                           ! total volume before riming
  CALL spheroid_geometry(sp, r_a, r_c, V_old)
  V_new  = V_old + Vgain ! TODO agg fillin

  ! change of shape by riming
  IF (r_drop > max(r_a,r_c) ) THEN
     ! if droplet is bigger than crystal, consider the new shape as spherical
     phi_new = 1.0_wp
     RETURN
  ELSEIF (spherical) THEN
    ! simple fill-in with riming towards a sphere
    IF(sp%phi <= 1.0_wp) THEN
      ! c-axis grows for oblate
      r_c = V_new / ( z4pi_3 * r_a**2)
    ELSE 
      ! a-axis grows for prolate
      r_a = SQRT( V_new / ( z4pi_3 * r_c ) )
    END IF
  ELSE
    ! as described in Jensen and Harrington 2015, first paragraph on section 2.3,  page 2575
    IF(sp%phi <= 0.8_wp .OR. ( sp%phi > 1._wp .AND. sp%phi <= 1.25_wp) ) THEN
      ! oblate or quasi-spherical prolate that is assumed to tumble as graupel
      r_c = V_new / ( z4pi_3 * r_a**2)
    ELSE 
      ! prolate or quasi-spherical oblate
      r_a = SQRT( V_new / ( z4pi_3 * r_c ) )
    END IF
  END IF

  phi_new = r_c / r_a
END FUNCTION habit_predict_riming


!
! Return the a- and c-axis of a particle with spheroid geometry
!
PURE SUBROUTINE spheroid_geometry(sp, r_a, r_c, V_tot)
  TYPE(t_sp),         INTENT(in)  :: sp
  REAL(wp),           INTENT(out) :: r_a, r_c, V_tot

  IF (sp%mm > 1._wp .AND. sp_habit > 1) THEN ! first fill rime into agg spheroid structure
    IF( (sp%V_i - sp%m_i*rhoii) > (sp%m_f*rhoii+sp%V_r) ) THEN ! rime fits inside
      V_tot = sp%V_i
    ELSE                                                       ! rime (partially) outside
      V_tot = sp%V_r + (sp%m_i+sp%m_f)*rhoii
    ENDIF
  ELSE
    V_tot = sp%V_i + sp%V_r + (sp%m_f * rhoii)
  ENDIF
  r_a   = ( z3_4pi * V_tot / sp%phi )**z1_3 
  r_c   = r_a * sp%phi
END SUBROUTINE spheroid_geometry

!
! Consideration of porosity for area following JH15 for monomers and Shima 2020 for aggregates
! TO DO: Think about porosity, when rime is included, particle could get more dense
PURE ELEMENTAL FUNCTION porosity_xi( sp ) result ( xi )
  TYPE(t_sp), INTENT(in) :: sp

  REAL(wp) :: xi, rho_app, r_a, r_c, V_tot, V_crit, Fr

  CALL spheroid_geometry(sp, r_a, r_c, V_tot)
  rho_app = (sp%m_i + sp%m_r + sp%m_f)/V_tot

  ! Aggregates need special treatment because of their low apparent density even at sphericity
  IF (sp%mm > 1._wp .AND. sp_habit > 1) THEN
    !xi = (rho_app * rhoii) ** exp(-sp%phi)        ! Shima 2020, Eq. 5 + 6 
    V_crit  = sp%V_i - sp%m_i*rhoii
    Fr = MERGE( (sp%m_f*rhoii+sp%V_r)/V_crit, 1.0_wp, v_crit>0.0_wp )      ! v_crit = 0 is possible
    IF( Fr < 1.0_wp ) THEN ! rime fits inside, interpolate
      xi = (1.0_wp-Fr) * (rho_app * rhoii)**(2.0_wp/3.0_wp) + Fr * 1.0_wp
    ELSE
      xi = 1.0_wp
    ENDIF
  ELSE IF ( (sp_habit > 0) .AND. sp%phi < 1._wp) THEN
    ! include branching: xi = (1 - phi) * (rho/rho_i) + phi (J&H 2015) ;
    ! Alternative: xi = ( (sp%m_i + sp%m_r) / (sp%V_i + sp%V_r) ) * rhoii 
    xi = (1._wp - sp%phi) * (rho_app * rhoii) + sp%phi
  ELSE
    xi = 1._wp          ! no reduction of effective area for hollowing or non-habit particles
  END IF 

END FUNCTION porosity_xi

PURE SUBROUTINE unrimed_geometry(sp,dunr,aunr)
   ! select the geometry of the unrimed particles (either pristine or aggregates)
   ! based on the aggeotype namelist parameter
   TYPE(t_sp),   INTENT(in)     :: sp
   REAL(wp),     INTENT(out)    :: dunr, aunr
   REAL(wp) :: unr_alf_agg, unr_bet_agg, unr_sig_agg, unr_gam_agg,dice,aice

   ! ice, distinguish between spherical small ice and power-law geometry, m_i = 0 is safe
   dice = (z6_pi * sp%m_i * rhoii)**z1_3
   aice = sph_sig * dice**sph_gam

  IF ( agggeo_type .eq. 3 ) THEN !   monomer number dependent fit to Jussi's plates
     call agg_monomer_depend( sp%mm, mono_alf_plates, mono_bet_plates , mono_sig_plates, mono_gam_plates, &
                            &        unr_alf_agg,     unr_bet_agg ,     unr_sig_agg,     unr_gam_agg)
     
     dunr = (sp%m_i / unr_alf_agg)**(1./unr_bet_agg)
     aunr = unr_sig_agg * dunr**unr_gam_agg
     
     IF (dunr<dice) THEN
        dunr = dice
        !aunr = aice
     END IF
  ELSE
    IF( agggeo_type .eq. 2 .AND. (sp%mm < 1.01) ) THEN !different geometry for monomers
      dunr = (sp%m_i / mono_alf_plates)**(1./mono_bet_plates)
      aunr = mono_sig_plates*dunr**mono_gam_plates
      IF (dunr<dice) THEN
        dunr = dice
        !aunr = aice
      END IF
    ELSE ! aggregate M96 in case of aggeotype<=2 and sp%mm>1
      IF( sp%m_i < mth) THEN
        ! small ice
        dunr = dice
        aunr = aice
      ELSE
        ! unrimed ice
        dunr = (sp%m_i * unr_alf_i)**unr_bet_i
        aunr = unr_sig * dunr**unr_gam 
        aunr = max(aunr,aice)
      END IF
    END IF
  END IF
END SUBROUTINE unrimed_geometry

!
! calculate d and p_area of the solid (ice+rime) part of the particle
!
PURE SUBROUTINE m2d_particle_solid( sp, d_solid, a_solid)
  TYPE(t_sp),   INTENT(in)        :: sp
  REAL(wp),     INTENT(out)       :: d_solid, a_solid

  IF ( ( sp_habit == 1 .AND. sp%mm < 1.01 ) .OR. (sp_habit == 2) ) THEN
    CALL habit_geometry( sp, d_solid, a_solid )
  ELSE 
    IF ( snowgeo_type .eq. 3 ) THEN
      CALL m2d_similarity( sp, d_solid, a_solid )
    ELSE
      CALL m2d_fillin(     sp, d_solid, a_solid )  ! snowgeo_type.eq.2.or.snowgeo_type.eq.22
    ENDIF
  END IF
END SUBROUTINE m2d_particle_solid


!
! compute diameter from m_i, m_r, v_r using the fill-in model
! compute proj. area using linear interpolation as in
!   based on Morrison and Grabowski 'A Novel Approach...' 2007
! 
PURE SUBROUTINE m2d_fillin( sp, d_solid, a_solid )
  TYPE(t_sp),   INTENT(in)    :: sp
  REAL(wp),     INTENT(out)   :: d_solid, a_solid

  REAL(wp) :: dunr, aunr
  REAL(wp) :: Fr, v_crit

  
  !get geometry of unrimed particles
  CALL unrimed_geometry(sp,dunr,aunr)

  ! rime, distinguish between fill-in and spherical graupel, m_i = 0 and/or m_r = 0 is safe
  v_crit  = pi_6*dunr**3 - sp%m_i*rhoii
  Fr = MERGE( (sp%m_f*rhoii+sp%v_r)/v_crit, 1.0_wp, v_crit>0.0_wp )      ! v_crit = 0 for small ice or no ice
  IF( Fr < 1.0_wp ) THEN ! rime inside (or no rime at all)
    IF(snowgeo_type.eq.22) Fr = sqrt(Fr)                  ! sqrt interpolation of cross sectional area
    d_solid = dunr
    a_solid = (1.0_wp-Fr) * aunr + Fr * gra_sig*dunr**gra_gam
  ELSE                   ! rime outside => spherical rime particle, i.e. graupel
    d_solid = (z6_pi*(sp%v_r + (sp%m_i+sp%m_f)*rhoii))**z1_3
    a_solid = gra_sig*d_solid**gra_gam
  END IF
END SUBROUTINE m2d_fillin

!
! compute maximum dimension and projected area based on m_w
! additional inputs are m_i, m_r, v_r, d_solid, a_solid
!
! First, rime is filled with water
! Then, water builds spherical particle with diameter d_solid 
! (TODO, might be not so nice for similarity model, as sphericity is only asymtotically reached)
! Next, the water builds up a half-torus, see Fig. 1 Rassmussen et al. JAS 41, 381pp 1984
! Lastly, if the torus gets larger than the solid core, the shape is interpolate to a pure flattened raindrop
! 
! optionally, the smooth/liquid fraction of the surface area returned
! if the liquid "fills-in" to a spherical particle at d_solid, the fill-in fraction Fr is used
! if the torus builds up, the fraction of halftorus surface area to the total surface area is calculated
! The second paths is motivated by RLP84a fig. 3-5 which show that the velocity of melting spherical particles increases 
!    despite the increase of the cross-sectional area due to the water torus
SUBROUTINE m2d_particle_liquid(sp, d_solid, a_solid, smoothFraction)
  TYPE(t_sp), INTENT(inout)         :: sp
  REAL(wp),   INTENT(in)            :: d_solid, a_solid
  REAL(wp),   OPTIONAL, INTENT(out) :: smoothFraction

  REAL(wp), PARAMETER :: wr = 33 ! Seifert, A. Geosci. Model Dev., 7, 463-478, 2014
  REAL(wp) :: Fr, v_crit, v_w_out, v_w_torus, d_torus, v_total, d_volume, d_max_w, part, smoothPart
  REAL(wp), PARAMETER :: lambda = 4.7_wp/1000.0_wp

  sp%statusb = IBCLR( sp%statusb, sp_stat%liquidOut)
  sp%statusb = IBCLR( sp%statusb, sp_stat%liquidFillin)
  sp%statusb = IBCLR( sp%statusb, sp_stat%liquidSurf)

  ! water, distinguish between water at surface and water inside rime/no water, m_w=0 is safe, m_r=m_i=0 is safe
  v_w_out = sp%m_r*rhoii + sp%m_w*rholi - sp%v_r
  IF( v_w_out > 0.0_wp ) THEN ! water outside rime
    sp%statusb = IBSET( sp%statusb, sp_stat%liquidOut )

    v_total    = (sp%m_i+sp%m_f)*rhoii + sp%v_r + v_w_out
    d_volume   = (v_total*z6_pi)**z1_3          
    d_max_w    = d_volume*EXP(wr*d_volume)                 ! max drop diameter, Seifert et al. GeoSci. Model Dev. 7,463,2014 eq.3
    ! distinguish between solid core and pure water drop
    IF( d_solid > 0.0_wp ) THEN   ! solid core
      v_crit  = pi_6*d_solid**3 - (sp%m_i+sp%m_f)*rhoii - sp%v_r
      Fr = MERGE( v_w_out/v_crit, 1.0_wp, v_crit>0.0_wp )  ! v_crit = 0 for small ice or graupel 
      ! distinguish between spherical fill-in and water torus
      IF( Fr < 1.0_wp ) THEN          ! water fits inside solid maximum dimension sphere
        sp%statusb = IBSET( sp%statusb, sp_stat%liquidFillin )
        IF(snowgeo_type.eq.22) Fr = sqrt(Fr)               ! sqrt interpolation of cross sectional area
          sp%d       = d_solid
          sp%p_area  = (1.0_wp-Fr) * a_solid + Fr * pi_4*d_solid**2
          smoothPart = Fr
      ELSE                            ! build up water torus
        v_w_torus  = v_w_out - v_crit                       ! Fr >=1.0 => v_w_torus >= 0.0
        d_torus    = SQRT(8.0_wp*v_w_torus/(pi**2*d_solid)) ! assume water to form half-torus with inner radius equal to d_solid/2
        IF( d_torus < d_solid ) THEN    ! max diameter is the sum of the solid diameter plus the torus diameter
          sp%d       = (d_solid + d_torus)
          ! if v_crit>0 and Fr > 1 => smooth at whole surface
          ! if v_crit=0, i.e. small Ice or graupel, smoothPart = A_halftorus/(A_halftorus - 2*A_sphericalcap)
          !smoothPart = MERGE( 1.0_wp, pi_2*d_torus/(d_solid+d_torus*(pi_2-1.0_wp)), v_crit>0.0_wp )
          smoothPart = MERGE( pi_2*d_torus/(d_solid+d_torus*(pi_2-1.0_wp)), 1.0_wp, v_crit < EPSILON(v_crit) )
        ELSE                            ! interpolate between sphere+torus and max dimension of pure water drop
          part       = (2.0_wp*d_solid/(d_solid+d_torus))**3
          sp%d       = (d_solid + d_torus)*part + (1.0_wp-part)*d_max_w
          sp%statusb = IBSET( sp%statusb, sp_stat%liquidSurf )
          smoothPart = 1.0_wp
        ENDIF  
        sp%p_area  = pi_4*sp%d**2
      END IF
    ELSE                          ! no ice/rime, pure water drop
          sp%d       = d_max_w
          sp%p_area  = pi_4*sp%d**2
          ! pure water, flatten rain drop
          sp%phi     = exp(-sp%d/lambda) + (1.0_wp - exp(-sp%d/lambda)) / (1.0_wp+sp%d/lambda)  
          sp%statusb = IBSET( sp%statusb, sp_stat%liquidSurf )
          smoothPart = 1.0_wp
    ENDIF
  ELSE                        ! water inside rime ( or no water at all )
          sp%d       = d_solid
          sp%p_area  = a_solid
          smoothPart = 0.0_wp
  ENDIF
  IF( PRESENT(smoothFraction) ) smoothFraction = smoothPart
END SUBROUTINE m2d_particle_liquid


!
! compute diameter and proj. area from m_i, m_r, v_r
! based on Monte-Carlo simulation of individual snowflakes
! and parameterization using similarity theory
!
PURE SUBROUTINE m2d_similarity( sp, d_solid, a_solid )
  TYPE(t_sp),   INTENT(in)    :: sp
  REAL(wp),     INTENT(out)   :: d_solid, a_solid

  REAL(wp)  :: Fr, vtan, s, kappa, f, df, ds, &
       &       dunr, aunr, dgrp, agrp, coeff, v_r

  IF( (sp%m_i + sp%m_f + sp%m_r) < TINY(sp%m_i) ) THEN
    d_solid = 0.0_wp
    a_solid = 0.0_wp
    RETURN
  ENDIF

  Fr  = sp%m_r / (sp%m_i + sp%m_f + sp%m_r)
  v_r = sp%v_r + sp%m_f*rhoii               ! zero order insertation of m_f ! TODO howto similarity with m_f?

  !get geometry of unrimed particles
  CALL unrimed_geometry(sp,dunr,aunr)

  dgrp = (z6_pi*v_r)**z1_3
  agrp = pi/4.*dgrp**2

  d_solid  = dunr
  s  = 1.0
  ds = 1.0
  IF (Fr < 1e-11_wp) THEN
     ! unrimed ice
     a_solid = aunr     
  ELSEIF (Fr < 0.9999_wp) THEN
     kappa = z6_pi*v_r/d_solid**3     
     IF (kappa < 1) THEN
        s = 0.8*(ATAN(kappa)*a_m_i)**p_m_i
     ELSE
        s = kappa**z1_3+1._wp
     END IF     
     DO WHILE (ds/s > 1e-4)
        vtan = TANH( a_m*s**p_m )
        f  = vtan*(s+1._wp)**3 - kappa
        df = (s+1._wp)**2*( (1._wp-vtan*vtan)*a_m*p_m * s**(p_m-1)*(s+1) + 3.*vtan)
        ds = f/df
        s  = s-ds
     END DO     
     d_solid = d_solid*(s+1._wp)
     kappa = (6.*v_r/pi)/d_solid**3
     coeff = 0.32*tanh(7.36*kappa)+0.52*kappa   ! rho_rime = 270/700, dendrites large
     agrp  = pi/4.*d_solid**2
     a_solid  = coeff * (agrp-aunr) + aunr
  ELSE
     ! graupel
     d_solid  = dgrp
     a_solid  = agrp
  END IF
END SUBROUTINE m2d_similarity

SUBROUTINE iceGeo_init(sp)
  TYPE(t_sp),   INTENT(inout)     :: sp

  REAL(wp)            :: aunr, dunr, V_i
    
  CALL unrimed_geometry(sp,dunr,aunr)
  IF( (agggeo_type > 1) .AND. (sp%mm < 1.01) ) THEN
    !sp%phi = MIN(mono_zeta_plates * dunr**mono_epsilon_plates, 1.0_wp)
    !sp%V_i = 3.0_wp*sqrt(3.0_wp)/8.0_wp *dunr**3 * sp%phi ! volume of hexagonal prism
    sp%V_i = sp%m_i * rhoii ! solid ice plate
    sp%phi = MIN( (sp%V_i + sp%V_r + (sp%m_f * rhoii)) / (pi_6 * dunr**3), 1.0_wp ) ! fill-in of rime
    ! for only ice this corresponds to (alpha/rhoi)/(pi/6) d**(beta-3), which seems close enough
    ! 0.00125058*d**-0.526 vs 0.0015488772*d**-0.53
  ELSE
    sp%V_i = pi_6 * dunr**3
    sp%phi = 1.0_wp
  ENDIF
END SUBROUTINE iceGeo_init

! ifdef WDROPS
#endif


END MODULE mo_mass2diam
