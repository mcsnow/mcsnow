!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_check

  USE mo_constants,      ONLY: pi, wp, pi_4, pi_6, z1_3, z4pi_3, z4_pi, z3_4pi
  USE mo_sp_types,       ONLY: t_sp, sp_stat
  USE mo_sp_nml,         ONLY: out_type, coll_kern_type
  USE mo_atmo_nml,       ONLY: ssat, get_IGF
  USE mo_grid,           ONLY: t_grid
#ifndef WDROPS
  USE mo_mass2diam,      ONLY: m2d_particle, unr_bet, unr_alf, &
                               porosity_xi, iceGeo_init, spheroid_geometry
  USE mo_velocity,       ONLY: boehmVel, Reynolds_Boehm, hydrodynamicValuesBoehm
  USE mo_velocity,       ONLY: vterm_snow, vterm_beard, vterm_hw10, vterm_kc05
  USE mo_colleffi,       ONLY: Ecbohm_spheroid, Ecbohm_riming, ecollBeheng, stick_effi_tempOnly
  USE mo_colleffi,       ONLY: impact_vel, EcBeardGrover, EcRobinSlinnHolger, rho_rime_CL
#endif
  USE mo_atmo,           ONLY: t_atmo, get_atmo, psat_water, psat_ice, dynamic_viscosity
  USE mo_atmo_types,     ONLY: T_3, rhol, rhoii, rhoi, rv, rd, grav
#ifdef HAVE_NETCDF
  USE netcdf
#endif

  IMPLICIT NONE

#ifndef WDROPS
  PUBLIC :: plot_colleffi, plot_colleffi_mtot, plot_colleffi_riming_drop_spectrum, plot_a2d_m2d,  &
            plot_vt_f_mw, plot_vt_f_mw_vivec, plot_rain, plot_colleffi_drops,                     & 
            plot_colleffi_pressure_dependency, plot_colleffi_aggregation,                         &
            plot_colleffi_aggregation_phi, plot_boehm_westbrook, plot_vt_boehm_height_depend,     &
            plot_Ec_boehm_height_depend, plot_vt_boehm_r_depend, plot_boehm_westbrook_phis
#endif


CONTAINS

!
! plot atmosphere profiles
!
SUBROUTINE plot_atmo( grid )
  TYPE(t_grid), INTENT(in) :: grid
  INTEGER, PARAMETER :: npts = 2001
  INTEGER :: i
  REAL(wp) :: dz, zi
  REAL(wp), DIMENSION(npts) :: z, rho, T, p, eta, ssat, rh, &
    psatw, psati, lwc, qv, IGF
  TYPE(t_atmo) :: atmo
  LOGICAL :: file_exists
  CHARACTER(len=*), PARAMETER :: fname = "atmo.dat"
  
  zi = grid%dom_bottom
  dz = grid%dom_top/REAL(npts-1,wp)
  DO i = 1, npts
    CALL get_atmo( zi, atmo )
    z(i)     = zi
    rho(i)   = atmo%rho
    T(i)     = atmo%T
    p(i)     = atmo%p
    eta(i)   = atmo%eta
    ssat(i)  = atmo%ssat
    rh(i)    = atmo%rh
    psatw(i) = atmo%psatw
    psati(i) = atmo%psati
    lwc(i)   = atmo%qc * atmo%rho
    qv(i)    = atmo%qv
    zi       = zi + dz
#ifndef WDROPS
    IGF(i)   = get_IGF(T(i))
#endif
  END DO


  SELECT CASE(out_type)

  CASE(1)
    ! ASCII out
    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF

    DO i = 1, npts
      WRITE(74,'(12es16.6)') z(i), rho(i), T(i), p(i), eta(i),  &
        ssat(i), rh(i), psatw(i), psati(i), lwc(i), qv(i), IGF(i)
    END DO

  CASE(2)
    STOP "plot_atmo:: NetCDF output not implemented"

  END SELECT
END SUBROUTINE plot_atmo


#ifndef WDROPS
!
! plot collision efficiency
!
SUBROUTINE plot_colleffi()
  INTEGER, PARAMETER :: npts = 200

  INTEGER  :: i, i2, ir, ifr
  REAL(wp) :: mii1, mi1(npts), mii2, mi2(npts), dmi, & ! q1, q2, &
              ce(npts,npts,4,4), d1(npts,npts,4,4) , &
              se(npts,npts,4,4), d2(npts,npts,4,4)
  REAL(wp) :: &
    rhor(4) = (/ 200._wp, 400._wp, 600._wp, 800._wp /), &
    Fr(4) = (/ 0., 0.3, 0.6, 0.9 /)
  CHARACTER(len=256) :: fname
  LOGICAL      :: file_exists
  TYPE(t_sp)   :: sp1, sp2
  TYPE(t_atmo) :: atmo
  
  ! plot (mi,mr) vs coll. eff., diam for fixed rime density
  DO ifr = 1, SIZE(Fr)
    DO ir = 1, SIZE(rhor)
      dmi = 10**(12/REAL(npts-1,wp))
      mii1 = 1e-14_wp
      DO i = 1, npts
        mii2 = 1e-14_wp
        mi1(i) = mii1
        DO i2 = 1, npts
          mi2(i2) = mii2
          sp1%z = 2000._wp
          sp1%m_i = REAL(mii1,wp)
          sp1%m_r = Fr(ifr) / (1-Fr(ifr)) * sp1%m_i
          sp1%v_r = sp1%m_r / rhor(ir)
          sp1%V_i = sp1%m_i / rhoi
          sp2%z = 2000._wp
          sp2%m_i = REAL(mii2,wp)
          sp2%m_r = Fr(ifr) / (1-Fr(ifr)) * sp2%m_i
          sp2%v_r = sp2%m_r / rhor(ir)
          sp2%V_i = sp2%m_i / rhoi

          ! particles would collide somewhere in the middle between
          ! their current positions
          CALL get_atmo( 0.5_wp*(sp1%z+sp2%z), atmo )

          CALL m2d_particle( sp1 )
          CALL m2d_particle( sp2 )

          sp1%v = vterm_snow( atmo, sp1 )
          sp2%v = vterm_snow( atmo, sp2 )

          ! ratio of the effective A over the circumscribed cross-sectional A_e area
          ! Heymsfield and Westbrook 2010, p.2471: q = A_e/A = pi/4 * D**2
          !q1 = ( 1e-3_wp*sp1%m_r / (sp1%v_r *(100*sp1%d)**(-0.6)* 0.078) )**(2/3._wp)
          !q2 = MERGE( 0.83_wp, 4/pi, q1 > 0.83 )
          !q1 = sp1%p_area / (pi/4 * sp1%d**2)
          !q2 = sp2%p_area / (pi/4 * sp2%d**2)

          ! Ecbohm_spheroid does include temperature-dep sticking eff.
          ! here we want E_stickeff = 1
          atmo%T = -15_wp + T_3

          ce(i,i2,ir,ifr) = Ecbohm_spheroid( atmo, sp1, sp2 )
          se(i,i2,ir,ifr) = stick_effi_tempOnly( atmo%T )
          !se(i,i2,ir,ifr) = stick_effi( sp1, sp2, atmo%rho, atmo%etai, atmo%T )

          d1(i,i2,ir,ifr) = sp1%d
          d2(i,i2,ir,ifr) = sp2%d

          mii2 = mii2 * dmi
        END DO
        mii1 = mii1 * dmi
      END DO
    END DO
  END DO


  SELECT CASE(out_type)
  CASE(1)

    ! ASCII out
    DO ifr = 1, SIZE(Fr)
      DO ir = 1, SIZE(rhor)

        WRITE(fname,'(a,i1,a,i1,a)') "ce_frId", ifr, "_rhorId", ir, ".dat"
        PRINT *, "   -> Writing ", fname

        INQUIRE(FILE=fname, EXIST=file_exists)
        IF( file_exists ) THEN
          OPEN(74, file=fname, status="old", position="append", action="write")
        ELSE
          OPEN(74, file=fname, status="new", action="write")
        END IF

        WRITE(74,'(a,f4.1,2(a,f5.1))') "# Fr = ", Fr(ifr), ", rhor = ", rhor(ir), &
             &                       ", Tc = ",atmo%T-T_3

        DO i = 1, npts
          DO i2 = 1, npts
            WRITE(74,'(8e14.7)') mi1(i), mi2(i2), ce(i,i2,ir,ifr), se(i,i2,ir,ifr), &
                                                   d1(i,i2,ir,ifr), d2(i,i2,ir,ifr)
          END DO
          WRITE(74,*) ""
        END DO

      END DO
    END DO

    CLOSE(74)

  CASE(2)
    ! NetCDF out
    STOP "plot_colleffi :: NetCDF output not implemented."
  END SELECT
END SUBROUTINE plot_colleffi

!
! plot collision efficiency
!
SUBROUTINE plot_colleffi_mtot()
  INTEGER, PARAMETER :: npts = 200

  INTEGER  :: i, i2, ir, ifr
  REAL(wp) :: mt1, m1(npts), mt2, m2(npts), dmt, & !q1, q2, &
              ce(npts,npts,4,4), d1(npts,npts,4,4) , &
              se(npts,npts,4,4), d2(npts,npts,4,4)
  REAL(wp) :: &
    rhor(4) = (/ 200._wp, 400._wp, 600._wp, 800._wp /), &
    Fr(4) = (/ 0., 0.3, 0.6, 0.9 /)  ! rime mass fraction
  CHARACTER(len=256) :: fname
  LOGICAL      :: file_exists
  TYPE(t_sp)   :: sp1, sp2
  TYPE(t_atmo) :: atmo

  se(:,:,:,:) = 0.0_wp
  
  ! plot (mi,mr) vs coll. eff., diam for fixed rime density
  DO ifr = 1, SIZE(Fr)
    DO ir = 1, SIZE(rhor)
      dmt = 10**(12/REAL(npts-1,wp))
      mt1 = 1e-14_wp
      DO i = 1, npts
        mt2 = 1e-14_wp
        m1(i) = mt1
        DO i2 = 1, npts
          m2(i2) = mt2
          sp1%z = 2000._wp
          sp2%z = 2000._wp
          sp1%m_i = REAL(mt1,wp) * (1-Fr(ifr))
          sp1%m_r = REAL(mt1,wp) * Fr(ifr)
          sp1%v_r = sp1%m_r / rhor(ir)
          sp1%m_w = 0.0_wp
          sp2%m_i = REAL(mt2,wp) * (1-Fr(ifr)) 
          sp2%m_r = REAL(mt2,wp) * Fr(ifr)
          sp2%v_r = sp2%m_r / rhor(ir)
          sp2%m_w = 0.0_wp

          ! particles would collide somewhere in the middle between
          ! their current positions
          CALL get_atmo( 0.5_wp*(sp1%z+sp2%z), atmo )

          CALL m2d_particle( sp1 )
          CALL m2d_particle( sp2 )

          sp1%v = vterm_snow( atmo, sp1 )
          sp2%v = vterm_snow( atmo, sp2 )

          ! ratio of the effective A over the circumscribed cross-sectional A_e area
          !q1 = sp1%p_area / (pi/4 * sp1%d**2)
          !q2 = sp2%p_area / (pi/4 * sp2%d**2)

          ! Ecbohm_spheroid does include temperature-dep sticking eff.
          ! here we want E_stickeff = 1 and therefore assume -15 C
          atmo%T = -15_wp + T_3

          ce(i,i2,ir,ifr) = Ecbohm_spheroid( atmo, sp1, sp2 )
          !se(i,i2,ir,ifr) = sp2%v

          d1(i,i2,ir,ifr) = sp1%d
          d2(i,i2,ir,ifr) = sp2%d

          mt2 = mt2 * dmt
        END DO
        mt1 = mt1 * dmt
      END DO
    END DO
  END DO


  SELECT CASE(out_type)
  CASE(1)

    ! ASCII out
    DO ifr = 1, SIZE(Fr)
      DO ir = 1, SIZE(rhor)

        WRITE(fname,'(a,i1,a,i1,a)') "colleff_frId", ifr, "_rhorId", ir, ".dat"
        PRINT *, "   -> Writing ", fname

        INQUIRE(FILE=fname, EXIST=file_exists)
        IF( file_exists ) THEN
          OPEN(74, file=fname, status="old", position="append", action="write")
        ELSE
          OPEN(74, file=fname, status="new", action="write")
        END IF

        WRITE(74,'(a,f4.1,2(a,f5.1))') "# Fr = ", Fr(ifr), ", rhor = ", rhor(ir), &
             &                       ", Tc = ",atmo%T-T_3

        DO i = 1, npts
          DO i2 = 1, npts
            WRITE(74,'(8e14.7)') m1(i), m2(i2), ce(i,i2,ir,ifr), se(i,i2,ir,ifr), &
                                                   d1(i,i2,ir,ifr), d2(i,i2,ir,ifr)
          END DO
          WRITE(74,*) ""
        END DO

      END DO
    END DO

    CLOSE(74)

  CASE(2)
    ! NetCDF out
    STOP "plot_colleffi_mtot :: NetCDF output not implemented."
  END SELECT
END SUBROUTINE plot_colleffi_mtot

!
! plot collision efficiency
!
SUBROUTINE plot_colleffi_riming(Frime,rhor)
  REAL(wp) :: Frime,rhor

  INTEGER, PARAMETER :: npts = 200
  INTEGER, PARAMETER :: jpts = 7 !500
  INTEGER  :: i, j 
  REAL(wp) :: mt1, m1(npts), dmt, d, K, d_c, r_c, v_c 
  REAL(wp) :: dc(jpts) = (/ 5.,7.,10.,15.,20.,30.,50. /) !droplet diameter (/ (j,j=1,jpts)/)
  CHARACTER(len=256) :: fname
  LOGICAL      :: file_exists
  TYPE(t_sp)   :: sp1
  TYPE(t_atmo) :: atmo
  REAL(wp)     :: ce_boehm(npts,jpts), ce_bulk(npts,jpts), ce_spheres(npts,jpts), &
                  ce_BeardGrover(npts,jpts), ce_Robin(npts,jpts), ce_Slinn(npts,jpts), ce_Holger(npts,jpts), &
                  d1(npts,jpts), d2(npts,jpts), rho_rime(npts,jpts), vel_rel(npts,jpts), St(npts,jpts), N_Re(npts,jpts), &
                  N_ReBig(npts,jpts)

  dmt = 10**(12/REAL(npts-1,wp))

  DO j=1,jpts
     d_c = dc(j)*1e-6
     r_c = 0.5_wp * d_c
     v_c = vterm_beard( r_c ) 
     mt1 = 1e-14_wp
     DO i=1, npts
        m1(i) = mt1
        sp1%z = 2000._wp
        sp1%m_i = REAL(mt1,wp) * (1-Frime)
        sp1%m_r = REAL(mt1,wp) * Frime
        sp1%v_r = sp1%m_r / rhor
        sp1%V_i = sp1%m_i / rhoi
        sp1%m_w = 0.0_wp

        CALL get_atmo( sp1%z, atmo )
        
        ! make sure that particle properties are set
        CALL m2d_particle( sp1 )
        sp1%v = vterm_snow( atmo, sp1 )

        d1(i,j) = sp1%d
        d2(i,j) = d_c

        ce_boehm(i,j) = Ecbohm_riming( atmo, sp1, d_c )

        vel_rel(i,j) = ABS(v_c-sp1%v)

        ! Stokes number (eq. 4, Rasmussen and Heymsfield '85)
        d = sp1%d * MIN( 1.0_wp, 1._wp / SQRT( sp1%phi ) )       ! correction in case of prolate particles
        K = 4_wp * rhol * vel_rel(i,j) * r_c**2 / (9._wp * atmo%eta * d)
        St(i,j) = K
        N_Re(i,j)    = ABS(v_c-sp1%v) * sp1%d * atmo%rho * atmo%etai
        N_ReBig(i,j) =         sp1%v  * sp1%d * atmo%rho * atmo%etai

        ce_BeardGrover(i,j) = EcBeardGrover( atmo, sp1, r_c )
        CALL EcRobinSlinnHolger( atmo, sp1, r_c, ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j) )

        if (sp1%v.gt.v_c) then
           ! bulk collision efficiency w.r.t. average droplet (Cober and List '93)
           ce_bulk(i,j) = MIN(MAX( 0.55_wp * LOG10(2.51_wp * K), 0._wp ), 1.0_wp)
 
           ! collision efficiency of spheres based on Hall-Beheng table
           ce_spheres(i,j) = ecollBeheng(100.0_wp*sp1%d/2.0_wp,100.0_wp*r_c)

          ! rime density
          rho_rime(i,j) = rho_rime_CL( sp1, atmo, K, r_c, vel_rel(i,j))
        else
          ce_bulk(i,j) = 0.0
          ce_spheres(i,j) = 0.0
          rho_rime(i,j) = 0.0
        end if

        mt1 = mt1 * dmt
     END DO
  END DO
  WRITE(fname,'(a,i3.3,a,i3.3,a)') "colleff_riming_Fr",int(Frime*100),"_rhor",int(rhor),".dat"
  PRINT *, "   -> Writing ", fname

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
     OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
     OPEN(74, file=fname, status="new", action="write")
  END IF

  DO j = 1, jpts
     DO i = 1, npts
        WRITE(74,'(14e14.7)') d1(i,j), d2(i,j), ce_boehm(i,j), ce_bulk(i,j), &
             &                                  ce_spheres(i,j), rho_rime(i,j), vel_rel(i,j), St(i,j), N_Re(i,j), N_ReBig(i,j), &
             &                ce_BeardGrover(i,j), ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j)
     END DO
     WRITE(74,*) ""
  END DO
  CLOSE(74)

END SUBROUTINE plot_colleffi_riming


!
! plot collision efficiency w.r.t. drop radius (Boehm 1992 Part III)
!
SUBROUTINE plot_colleffi_riming_drop_spectrum( whichCase )
  USE mo_atmo_types, ONLY: grav
  INTEGER, INTENT(in) :: whichCase


  REAL(wp) :: Frime,rhor

  INTEGER  :: i, j 
  CHARACTER(len=256) :: fname
  LOGICAL      :: file_exists
  TYPE(t_sp)   :: sp1, sp2
  TYPE(t_atmo) :: atmo

  INTEGER, PARAMETER :: jpts = 8 
  INTEGER, PARAMETER :: npts = 200

  REAL(wp) :: a(jpts), c(jpts)
  CHARACTER(len=20) :: ty

  ! Boehm Data!
  ! simple hexagonal plates
  ! REAL(wp) :: a(jpts) = (/ 51.,88.,113.,147.,160.,194.,213.,289.,404.,639. /) ! basal facet dimension
  ! REAL(wp) :: c(jpts) = (/ 2.55,4.4,5.65,7.35,8.,9.7,10.65,14.45,20.2,31.95/) ! prism facet 
  REAL(wp) :: a_ob(jpts) = (/ 80.,113.3,253.3,358.2,473.8,620.,750.,850. /) ! ! WJ
  REAL(wp) :: c_ob(jpts) = (/ 9.0,10.,16.,18.5,20.5,22.5,24.,24.5/) ! WJ                                             

  ! hexagonal columns / cylinders
  ! REAL(wp) :: a(jpts) = (/ 23.5,32.7,36.6,41.5,53.4,77.2,107.,146. /)           ! basal facet dimension
  ! REAL(wp) :: c(jpts) = (/ 32.9,45.78,54.9,70.55,117.48,254.76,535.,1211.8 /)   ! prism facet dimension
  REAL(wp) :: a_cyl(jpts) = (/ 23.5,32.7,36.6,41.5,53.4,77.2,106.7,146.4 /)           ! WJ 
  REAL(wp) :: c_cyl(jpts) = (/ 33.55,46.55,56.3,69.15,118.7,257.45,533.5,1220. /)     ! WJ 

  ! branched crystals
  ! REAL(wp) :: a(jpts) = (/ 104.,134.,174.,205.,255.,348.,487.,778. /)           ! basal facet dimension
  ! REAL(wp) :: c(jpts) = (/ 5.2,6.7,8.7,10.25,12.75,17.4,24.35,38.9 /)           ! prism facet dimension
  REAL(wp) :: a_branched(jpts) = (/ 100.,125.,350.,500.,750.,1000.,1250.,1550. /)          ! WJ
  REAL(wp) :: c_branched(jpts) = (/ 15.,18.,32.,40.,50.,60.,65.,73. /)                     ! WJ

  REAL(wp)     :: ce_boehm(npts,jpts), ce_bulk(npts,jpts), ce_spheres(npts,jpts), &
                  ce_BeardGrover(npts,jpts), ce_Robin(npts,jpts), ce_Slinn(npts,jpts), ce_Holger(npts,jpts), &
                  d1(npts,jpts), d2(npts,jpts), rho_rime(npts,jpts), vel_rel(npts,jpts), St(npts,jpts), N_Re(npts,jpts), &
                  N_ReBig(npts,jpts), CD(npts,jpts)

  REAL(wp) :: K, d_c, v_c, r_c, V_i(npts), m1(npts), xi, d

  Frime = 0.0_wp
  rhor  = 600.0_wp
  IF( whichCase == 1 ) THEN
    ty = 'oblate'
    a = a_ob
    c = c_ob
  ELSEIF ( whichCase == 2 ) THEN
    ty = 'cylinder'  
    a = a_cyl
    c = c_cyl
  ELSE
    ty = 'branched'   
    a = a_branched
    c = 0.5_wp * c_branched
  ENDIF

  DO j=1,jpts
    sp1%d   = 2._wp * MAX(a(j), c(j)) * 1e-6
    sp1%phi = c(j) / a(j) 
    IF (ty == 'branched') THEN
      V_i(j)  = SQRT(3._wp)*9.0_wp/8.0_wp*2.0_wp * ( a(j) * 1e-6)**2 * ( c(j) * 1e-6)
    ELSEIF(ty == 'oblates') THEN
      V_i(j)  = 3.0_wp*SQRT(3.0_wp) * ( a(j) * 1e-6)**2 * ( c(j) * 1e-6)
    ELSE
      !V_i(j)  = 4. / 3. * pi * ( a(j) * 1e-6)**2 * ( c(j) * 1e-6)
      V_i(j)  = 2.0 * pi  * ( a(j) * 1e-6)**2 * ( c(j) * 1e-6)
    END IF
    m1(j)   = rhoi * V_i(j)
    sp1%z   = 2000._wp 
    sp1%m_i = REAL(m1(j),wp) * (1-Frime)
    sp1%m_r = REAL(m1(j),wp) * Frime
    !sp1%V_i = 4. / 3. * pi * ( a(j) * 1e-6)**2 * ( c(j) * 1e-6)
    sp1%V_i = sp1%m_i / rhoi ! TODO not correct for branched
    sp1%V_r = sp1%m_r / rhor
    sp1%m_w = 0.0_wp

    CALL get_atmo( sp1%z, atmo )
    !atmo%rho = 1.2933
    !atmo%eta = 1.7231e-5
    !atmo%etai = 1.0_wp/atmo%eta ! TODO temperature?

    ! make sure that particle properties are set
    ! CALL m2d_particle( sp1 )
    ! reduction of effective area when branching (only for oblates), not for hollowing 
    xi = porosity_xi( sp1 )
    IF (ty == 'branched') THEN
        WRITE(*,*) "xi = ", xi, sqrt(3.0_wp) / pi
        xi = sqrt(3.0_wp)*9.0_wp/8.0_wp / pi
    ELSEIF( ty == 'oblates') THEN
        xi = 3.0_wp*SQRT(3.0_wp)/2.0_wp/pi
    ELSE
        xi = 4.0_wp / pi
    ENDIF
    ! A = xi * A_ellipsis 
    sp1%p_area = xi * pi * (a(j) * 1e-6) * max((a(j) * 1e-6), (c(j) * 1e-6))
    ! sp1%p_area = SQRT(3._wp) / 2. * ( a(j) * 1e-6)**2

    sp1%v = vterm_snow( atmo, sp1 )

    DO i=1, npts
      d1(i,j) = sp1%d
      d2(i,j) = i * 1e-6
      d_c     = d2(i,j)
      r_c     = 0.5_wp * d_c
      sp2%z       = sp1%z
      sp2%m_w     = pi/6.0_wp * rhol * d_c**3
      sp2%d       = d_c
      sp2%p_area  = pi/4.0_wp * d_c**2
      sp2%statusb = IBSET( sp2%statusb, sp_stat%liquidOut)
      sp2%statusb = IBSET( sp2%statusb, sp_stat%liquidSurf)
      sp2%phi     = 1._wp
      sp2%v       = vterm_snow( atmo, sp2 )
      v_c         = sp2%v
      !v_c     = vterm_beard( r_c ) 
      IF ( (sp1%v - v_c) .GT. 0._wp ) THEN                ! set SIG in boehmVel to 0. to prevent jumps near v = v_c
        ce_boehm(i,j) = Ecbohm_riming( atmo, sp1, d_c )
      ELSE
        ce_boehm(i,j) = 0._wp
      END IF
      CD(i,j)       = 2._wp * (sp1%m_w + sp1%m_i + sp1%m_r + sp1%m_f) * grav / (atmo%rho * sp1%v**2 * sp1%p_area)

      vel_rel(i,j) = (sp1%v-v_c)

      ! Stokes number (eq. 4, Rasmussen and Heymsfield '85)
      d = sp1%d * MIN( 1.0_wp, 1._wp / SQRT( sp1%phi ) )       ! correction in case of prolate particles
      K = 4_wp * rhol * vel_rel(i,j) * r_c**2 / (9._wp * atmo%eta * d)
      St(i,j)      = K
      N_Re(i,j)    = ABS(v_c - sp1%v) * sp1%d * atmo%rho * atmo%etai
      N_ReBig(i,j) =           sp1%v  * sp1%d * atmo%rho * atmo%etai

      ce_BeardGrover(i,j) = EcBeardGrover( atmo, sp1, r_c )
      CALL EcRobinSlinnHolger( atmo, sp1, r_c, ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j) )

      IF (sp1%v .GT. v_c) THEN
        ! bulk collision efficiency w.r.t. average droplet (Cober and List '93)
        ce_bulk(i,j) = MIN(MAX( 0.55_wp * LOG10(2.51_wp * K), 0._wp ), 1.0_wp)
 
        ! collision efficiency of spheres based on Hall-Beheng table
        ce_spheres(i,j) = ecollBeheng(100.0_wp * sp1%d / 2.0_wp, 100.0_wp * r_c)

        rho_rime(i,j)   = rho_rime_CL(sp1, atmo, K, r_c, vel_rel(i,j))
      ELSE
        ce_bulk(i,j)    = 0.0
        ce_spheres(i,j) = 0.0
        rho_rime(i,j)   = 0.0
      END IF

    END DO
  END DO
  WRITE(*,*) "rho ", atmo%rho, " eta ", atmo%eta
  WRITE(fname,'(a,i3.3,a,i3.3,a,a,a)') "colleff_riming_Fr",int(Frime*100),"_rhor",int(rhor),"_drop_spectrum_",TRIM(ty),".dat"
  PRINT *, "   -> Writing ", fname

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
     OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
     OPEN(74, file=fname, status="new", action="write")
  END IF

  DO j = 1, jpts
     DO i = 1, npts
        WRITE(74,'(14e15.7)') d1(i,j), d2(i,j), ce_boehm(i,j), ce_bulk(i,j), &
             &                                  ce_spheres(i,j), rho_rime(i,j), vel_rel(i,j), St(i,j), N_Re(i,j), N_ReBig(i,j), &
             &                ce_BeardGrover(i,j), ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j)
     END DO
     WRITE(74,*) ""
  END DO
  CLOSE(74)

END SUBROUTINE plot_colleffi_riming_drop_spectrum


!
! plot collision efficiency w.r.t. drop radius (Boehm 1992 Part II)
!
SUBROUTINE plot_colleffi_drops()

  INTEGER, PARAMETER :: npts = 200
  INTEGER  :: i, j 
  REAL(wp) :: m1(npts), K, d, d_c, v_c, r_c
  CHARACTER(len=256) :: fname
  LOGICAL      :: file_exists
  TYPE(t_sp)   :: sp1
  TYPE(t_atmo) :: atmo

  !  drops
  INTEGER, PARAMETER :: jpts = 7
  REAL(wp) :: di(jpts) = (/ 28.8, 36.4, 46.2, 63.4, 81.4, 105.6, 152. /)       ! diameters B92b Fig. 1, Hall comparison
  ! REAL(wp) :: di(jpts) = (/ 60., 80., 110., 146., 226., 310., 500. /)         ! diameter B92b Fig. 3, multi comparison
  REAL(wp) :: height  = 0._wp                                                  ! corresponds to -0°C

  REAL(wp)     :: ce_boehm(npts,jpts), ce_bulk(npts,jpts), ce_spheres(npts,jpts), &
                  ce_BeardGrover(npts,jpts), ce_Robin(npts,jpts), ce_Slinn(npts,jpts), ce_Holger(npts,jpts), &
                  d1(npts,jpts), d2(npts,jpts), rho_rime(npts,jpts), vel_rel(npts,jpts), St(npts,jpts), N_Re(npts,jpts), &
                  N_ReBig(npts,jpts)

  REAL(wp), PARAMETER :: lambda = 4.7_wp/1000.0_wp
  
  DO j=1,jpts
    sp1%d   = di(j) * 1e-6
    sp1%phi = exp(-sp1%d/lambda) + (1.0_wp - exp(-sp1%d/lambda))/(1.0_wp+sp1%d/lambda)
     
    m1(j)   = pi/6._wp * sp1%d**3 * rhol
    sp1%z   = height 
    sp1%m_w = REAL(m1(j),wp) 
    ! sp1%V_i = sp1%m_i / rhoi
    ! sp1%V_r = sp1%m_r / rhor
    sp1%m_i = 0.0_wp
    sp1%m_r = 0.0_wp

    CALL get_atmo( sp1%z, atmo )

    ! make sure that particle properties are set
    CALL m2d_particle( sp1 )

    sp1%v = vterm_snow( atmo, sp1 )

    DO i=1, npts
      d1(i,j) = sp1%d
      d2(i,j) = i * 1e-6
      d_c     = d2(i,j)
      r_c     = 0.5_wp * d_c
      v_c     = vterm_beard( r_c ) 

      ce_boehm(i,j) = Ecbohm_riming( atmo, sp1, d_c )

      vel_rel(i,j) = ABS(v_c-sp1%v)

      ! Stokes number (eq. 4, Rasmussen and Heymsfield '85)
      d = sp1%d * MIN( 1.0_wp, 1._wp / SQRT( sp1%phi ) )       ! correction in case of prolate particles
      K = 4_wp * rhol * vel_rel(i,j) * r_c**2 / (9._wp * atmo%eta * d)
      St(i,j)      = K
      N_Re(i,j)    = ABS(v_c - sp1%v) * sp1%d * atmo%rho * atmo%etai
      N_ReBig(i,j) =           sp1%v  * sp1%d * atmo%rho * atmo%etai

      ce_BeardGrover(i,j) = EcBeardGrover( atmo, sp1, r_c )
      CALL EcRobinSlinnHolger( atmo, sp1, r_c, ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j) )

      IF (sp1%v .GT. v_c) THEN
        ! bulk collision efficiency w.r.t. average droplet (Cober and List '93)
        ce_bulk(i,j) = MIN(MAX( 0.55_wp * LOG10(2.51_wp * K), 0._wp ), 1.0_wp)
 
        ! collision efficiency of spheres based on Hall-Beheng table
        ce_spheres(i,j) = ecollBeheng(100.0_wp * sp1%d / 2.0_wp, 100.0_wp * r_c)

        ! rime density
        rho_rime(i,j) = rho_rime_CL(sp1, atmo, K, r_c, vel_rel(i,j))
      ELSE
        ce_bulk(i,j)    = 0.0
        ce_spheres(i,j) = 0.0
        rho_rime(i,j)   = 0.0
      END IF

    END DO
  END DO
  WRITE(fname,'(a)') "colleff_riming_drops_spectrum.dat"
  PRINT *, "   -> Writing ", fname

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
     OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
     OPEN(74, file=fname, status="new", action="write")
  END IF

  DO j = 1, jpts
     DO i = 1, npts
        WRITE(74,'(14e14.7)') d1(i,j), d2(i,j), ce_boehm(i,j), ce_bulk(i,j), &
             &                                  ce_spheres(i,j), rho_rime(i,j), vel_rel(i,j), St(i,j), N_Re(i,j), N_ReBig(i,j), &
             &                ce_BeardGrover(i,j), ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j)
     END DO
     WRITE(74,*) ""
  END DO
  CLOSE(74)


END SUBROUTINE plot_colleffi_drops

!
! plot collision efficiency for aggregation (Boehm 1992 Part III)
!
SUBROUTINE plot_colleffi_aggregation(Frime,rhor)

  REAL(wp) :: Frime,rhor

  INTEGER  :: i, j 
  CHARACTER(len=256) :: fname
  LOGICAL      :: file_exists
  TYPE(t_sp)   :: sp1, sp2
  TYPE(t_atmo) :: atmo

! Boehm Data!
  ! simple hexagonal plates
  ! INTEGER, PARAMETER :: jpts = 10 
  ! INTEGER, PARAMETER :: npts = 180
  ! REAL(wp) :: a(jpts) = (/ 51.,88.,113.,147.,160.,194.,213.,289.,404.,639. /) ! basal facet dimension
  ! REAL(wp) :: c(jpts) = (/ 2.55,4.4,5.65,7.35,8.,9.7,10.65,14.45,20.2,31.95/) ! prism facet dimension                                             
  ! INTEGER  :: ty = 1
  ! REAL(wp) :: rho_cryst = rhoi

  ! ! hexagonal columns / cylinders
  ! INTEGER, PARAMETER :: jpts = 8 
  ! INTEGER, PARAMETER :: npts = 280
  ! REAL(wp) :: a(jpts) = (/ 23.5,32.7,36.6,41.5,53.4,77.2,107.,146. /)           ! basal facet dimension
  ! REAL(wp) :: c(jpts) = (/ 32.9,45.78,54.9,70.55,117.48,254.76,535.,1211.8 /)   ! prism facet dimension
  ! INTEGER  :: ty = 2  
  ! REAL(wp) :: rho_cryst = rhoi

  ! branched crystals
  INTEGER, PARAMETER :: jpts = 8
  INTEGER, PARAMETER :: npts = 1600
  REAL(wp) :: a(jpts) = (/ 104.,134.,174.,205.,255.,348.,487.,778. /)           ! basal facet dimension
  REAL(wp) :: c(jpts) = (/ 5.2,6.7,8.7,10.25,12.75,17.4,24.35,38.9 /)           ! prism facet dimension
  INTEGER  :: ty = 3  
  REAL(wp) :: rho_cryst = .6*rhoi!600._wp


  REAL(wp)     :: ce_boehm(npts,jpts), ce_bulk(npts,jpts), ce_spheres(npts,jpts), &
                  ce_BeardGrover(npts,jpts), ce_Robin(npts,jpts), ce_Slinn(npts,jpts), ce_Holger(npts,jpts), &
                  d1(npts,jpts), d2(npts,jpts), rho_rime(npts,jpts), vel_rel(npts,jpts), St(npts,jpts), N_Re(npts,jpts), &
                  N_ReBig(npts,jpts), CD(npts,jpts)

  REAL(wp) :: K, d, rho_app, V_i(npts), m1(npts), xi, rm

  DO j=1,jpts
    sp1%d   = 2._wp * MAX(a(j), c(j)) * 1e-6
    sp1%phi = c(j) / a(j)

    V_i(j)  = 4. / 3. * pi * ( a(j) * 1e-6)**2 * ( c(j) * 1e-6)
    m1(j)   = V_i(j) * rho_cryst
    sp1%z   = 2000._wp 
    sp1%m_i = REAL(m1(j),wp) * (1-Frime)
    sp1%m_r = REAL(m1(j),wp) * Frime
    ! sp1%V_i = sp1%m_i / rhoi
    sp1%V_r = sp1%m_r / rhor
    sp1%m_w = 0.0_wp

    CALL get_atmo( sp1%z, atmo )

    ! make sure that particle properties are set
    ! CALL m2d_particle( sp1 )

    ! reduction of effective area when branching (only for oblates), not for hollowing 
    IF (sp1%phi > 1._wp) THEN
      sp1%p_area = pi * (a(j) * 1e-6) * (c(j) * 1e-6)
      ! sp1%p_area = 4._wp * (a(j) * 1e-6) * (c(j) * 1e-6)
    ELSE
      ! For aggregation 'Ec defined according to the circumscribed cross-sec area of collector' B92P3 P.116
      rho_app    = ( (sp1%m_i + sp1%m_r) / (sp1%V_i + sp1%V_r) )
      xi         = (1._wp - sp1%phi) * (rho_app * rhoii) + sp1%phi        
      sp1%p_area = xi * pi * (a(j) * 1e-6)**2 
    END IF 

    sp1%v = vterm_snow( atmo, sp1 )
    IF (sp1%phi  .GT. 1._wp) THEN
      d = sp1%d / sp1%phi 
    ELSE
      d = sp1%d
    END IF

    sp2%z       = sp1%z
    DO i=1, npts
      d1(i,j)     = sp1%d

      sp2%d       = i * 1e-6
      d2(i,j)     = sp2%d
      sp2%phi     = 0.05_wp
      sp2%m_i     = pi/6.0_wp * sp2%d**3 * sp2%phi * rho_cryst
      sp2%m_r     = 0._wp
      sp2%V_r     = sp2%m_r / rhor
      sp2%m_w     = 0.0_wp
      sp2%p_area  = .5_wp * (pi/4._wp * sp2%d**2)

      sp2%v = vterm_snow( atmo, sp2 )

      IF ((sp1%v-sp2%v) .GT. 0._wp) THEN
        ce_boehm(i,j) = Ecbohm_spheroid( atmo, sp1, sp2 )
      ELSE
        ce_boehm(i,j) = 0._wp
      END IF
      CD(i,j)       = 2._wp * (sp1%m_w + sp1%m_i + sp1%m_r + sp1%m_f) * grav / (atmo%rho * sp1%v**2 * sp1%p_area)

      vel_rel(i,j) = ABS(sp2%v-sp1%v)

      ! Stokes number (eq. 4, Rasmussen and Heymsfield '85)
      d = sp1%d * MIN( 1.0_wp, 1._wp / SQRT( sp1%phi ) )       ! correction in case of prolate particles
      K = 4_wp * rhol * vel_rel(i,j) * rm**2 / (9._wp * atmo%eta * d)
      St(i,j)      = K
      N_Re(i,j)    = vel_rel(i,j) * sp1%d * atmo%rho * atmo%etai
      ! N_ReBig(i,j) =           sp1%v  * sp1%d * atmo%rho * atmo%etai
      N_ReBig(i,j) = sp1%v  * d * atmo%rho * atmo%etai

      ce_BeardGrover(i,j) = EcBeardGrover( atmo, sp1, (d2(i,j)*.5) )
      CALL EcRobinSlinnHolger( atmo, sp1, (d2(i,j)*.5), ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j) )

      IF (sp1%v .GT. sp2%v) THEN
        ! bulk collision efficiency w.r.t. average droplet (Cober and List '93)
        ce_bulk(i,j) = MIN(MAX( 0.55_wp * LOG10(2.51_wp * K), 0._wp ), 1.0_wp)
 
        ! collision efficiency of spheres based on Hall-Beheng table
        ce_spheres(i,j) = ecollBeheng(100.0_wp * sp1%d / 2.0_wp, 100.0_wp * (d2(i,j)*.5))

        ! rime density
        rho_rime(i,j) = rho_rime_CL(sp1, atmo, K, rm, vel_rel(i,j))
      ELSE
        ce_bulk(i,j)    = 0.0
        ce_spheres(i,j) = 0.0
        rho_rime(i,j)   = 0.0
      END IF

    END DO
  END DO
  WRITE(fname,'(a,i3.3,a,i3.3,a,i3.3,a)') "colleff_riming_Fr",int(Frime*100),"_rhor",int(rhor),"_aggregation_",int(ty),".dat"
  PRINT *, "   -> Writing ", fname

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
     OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
     OPEN(74, file=fname, status="new", action="write")
  END IF

  DO j = 1, jpts
     DO i = 1, npts
        WRITE(74,'(14e14.7)') d1(i,j), d2(i,j), ce_boehm(i,j), ce_bulk(i,j), &
             &                                  ce_spheres(i,j), rho_rime(i,j), vel_rel(i,j), St(i,j), N_Re(i,j), N_ReBig(i,j), &
             &                ce_BeardGrover(i,j), ce_Robin(i,j), ce_Slinn(i,j), ce_Holger(i,j)
     END DO
     WRITE(74,*) ""
  END DO
  CLOSE(74)

END SUBROUTINE plot_colleffi_aggregation


!
! plot collision efficiency for two particles wrt. their AR
!
SUBROUTINE plot_colleffi_aggregation_phi()

  TYPE(t_sp)   :: sp1, sp2
  TYPE(t_atmo) :: atmo

  INTEGER, PARAMETER :: jpts = 925
  INTEGER  :: i, j
  LOGICAL  :: file_exists
  REAL(wp) :: Frime, r_a, r_c, stretch, phi_0, ce_boehm, kern, m1, m2, geom, r1, r2, V_tot

  CHARACTER(len=256) :: fname

! Definitions
  m1     = 6.55e-8                       !mass of a sphere of pure ice with d = 5 mm
  ! m2     = 7.5e-12_wp                  ! mass of a sphere of pure ice with d = 25 mu m
  m2     = 6e-11_wp                    ! mass of a sphere of pure ice with d = 50 mu m
  ! m2     = 4.8e-10_wp                  ! mass of a sphere of pure ice with d = 100 mu m
  Frime = 0._wp
  sp1%z = 2000._wp
  sp2%z = sp1%z

  CALL get_atmo( sp1%z, atmo )

  phi_0   = 0.01
  stretch = 1.01
!-------------------------------------------------------------------------------------------------
! OUTPUT
  WRITE(fname,'(a,f4.2,a)') "colleff_riming_aggregation_phi_",real(phi_0),"-100.dat"

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
     OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
     OPEN(74, file=fname, status="new", action="write")
  END IF
!-------------------------------------------------------------------------------------------------
  DO j=1, jpts
    sp1%phi = phi_0 * stretch**(j-1)
    sp1%V_i = m1 / rhoi
    sp1%m_i = REAL(m1,wp) * (1-Frime)
    sp1%m_r = REAL(m1,wp) * Frime
    sp1%V_r = 0._wp!sp1%m_r / rhor
    sp1%m_w = 0.0_wp

    CALL spheroid_geometry(sp1, r_a, r_c, V_tot)
    sp1%d      = 2._wp * max(r_a, r_c)
    sp1%p_area = pi * r_a * max(r_a, r_c)

    sp1%v = vterm_snow( atmo, sp1 )

    DO i=1, jpts
      sp2%phi = phi_0 * stretch**(i-1)
      sp2%m_i = REAL(m2,wp) * (1-Frime)
      sp2%m_r = REAL(m2,wp) * Frime
      sp2%V_r = 0._wp!sp2%m_r / rhor
      sp2%V_i = m2 / rhoi
      sp2%m_w = 0.0_wp

      CALL spheroid_geometry(sp2, r_a, r_c, V_tot)
      sp2%d      = 2._wp * max(r_a, r_c)
      sp2%p_area = pi * r_a * max(r_a, r_c)
      
      sp2%v      = vterm_snow( atmo, sp2 )

      ce_boehm = Ecbohm_spheroid( atmo, sp1, sp2 )
      ! kern     = pi * (0.5*(sp1%d+sp2%d))**2 * ce_boehm * ABS(sp1%v - sp2%v)
      IF (coll_kern_type == 0) THEN
        r1   = 0.5_wp * sp1%d * MIN( 1.0_wp, 1 / SQRT( sp1%phi ) )
        r2   = 0.5_wp * sp2%d * MIN( 1.0_wp, 1 / SQRT( sp2%phi ) )
        geom = (r1 + r2)**2 
      ELSE
        geom = (0.5*(sp1%d+sp2%d))**2
      END IF
      kern     = pi * geom * ce_boehm * ABS(sp1%v - sp2%v)

      ! WRITE(*,*) ce_boehm(i,j), sp1%v, sp2%v
      WRITE(74,'(f10.6,a,f10.6,a,f7.5,a,f10.6,a,f10.6,a,f14.10)') sp1%phi, "  " ,sp2%phi, "   ",  &
                ABS(sp1%v - sp2%v), "  ", geom, "  ", ce_boehm, "  ", kern
       
    END DO
  END DO

  CLOSE(74)

END SUBROUTINE plot_colleffi_aggregation_phi

!
! check pressure dependency of Boehms collision efficiency (similar to B92P2 Fig. 5-7)
SUBROUTINE plot_colleffi_pressure_dependency()

  TYPE(t_atmo) :: atmo
  TYPE(t_sp)   :: sp1, sp2

  REAL(wp)     :: m1, m2, rho_p, d
  INTEGER      :: i, j
  LOGICAL      :: file_exists
  INTEGER, PARAMETER  :: jpts = 120
  REAL(wp), PARAMETER :: lambda = 4.7_wp/1000.0_wp
  CHARACTER(len=256)  :: fname
  REAL(wp), DIMENSION(3)    :: p, T
  REAL(wp), DIMENSION(jpts) :: d2, ce_boehm, kernel, Re
  CHARACTER(len=256), DIMENSION(3) :: for

  for = (/'(a,i4,a)','(a,i3,a)','(a,i3,a)'/)
  ! density corresponding to p = 1000, 750 and 500 hP and temperatures given
  ! for drops adopted from Pinsky et al. 2001
  T   = (/291.15,271.15,251.15/)
  rho_p = rhol
  ! for ice particles
  ! T   = (/273.15,266.15,258.15/)
  ! rho_p = rhoi
  p   = (/100000., 75000., 50000./)


  sp1%d   = 60. * 1e-6
  sp1%phi = exp(-sp1%d/lambda) + (1.0_wp - exp(-sp1%d/lambda))/(1.0_wp+sp1%d/lambda)
  sp2%phi = exp(-sp2%d/lambda) + (1.0_wp - exp(-sp2%d/lambda))/(1.0_wp+sp2%d/lambda)
     
  m1      = pi/6._wp * sp1%d**3 * rho_p
  sp1%z   = 100._wp
  sp1%m_w = REAL(m1,wp) 
  sp1%m_i = 0.0_wp !REAL(m1,wp) 
  ! sp1%m_w = 0.0_wp 
  ! sp1%m_i = REAL(m1,wp) 
  sp1%m_r = 0.0_wp

  DO i = 1, 3
    WRITE(fname,for(i)) "drops_p",int(p(i)/100),".dat"
    PRINT *, "   -> Writing ", fname

    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
     OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
     OPEN(74, file=fname, status="new", action="write")
    END IF

    atmo%T     = T(i)
    atmo%psatw = psat_water( atmo%T )
    atmo%psati = psat_ice( atmo%T )
    atmo%rh    = ssat
    atmo%pv    = ssat/100.0_wp*atmo%psatw 
    atmo%ssat  = atmo%pv/atmo%psati - 1
    atmo%eta   = dynamic_viscosity( atmo%T )
    atmo%etai  = 1._wp / atmo%eta

    atmo%rho   = p(i) / ( T(i) * Rd) !rho(i)
    atmo%p     = p(i)
    atmo%qv    = 1.0_wp/(1.0_wp + (atmo%p/atmo%pv-1.0_wp)*Rv/Rd)  
    atmo%qc    = 0.0_wp 
    sp1%V_i    = sp1%m_i / rhoi
    ! sp1%V_r = sp1%m_r / rhor
    ! make sure that particle properties are set
    CALL m2d_particle( sp1 )

    sp1%v = vterm_snow( atmo, sp1 )

    WRITE(74,'(a,f6.1,a,f6.2,a,f6.4)') ' p = ', p(i)/100, ', T = ', T(i), ', rho = ', atmo%rho
    DO j=1, jpts
      d2(j) = j * 0.5e-6
      m2    = pi/6._wp * d2(j)**3 * rho_p
      
      sp2%d   = d2(j)
      sp2%z   = 100._wp
      sp2%m_w = REAL(m2,wp) 
      sp2%m_i = 0._wp !REAL(m2,wp)
      ! sp2%m_w = 0._wp 
      ! sp2%m_i = REAL(m2,wp)  
      sp2%V_i = sp2%m_i / rhoi
      sp2%m_r = 0._wp

      CALL m2d_particle( sp2 )
      sp2%v = vterm_snow( atmo, sp2 )

      ce_boehm(j) = Ecbohm_spheroid( atmo, sp1, sp2 )
      kernel(j)   = pi * (SQRT(sp1%p_area / pi) + SQRT(sp2%p_area / pi))**2 * ce_boehm(j) * ABS(sp1%v - sp2%v)
      IF (sp1%phi > 1.) THEN
        d = sp1%d / sp1%phi
      ELSE
        d = sp1%d
      END IF
      Re(j)       = sp1%v * d * atmo%rho * atmo%etai

      WRITE(74,'(7e14.7)') sp1%d, sp2%d, ce_boehm(j), kernel(j), sp1%v, sp2%v, Re(j)
    END DO
    CLOSE(74)
  END DO

END SUBROUTINE plot_colleffi_pressure_dependency

!
! Comparison of fall speed parameterization of ice particles with changing aspect ratio
SUBROUTINE plot_vt_boehm( cylinder )
  LOGICAL,      INTENT(in)    :: cylinder

  INTEGER,  PARAMETER :: npts = 500

  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo

  INTEGER      :: i, j
  REAL(wp)     :: Frime, vt_hw(npts),vt_kc(npts), phi_0, r_a, r_c, stretch, rhor, V_tot
  REAL(wp)     :: N_Re, X, X_pro, vterm_bohm, q, N_Re_pro, mtot, k_pro, k
  REAL(wp)     :: gama_big, gama_big_pro, d, X_0, alpha, vterm_bohm_pro, logm, fx, alf, beta
  REAL(wp), DIMENSION(5):: m_set
  CHARACTER(len=256) :: fname 
  LOGICAL  :: file_exists, prolate_intp

  !cylinder = .FALSE.
  prolate_intp = .FALSE.

  sp%z  = 2000._wp
  CALL get_atmo( sp%z, atmo )
  WRITE(*,*) "rho ", atmo%rho , " eta ", atmo%eta

  !m_set = (/9.18935916e-10, 3.94643958e-09, 1.48535394e-08, 5.24548070e-08, 1.81124049e-07/)
  m_set = (/4.74869608e-09, 2.03936334e-08, 7.67571963e-08, 2.71065623e-07, 9.35977197e-07/)
  WRITE(*,*) "mass ", m_set
  DO j = 1, 5
    IF (cylinder .EQV. .TRUE.) THEN
      WRITE(fname,'(a,i1,a)') "vt_boehm_vary_phi_cylinder_m",int(j),".dat"
    ELSE
      WRITE(fname,'(a,i1,a)') "vt_boehm_vary_phi_prolate_m",int(j),".dat"
    END IF

    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF
    mtot   = m_set(j)  ! mass corresponding to test particle chosen from TRAIL   1.482900926877611e-08_wp    
    Frime  = 0._wp
    rhor   = 200._wp
    sp%m_i = REAL(mtot,wp) * (1._wp-Frime)
    sp%m_r = REAL(mtot,wp) * Frime
    sp%V_i = sp%m_i / rhoi
    sp%V_r = sp%m_r / rhor
    sp%m_w = 0.0_wp
    sp%m_f = 0._wp
    sp%mm  = 1._wp
    
    phi_0 = 0.1_wp
    stretch = 1.01

    DO i = 1, npts
      sp%phi = phi_0 * stretch**(i-1)
      ! sp%phi  = phi_0
      ! m       = z4pi_3 * ( i / 2._wp * 1e-6)**3 * rhoi

      CALL spheroid_geometry(sp, r_a, r_c, V_tot)

      IF( BTEST(sp%statusb,sp_stat%liquidOut)  ) THEN
      !IF( BTEST(sp%statusb,sp_stat%liquidSurf) ) THEN   ! smooth
        X_0 = 6.7D6
      ELSE                                              ! rough
        X_0 = 2.8D6
      ENDIF

      sp%d      = 2._wp * max(r_a, r_c)
      IF (sp%phi .GT. 1._wp) THEN
        sp%p_area = pi * r_a * r_c
      ELSE
        sp%p_area = pi * r_a**2
      END IF
      ! characteristic length is defined regarding the radius/a-axis, transformation needed for prolates
      IF (sp%phi .GT. 1._wp) THEN   
        IF ( cylinder ) THEN
          q = z4_pi
          d = (4.0_wp*V_tot/pi/sp%phi)**(1.0_wp/3.0_wp)
        ELSE
          q = sp%p_area / (pi_4 * sp%d**2 / sp%phi)
          d = sp%d / sp%phi                                
        END IF  
      ELSE
        q = sp%p_area / (pi_4 * sp%d**2)
        d = sp%d
      END IF

      ! cylindrical assumption q = 4/pi
      N_Re       = Reynolds_Boehm(atmo, sp%phi, mtot, q, X_0, k, gama_big, X)
      vterm_bohm = N_Re * atmo%eta / (d * atmo%rho)         ! I (22)
      ! possible interpolation between prolate and cylindrical geometry.
      ! TRAIL data (McCorquodale & Westbrook 2020) show transition instead of sudden jump
      IF ( (sp%phi .GT. 1._wp) .AND. (prolate_intp) ) THEN
        ! prolate assumption q = A / A_ce 
        N_Re_pro       = Reynolds_Boehm(atmo, sp%phi, mtot, sp%p_area / (pi_4 * sp%d**2 / sp%phi),    &
                                  X_0, k_pro, gama_big_pro, X_pro)
        vterm_bohm_pro = N_Re_pro * atmo%eta / (d * atmo%rho)

        logm        = LOG(mtot)
        alf         = 8.60e-2_wp * logm + 1.722 
        beta        = 3.08e-2_wp * logm + 1.691  

        fx          = MIN(1._wp, beta * exp(-alf * sp%phi))         ! limiter to prevent overestimation
        vterm_bohm  = fx * vterm_bohm_pro + (1._wp - fx) * vterm_bohm
      END IF

      vt_hw(i) = vterm_hw10( atmo, sp )
      vt_kc(i) = vterm_kc05( atmo, sp )
  !---------------------------------------------------------------------------------------------------
      WRITE(74,'(8e14.7)') REAL(i), mtot, sp%phi, vterm_bohm, vt_hw(i), vt_kc(i), N_Re, X
    END DO
    CLOSE(74)
  END DO
END SUBROUTINE plot_vt_boehm

!
! Subroutine that outputs Re, X & C_d, switch can be used to check behavior of prolates vs.
! cylinders
SUBROUTINE plot_boehm_westbrook( cylinder )
  LOGICAL,      INTENT(in)    :: cylinder

  INTEGER,  PARAMETER :: npts = 10000

  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo

  INTEGER                :: i, j, ii
  REAL(wp)               :: Frime, m, r_a, r_c, N_Re, Cd, X, d, Cd2, q, mtot, N_Re_d, Ar, V_tot
  REAL(wp)               :: beta, gama_small, gama_big, N_Re0, C_DO, C_DP, C_DP_prim, X_0, k
  REAL(wp), PARAMETER, DIMENSION(8) :: phi = [0.10, 1.0, 1.2, 2.0, 3.0, 5.0, 10.0, 50.0]
  !REAL(wp), DIMENSION(5) :: A_r
  CHARACTER(len=256)     :: fname 
  LOGICAL                :: file_exists

  ! sp%phi   = 0.100_wp
  !phi      = (/0.04, 0.10, 1.00, 1.0476, 2.00, 5.00/)
  !A_r      = (/0.2 , 0.4 , 0.6 , 0.8 , 1.0 /)
  sp%z     = 2000._wp
  Frime    = 0._wp
  
  CALL get_atmo( sp%z, atmo )

  WRITE(*,*) "boehm_westbrook rho", atmo%rho, " eta ", atmo%eta

  DO j = 1, SIZE(phi)
    sp%phi = phi(j)
    !DO ii = 1,5
      IF (cylinder .EQV. .TRUE.) THEN
        IF (sp%phi >= 1.) THEN
          WRITE(fname,'(a,f5.2,a)') "vt_boehm_westbrook_cylinder_phi",sp%phi,".dat"
          Ar = 4.0_wp/pi/sp%phi
        ELSE
          CYCLE
        ENDIF
      ELSE
        WRITE(fname,'(a,f5.2,a)') "vt_boehm_westbrook_prolate_phi",sp%phi,".dat"
        IF( sp%phi > 1.0_wp ) THEN
          Ar = 1.0_wp/sp%phi
        ELSE
          Ar = 1.0_wp
        ENDIF
      END IF
      INQUIRE(FILE=fname, EXIST=file_exists)
      IF( file_exists ) THEN
          OPEN(74, file=fname, status="old", position="append", action="write")
      ELSE
          OPEN(74, file=fname, status="new", action="write")
      END IF
      
      DO i = 1, npts
        sp%V_i  = z4pi_3 * ( i / 2._wp * 1e-5)**3 
        m       = sp%V_i * rhoi

        sp%m_i = REAL(m,wp) * (1._wp-Frime)
        sp%m_r = REAL(m,wp) * Frime
        sp%V_r = 0._wp!sp%m_r / rhor
        sp%m_w = 0._wp
        sp%m_f = 0._wp

        mtot = (sp%m_w + sp%m_i + sp%m_r + sp%m_f)

        !IF ( (cylinder .EQV. .TRUE.) .AND. (sp%phi .GT. 1_wp) ) THEN
        !  r_a = ( sp%V_i / (2. * pi * sp%phi ) )**z1_3 
        !ELSE
          CALL spheroid_geometry(sp, r_a, r_c, V_tot)
        !END IF

        sp%d = 2._wp * max(r_a, r_c)

        IF (sp%phi >= 1._wp) THEN
          IF (cylinder .EQV. .TRUE.) THEN
            d = (4.0_wp*V_tot/(pi*sp%phi))**(1.0_wp/3.0_wp)
            !d = (2.0_wp*V_tot/(sqrt(3.0_wp)*sp%phi))**(1.0_wp/3.0_wp)
            !WRITE(*,*) "Boehm d ", d , (4.0_wp*V_tot/(pi*sp%phi))**(1.0_wp/3.0_wp), " A ", d**2 * sp%phi, r_a * r_c * pi
            sp%p_area =  d**2 * sp%phi ! r_a * r_c * pi ! 4.0_wp ! pi * r_a * r_c
            !WRITE(*,*) "Boehm d**2/A ", d**2/sp%p_area
            q = z4_pi
          ELSE
            d = sp%d / sp%phi
            sp%p_area = pi * r_a * r_c
            q = sp%p_area / (pi_4 * sp%d**2/sp%phi)!Ar
          END IF
        ELSE
          d = sp%d
          sp%p_area = Ar * pi * r_a**2
          q = sp%p_area / (pi_4 * sp%d**2)
        END IF

        CALL hydrodynamicValuesBoehm( sp%phi, q, k, gama_big, C_DP )
        X_0 = 2.8D6
        !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)*q**(1.0_wp/4.0_wp))
        !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)*MAX(q**(1.0_wp/4.0_wp),q))
        X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)*MIN(q**(1.0_wp/4.0_wp),1.0_wp))
        !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)) !*q**(1.0_wp/4.0_wp))
        C_DP_prim = C_DP*((1.0_wp+1.6_wp*(X/X_0)**2)/(1.0_wp+(X/X_0)**2))
        beta = SQRT(1.0_wp+C_DP_prim/6.0_wp/k*SQRT(X/C_DP_prim))-1
        N_Re0 = 6.0_wp*k/C_DP_prim*beta**2
        C_DO = 4.5_wp*k**2*MAX(sp%phi,1.0_wp)
        gama_small = (C_DO - C_DP)/4.0_wp/C_DP
        N_Re  = N_Re0*(1.0_wp + (2.0_wp*beta*EXP(-beta*gama_small))/((2.0_wp+beta)*(1.0_wp+beta)) )


        sp%v = N_Re * atmo%eta / (d * atmo%rho)
        Cd2  = 2._wp * (sp%m_w + sp%m_i + sp%m_r + sp%m_f) * grav / (atmo%rho * sp%v**2 * sp%p_area)
        !IF( cylinder .EQV. .TRUE. ) THEN
        !  WRITE(*,*) " v ", sp%v, N_Re * atmo%eta / (sp%d/sp%phi * atmo%rho)
        !  WRITE(*,*) "Cd2 ", Cd2, 2._wp*V_tot*rhoi*grav/(atmo%rho * (N_Re*atmo%eta/(sp%d/sp%phi*atmo%rho))**2 * sp%p_area)
        !ENDIF
        !Cd2  = pi / 2. * d/sp%v**2 * (rhoi - atmo%rho) * grav / atmo%rho
        N_Re = sp%v * d * atmo%rho * atmo%etai
        N_Re_d = sp%v * ( 2.* ( z3_4pi * sp%V_i  )**z1_3 ) * atmo%rho * atmo%etai
        Cd   = X / N_Re**2

        WRITE(74,'(12e14.7)') REAL(i), m, sp%phi, sp%v, sp%d, Cd, Cd2, N_Re, N_Re_d, X, q, Ar
        !                        1     2      3     4    5    6    7    8      9    10  11 12
      END DO

      CLOSE(74)
    !END DO
  END DO

END SUBROUTINE plot_boehm_westbrook


SUBROUTINE plot_boehm_westbrook_phis( cylinder )
  LOGICAL,      INTENT(in)    :: cylinder

  INTEGER,  PARAMETER :: npts = 3000

  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo

  INTEGER                :: i, j, ii
  REAL(wp)               :: Frime, m, r_a, r_c, N_Re, Cd, X, d, Cd2, q, mtot, N_Re_d, Ar, V_tot
  REAL(wp)               :: beta, gama_small, gama_big, N_Re0, C_DO, C_DP, C_DP_prim, X_0, k
  REAL(wp), PARAMETER, DIMENSION(10) :: phi = [1.0, 1.2, 2.0, 3.0, 4.0, 5.0, 8.0, 10.0, 16.0, 32.0] 
  !REAL(wp), DIMENSION(5) :: A_r
  CHARACTER(len=256)     :: fname 
  LOGICAL                :: file_exists

  ! sp%phi   = 0.100_wp
  !phi      = (/0.04, 0.10, 1.00, 1.0476, 2.00, 5.00/)
  !A_r      = (/0.2 , 0.4 , 0.6 , 0.8 , 1.0 /)
  sp%z     = 2000._wp
  Frime    = 0._wp
  
  CALL get_atmo( sp%z, atmo )

  WRITE(*,*) "boehm_westbrook rho", atmo%rho, " eta ", atmo%eta

  IF (cylinder .EQV. .TRUE.) THEN
     WRITE(fname,'(a,f4.2,a)') "vt_boehm_westbrook_cylinder.dat"
  ELSE
     WRITE(fname,'(a,f4.2,a)') "vt_boehm_westbrook_prolate.dat"
  END IF
  OPEN(75, file=fname, status="new", action="write")

  DO j = 1, SIZE(phi)
    sp%phi = phi(j)
    !DO ii = 1,5
      IF (cylinder .EQV. .TRUE.) THEN
        IF (sp%phi >= 1.) THEN
          Ar = 4.0_wp/pi/sp%phi
        ELSE
          CYCLE
        ENDIF
      ELSE
        IF( sp%phi >= 1.0_wp ) THEN
          Ar = 1.0_wp/sp%phi
        ELSE
          Ar = 1.0_wp
        ENDIF
      END IF
      
      DO i = 1, npts
        sp%V_i  = z4pi_3 * ( i / 2._wp * 1e-5)**3 
        m       = sp%V_i * rhoi

        sp%m_i = REAL(m,wp) * (1._wp-Frime)
        sp%m_r = REAL(m,wp) * Frime
        sp%V_r = 0._wp!sp%m_r / rhor
        sp%m_w = 0._wp
        sp%m_f = 0._wp

        mtot = (sp%m_w + sp%m_i + sp%m_r + sp%m_f)

        !IF ( (cylinder .EQV. .TRUE.) .AND. (sp%phi .GT. 1_wp) ) THEN
        !  r_a = ( sp%V_i / (2. * pi * sp%phi ) )**z1_3 
        !ELSE
          CALL spheroid_geometry(sp, r_a, r_c, V_tot)
        !END IF

        sp%d = 2._wp * max(r_a, r_c)

        IF (sp%phi >= 1._wp) THEN
          IF (cylinder .EQV. .TRUE.) THEN
            d = (4.0_wp*V_tot/(pi*sp%phi))**(1.0_wp/3.0_wp)
            !d = (2.0_wp*V_tot/(sqrt(3.0_wp)*sp%phi))**(1.0_wp/3.0_wp)
            !WRITE(*,*) "Boehm d ", d , (4.0_wp*V_tot/(pi*sp%phi))**(1.0_wp/3.0_wp), " A ", d**2 * sp%phi, r_a * r_c * pi
            sp%p_area =  d**2 * sp%phi ! r_a * r_c * pi ! 4.0_wp ! pi * r_a * r_c
            !WRITE(*,*) "Boehm d**2/A ", d**2/sp%p_area
            q = z4_pi
          ELSE
            d = sp%d / sp%phi
            sp%p_area = pi * r_a * r_c
            q = sp%p_area / (pi_4 * sp%d**2/sp%phi)!Ar
          END IF
        ELSE
          d = sp%d
          sp%p_area = Ar * pi * r_a**2
          q = sp%p_area / (pi_4 * sp%d**2)
        END IF

        CALL hydrodynamicValuesBoehm( sp%phi, q, k, gama_big, C_DP )
        X_0 = 2.8D6
        !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)*q**(1.0_wp/4.0_wp))
        !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)*MAX(q**(1.0_wp/4.0_wp),q))
        X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)*MIN(q**(1.0_wp/4.0_wp),1._wp))
        !X = 8.0_wp*mtot*grav*atmo%rho/(pi*(atmo%eta**2)*MAX(sp%phi,1.0_wp)) !*q**(1.0_wp/4.0_wp))
        C_DP_prim = C_DP*((1.0_wp+1.6_wp*(X/X_0)**2)/(1.0_wp+(X/X_0)**2))
        beta = SQRT(1.0_wp+C_DP_prim/6.0_wp/k*SQRT(X/C_DP_prim))-1
        N_Re0 = 6.0_wp*k/C_DP_prim*beta**2
        C_DO = 4.5_wp*k**2*MAX(sp%phi,1.0_wp)
        gama_small = (C_DO - C_DP)/4.0_wp/C_DP
        N_Re  = N_Re0*(1.0_wp + (2.0_wp*beta*EXP(-beta*gama_small))/((2.0_wp+beta)*(1.0_wp+beta)) )


        sp%v = N_Re * atmo%eta / (d * atmo%rho)
        Cd2  = 2._wp * (sp%m_w + sp%m_i + sp%m_r + sp%m_f) * grav / (atmo%rho * sp%v**2 * sp%p_area)
        !IF( cylinder .EQV. .TRUE. ) THEN
        !  WRITE(*,*) " v ", sp%v, N_Re * atmo%eta / (sp%d/sp%phi * atmo%rho)
        !  WRITE(*,*) "Cd2 ", Cd2, 2._wp*V_tot*rhoi*grav/(atmo%rho * (N_Re*atmo%eta/(sp%d/sp%phi*atmo%rho))**2 * sp%p_area)
        !ENDIF
        !Cd2  = pi / 2. * d/sp%v**2 * (rhoi - atmo%rho) * grav / atmo%rho
        N_Re = sp%v * d * atmo%rho * atmo%etai
        N_Re_d = sp%v * ( 2.* ( z3_4pi * sp%V_i  )**z1_3 ) * atmo%rho * atmo%etai
        Cd   = X / N_Re**2

        WRITE(75,'(i7,12e16.7)') i, m, sp%phi, sp%v, sp%d, Cd, Cd2, N_Re, N_Re_d, X, q, Ar
        ! WRITE(74,'(12e14.7)') REAL(i), m, sp%phi, sp%v, sp%d, Cd, Cd2, N_Re, N_Re_d, X, q, Ar
        ! !                        1     2      3     4    5    6    7    8      9    10  11 12
      END DO

      WRITE(75,*)
    !END DO
  END DO
  CLOSE(75)
END SUBROUTINE plot_boehm_westbrook_phis

!
! subroutine to calculate height dependence of Boehm's vt-parameterization
SUBROUTINE plot_vt_boehm_height_depend()
  !USE mo_grid_nml,       ONLY: nz
  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo

  INTEGER      :: k, i, j, nz =275
  LOGICAL      :: file_exists
  REAL(wp)     :: vt_boehm, d, mtot, N_Re, V, rhor = 800._wp
  CHARACTER(len=256)  :: fname
  CHARACTER(len=256), DIMENSION(4) :: ptype 

  REAL(wp), DIMENSION(5) :: d_part
  REAL(wp), DIMENSION(3) :: phi
  REAL(wp), PARAMETER    :: lambda = 4.7_wp/1000.0_wp

  d_part = (/20e-6, 50e-6, 100e-6, 200e-6, 500e-6/)
  phi    = (/1.,0.25,4./)
  ptype = (/'drop','obla','prol','grau'/)
  DO j = 1, 4
    WRITE(fname,'(a,a,a)') "boehm_height_dep_",TRIM(ptype(j)),".dat"
    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF
    DO i = 1, 5   
      sp%d = d_part(i)
      V    = z4pi_3 * ( sp%d / 2._wp)**3 
      sp%m_r = 0._wp !REAL(m,wp) * Frime
      sp%V_r = 0._wp
      sp%m_f = 0._wp

      IF (j < 2) THEN
        sp%m_i = 0._wp !REAL(m,wp) * (1._wp-Frime)
        sp%m_w = V * rhol
        sp%phi = exp(-sp%d/lambda) + (1.0_wp - exp(-sp%d/lambda))/(1.0_wp+sp%d/lambda)
      ELSE IF (j == 4) THEN
        sp%m_i = 0._wp
        sp%V_i = 0._wp
        sp%m_w = 0._wp
        sp%m_r = V * rhor
        sp%V_r = V
        sp%phi = 1._wp
      ELSE
        sp%m_i = V * rhoi
        sp%m_w = 0._wp
        sp%phi = phi(j)
        sp%V_i = V
      END IF

      CALL m2d_particle( sp )

      mtot = sp%m_i + sp%m_r + sp%m_f + sp%m_w

      IF (sp%phi .GT. 1._wp) THEN  
        d = sp%d / sp%phi                                
      ELSE
        d = sp%d
      END IF

      DO k = 1, nz+1
        sp%z = (k - 1) * 20._wp
        CALL get_atmo( sp%z, atmo )
        vt_boehm = boehmVel( atmo, sp )
        N_Re = vt_boehm * (d * atmo%rho) / atmo%eta  
        WRITE(74,'(9e14.7)') REAL(i), sp%z, sp%d, mtot, sp%phi, vt_boehm, N_Re, atmo%T, atmo%p
      END DO    
    END DO
    CLOSE(74)
  END DO

END SUBROUTINE plot_vt_boehm_height_depend

!
! subroutine to calculate r dependence of Boehm's vt-parameterization for 3 pressure levels to
! compare with results of Khain and Pinsky 2001
SUBROUTINE plot_vt_boehm_r_depend()
  USE mo_grid_nml,       ONLY: nz
  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo

  INTEGER      :: k, i, j
  LOGICAL      :: file_exists
  REAL(wp)     :: vt_boehm, d, mtot, m , N_Re, Frime, V
  CHARACTER(len=256)  :: fname 
  CHARACTER(len=256), DIMENSION(4) :: fmt
  CHARACTER(len=256), DIMENSION(3) :: atype

  REAL(wp), DIMENSION(4) :: rho_p
  REAL(wp), DIMENSION(3) :: height
  REAL(wp), PARAMETER    :: lambda = 4.7_wp/1000.0_wp

  height = (/ 104.5_wp, 2343._wp, 5302._wp/)
  atype  = (/'1000hPa','0750hPa','0500hPa'/)
  rho_p  = (/rhol, 800._wp, 400._wp, 100._wp/)
  fmt    = (/"1000", "0800", "0400", "0100"/)

  DO k = 1,4
    IF (k == 1) THEN
      Frime = 0._wp
      sp%phi = exp(-sp%d/lambda) + (1.0_wp - exp(-sp%d/lambda))/(1.0_wp+sp%d/lambda)
    ELSE
      Frime = 1._wp
      sp%phi = 1._wp
    END IF

    DO j = 1, 3
      WRITE(fname,'(a,a,a,a,a)') "boehm_r_dep_",TRIM(fmt(k)),"_",TRIM(atype(j)),".dat"
      INQUIRE(FILE=fname, EXIST=file_exists)
      IF( file_exists ) THEN
        OPEN(74, file=fname, status="old", position="append", action="write")
      ELSE
        OPEN(74, file=fname, status="new", action="write")
      END IF

      sp%z = height(j)
      CALL get_atmo( sp%z, atmo )
      DO i = 1, 500   
        sp%d = i * 2e-6
        V    = z4pi_3 * ( sp%d / 2._wp)**3 
        m    = V * rho_p(k)
        sp%m_i = 0._wp !REAL(m,wp) * (1._wp-Frime)
        sp%m_w = m * (1._wp-Frime)

        sp%m_r = REAL(m,wp) * Frime
        sp%V_r = sp%m_r / rho_p(k)
        sp%m_f = 0._wp
        
        CALL m2d_particle( sp )

        mtot = sp%m_i + sp%m_r + sp%m_f + sp%m_w

        IF (sp%phi .GT. 1._wp) THEN  
          d = sp%d / sp%phi                                
        ELSE
          d = sp%d
        END IF

        vt_boehm = boehmVel( atmo, sp )
        N_Re = vt_boehm * (d * atmo%rho) / atmo%eta  
        WRITE(74,'(9e14.7)') REAL(i), sp%z, sp%d, mtot, sp%phi, vt_boehm, N_Re, atmo%T, atmo%p  
      END DO
    END DO
    CLOSE(74)
  END DO

END SUBROUTINE plot_vt_boehm_r_depend


!
! subroutine to calculate height dependence of Boehm's Ec-parameterization
SUBROUTINE plot_Ec_boehm_height_depend()
  !USE mo_grid_nml,       ONLY: nz
  TYPE(t_sp)   :: sp1, sp2
  TYPE(t_atmo) :: atmo

  INTEGER      :: k, i, j, nz =275
  LOGICAL      :: file_exists
  REAL(wp)     :: ce_boehm, kernel, dv, vt_boehm1, vt_boehm2, d, mtot, V1, V2, N_Re, rhor = 800._wp
  CHARACTER(len=256)  :: fname
  CHARACTER(len=256), DIMENSION(4) :: ptype 

  REAL(wp), DIMENSION(5) :: d_part
  REAL(wp), DIMENSION(3) :: phi
  REAL(wp), PARAMETER    :: lambda = 4.7_wp/1000.0_wp

  d_part = (/20e-6, 50e-6, 100e-6, 200e-6, 500e-6/)
  phi    = (/1.,0.25,4./)
  ptype = (/'d-d','d-g','o-o','p-p'/)
  DO j = 1, 4
    WRITE(fname,'(a,a,a)') "boehm_Ec_height_dep_",TRIM(ptype(j)),".dat"
    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF
    DO i = 2, 5   
      sp1%d = d_part(i)
      sp2%d = d_part(i-1)
      V1    = z4pi_3 * ( sp1%d / 2._wp)**3 
      V2    = z4pi_3 * ( sp2%d / 2._wp)**3 

      sp1%m_r = 0._wp !REAL(m,wp) * Frime
      sp1%V_r = 0._wp
      sp1%m_f = 0._wp
      sp2%m_r = 0._wp !REAL(m,wp) * Frime
      sp2%V_r = 0._wp
      sp2%m_f = 0._wp

      IF (j == 1) THEN
        sp1%m_i = 0._wp !REAL(m,wp) * (1._wp-Frime)
        sp1%m_w = V1 * rhol
        sp1%phi = exp(-sp1%d/lambda) + (1.0_wp - exp(-sp1%d/lambda))/(1.0_wp+sp1%d/lambda)
        sp2%m_i = 0._wp !REAL(m,wp) * (1._wp-Frime)
        sp2%m_w = V2 * rhol
        sp2%phi = exp(-sp2%d/lambda) + (1.0_wp - exp(-sp2%d/lambda))/(1.0_wp+sp2%d/lambda)
      ELSE IF (j == 2) THEN
        sp1%m_i = 0._wp
        sp1%m_r = V1 * rhor
        sp1%m_w = 0._wp
        sp1%phi = phi(1)
        sp1%V_r = V1
        sp2%m_i = 0._wp !REAL(m,wp) * (1._wp-Frime)
        sp2%m_w = V2 * rhol
        sp2%phi = exp(-sp2%d/lambda) + (1.0_wp - exp(-sp2%d/lambda))/(1.0_wp+sp2%d/lambda)
      ELSE IF (j == 3) THEN
        sp1%m_i = V1 * rhoi
        sp1%m_w = 0._wp
        sp1%phi = phi(2)
        sp1%V_i = V1

        sp2%m_i = V2 * rhoi
        sp2%m_w = 0._wp
        sp2%phi = phi(2)
        sp2%V_i = V2
      ELSE 
        sp1%m_i = V1 * rhoi
        sp1%m_w = 0._wp
        sp1%phi = phi(3)
        sp1%V_i = V1

        sp2%m_i = V2 * rhoi
        sp2%m_w = 0._wp
        sp2%phi = phi(3)
        sp2%V_i = V2
      END IF     
      
      CALL m2d_particle( sp1 )
      CALL m2d_particle( sp2 )

      mtot = sp1%m_i + sp1%m_r + sp1%m_f + sp1%m_w

      IF (sp1%phi .GT. 1._wp) THEN  
        d = sp1%d / sp1%phi                                
      ELSE
        d = sp1%d
      END IF


      DO k = 1, nz+1
        sp1%z = (k - 1) * 20._wp
        CALL get_atmo( sp1%z, atmo )
        
        vt_boehm1 = boehmVel( atmo, sp1 )
        vt_boehm2 = boehmVel( atmo, sp2 )
        N_Re = vt_boehm1 * (d * atmo%rho) / atmo%eta  

        ce_boehm = Ecbohm_spheroid( atmo, sp1, sp2 )
        dv       = ABS(vt_boehm1 - vt_boehm2)
        kernel   = pi * (SQRT(sp1%p_area / pi) + SQRT(sp2%p_area / pi))**2 * ce_boehm * dv

        WRITE(74,'(13e14.7)') REAL(i), sp1%z, sp1%d, sp2%d, mtot, sp1%phi, sp2%phi, dv, &
                              N_Re, atmo%T, atmo%p, ce_boehm, kernel
      END DO    
    END DO
    CLOSE(74)
  END DO

END SUBROUTINE plot_Ec_boehm_height_depend


!
! plot diameter, projected area, and fall velocity in 
! terms of m_i and m_r.
!
SUBROUTINE plot_a2d_m2d(rhor)

  REAL(wp), INTENT(IN) :: rhor
  INTEGER, PARAMETER :: npts = 200 ! 100
  INTEGER  :: i, r, w
  REAL(wp) :: mii, mrr, mi(npts), mr(npts), vr(npts), dmi, dmr
  REAL(wp) :: dagg(npts,npts,1), d(npts,npts,1), a(npts,npts,1), aagg(npts,npts,1), vt(npts,npts,1)
  !REAL(wp) :: mww, mw(npts), dmw, dagg(npts,npts,npts), d(npts,npts,npts), a(npts,npts,npts), aagg(npts,npts,npts), vt(npts,npts,npts)
  CHARACTER(len=30) :: fname 
  LOGICAL  :: file_exists
  TYPE(t_sp) :: sp 
  TYPE(t_atmo) :: atmo

#ifdef HAVE_NETCDF 
  ! NetCDF
  CHARACTER(len=*), PARAMETER :: &
    netcdf_fname = "mass_area_diam.nc", &
    units      = "units",  &
    x_units    = "kg",     &
    y_units    = "kg",     &
    z_units    = "kg/m3",  &
    area_units = "m2",     &
    diam_units = "m",      &
    vt_units   = "m/s"
  INTEGER :: ncid, d2(2), &
             x_dimid, y_dimid, z_dimid, xvarid, yvarid, zvarid, &
             dvarid, avarid, vtvarid
#endif

  PRINT *, 'Plotting m-D and m-Vt...'

  write (*,'(a,i3.0,a)') 'mass_area_diam_',int(rhor),'.dat'
  write (fname,'(a,i3.0,a)') 'mass_area_diam_',int(rhor),'.dat'

 ! plot (mr,mr) vs proj_area, diam for fixed rime density
  dmi = 10**(12/REAL(npts-1,wp))
  dmr = dmi
  !dmw = dmi
  mii = 1e-14_wp
  DO i = 1, npts
    mrr = 1e-14_wp
    mi(i) = mii
    DO r = 1, npts
      !mww = 1e-14_wp
      mr(r) = mrr
      vr(r) = mrr/rhor
      w = 1
      !DO w = 1, npts
        !mw(w) = mww

        sp%z = 10._wp !250._wp  ! at this height is T=-20.15 C
        sp%m_i = REAL(mii,wp)
        sp%V_i = sp%m_i / rhoi

        ! compute crystal diameter D_agg and p. area A_agg first
        sp%m_r = 0._wp
        sp%v_r = 0._wp
        sp%m_w = 0._wp
        CALL m2d_particle( sp )
        dagg(i,r,w) = sp%d
        aagg(i,r,w) = sp%p_area

        ! now compute the actual particle
        !IF( mrr < 1.0e-13_wp ) THEN
        !  sp%m_r = 0.0_wp
        !  sp%v_r = 0.0_wp
        !ELSE
          sp%m_r = REAL(mrr,wp)
          sp%v_r = REAL(mrr,wp)/rhor
        !END IF
        !IF( mww < 1.0e-13_wp ) THEN
        !  sp%m_w = 0.0_wp
        !ELSE
        !  sp%m_w = REAL(mww,wp)
        !END IF
 
        CALL m2d_particle( sp )
        d(i,r,w) = sp%d
        a(i,r,w) = sp%p_area 

        CALL get_atmo( sp%z, atmo )
        vt(i,r,w) = vterm_snow( atmo, sp )
  
        !mww = mww*dmw
      !END DO 

      mrr = mrr*dmr
    END DO
    mii = mii*dmi
  END DO

  SELECT CASE(out_type)
  CASE(1)
    ! ASCII out
    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF

    DO r = 1, npts
      DO i = 1, npts
        w = 1
        !DO w = 1, npts
          !WRITE(74,'(9e14.7)') mi(i), mr(r), vr(r), mw(w), dagg(i,r,w), d(i,r,w), a(i,r,w), aagg(i,r,w), vt(i,r,w) 
          WRITE(74,'(8e14.7)') mi(i), mr(r), vr(r), dagg(i,r,w), d(i,r,w), a(i,r,w), aagg(i,r,w), vt(i,r,w)
        !END DO
        WRITE(74,*) ""
      END DO
      WRITE(74,*) ""
    END DO

    CLOSE(74)

  CASE(2)
#ifdef HAVE_NETCDF
    ! NetCDF out

    ! Always check the return code of every netCDF function call. In
    ! this example program, wrapping netCDF calls with "call check()"
    ! makes sure that any return which is not equal to nf90_noerr (0)
    ! will print a netCDF error message and exit.

    ! Create the netCDF file. The nf90_clobber parameter tells netCDF to
    ! overwrite this file, if it already exists.
    CALL check( nf90_create( netcdf_fname, NF90_CLOBBER, ncid ) )

    ! Define the dimensions. NetCDF will hand back an ID for each. 
    CALL check( nf90_def_dim(ncid, "mi", npts, x_dimid) )
    CALL check( nf90_def_dim(ncid, "mr", npts, y_dimid) )
    CALL check( nf90_def_dim(ncid, "vr", npts, z_dimid) )

    CALL check( nf90_def_var(ncid, "mi", NF90_REAL, x_dimid, xvarid) )
    CALL check( nf90_def_var(ncid, "mr", NF90_REAL, y_dimid, yvarid) )
    CALL check( nf90_def_var(ncid, "vr", NF90_REAL, z_dimid, zvarid) )

    ! Assign units attributes to coordinate var data. This attaches a
    ! text attribute to each of the coordinate variables, containing the
    ! units.
    CALL check( nf90_put_att(ncid, xvarid, units, x_units) )
    CALL check( nf90_put_att(ncid, yvarid, units, y_units) )
    CALL check( nf90_put_att(ncid, zvarid, units, z_units) )

    ! Define the netCDF variables. The dimids array is used to pass the
    ! dimids of the dimensions of the netCDF variables.
    d2 =  (/ x_dimid, y_dimid /)
    CALL check( nf90_def_var(ncid, "diam", NF90_REAL, d2, dvarid) )
    CALL check( nf90_def_var(ncid, "parea", NF90_REAL, d2, avarid) )
    CALL check( nf90_def_var(ncid, "vterm", NF90_REAL, d2, vtvarid) )

    ! Assign units attributes to the pressure and temperature netCDF
    ! variables.
    CALL check( nf90_put_att(ncid, dvarid, units, diam_units) )
    CALL check( nf90_put_att(ncid, dvarid, "position", "mi,mr")) 
    CALL check( nf90_put_att(ncid, avarid, units, area_units) )
    CALL check( nf90_put_att(ncid, avarid, "position", "mi,mr")) 
    CALL check( nf90_put_att(ncid, avarid, units, vt_units) )
    CALL check( nf90_put_att(ncid, avarid, "position", "mi,mr")) 

    ! End define mode. This tells netCDF we are done defining metadata.
    CALL check( nf90_enddef(ncid) )

    ! Write the coordinate variable data. This will put the latitudes
    ! and longitudes of our data grid into the netCDF file.
    CALL check( nf90_put_var(ncid, xvarid, mi) )
    CALL check( nf90_put_var(ncid, yvarid, mr) )
    CALL check( nf90_put_var(ncid, zvarid, vr) )

    CALL check( nf90_put_var(ncid, dvarid, d) )
    CALL check( nf90_put_var(ncid, avarid, a) )
    CALL check( nf90_put_var(ncid, avarid, vt) )

    ! Close the file. This frees up any internal netCDF resources
    ! associated with the file, and flushes any buffers.
    CALL check( nf90_close(ncid) )
   
    ! end ifdef HAVE_NETCDF
#endif

  END SELECT

  PRINT *, '                         [ok]'
END SUBROUTINE

!
! plot diameter, projected area, and fall velocity etc. in 
! terms of m_tot and F_r.
!
SUBROUTINE plot_geo_vt()

  INTEGER, PARAMETER :: npts = 200, fpts = 6
  INTEGER  :: i, r
  REAL(wp) :: mass, mi(npts,fpts), mr(npts,fpts), vr(npts,fpts), &
          dmi, Frime, diam(npts,fpts), fr(fpts),  &
          area(npts,fpts), vt(npts,fpts), Vi(npts,fpts)
  REAL, PARAMETER :: rhor = 600._wp
  CHARACTER(len=*), PARAMETER :: fname = "mass_geo_vt.dat"
  LOGICAL  :: file_exists
  TYPE(t_sp) :: sp
 
  TYPE(t_atmo) :: atmo

!  fr = (/0.0,0.01,0.1,0.2,0.7,0.9,0.99,1.0/)
  fr = (/0.0_wp,0.5_wp,0.8_wp,0.9_wp,0.99_wp,1.0_wp/)

  PRINT *, 'Plotting m-D and m-Vt for different rime fractions'

  ! calc diam, proj_area, and vt for fixed rime density
  dmi  = 10**(12/REAL(npts-1,wp))
  mass = 1e-14_wp
  DO i = 1, npts
     DO r = 1, fpts
        Frime = fr(r)

        mr(i,r) = mass * Frime
        mi(i,r) = mass * (1-Frime)
        vr(i,r) = mr(i,r)/rhor
        Vi(i,r) = mi(i,r)/rhoi

        sp%z = 10._wp !250._wp  ! at this height is T=-20.15 C
        sp%m_i = REAL(mi(i,r),wp)
        sp%V_i = sp%m_i / rhoi

        ! compute the actual particle
        sp%m_r = REAL(mr(i,r),wp)
        sp%v_r = REAL(vr(i,r),wp) 
        sp%m_w = 0.0_wp

        CALL m2d_particle( sp )
        diam(i,r) = sp%d
        area(i,r) = sp%p_area
        
        CALL get_atmo( sp%z, atmo )
        vt(i,r) = vterm_snow( atmo, sp )
        
     END DO
     mass = mass*dmi
  END DO

  SELECT CASE(out_type)
  CASE(1)
    ! ASCII out
    INQUIRE(FILE=fname, EXIST=file_exists)
    IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
    ELSE
      OPEN(74, file=fname, status="new", action="write")
    END IF

    DO r = 1, fpts
       Frime = fr(r) 
       DO i = 1, npts
          WRITE(74,'(8e14.7)') mi(i,r),mr(i,r),vr(i,r),diam(i,r),area(i,r),vt(i,r)
      END DO
      WRITE(74,*) ""
    END DO

    CLOSE(74)

  END SELECT

  PRINT *, '                         [ok]'
END SUBROUTINE

!
! plot diameter, projected area, and fall velocity etc. 
! at const mtot, divided between ice and water 
!
SUBROUTINE plot_vt_f_mw( ice )
  INTEGER, INTENT(in) :: ice

  INTEGER,  PARAMETER :: npts = 1001, mpts = 51
  REAL(wp), PARAMETER :: rhor = 450._wp
  !REAL(wp), PARAMETER :: m0 = 0.268_wp/1000_wp
  !REAL(wp), PARAMETER :: alpha = 0.1389

  CHARACTER(len=*), PARAMETER :: fname  = "mass_const_vt_iw.dat"
  CHARACTER(len=*), PARAMETER :: fnamer = "mass_const_vt_rw.dat"
  CHARACTER(len=*), PARAMETER :: fnamem = "mass_const_vt_mw.dat"
  
  LOGICAL  :: file_exists

  INTEGER  :: i, m
  REAL(wp) :: d_ideal(mpts), mtot(mpts), part
  REAL(wp) :: mi(mpts,npts), mr(mpts,npts), mw(mpts,npts), d(mpts,npts), A(mpts,npts), vt(mpts,npts)
  
  TYPE(t_sp) :: sp
  TYPE(t_atmo) :: atmo
 
  !d_ideal = (/0.5, 2.0, 6.0, 8.0, 10.0, 12.0, 15.0, 18.0/)
  DO m = 1, mpts
    d_ideal(m) = 1000.0_wp/1000.0_wp + (m-1)/REAL(mpts-1,wp)*(6.0_wp-1.0_wp)  
    mtot(m)    = 1000.0_wp * pi / 6.0_wp * (d_ideal(m)/1000.0_wp)**3
  END DO
 
  DO m = 1, mpts
    DO i = 1, npts
      part = (i-1)/REAL(npts-1,wp)
      !part = (i-1)*0.2
      !IF( mtot(m) > m0 ) THEN
      !  part = part*(m0/mtot(m) + alpha)/(1 + alpha)
      !END IF
      IF( ice == 1 ) THEN 
        mi(m,i) = (1.0_wp - part)*mtot(m)
        mr(m,i) = 0.0_wp 
        sp%v_r = 0.0_wp
      END IF
      IF( ice == 0) THEN
        mi(m,i) = 0.0_wp
        mr(m,i) = (1.0_wp - part)*mtot(m)
        sp%v_r = mr(m,i)*rhoii
      END IF
      IF( ice == 2) THEN
        mi(m,i) = ((6.0_wp*(1.0_wp - part)*mtot(m)/pi/rhor)**(unr_bet/3.0_wp))*unr_alf
        mr(m,i) = (1.0_wp - part)*mtot(m) - mi(m,i)
        sp%v_r = pi/6.0_wp*(mi(m,i)/unr_alf)**(3.0_wp/unr_bet) - mi(m,i)*rhoii
      END IF
      mw(m,i) = part*mtot(m)
      
      sp%z = 10.0_wp
      sp%m_i = mi(m,i)
      sp%m_r = mr(m,i)
      sp%m_w = mw(m,i)
      call iceGeo_init(sp) ! set Vi and phi
      CALL m2d_particle( sp ) 
      d(m,i) = sp%d
      A(m,i) = sp%p_area
      
      CALL get_atmo( sp%z, atmo )
      vt(m,i) = vterm_snow( atmo, sp )
    END DO
  END DO
  
  IF( ice == 1 ) INQUIRE(FILE=fname,  EXIST=file_exists)
  IF( ice == 0 ) INQUIRE(FILE=fnamer, EXIST=file_exists)
  IF( ice == 2 ) INQUIRE(FILE=fnamem, EXIST=file_exists)
  IF( file_exists ) THEN
    IF(ice == 1) OPEN(74, file=fname, status="old", position="append", action="write")
    IF(ice == 0) OPEN(74, file=fnamer, status="old", position="append", action="write")
    IF(ice == 2) OPEN(74, file=fnamem, status="old", position="append", action="write")
  ELSE
    IF(ice == 1) OPEN(74, file=fname, status="new", action="write")
    IF(ice == 0) OPEN(74, file=fnamer, status="new", action="write")
    IF(ice == 2) OPEN(74, file=fnamem, status="new", action="write")
  END IF
 
  DO m = 1, mpts
    DO i = 1, npts
      WRITE(74,'(6e14.7)') mi(m,i), mr(m,i), mw(m,i), d(m,i), A(m,i), vt(m,i)
    END DO
    WRITE(74,*) ""
  END DO

  CLOSE(74)
END SUBROUTINE plot_vt_f_mw  

!
! plot mass, diameter, projected area, fall velocity, Reynolds number, and drag coefficient for pure water
!
SUBROUTINE plot_rain()
  USE mo_atmo_types, ONLY: grav, T_3, rhoi
  INTEGER,  PARAMETER :: npts = 200
  REAL(wp), PARAMETER :: Dmin = 20.0_wp/1000.0_wp**2
  REAL(wp), PARAMETER :: Dmax = 10.0_wp/1000.0_wp
  REAL(wp), PARAMETER :: Dinc = (Dmax-Dmin)/REAL(npts-1,wp)
  CHARACTER(len=*), PARAMETER :: fname = "rain.dat"

  TYPE(t_sp)   :: sp
  TYPE(t_atmo) :: atmo
  LOGICAL      :: file_exists
  REAL(wp)     :: m(npts), d(npts), A(npts), vt(npts), Re(npts), Cd(npts)
  INTEGER      :: i

  DO i=1, npts
    sp%z   = 0.0_wp
    sp%m_i = rhoi*pi/6.0_wp*(Dmin + Dinc*REAL(i-1,wp))**3  ! 0.0_wp
    sp%m_r = 0.0_wp
    sp%v_r = 0.0_wp
    sp%V_i = sp%m_i / rhoi
    sp%m_w = 0.0_wp !rhol*pi/6.0_wp*(Dmin + Dinc*REAL(i-1,wp))**3
    CALL m2d_particle( sp )
    CALL get_atmo( sp%z, atmo )
    sp%v   = vterm_snow( atmo, sp )
    m(i)   = sp%m_w
    d(i)   = sp%d
    A(i)   = sp%p_area
    vt(i)  = sp%v
    Re(i)  = atmo%rho*sp%d*sp%v/atmo%eta
    Cd(i)  = sp%m_w*grav/(0.5_wp*atmo%rho*sp%v**2*sp%p_area)
  END DO

  WRITE(*,*) " T ", atmo%T - T_3, " p ", atmo%p, " rho ", atmo%rho

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
    OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
    OPEN(74, file=fname, status="new", action="write")
  END IF
  DO i = 1, npts
    WRITE(74,'(6e14.7)') m(i), d(i), A(i), vt(i), Re(i), Cd(i)
  END DO
  CLOSE(74)
END SUBROUTINE plot_rain


!
! plot diameter, projected area, and fall velocity etc. 
! at const mtot, divided between ice and water 
!
SUBROUTINE plot_vt_f_mw_vivec()

  INTEGER,  PARAMETER :: npts = 5, mpts = 591
  REAL(wp), PARAMETER :: rhor = 919._wp
  REAL(wp), PARAMETER :: m0 = 0.268_wp/1000_wp
  REAL(wp), PARAMETER :: alpha = 0.1389

  CHARACTER(len=*), PARAMETER :: fname = "vivec_Deq_vt.dat"
  LOGICAL  :: file_exists

  INTEGER  :: i, m
  REAL(wp) :: d_ideal(mpts), mtot(mpts), part
  REAL(wp) :: mi(mpts,npts), mr(mpts,npts), mw(mpts,npts), d(mpts,npts), A(mpts,npts), vt(mpts,npts)

  TYPE(t_sp) :: sp
  TYPE(t_atmo) :: atmo

  !d_ideal = (/0.5, 2.0, 6.0, 8.0, 10.0, 12.0, 15.0, 18.0/)
  DO m = 1, mpts
    d_ideal(m) = 1.0_wp + (m-1)/REAL(mpts-1,wp)*(60.0_wp-1.0_wp)
    mtot(m)    = 1000.0_wp * pi / 6.0_wp * (d_ideal(m)/1000.0_wp)**3
  END DO

  DO m = 1, mpts
    DO i = 1, npts
      !part = (i-1)/REAL(npts-1,wp)
      part = (i-1)*0.25
      IF( mtot(m) > m0 ) THEN
        part = part*(m0/mtot(m) + alpha)/(1 + alpha)
      END IF

      mi(m,i) = 0.0_wp ! (1.0_wp - part)*mtot(m)
      mr(m,i) = (1.0_wp - part)*mtot(m)
      mw(m,i) = part*mtot(m)

      sp%z = 10.0_wp
      sp%m_i = mi(m,i)
      sp%m_r = mr(m,i)
      sp%v_r = mr(m,i)/rhor
      sp%V_i = mi(m,i) / rhoi
      sp%m_w = mw(m,i)

      CALL m2d_particle( sp )
      d(m,i) = sp%d
      A(m,i) = sp%p_area

      CALL get_atmo( sp%z, atmo )
      vt(m,i) = vterm_snow( atmo, sp )
    END DO
  END DO

  INQUIRE(FILE=fname, EXIST=file_exists)
  IF( file_exists ) THEN
      OPEN(74, file=fname, status="old", position="append", action="write")
  ELSE
      OPEN(74, file=fname, status="new", action="write")
  END IF

  DO m = 1, mpts
    DO i = 1, npts
      WRITE(74,'(6e14.7)') mi(m,i), mr(m,i), mw(m,i), d(m,i), A(m,i), vt(m,i)
    END DO
    WRITE(74,*) ""
  END DO

  CLOSE(74)
END SUBROUTINE plot_vt_f_mw_vivec



#ifdef HAVE_NETCDF
!
! check NetCDF operation
!
SUBROUTINE check( stat )
  INTEGER, INTENT(in) :: stat

    IF( stat /= nf90_noerr ) THEN 
      PRINT *, TRIM(nf90_strerror( stat ))
      STOP "mo_check :: check  Stopped"
    END IF
END SUBROUTINE check
#endif

! end ifndef WDROPS
#endif

END MODULE mo_check
