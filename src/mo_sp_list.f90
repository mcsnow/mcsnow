!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_sp_list

  USE mo_constants,    ONLY: wp, i8, pi
  USE mo_grid,         ONLY: t_grid
  USE mo_sp_types,     ONLY: t_sp, t_list, t_node, t_sortNode, &
                             t_list_stats, t_tnode, sp_stat
  USE mo_sp_nml,       ONLY: ini_type, xi0, &
                             agg_hist, mergeRatioLimit, mergeNoTresh
  USE mo_atmo,         ONLY: t_atmo, get_atmo
  USE mo_atmo_types,   ONLY: rhol
#ifndef WDROPS
  USE mo_mass2diam,    ONLY: mth
#endif


  IMPLICIT NONE
  PRIVATE


  PUBLIC ::           &
    add_to_list,      &
    print_list,       &
    sp_sanity,        &
    alloc_sp_list,    &
    delete_sp_list,   &
    mergeSPs,         &
    list_stats


CONTAINS

!
! add a super-droplet node to the list
!
SUBROUTINE add_to_list( sp_list, sp_list_node )
  TYPE(t_list),      INTENT(inout)          :: sp_list
  TYPE(t_node), INTENT(in) :: sp_list_node

  TYPE(t_node), POINTER :: sp_node

  IF( .NOT.ASSOCIATED(sp_list%head) ) THEN
    ALLOCATE( sp_list%head )
    sp_list%head = sp_list_node ! copy
    NULLIFY(sp_list%head%next)
    sp_list%length = 1
  ELSE
    ALLOCATE(sp_node)
    sp_node = sp_list_node
    sp_node%next => sp_list%head
    sp_list%head => sp_node
    sp_list%length = sp_list%length + 1
  END IF
END SUBROUTINE add_to_list

!
! print linked list
!
SUBROUTINE print_list( sp_list, opt_name )
  TYPE(t_list),  INTENT(in) :: sp_list(:)
  CHARACTER(len=*), OPTIONAL   :: opt_name

  TYPE(t_node), POINTER :: isp_list_node
  TYPE(t_sp)    :: sp
  CHARACTER(10) :: sname
  INTEGER       :: i,ii

  IF(PRESENT(opt_name)) THEN
    sname = opt_name
  ELSE 
    sname = "The list"
  END IF

  PRINT *, "---------------------"
  DO i = 1, size(sp_list)
    PRINT *, sname, " in cell ", i,": "
    isp_list_node => sp_list(i)%head
    DO ii=1,sp_list(i)%length
      sp = isp_list_node%sp
#ifndef WDROPS
      PRINT *, ii, sp%z, sp%xi, sp%m_i, sp%m_r, sp%v_r
#else
      PRINT *, ii, sp%z, sp%xi, sp%m_w
#endif
      isp_list_node => isp_list_node%next
    END DO
  END DO
END SUBROUTINE print_list


!
! check if SPs are realistic
!
FUNCTION sp_sanity( sp_list, opt_name ) RESULT(sane)
  TYPE(t_list), INTENT(in) :: sp_list
  CHARACTER(len=*), OPTIONAL :: opt_name
  LOGICAL :: sane

  TYPE(t_node), POINTER :: isp_list_node
  TYPE(t_sp),           POINTER :: sp
  CHARACTER(10) :: sname
  INTEGER :: ii

  IF(PRESENT(opt_name)) THEN
    sname = opt_name
  ELSE 
    sname = "The list"
  END IF

  sane = .TRUE.

  isp_list_node => sp_list%head
  DO ii=1, sp_list%length
    sp => isp_list_node%sp
#ifndef WDROPS
    IF( sp%xi <= 0 .OR. sp%m_i < 0._wp .OR. sp%m_r<0._wp .OR. sp%v_r<0._wp .OR. sp%m_w<0.0_wp .OR. sp%mm < 1 ) THEN
      PRINT *, "xi, mass", sp%xi, sp%m_i, sp%m_r, sp%v_r, sp%m_w, sp%mm
#else
    IF( sp%xi <= 0 .OR. sp%m_w < 0._wp ) THEN
      PRINT *, "xi, mass", sp%xi, sp%m_w
#endif
      sane = .FALSE.
      WRITE(*,*) sp
      EXIT
    END IF
    isp_list_node => isp_list_node%next
  END DO

  IF ( .NOT. sane ) THEN
    PRINT *, sname, " is not sane."
  END IF
END FUNCTION sp_sanity


!
! allocate alloc sp_list
!
SUBROUTINE alloc_sp_list( sp_list, grid )
  TYPE(t_list), ALLOCATABLE, INTENT(inout) :: sp_list(:)
  TYPE(t_grid), INTENT(in)    :: grid

  ALLOCATE( sp_list(grid%nz) )
END SUBROUTINE alloc_sp_list


!
! delete array od linked lists
!
SUBROUTINE delete_sp_list( sp_list )
  TYPE(t_list), ALLOCATABLE, INTENT(inout) :: sp_list(:)

  INTEGER :: i, ii
  TYPE(t_node), POINTER :: this, next

  DO i = 1, SIZE( sp_list )
    this => sp_list(i)%head
    DO ii = 2, sp_list(i)%length
      next => this%next
      !CALL delete_tnode( this%hist%l )
      !CALL delete_tnode( this%hist%r )
      DEALLOCATE( this )
      this => next
    END DO
  END DO

  DEALLOCATE( sp_list )
END SUBROUTINE delete_sp_list


!
! delete node in a list
!
RECURSIVE SUBROUTINE delete_tnode( tnode )
  TYPE(t_tnode), POINTER, INTENT(inout) :: tnode

  IF ( .NOT.ASSOCIATED(tnode) ) RETURN
  CALL delete_tnode( tnode%l )
  CALL delete_tnode( tnode%r )
  DEALLOCATE( tnode%l, tnode%r )
END SUBROUTINE delete_tnode


!
! merge pure water or ice super particles at similar mass
! use namelist parameter mergeRatioLimit to control
! warning breaks agg_hist
!
! Implementation Notes: 
! 1.) deletion from list is O(n) as sp_list is only forward list, that's why 
! necessary information is copied into array which is sorted by quicksort, 
! hopefully being more efficient than mergesort of sp_list itself
! 2.) However, not very efficient for WDROPS as the whole list copied
!
SUBROUTINE mergeSPs( sp_list )
  TYPE(t_list), INTENT(inout) :: sp_list

  INTEGER :: nsp, isp, n_drop, iter
  TYPE(t_node), POINTER :: node, prev_node, del_node
  TYPE(t_sp), POINTER :: sp1, sp2
  TYPE(t_sortNode), ALLOCATABLE :: dropArray(:)
#ifndef WDROPS
  TYPE(t_sortNode), ALLOCATABLE :: iceArray(:)
  INTEGER :: n_ice
#endif

  IF( mergeRatioLimit <= 1.0 ) RETURN

    nsp = sp_list%length
    IF( nsp > mergeNoTresh ) THEN
      ! IF( sp_list%head%sp%z > 7500.0_wp ) RETURN
#ifndef WDROPS
      ALLOCATE( iceArray(nsp) )
      n_ice = 0
#endif
      ALLOCATE( dropArray(nsp) )
      n_drop = 0
      node => sp_list%head
      prev_node => node
      DO isp = 1, nsp                                                         ! loop over particles and sort in pure ice or drop arrays
#ifndef WDROPS
        IF      ( node%sp%m_r + node%sp%m_w < TINY(node%sp%m_r) ) THEN
          n_ice = n_ice + 1
          iceArray(n_ice)%val = node%sp%m_i
          iceArray(n_ice)%targetNode => node
        ELSE IF ( node%sp%m_i + node%sp%m_r < TINY(node%sp%m_r) ) THEN
#endif
          n_drop = n_drop + 1
          dropArray(n_drop)%val = node%sp%m_w
          dropArray(n_drop)%targetNode => node
#ifndef WDROPS
        END IF 
#endif
        prev_node => node
        node => node%next 
      END DO                                                                  ! end loop over particles
#ifndef WDROPS
      IF( n_ice > 1 ) THEN
        CALL quickSort( iceArray(1:n_ice) )                                   ! sort ice particle array according to m_i
        DO isp = 1, (n_ice-1)                                                 ! loop over pure ice particles and possible merge similar ones
          IF( iceArray(isp+1)%val/iceArray(isp)%val < mergeRatioLimit ) THEN  ! merge sp2 with sp1 (ignoring z, mm, etc) and delete sp1
            sp1 => iceArray(isp+0)%targetNode%sp
            sp2 => iceArray(isp+1)%targetNode%sp 
                                                                         
            sp2%m_i = (sp1%xi*sp1%m_i + sp2%xi*sp2%m_i)/(sp1%xi+sp2%xi)
            sp2%xi  = sp1%xi+sp2%xi 
            iceArray(isp+1)%val = sp2%m_i
            sp1%statusb = IBSET(sp1%statusb, sp_stat%merged)
          END IF
        END DO                                                                ! end loop over pure ice particles
      END IF
      DEALLOCATE( iceArray )
#endif
      IF( n_drop > 1 ) THEN
        CALL quickSort( dropArray(1:n_drop) )                                 ! sort water drop array according to m_w
        DO isp = 1, (n_drop-1)                                                ! loop over pure water particles and possible merge similar ones
          IF( dropArray(isp+1)%val/dropArray(isp)%val < mergeRatioLimit ) THEN! merge sp2 with sp1 (ignoring z, mm, etc) and delete sp1
            sp1 => dropArray(isp+0)%targetNode%sp
            sp2 => dropArray(isp+1)%targetNode%sp
            sp2%m_w = (sp1%xi*sp1%m_w + sp2%xi*sp2%m_w)/(sp1%xi+sp2%xi)
            dropArray(isp+1)%val = sp2%m_w
            sp2%xi  = sp1%xi + sp2%xi
            sp1%statusb = IBSET(sp1%statusb, sp_stat%merged)
          END IF
        END DO                                                                ! end loop over pure water particles
      END IF
      DEALLOCATE( dropArray )

      nsp = sp_list%length
      node => sp_list%head
      DO isp = 1, nsp                                                       ! loop over list to delete inactive
        IF( BTEST(node%sp%statusb, sp_stat%merged) ) THEN
          sp_list%head => node%next
          del_node => node
          node => node%next
          DEALLOCATE( del_node )
          sp_list%length = sp_list%length - 1
        ELSE
          DO iter = (isp+1), nsp
            prev_node => node
            node => node%next
            IF( BTEST(node%sp%statusb, sp_stat%merged) ) THEN
              prev_node%next => node%next 
              del_node => node
              node => prev_node
              DEALLOCATE( del_node )
              sp_list%length = sp_list%length - 1
            END IF
          END DO
          EXIT
        END IF
      END DO

    END IF
CONTAINS
  !
  ! quicksort algorithm
  !
  RECURSIVE SUBROUTINE quickSort( array )
    TYPE(t_sortNode), INTENT(inout) :: array(:)

    INTEGER :: left, right, length, marker
    REAL(wp) :: random, pivot
    TYPE(t_sortNode) temp

    length = SIZE(array)

    IF( length > 1 ) THEN
      CALL random_number(random)
      pivot = array(int(random*REAL(length-1))+1)%val
      left  = 0
      right = length + 1
      DO WHILE (left < right)
        right = right - 1
        DO WHILE ( array(right)%val > pivot )
          right = right - 1
        END DO
        left = left + 1
        DO WHILE ( array(left)%val < pivot )
          left = left + 1
        END DO
        IF( left < right ) THEN
          temp = array(left)
          array(left)  = array(right)
          array(right) = temp
        END IF
      END DO
      IF( left == right ) THEN
        marker = left + 1
      ELSE
        marker = left
      END IF
      CALL quickSort( array(1:(marker-1)) )
      CALL quickSort( array(marker:length) )
    END IF
  END SUBROUTINE quickSort

END SUBROUTINE mergeSPs


!
! compute total liquid mass
!
SUBROUTINE list_stats( sp_list, grid, stats )
  TYPE(t_list),       INTENT(in)  :: sp_list(:)
  TYPE(t_grid),          INTENT(in)  :: grid
  TYPE(t_list_stats), INTENT(inout) :: stats

  TYPE(t_sp),           POINTER :: sp
  TYPE(t_node), POINTER :: node
  INTEGER       :: i, isp
  REAL(wp) :: tot_mass_sys, tot_prec_sys, nrp, nsp

  tot_mass_sys = 0._wp  ! [kg]
  tot_prec_sys = 0._wp  ! [kg * m/s]
  nsp         = 0._wp  ! [SP/m3]
  nrp         = 0._wp  ! [RP/m3]

  DO i = 1, SIZE( sp_list )

    node => sp_list(i)%head
    nsp = nsp + sp_list(i)%length

    DO isp = 1, sp_list(i)%length
      sp => node%sp
#ifndef WDROPS
      tot_mass_sys = tot_mass_sys + (sp%m_i+sp%m_r+sp%m_w)*REAL(sp%xi,wp)
      tot_prec_sys = tot_prec_sys + (sp%m_i+sp%m_r+sp%m_w)*REAL(sp%xi,wp) * sp%v
#else
      tot_mass_sys = tot_mass_sys + sp%m_w*REAL(sp%xi,wp)
      tot_prec_sys = tot_prec_sys + sp%m_w*REAL(sp%xi,wp) * sp%v
#endif
      nrp = nrp + REAL(sp%xi,wp)
      node => node%next
    END DO
  END DO

  stats%tot_mass_sys = tot_mass_sys
  stats%nsp = nsp / grid%dom_vol
  stats%nrp = nrp / grid%dom_vol
  stats%tot_mass = tot_mass_sys / grid%dom_vol
  stats%tot_prec = tot_prec_sys / grid%dom_vol
  stats%mean_mass = stats%tot_mass / MAX(stats%nrp, 1._wp)
END SUBROUTINE list_stats

END MODULE mo_sp_list
