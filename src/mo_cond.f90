!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_cond

  USE mo_constants,    ONLY: wp, z1_3, z4pi_3, z3_4pi
  USE mo_sp_types,     ONLY: t_sp
  USE mo_sp_nml,       ONLY: msg_level
  USE mo_atmo_types,   ONLY: T_3i, ps, rhol, rholi, t_atmo, Rv

  IMPLICIT NONE
  PRIVATE

#ifdef WDROPS
  INTEGER, PARAMETER  :: subcycles = 1
  REAL(wp), PARAMETER :: tol = 1e-2_wp

  PUBLIC :: sp_cond
#endif
  

CONTAINS


#ifdef WDROPS

!
! condensation/evaporation of super droplets
!
SUBROUTINE sp_cond( sp, atmo, dt )
  TYPE(t_sp),    INTENT(inout) :: sp
  TYPE(t_atmo),  INTENT(in)    :: atmo
  REAL(wp),      INTENT(in)    :: dt

  INTEGER :: inewton
  REAL(wp) :: r, fr, dtg_f, dm, m0

  dtg_f = dt/REAL(subcycles,wp)  ! time fraction

  m0 = sp%m_w
  r  = (z3_4pi*rholi*sp%m_w)**z1_3

  DO inewton = 1, subcycles
   fr = condf( r, atmo )
   r  = (z3_4pi * rholi * sp%m_w)**z1_3
   dm = z4pi_3 * rhol * (r + fr*dtg_f)**3
   sp%m_w = sp%m_w + dm
   IF ( ABS(dm) < sp%m_w * tol ) EXIT
  END DO

  ! check convergence
  IF ( ABS(dm) > sp%m_w * tol ) THEN
    PRINT *, "WARNING Newton did not converge"
    PRINT *, "        |m1-m2|: ", ABS(dm)
  END IF

#ifndef NDEBUG
  IF ( msg_level > 15 ) &
    PRINT *, "growth due to cond/evap: ", sp%m_w - m0
#endif
END SUBROUTINE sp_cond

!
! condensation equation in form f(x) = 0 
! valued for a super-droplet
!
FUNCTION condf( r, atmo ) RESULT( cond )
  REAL(wp),     INTENT(in) :: r
  TYPE(t_atmo), INTENT(in) :: atmo
  REAL(wp)                 :: cond

  REAL(wp) :: Tc, S, L, Ka, Fk, Fd, Dv

  Tc = atmo%T - 273.15_wp

  ! saturation ratio
  S = atmo%rh*0.01_wp - 1.0_wp

  ! [J/kg] latent heat release of water
  L = 2500800_wp + Tc*(-2360_wp + Tc*(1.6_wp - 0.06_wp*Tc))

  ! [m2/s] diffusivity of water vapor (PK '97, p. 503)
  Dv = 0.211e-4_wp * (atmo%T*T_3i)**1.94_wp * ps / atmo%p

  ! [J/msK] thermal conductivity of dry air (PK '97, p. 508)
  Ka = 41.84e-5_wp*(5.69_wp + 0.017_wp*Tc)

  Fk = L*rhol*(L-Rv*atmo%T) / (Rv*Ka*atmo%T**2)
  Fd = rhol*Rv*atmo%T / (Dv*atmo%psatw)

  ! Shima et al (2006), eq. 2 without b/r^3
  cond = (S - 1._wp - 3.3e-7_wp/(atmo%T*r)) / (r*(Fk + Fd))
END FUNCTION


! end def WDROPS
#endif

END MODULE mo_cond
