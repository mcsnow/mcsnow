!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_exception

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: message_text
  PUBLIC :: message, finish

  CHARACTER(len=1000) :: message_text = ''

CONTAINS

  SUBROUTINE finish (name, text, exit_no)
    CHARACTER(len=*), INTENT(in)           :: name
    CHARACTER(len=*), INTENT(in), OPTIONAL :: text
    INTEGER,          INTENT(in), OPTIONAL :: exit_no

    INTEGER           :: iexit

    WRITE (*,'(/,80("="),/)')
    IF (PRESENT(exit_no)) THEN
       iexit = exit_no
    ELSE
       iexit = 1     ! POSIX defines this as EXIT_FAILURE
    END IF

    IF (PRESENT(text)) THEN
      IF (iexit == 1) THEN
        WRITE (*,'(a,a,a,a)') 'FATAL ERROR in ', TRIM(name), ': ', TRIM(text)
      ELSE
        WRITE (*,'(1x,a,a,a)') TRIM(name), ': ', TRIM(text)
      ENDIF
    ELSE
      IF (iexit == 1) THEN
        WRITE (*,'(a,a)') 'FATAL ERROR in ', TRIM(name)
      ELSE
        WRITE (*,'(1x,a)') TRIM(name)
      ENDIF
    ENDIF

    WRITE (*,'(/,80("-"),/,/)')
    STOP 'mo_exception: finish ..'
  END SUBROUTINE finish



  SUBROUTINE message (name, text)
    CHARACTER (len=*), INTENT(in) :: name
    CHARACTER (len=*), INTENT(in) :: text

    CHARACTER(len=LEN(message_text)) :: write_text
    CHARACTER(len=10) :: ctime
    CHARACTER(len=8)  :: cdate

    message_text = TRIM(text)
    IF (name /= '')  THEN
      message_text = TRIM(name) // ': ' // TRIM(message_text)
    ENDIF
    write_text = message_text

    CALL DATE_AND_TIME(date=cdate,time=ctime)
    WRITE(*,'(a)') '['//cdate//' '//ctime//'] '//TRIM(write_text)
  END SUBROUTINE message
  


END MODULE mo_exception
