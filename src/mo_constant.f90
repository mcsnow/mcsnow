!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_constants

  IMPLICIT NONE

  INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6,37) !< single precision
  INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(12,307) !< double precision
  INTEGER, PARAMETER :: i2 = SELECTED_INT_KIND(4)   !< at least 2 byte integer
  INTEGER, PARAMETER :: i4 = SELECTED_INT_KIND(9)   !< at least 4 byte integer
  INTEGER, PARAMETER :: i8 = SELECTED_INT_KIND(14)   !< at least 8 byte integer
  !INTEGER, PARAMETER :: i8 = i4
  INTEGER, PARAMETER :: mp = wp ! enable mixed precision by switching to mp = sp

  REAL(wp), PARAMETER :: pi = ATAN(1._wp) * 4._wp

  REAL(wp), PARAMETER :: z1_6   = 1.0_wp/6.0_wp
  REAL(wp), PARAMETER :: z1_3   = 1.0_wp/3.0_wp
  REAL(wp), PARAMETER :: z2_3   = 2.0_wp/3.0_wp

  REAL(wp), PARAMETER :: pi_6   = pi/6.0_wp
  REAL(wp), PARAMETER :: pi_4   = pi/4.0_wp
  REAL(wp), PARAMETER :: pi_2   = pi/2.0_wp 
  
  REAL(wp), PARAMETER :: z4_pi  = 4.0_wp/pi
  REAL(wp), PARAMETER :: z6_pi  = 6.0_wp/pi

  REAL(wp), PARAMETER :: z4pi_3 = 4._wp*pi/3._wp
  REAL(wp), PARAMETER :: z3_4pi = 3._wp/(4._wp*pi)

  REAL(wp), PARAMETER :: zs1_2pi = 1/SQRT(2.0_wp*pi)


END MODULE mo_constants

