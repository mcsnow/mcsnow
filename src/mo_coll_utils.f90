!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_coll_utils

  USE mo_constants,         ONLY: wp, pi, z1_3, z2_3, z3_4pi, z4pi_3
  USE mo_sp_nml,            ONLY: sp_kernel_id, coll_kern_type, sp_kern_sig
  USE mo_sp_types,          ONLY: t_sp
  USE mo_atmo,              ONLY: t_atmo, get_atmo
#ifndef WDROPS
  USE mo_mass2diam,         ONLY: m2d_particle, spheroid_geometry
  USE mo_velocity,          ONLY: vterm_snow
  USE mo_colleffi,          ONLY: Ecbohm_spheroid
  USE mo_atmo_types,        ONLY: rhoi, rhoii
#else
  USE mo_velocity,         ONLY: vterm_beard
  USE mo_atmo_types,        ONLY: rholi
  USE mo_colleffi,          ONLY: ecollBeheng
#endif

  IMPLICIT NONE
  PRIVATE

  PROCEDURE(sp_kernel), POINTER :: coll_kernel

  ABSTRACT INTERFACE
    FUNCTION sp_kernel( sp1, sp2, ce ) RESULT(kernel)
      IMPORT :: t_sp, wp
      TYPE(t_sp), INTENT(inout) :: sp1, sp2
      REAL(wp),     INTENT(out) :: ce
      REAL(wp) :: kernel
    END FUNCTION
  END INTERFACE

  PUBLIC  ::              &
    coll_kernel,          & ! pointer to aggr.-riming kernel
    golovin_kernel,       &
    hydrodynamic_kernel,  &
    set_coll_kernel
#ifndef WDROPS
  PUBLIC  ::              &
    habit_aggregates
#endif

CONTAINS


!
! set aggregation-riming/coalescence kernel
!
SUBROUTINE set_coll_kernel()
  SELECT CASE (sp_kernel_id)
    CASE (1)
      coll_kernel => golovin_kernel
    CASE (2) 
      coll_kernel => hydrodynamic_kernel
    CASE DEFAULT
      WRITE(*,*) "sp_kernel_id not 1 or 2., collisions deactivated"
      !STOP "sp_kernel_id not 1 or 2."
  END SELECT
END SUBROUTINE set_coll_kernel


!
! Golovin's kernel
!
FUNCTION golovin_kernel( sp1, sp2, ce  ) RESULT(kern)
  TYPE(t_sp), INTENT(inout) :: sp1, sp2
  REAL(wp),     INTENT(out) :: ce
  REAL(wp) :: kern

  ce = 1._wp

  ! the coefficient is in [m3/(kg*s)]
  kern = &
#ifndef WDROPS
  1.5_wp*(sp1%m_i + sp1%m_f + sp1%m_r + sp1%m_w + sp2%m_i + sp2%m_f + sp2%m_r + sp2%m_w)
#else
  1.5_wp*(sp1%m_w + sp2%m_w)
#endif
END FUNCTION golovin_kernel


!
! Hydrodynamic kernel
!
FUNCTION hydrodynamic_kernel( sp1, sp2, ce ) RESULT( kern )
  TYPE(t_sp), INTENT(inout) :: sp1, sp2
  REAL(wp),     INTENT(out) :: ce        !> coll. eff.

  REAL(wp)     :: kern, r1, r2, vel, rand, v1, v2

#ifndef WDROPS
  TYPE(t_atmo) :: atmo
  !REAL(wp) :: q1, q2

  ! particles would collide somewhere in the middle between
  ! their current positions
  CALL get_atmo( 0.5_wp*(sp1%z + sp2%z), atmo )

  CALL m2d_particle( sp1 )
  CALL m2d_particle( sp2 )


  IF (coll_kern_type > 0 ) THEN
    r1 = 0.5_wp * sp1%d
    r2 = 0.5_wp * sp2%d
  ELSE 
    ! area equivalent radii
    r1 = 0.5_wp * sp1%d * MIN( 1.0_wp, 1._wp / SQRT( sp1%phi ) )
    r2 = 0.5_wp * sp2%d * MIN( 1.0_wp, 1._wp / SQRT( sp2%phi ) )
  END IF


  ! has the term. vel. been already computed elsewhere?
  sp1%v = vterm_snow( atmo, sp1 )
  sp2%v = vterm_snow( atmo, sp2 )

  ! ratio of the effective A over the circumscribed cross-sectional A_e area
  ! Heymsfield and Westbrook 2010, p.2471: q = A_e/A = A_e / (pi/4 * D**2)
  !q1 = ( 1e-3_wp*sp1%m_r / (sp1%v_r *(100*sp1%d)**(-0.6)* 0.078) )**z2_3
  !q2 = MERGE( 0.83_wp, 4/pi, q1 > 0.83 )
  !q1 = sp1%p_area / (0.25_wp * pi * sp1%d**2)
  !q2 = sp2%p_area / (0.25_wp * pi * sp2%d**2)

  ! collision efficiency with temp. based sticking efficiency
  ce = Ecbohm_spheroid( atmo, sp1, sp2 ) !, q1, q2 )
  !ce = ce * stick_effi( sp1, sp2, atmo%rho, atmo%etai, atmo%T )

  ! compute sticking efficiency
  !sa = pi * MERGE( sp1%d**2, sp2%d**2, sp1%d < sp2%d )
  !rhob = (sp1%m_i*rhoi + sp1%m_r**2/sp1%v_r) / (sp1%m_i + sp1%m_r)
  !nre = vt1 * SQRT( rhob * 1._wp ) * sp1%d * rhoa * etaa_i
  !vel_factor = 

#else
  !ifdef WDROPS

  r1 = ( z3_4pi * rholi * sp1%m_w )**z1_3
  r2 = ( z3_4pi * rholi * sp2%m_w )**z1_3

  ! has the term. vel. been already computed elsewhere?
  sp1%v = vterm_beard( r1 )
  sp2%v = vterm_beard( r2 )

  ce = ecollBeheng( 100._wp*r1, 100._wp*r2 )
#endif

  IF (sp_kern_sig .gt. 0._wp) THEN
    CALL random_number( rand )
    v1  = sp1%v + ( (rand - 0.5_wp) * sp_kern_sig)
    CALL random_number( rand )
    v2  = sp2%v + ( (rand - 0.5_wp) * sp_kern_sig)
    vel = ABS(v1 - v2) 
  ELSE
    vel = ABS(sp1%v - sp2%v)
  END IF 

  kern = ce * pi * (r1+r2)**2 * vel

END FUNCTION hydrodynamic_kernel

#ifndef WDROPS
!
! Aggregation procedure for habits following Shima et al.(2020, p.12/13) 
! Shima rules out model of Chen & Lamb 1994b due to impossibly low density particles
!
SUBROUTINE habit_aggregates(sp1, sp2, gama_t, V_upd, phi_upd)
  TYPE(t_sp),  TARGET, INTENT(in) :: sp1, sp2
  INTEGER,     INTENT(in)         :: gama_t
  REAL(wp),    INTENT(out)        :: V_upd, phi_upd

  TYPE(t_sp), POINTER :: spl, sps
  INTEGER :: f1, f2
  REAL(wp), PARAMETER :: rho_crit = 10._wp
  REAL(wp) :: m_tot, r_a1, r_a2, r_c1, r_c2, rho_min, rho_new, rho_vol, V_1, V_2, V_new

  IF (sp1%d > sp2%d) THEN
    spl => sp1
    sps => sp2
    f1  = gama_t 
    f2  = 1
  ELSE
    spl => sp2
    sps => sp1
    f1  = 1
    f2  = gama_t 
  END IF

  ! mass, volume, and geometry of the two aggregating particles, only ice phase variables
  m_tot = (spl%m_i + spl%m_r + spl%m_f) * f1 + (sps%m_i + sps%m_r + sps%m_f) * f2
  CALL spheroid_geometry(spl, r_a1, r_c1, V_1)
  CALL spheroid_geometry(sps, r_a2, r_c2, V_2)

  rho_vol = m_tot / ( V_1 * f1 + V_2 *f2 )                 ! largest possible volume

  ! Shima '20, text p.12
  IF (spl%phi .LT. 1._wp) THEN
    V_new = z4pi_3 * r_a1**2 * ( r_c1 * f1 + MIN(r_a2, r_c2) * f2 )                      
  ELSE
    V_new = z4pi_3 * r_c1 *    ( r_a1 * f1 + MIN(r_a2, r_c2) * f2 ) * MAX(r_a1, r_a2, r_c2)
  END IF
  rho_min = m_tot / V_new                         ! Shima 20, Eq.61
  ! Shima 20, Eq. 62
  rho_new = ( ( rhoi - rho_vol ) * rho_vol + (rho_vol - rho_crit) * rho_min ) / (rhoi - rho_crit)

  IF (spl%phi .LT. 1._wp) THEN
    r_c1 = m_tot / ( rho_new * z4pi_3 * r_a1**2 )       ! Shima 20, Eq. 63
  ELSE
    r_a1 = SQRT( m_tot / ( rho_new * z4pi_3 * r_c1 ) )  ! Shima 20, Eq. 70
  END IF
  ! diagnose new geometry
  phi_upd = r_c1 / r_a1
  V_upd   = z4pi_3 * r_a1**2 * r_c1
END SUBROUTINE habit_aggregates
#endif

END MODULE mo_coll_utils
