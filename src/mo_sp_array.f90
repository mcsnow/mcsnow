!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_sp_array
#ifdef HAVE_OPENMP
  USE omp_lib
#endif 
  USE mo_constants,    ONLY: wp, i4, i8
  USE mo_sp_types,     ONLY: t_traj, t_sp, t_list_stats, sp_stat
  USE mo_sp_nml,       ONLY: mergeRatioLimit, mergeNoTresh
  USE mo_grid,         ONLY: coord2index

  IMPLICIT NONE
  PRIVATE
  SAVE
  
  !>
  !! <Basic type that holds the number of Lagrangian objects per cell>
  !!
  !! <Describe type rationale in more detail.>
  !!
  TYPE :: t_cell
    INTEGER(i4) :: myCellId       = -1 !< order in the 1D Lagragian object array
    INTEGER     :: startPos       =  0 !< start iteration at this offset
    INTEGER     :: noParts        =  0 !< iterate over this number and check whether they are active
    INTEGER     :: noDelParts     =  0 !< since some of them are inactive
    INTEGER     :: noNewParts     =  0 !< number of particle that will be accessable after resorting
    INTEGER     :: noReceiveParts =  0 !< number of particle to receive from another MPI task
  END TYPE t_cell

  !>
  !! <Basic type that holds >
  !!
  !! <Describe type rationale in more detail.>
  !!
  TYPE :: t_containerBase
    INTEGER(i8) :: endOfActSParray =  1
    INTEGER(i8) :: beginOfHalo     =  1
    INTEGER(i8) :: endOfSParray    =  1
    INTEGER(i8) :: currLocalInd    =  1
    INTEGER(i8) :: maxNoParts      = -1
    INTEGER(i4) :: noCells         =  0
    INTEGER(i4) :: endProgCellId   = -1
    !INTEGER(KIND=MPI_ADDRESS_KIND) :: objSizeBytes
    !INTEGER     :: commType
    TYPE(t_patch), POINTER     :: p_patch
    !INTEGER,       ALLOCATABLE :: send_buf(:,:)
    !INTEGER,       ALLOCATABLE :: recv_buf(:,:)
    !INTEGER,       ALLOCATABLE :: requests(:)
    TYPE(t_cell),  ALLOCATABLE :: cellSParray(:,:,:)
  END TYPE t_containerBase

  !>
  !! <Extention of t_containerBase to hold t_traj>
  !!
  !! <Describe type rationale in more detail.>
  !!
  TYPE, EXTENDS(t_containerBase)  :: t_cont
    TYPE(t_sp),         ALLOCATABLE :: sp_array(:)
  END TYPE t_cont

  INTEGER, PARAMETER :: end_all_cells = 1
  INTEGER, PARAMETER :: start_prog_cells = 1
  INTEGER, PARAMETER :: end_prog_cells = 1

  TYPE :: t_virt_cells
    contains
      procedure, nopass :: start_block
      procedure, nopass :: end_block
  END TYPE t_virt_cells

  TYPE :: t_patch
    TYPE(t_virt_cells) :: cells
    INTEGER :: rank = -1
    INTEGER :: nproma = 1
    INTEGER :: nblks_c = 1
    INTEGER :: nlev
  END TYPE t_patch

  ! type for sorting and merging
  TYPE :: t_sort
    REAL(wp) :: val
    INTEGER  :: ind
  END TYPE t_sort

  PUBLIC :: t_patch, t_cont, t_cell
  PUBLIC :: addParticle, delParticle, allocContainer, deallocContainer, moveSpsToNewCell, & 
          & sp_sanity, gather_stats, mergeSPs !, updateArrayInplace

  INTERFACE delParticle ! due to gnu OpenMP bug (does not make vtab shared) this function cannot be CLASS but has to be duplicated
    MODULE PROCEDURE delParticleSp, delParticleTraj
  END INTERFACE delParticle
  

CONTAINS

!! fake icon structure

  INTEGER FUNCTION start_block( dummy )
    INTEGER, INTENT(in) :: dummy
    start_block = 1
  END FUNCTION

  INTEGER FUNCTION end_block( dummy )
    INTEGER, INTENT(in) :: dummy
    end_block = 1
  END FUNCTION end_block

  SUBROUTINE get_indices_c(p_patch, jb, i_startblk, i_endblk, is, ie, ii, jj)
    TYPE(t_patch), OPTIONAL, INTENT(IN)  :: p_patch    !< current patch
    INTEGER,       OPTIONAL, INTENT(IN)  :: jb, i_startblk, i_endblk, ii, jj
    INTEGER,       OPTIONAL, INTENT(OUT) :: is, ie
    is = 1
    ie = 1
  END SUBROUTINE get_indices_c



!! add, del, move particles
  SUBROUTINE addParticle( container, sp, jc, jk, jb )
    TYPE(t_cont),             INTENT(INOUT) :: container
    TYPE(t_sp),               INTENT(IN)    :: sp
    INTEGER,                  INTENT(IN)    :: jc, jk, jb

    container%sp_array(container%endOfSParray) = sp
    CALL addParticleBase( container, container%sp_array(container%endOfSParray), jc, jk, jb )
  END SUBROUTINE addParticle

  SUBROUTINE addParticleBase( container, sp, jc, jk, jb )
    CLASS(t_containerBase), INTENT(INOUT) :: container
    CLASS(t_traj),          INTENT(INOUT) :: sp
    INTEGER,                INTENT(IN)    :: jc, jk, jb

    IF( container%cellSParray(jk,jc,jb)%myCellId < 1 ) THEN
      WRITE(*,*) & !container%p_patch%rank, 
               &" CHRS Error, adding into non-local cell: index ", &
               & jc, " level ", jk, " block ", jb, &
               & " myCellId ", container%cellSParray(jk,jc,jb)%myCellId
      STOP
    END IF
    container%endOfSParray = container%endOfSParray + 1
    sp%local_idx = container%currLocalInd
    container%currLocalInd = container%currLocalInd + 1
    sp%start_pe  = container%p_patch%rank
    sp%cellId    = container%cellSParray(jk,jc,jb)%myCellId
    !$OMP ATOMIC
    container%cellSParray(jk,jc,jb)%noNewParts = container%cellSParray(jk,jc,jb)%noNewParts + 1

!#ifndef NDEBUG
    IF( container%endOfSParray > container%maxNoParts ) THEN
      WRITE(*,*) "Error more SPs then allocated maxNoParts ", container%maxNoParts
      STOP
    END IF
!#endif
  END SUBROUTINE addParticleBase

  SUBROUTINE delParticleSp( cell, sp )
    !CLASS(t_containerBase), INTENT(INOUT) :: container
    !CLASS(t_traj),          INTENT(INOUT) :: sp
    TYPE(t_cell), INTENT(INOUT) :: cell
    TYPE(t_sp),   INTENT(INOUT) :: sp
    sp%cellId = -1
    cell%noDelParts = cell%noDelParts + 1
  END SUBROUTINE delParticleSp

  SUBROUTINE delParticleTraj( cell, sp )
    !CLASS(t_containerBase), INTENT(INOUT) :: container
    !CLASS(t_traj),          INTENT(INOUT) :: sp
    TYPE(t_cell), INTENT(INOUT) :: cell
    TYPE(t_traj), INTENT(INOUT) :: sp
    sp%cellId = -1
    cell%noDelParts = cell%noDelParts + 1
  END SUBROUTINE delParticleTraj

  SUBROUTINE moveParticle( container, sp, jc, jk, jb, new_jc, new_jk, new_jb )
    CLASS(t_containerBase), INTENT(INOUT) :: container
    CLASS(t_traj),          INTENT(INOUT) :: sp
    INTEGER,                INTENT(IN)    :: jc, jk, jb, new_jc, new_jk, new_jb

    IF( container%cellSParray(new_jk,new_jc,new_jb)%myCellId < 1 ) THEN
      WRITE(*,*) "Error loosing moveParticle! "
      CALL delParticle( container%cellSParray(jk,jc,jb), sp )
    ELSE
      !container%cellSParray(    jk,    jc,    jb)%noParts    = container%cellSParray(    jk,    jc,    jb)%noParts    - 1
      container%cellSParray(    jk,    jc,    jb)%noDelParts = container%cellSParray(    jk,    jc,    jb)%noDelParts + 1
      !$OMP ATOMIC
      container%cellSParray(new_jk,new_jc,new_jb)%noNewParts = container%cellSParray(new_jk,new_jc,new_jb)%noNewParts + 1
      sp%cellId = container%cellSParray(new_jk,new_jc,new_jb)%myCellId
    END IF
  END SUBROUTINE moveParticle

  !
  ! this subroutine moves the sps to their new cell
  ! Important: this function cannot be paralellized by OpenMP
  !
  SUBROUTINE moveSpsToNewCell( cont, stats, dom_bottom, box_area )
    !CLASS(t_containerBase), INTENT(INOUT) :: cont
    !CLASS(t_traj), TARGET,  INTENT(INOUT) :: sps(:)
    TYPE(t_cont), TARGET,  INTENT(INOUT) :: cont
    TYPE(t_list_stats),    INTENT(INOUT) :: stats
    REAL(wp),              INTENT(IN)    :: dom_bottom, box_area

    INTEGER :: ic, jk, jc, jb, nsp, startPos, isp, icell
    REAL(wp) :: mtot
    !CLASS(t_traj),   POINTER :: sp
    TYPE(t_sp), POINTER :: sp

    IF( 2*cont%endOfSParray > cont%maxNoParts ) THEN
      WRITE(*,*) "Warning reallocating particle array, consider setting namelist parameter maxTotaNoSps to ", 2*cont%maxNoParts
      CALL reallocArray( cont )
    END IF
                                 
    DO ic = 1, cont%endProgCellId
      jk = ic                      
      jc = 1
      jb = 1
      nsp      = cont%cellSParray(jk,jc,jb)%noParts
      startPos = cont%cellSParray(jk,jc,jb)%startPos
      DO isp = 1, nsp
        sp => cont%sp_array(startPos+isp-1)
        IF( sp%cellId < 1 ) CYCLE ! inactive particle, e.g. xi=0 in coll
        IF ( sp%z - dom_bottom < 1e-2_wp ) THEN
#ifndef WDROPS
          mtot = sp%m_i + sp%m_r + sp%m_w
#else
          mtot = sp%m_w
#endif        
          stats%nsedi = stats%nsedi + REAL(sp%xi,wp)/box_area
          stats%msedi = stats%msedi + REAL(sp%xi,wp)*mtot/box_area
          CALL delParticle( cont%cellSParray(jk,jc,jb), sp )
        ELSE
          icell = coord2index( sp%z ) ! did the SP stay in the same cell?
          IF ( icell /= ic ) THEN
            CALL moveParticle( cont, sp, jc, jk, jb, jc, icell, jb )
          END IF
        END IF
      END DO
    END DO

    CALL updateArrayInplace( cont )
  END SUBROUTINE moveSpsToNewCell

  !
  ! check if SPs are realistic
  !
  LOGICAL FUNCTION sp_sanity( sps, opt_name ) RESULT(sane)
    TYPE(t_sp),    TARGET,  INTENT(INOUT) :: sps(:)
    CHARACTER(len=*), OPTIONAL            :: opt_name

    CHARACTER(100) :: sname
    INTEGER :: isp
    TYPE(t_sp),   POINTER :: sp

    IF(PRESENT(opt_name)) THEN
      sname = opt_name
    ELSE
      sname = "The Array"
    END IF

    sane = .TRUE.
    DO isp = 1, SIZE(sps)
      sp => sps(isp)
      IF( sp%cellId < 1 ) CYCLE ! inactive particle
      IF(      sp%xi  <= 0     & 
        & .OR. sp%mm  < 1._wp  &
        & .OR. sp%m_w < 0._wp  &
#ifndef WDROPS        
        & .OR. sp%m_i < 0._wp &
        & .OR. sp%m_r < 0._wp &
        & .OR. sp%v_r < 0._wp &
        & .OR. sp%V_i < 0._wp &
        & .OR. sp%m_f < 0._wp &
        & .OR. sp%phi < 0._wp &
#endif
        & ) THEN
        sane = .FALSE.
        WRITE(*,*) "insane particle ", sp
        EXIT 
      END IF
    END DO

    IF ( .NOT. sane ) THEN
      PRINT *, sname, " is not sane."
    END IF
  END FUNCTION sp_sanity

  !
  ! merge pure water or ice super particles at similar mass
  ! use namelist parameter mergeRatioLimit to control
  ! warning breaks agg_hist
  !
  ! Implementation Notes: 
  ! 1.) deletion from list is O(n) as sp_list is only forward list, that's why 
  ! necessary information is copied into array which is sorted by quicksort, 
  ! hopefully being more efficient than mergesort of sp_list itself
  ! 2.) However, not very efficient for WDROPS as the whole list copied
  !
  SUBROUTINE mergeSPs( cell, sps )
    TYPE(t_cell), INTENT(inout) :: cell
    TYPE(t_sp),   INTENT(inout) :: sps(:)

    INTEGER :: nsp, isp, ind1, ind2
    INTEGER :: n_drop
    TYPE(t_sort), ALLOCATABLE :: dropArray(:)
#ifndef WDROPS
    INTEGER :: n_ice, n_froozen
    TYPE(t_sort), ALLOCATABLE :: iceArray(:), frozenArray(:)
#endif

    IF( mergeRatioLimit <= 1.0 ) RETURN

      nsp = cell%noParts
      IF( nsp > mergeNoTresh ) THEN
      !IF( sp_list%head%sp%z > 7500.0_wp ) RETURN
#ifndef WDROPS
        ALLOCATE( iceArray(nsp) )
        ALLOCATE( frozenArray(nsp))
        n_ice = 0
        n_froozen = 0
#endif
        ALLOCATE( dropArray(nsp) )
        n_drop = 0
        DO isp = 1, nsp                                                       ! loop over particles and sort in pure ice or drop arrays
          IF( sps(isp)%cellId < 1 ) CYCLE
#ifndef WDROPS
          IF      ( sps(isp)%m_r + sps(isp)%m_f + sps(isp)%m_w < TINY(sps(isp)%m_r) ) THEN
            n_ice = n_ice + 1
            iceArray(n_ice)%val = sps(isp)%m_i
            iceArray(n_ice)%ind = isp
          ELSE IF ( sps(isp)%m_r + sps(isp)%m_i + sps(isp)%m_w < TINY(sps(isp)%m_i) ) THEN
            n_froozen  = n_froozen + 1
            frozenArray(  n_froozen )%val = sps(isp)%m_f
            frozenArray(  n_froozen )%ind = isp
          ELSE IF ( sps(isp)%m_i + sps(isp)%m_f + sps(isp)%m_r < TINY(sps(isp)%m_r) ) THEN
#endif
            n_drop = n_drop + 1
            dropArray(n_drop)%val = sps(isp)%m_w
            dropArray(n_drop)%ind = isp
#ifndef WDROPS
          END IF
#endif
        END DO                                                                  ! end loop over particles
#ifndef WDROPS
        IF( n_ice > mergeNoTresh ) THEN
          CALL quickSort( iceArray(1:n_ice) )                                   ! sort ice particle array according to m_i
          DO isp = 1, (n_ice-1)                                                 ! loop over pure ice particles and possible merge similar ones
            IF( iceArray(isp+1)%val/iceArray(isp)%val < mergeRatioLimit ) THEN  ! merge sp2 with sp1 (ignoring z, mm, etc) and delete sp1
              ind1 = iceArray(isp+0)%ind
              ind2 = iceArray(isp+1)%ind
              sps(ind1)%m_i = (sps(ind1)%xi*sps(ind1)%m_i + sps(ind2)%xi*sps(ind2)%m_i)/(sps(ind1)%xi+sps(ind2)%xi)
              sps(ind1)%xi  = sps(ind1)%xi+sps(ind2)%xi
              sps(ind1)%statusb = IBSET(sps(ind1)%statusb, sp_stat%merged)
              iceArray(isp+1)%val = TINY(iceArray(isp+1)%val)
              CALL delParticle( cell, sps(ind2) )
            END IF
          END DO                                                                ! end loop over pure ice particles
        END IF
        DEALLOCATE( iceArray )

        IF( n_froozen > mergeNoTresh ) THEN
          CALL quickSort( frozenArray(1:n_froozen) )                            ! sort ice particle array according to m_f
          DO isp = 1, (n_froozen-1)                                             ! loop over pure ice particles and possible merge similar ones
            IF( frozenArray(isp+1)%val/frozenArray(isp)%val < mergeRatioLimit ) THEN  ! merge sp2 with sp1 (ignoring z, mm, etc) and delete sp1
              ind1 = frozenArray(isp+0)%ind
              ind2 = frozenArray(isp+1)%ind
              sps(ind1)%m_i = (sps(ind1)%xi*sps(ind1)%m_i + sps(ind2)%xi*sps(ind2)%m_i)/(sps(ind1)%xi+sps(ind2)%xi)
              sps(ind1)%xi  = sps(ind1)%xi+sps(ind2)%xi
              sps(ind1)%statusb = IBSET(sps(ind1)%statusb, sp_stat%merged)
              frozenArray(isp+1)%val = TINY(frozenArray(isp+1)%val)
              CALL delParticle( cell, sps(ind2) )
            END IF
          END DO                                                                ! end loop over pure ice particles
        END IF
        DEALLOCATE( frozenArray )

#endif
        IF( n_drop > mergeNoTresh ) THEN
          CALL quickSort( dropArray(1:n_drop) )                                 ! sort water drop array according to m_w
          DO isp = 1, (n_drop-1)                                                ! loop over pure water particles and possible merge similar ones
            IF( dropArray(isp+1)%val/dropArray(isp)%val < mergeRatioLimit ) THEN! merge sp2 with sp1 (ignoring z, mm, etc) and delete sp1
              ind1 = dropArray(isp+0)%ind
              ind2 = dropArray(isp+1)%ind
              sps(ind1)%m_w = (sps(ind1)%xi*sps(ind1)%m_w + sps(ind2)%xi*sps(ind2)%m_w)/(sps(ind1)%xi+sps(ind2)%xi)
              sps(ind1)%xi  = sps(ind1)%xi + sps(ind2)%xi
              sps(ind1)%statusb = IBSET(sps(ind1)%statusb, sp_stat%merged)
              dropArray(isp+1)%val = TINY(dropArray(isp+1)%val)
              CALL delParticle( cell, sps(ind2) )
            END IF
          END DO                                                                ! end loop over pure water particles
        END IF
        DEALLOCATE( dropArray )

      END IF
  CONTAINS
    !
    ! quicksort algorithm
    !
    RECURSIVE SUBROUTINE quickSort( array )
      TYPE(t_sort), INTENT(inout) :: array(:)

      INTEGER :: left, right, length, marker
      REAL(wp) :: random, pivot
      TYPE(t_sort) temp

      length = SIZE(array)

      IF( length > 1 ) THEN
        CALL random_number(random)
        pivot = array(int(random*REAL(length-1))+1)%val
        left  = 0
        right = length + 1
        DO WHILE (left < right)
          right = right - 1
          DO WHILE ( array(right)%val > pivot )
            right = right - 1
          END DO
          left = left + 1
          DO WHILE ( array(left)%val < pivot )
            left = left + 1
          END DO
          IF( left < right ) THEN
            temp = array(left)
            array(left)  = array(right)
            array(right) = temp
          END IF
        END DO
        IF( left == right ) THEN
          marker = left + 1
        ELSE
          marker = left
        END IF
        CALL quickSort( array(1:(marker-1)) )
        CALL quickSort( array(marker:length) )
      END IF
    END SUBROUTINE quickSort

  END SUBROUTINE mergeSPs


  !
  ! compute total liquid mass, (call only after updateArray)
  !
  SUBROUTINE gather_stats( cont, sps, stats, dom_vol )
    TYPE(t_cont),           INTENT(IN)    :: cont
    TYPE(t_sp),             INTENT(IN)    :: sps(:)
    TYPE(t_list_stats),     INTENT(inout) :: stats
    REAL(wp),               INTENT(IN)    :: dom_vol

    REAL(wp) :: tot_mass_sys, tot_prec_sys, nrp, nsp
    INTEGER :: jc, jb, endInd

    tot_mass_sys = 0._wp  ! [kg]
    tot_prec_sys = 0._wp  ! [kg * m/s]
    nsp          = 0._wp  ! [SP/m3]
    nrp          = 0._wp  ! [RP/m3]

    jc = 1
    jb = 1
    nsp = SUM( cont%cellSParray(:,jc,jb)%noParts )
    endInd = cont%endOfSParray-1
    nrp = SUM( sps(1:endInd)%xi )
#ifndef WDROPS
    tot_mass_sys = SUM( (sps(1:endInd)%m_i + sps(1:endInd)%m_r + sps(1:endInd)%m_w) & 
                      & *sps(1:endInd)%xi )
    tot_prec_sys = SUM( (sps(1:endInd)%m_i + sps(1:endInd)%m_r + sps(1:endInd)%m_w) & 
                      & *sps(1:endInd)%xi * sps(1:endInd)%v )
#else
    tot_mass_sys = SUM( sps(1:endInd)%m_w*sps(1:endInd)%xi )
    tot_prec_sys = SUM( sps(1:endInd)%m_w*sps(1:endInd)%xi*sps(1:endInd)%v )
#endif

    stats%tot_mass_sys = tot_mass_sys
    stats%nsp = nsp / dom_vol
    stats%nrp = nrp / dom_vol
    stats%tot_mass = tot_mass_sys / dom_vol
    stats%tot_prec = tot_prec_sys / dom_vol
    stats%mean_mass = stats%tot_mass / MAX(stats%nrp, 1._wp)
  END SUBROUTINE gather_stats

!! (de)alloc container
  SUBROUTINE allocContainer( p_patch, container, maxNoParts )
    TYPE(t_patch),   TARGET,  INTENT(IN)    :: p_patch    !< current patch
    CLASS(t_containerBase),   INTENT(INOUT) :: container  !< container to allocate
    INTEGER(i8),              INTENT(IN)    :: maxNoParts !< max number of particles per mpi-task

    CALL allocContainerBase( p_patch, container, maxNoParts )

    SELECT TYPE ( container ) ! called only few times, hence usage of select type is acceptable
      TYPE IS ( t_cont )
        ALLOCATE(container%sp_array(maxNoParts))
        container%sp_array(:)%cellId = -1
      CLASS DEFAULT
        WRITE(*,*) " CHRS, Error in allocContainer, unkown container type "
        STOP " CHRS, Error in allocContainer, unkown container type "
    END SELECT
  END SUBROUTINE allocContainer

  SUBROUTINE reallocArray( container )
    TYPE(t_cont), INTENT(inout) :: container

    TYPE(t_sp), ALLOCATABLE :: temp_array(:)
    INTEGER(i8) :: newMaxNoParts

    newMaxNoParts = 2*container%maxNoParts
    ALLOCATE( temp_array(newMaxNoParts) )
    temp_array(1:container%maxNoParts) = container%sp_array(:)
    CALL move_alloc(temp_array, container%sp_array)

    IF( ALLOCATED( temp_array ) ) THEN
      WRITE(*,*) "Warning temp_array still allocated in reallocArray"
      DEALLOCATE( temp_array )
    END IF
    container%maxNoParts = newMaxNoParts
  END SUBROUTINE reallocArray

  SUBROUTINE allocContainerBase( p_patch, container, maxNoParts )
    !USE mo_impl_constants,      ONLY: start_prog_cells, end_prog_cells !, end_all_cells
    !USE mo_loopindices,         ONLY: get_indices_c
    !USE mo_communication,       ONLY: idx_1d

    TYPE(t_patch),   TARGET,  INTENT(IN)    :: p_patch    !< current patch
    CLASS(t_containerBase),   INTENT(INOUT) :: container  !< container to allocate
    INTEGER(i8),              INTENT(IN)    :: maxNoParts !< max number of particles per mpi-task

    INTEGER     :: i_startblk, i_endblk, jb, jc, jk, is ,ie!, ii, jj
    INTEGER(i4) :: currentCellId

    IF( 2*i4 /= i8 ) THEN
      WRITE(*,*) "Error: two i4 variables do not have the same number of bytes as a i8 variable"
      STOP
    END IF
    IF( wp   /= i8 ) THEN
      WRITE(*,*) "Error: a REAL(wp) variable does not have the same number of bytes as a i8 variable"
      STOP
    END IF

    container%currLocalInd = 1
    container%endOfSParray = 1
    container%endOfActSParray = 1
    container%maxNoParts = maxNoParts
    container%beginOfHalo = 1
    container%p_patch => p_patch
    !ALLOCATE(container%recv_buf(p_patch%nlev,p_patch%comm_pat_c%n_recv))
    !ALLOCATE(container%send_buf(p_patch%nlev,p_patch%comm_pat_c%n_send))
    !container%recv_buf(:,:) = 0
    !container%send_buf(:,:) = 0
    !ALLOCATE(container%requests(p_patch%comm_pat_c%np_send+p_patch%comm_pat_c%np_recv))
    !container%requests(:) = MPI_REQUEST_NULL
    ALLOCATE(container%cellSParray(p_patch%nlev,p_patch%nproma,p_patch%nblks_c)) ! TODO switched nlev and nproma 
    container%cellSParray(:,:,:)%myCellId   = -1
    container%cellSParray(:,:,:)%startPos   = -1
    container%cellSParray(:,:,:)%noParts    =  0
    container%cellSParray(:,:,:)%noDelParts =  0
    container%cellSParray(:,:,:)%noNewParts =  0
    container%cellSParray(:,:,:)%noReceiveParts = 0
    ! init myCellId for cellSParray in 4 steps
    ! 1. mark communication cells, halo cells, and interpolation cells
    ! mark communication cells
    !DO ii = 1, p_patch%comm_pat_c%n_send
    !  jb = p_patch%comm_pat_c%send_src_blk(ii)
    !  jc = p_patch%comm_pat_c%send_src_idx(ii)
    !  jk = 1
    !  !DO jk = 1, p_patch%nlev
    !    container%cellSParray(jk,jc,jb)%myCellId = -2
    !  !END DO
    !END DO
    ! mark halo cells
    !DO ii = 1, p_patch%comm_pat_c%n_pnts
    !  jb = p_patch%comm_pat_c%recv_dst_blk(ii)
    !  jc = p_patch%comm_pat_c%recv_dst_idx(ii)
    !  jk = 1
    !  !DO jk = 1, p_patch%nlev
    !    container%cellSParray(jk,jc,jb)%myCellId = -3
    !  !END DO
    !END DO
    ! mark interpolation cells
    !i_startblk = p_patch%cells%start_block(1)
    !i_endblk   = p_patch%cells%end_block(start_prog_cells-1)
    !DO jb = i_startblk, i_endblk
    !  CALL get_indices_c(p_patch, jb, i_startblk, i_endblk, is, ie, 1, start_prog_cells-1)
    !  DO jc = is, ie
    !    jk = 1
    !    !DO jk = 1, p_patch%nlev
    !      container%cellSParray(jk,jc,jb)%myCellId = -4
    !    !END DO
    !  END DO
    !END DO
    ! 2. include local cells (skip marked)
    currentCellId = 1
    i_startblk = p_patch%cells%start_block(start_prog_cells)
    i_endblk   = p_patch%cells%end_block(end_prog_cells)
    DO jb = i_startblk, i_endblk
      CALL get_indices_c(p_patch, jb, i_startblk, i_endblk, is, ie, start_prog_cells, end_prog_cells)
      DO jc = is, ie
        IF( container%cellSParray(1,jc,jb)%myCellId > -2 ) THEN
          DO jk = 1, p_patch%nlev
            container%cellSParray(jk,jc,jb)%myCellId = currentCellId
            currentCellId = currentCellId + 1
          END DO
        !ELSE
        !  WRITE(120+p_pe_work,*) p_pe_work, "/1 CHRS, skipped jb = ", jb, " jc = ", jc, &
        !           & p_patch%cells%decomp_info%glb_index(idx_1d(jc,jb)), " as comm cell"
        END IF
      END DO
    END DO
    ! 3. include communication cells
    !DO ii = 1, p_patch%comm_pat_c%n_send
    !  jb = p_patch%comm_pat_c%send_src_blk(ii)
    !  jc = p_patch%comm_pat_c%send_src_idx(ii)
    !  IF( container%cellSParray(1,jc,jb)%myCellId < -3 ) THEN
    !    !WRITE(*,*) p_patch%rank, " CHRS skip comm cell as it is in interpolation zone ", jb, jc, &
    !    !         & p_patch%cells%decomp_info%glb_index(idx_1d(jc,jb))
    !    CYCLE
    !  END IF
    !  IF( container%cellSParray(1,jc,jb)%myCellId > 0 ) THEN
    !    !WRITE(*,*) "CHRS Comm cell has already cell id"
    !    CYCLE ! add cells only once, for which multiple domains have halos
    !  END IF
    !  DO jk = 1, p_patch%nlev
    !    container%cellSParray(jk,jc,jb)%myCellId = currentCellId
    !    currentCellId = currentCellId + 1
    !  END DO
    !END DO
    container%endProgCellId = currentCellId - 1
    ! 4. include halo cells, attention with the order:
    !DO ii = 1, p_patch%comm_pat_c%n_pnts
    !  DO jj = 1, p_patch%comm_pat_c%n_pnts
    !    IF(ii == p_patch%comm_pat_c%recv_src(jj)) EXIT
    !  END DO
    !  jb = p_patch%comm_pat_c%recv_dst_blk(jj)
    !  jc = p_patch%comm_pat_c%recv_dst_idx(jj)
    !  IF( container%cellSParray(1,jc,jb)%myCellId < -3 ) THEN
    !    !WRITE(*,*) p_patch%rank, " CHRS skip halo cell as it is in interpolation zone ", jb, jc, &
    !    !         & p_patch%cells%decomp_info%glb_index(idx_1d(jc,jb))
    !    CYCLE
    !  END IF
    !  DO jk = 1, p_patch%nlev
    !    IF( container%cellSParray(jk,jc,jb)%myCellId > 0 ) WRITE(*,*) "CHRS Halo cell has already cell id"
    !    container%cellSParray(jk,jc,jb)%myCellId = currentCellId
    !    currentCellId = currentCellId + 1
    !  END DO
    !END DO
    container%noCells = currentCellId - 1
  END SUBROUTINE allocContainerBase

  SUBROUTINE deallocContainer( container )
    CLASS(t_containerBase), INTENT(INOUT) :: container
    container%maxNoParts = -1
    container%noCells    =  0
    !IF( ALLOCATED( container%requests )   ) DEALLOCATE(container%requests)
    !IF( ALLOCATED( container%recv_buf )   ) DEALLOCATE(container%recv_buf)
    !IF( ALLOCATED( container%send_buf )   ) DEALLOCATE(container%send_buf)
    IF( ALLOCATED( container%cellSParray) ) DEALLOCATE(container%cellSParray)
    SELECT TYPE ( container ) ! called only few times, hence usage of select type is acceptable
      TYPE IS ( t_cont )
        IF( ALLOCATED( container%sp_array )   ) DEALLOCATE(container%sp_array)
      CLASS DEFAULT
        WRITE(*,*) " CHRS, Error in deallocContainer, unkown container type "
    END SELECT
  END SUBROUTINE deallocContainer

!! resort array
  SUBROUTINE updateArrayInplace( cont )
    TYPE(t_cont), INTENT(INOUT) :: cont
    TYPE(t_sp) :: saveSp, tmpSp
    INTEGER(i8)  :: i
    INTEGER      :: j, val
    INTEGER, ALLOCATABLE :: endPos(:), startPos(:), endPosIntern(:)
    ALLOCATE(startPos    (cont%noCells+1))
    ALLOCATE(endPos      (cont%noCells+1))
    ALLOCATE(endPosIntern(cont%noCells)  )
    CALL computeCellHistogram( cont, startPos, endPos, endPosIntern )
    DO i = 1, (cont%endOfSParray-1)   ! loop over particles to sort them according to the cells in which they reside
      IF( cont%sp_array(i)%cellId < 1 ) CYCLE
      val = cont%sp_array(i)%cellId
      IF( i == endPos(val) ) THEN
        endPos(val) = endPos(val) + 1
        CYCLE
      END IF
      ! always noReceiveParts=0 => endPosIntern(val)==startPos(val+1)
      IF( (i >= startPos(val)) .AND. (i < endPosIntern(val)) ) THEN 
      !IF( (i >= startPos(val)) .AND. (i < startPos(val+1)) ) THEN
        CYCLE
      END IF
      saveSP = cont%sp_array(i)
      cont%sp_array(i)%cellId = -1
      DO                ! swap particles until it can stay in an inactive
        val = saveSP%cellId
        j   = endPos(val)
        endPos(val)  = endPos(val) + 1
        IF( cont%sp_array(j)%cellId < 1 ) THEN
          cont%sp_array(j)  = saveSP
          EXIT
        ELSE
          IF( val == cont%sp_array(j)%cellId ) CYCLE 
          tmpSP       = cont%sp_array(j)
          cont%sp_array(j) = saveSP
          saveSP      = tmpSP
        END IF
      END DO            ! end swap particles
    END DO              ! end loop over particles
    CALL updateCells( cont, startPos, endPosIntern )
    DEALLOCATE( startPos )
    DEALLOCATE( endPos )
    DEALLOCATE( endPosIntern )
  END SUBROUTINE updateArrayInplace

  SUBROUTINE computeCellHistogram( cont, startPos, endPos, endPosIntern )
    !USE mo_impl_constants,          ONLY: end_all_cells
    !USE mo_loopindices,             ONLY: get_indices_c
    CLASS(t_containerBase), INTENT(IN)    :: cont
    INTEGER,                INTENT(OUT)   :: startPos(:)
    INTEGER,                INTENT(OUT)   :: endPos(:)
    INTEGER,                INTENT(OUT)   :: endPosIntern(:)
    INTEGER :: i_startblk, i_endblk, jb, is, ie, jc, jk, i
    startPos(:) = 0
    ! loop over all cells to get the start and end positions
    i_startblk = cont%p_patch%cells%start_block(1) !start_prog_cells)
    !i_startblk = cont%p_patch%cells%start_block(start_prog_cells)
    i_endblk   = cont%p_patch%cells%end_block(end_all_cells)
    DO jb = i_startblk, i_endblk
      !CALL get_indices_c(cont%p_patch, jb, i_startblk, i_endblk, is, ie, start_prog_cells, end_all_cells)
      CALL get_indices_c(cont%p_patch, jb, i_startblk, i_endblk, is, ie, 1, end_all_cells)
      DO jc = is, ie
        DO jk = 1, cont%p_patch%nlev
          IF( cont%cellSParray(jk,jc,jb)%myCellId > 0 ) THEN
            startPos(    cont%cellSParray(jk,jc,jb)%myCellId) = cont%cellSParray(jk,jc,jb)%noParts &
                                                            & + cont%cellSParray(jk,jc,jb)%noNewParts &
                                                            & + cont%cellSParray(jk,jc,jb)%noReceiveParts &
                                                            & - cont%cellSParray(jk,jc,jb)%noDelParts
            endPosIntern(cont%cellSParray(jk,jc,jb)%myCellId) = cont%cellSParray(jk,jc,jb)%noReceiveParts
          ELSE
            !WRITE(*,*) "CHRS in updateCellArraysInplace, myCellId ", cont%cellSParray(jk,jc,jb)%myCellId
          END IF
        END DO
      END DO
    END DO

    endPos(1) = 1
    DO i = 1, cont%noCells
      endPos(i+1)     = endPos(i)   + startPos(i)
      endPosIntern(i) = endPos(i+1) - endPosIntern(i)
    END DO
    startPos(:) = endPos(:)
  END SUBROUTINE computeCellHistogram

  SUBROUTINE updateCells( cont, startPos, endPos )
    !USE mo_impl_constants,          ONLY: end_all_cells
    !USE mo_loopindices,             ONLY: get_indices_c
    CLASS(t_containerBase), INTENT(INOUT) :: cont
    INTEGER,                INTENT(IN)    :: startPos(:), endPos(:)
    INTEGER :: i_startblk, i_endblk, jb, is, ie, jc, jk, thisCellId
    !i_startblk = cont%p_patch%cells%start_block(start_prog_cells)
    i_startblk = cont%p_patch%cells%start_block(1) ! TODO do we need particles in interpolation zone?
    i_endblk   = cont%p_patch%cells%end_block(end_all_cells)
    DO jb = i_startblk, i_endblk
      CALL get_indices_c(cont%p_patch, jb, i_startblk, i_endblk, is, ie, 1, end_all_cells)
      !CALL get_indices_c(p_patch, jb, i_startblk, i_endblk, is, ie, start_prog_cells, end_all_cells)
      DO jc = is, ie
        DO jk = 1, cont%p_patch%nlev
          IF( cont%cellSParray(jk,jc,jb)%myCellId > 0 ) THEN
            thisCellId = cont%cellSParray(jk,jc,jb)%myCellId
            cont%cellSParray(jk,jc,jb)%noParts    = endPos(thisCellId) - startPos(thisCellId)
            cont%cellSParray(jk,jc,jb)%noNewParts = 0
            cont%cellSParray(jk,jc,jb)%noDelParts = 0
            cont%cellSParray(jk,jc,jb)%startPos   = startPos(thisCellId)
          !ELSE
          !  WRITE(*,*) "CHRS ERROR: cell has no myCellId in updateCellArraysInplace"
          END IF
        END DO
      END DO
    END DO
    cont%endOfSParray    = endPos(cont%noCells)  ! the last cell is a halo cell, hence noReceiveParts == 0
    cont%endOfActSParray = cont%endOfSParray
    !WRITE(*,*) cont%p_patch%rank, " endOfSParray ", cont%endOfSParray, " noParts ", noParts

    cont%beginOfHalo = endPos(cont%endProgCellId)
  END SUBROUTINE updateCells

END MODULE mo_sp_array

