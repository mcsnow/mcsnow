!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_breakup

  USE mo_constants,      ONLY: wp, i8, pi, z4pi_3, z1_3
  USE mo_sp_nml,         ONLY: ibreakup => collbreakup_type
  USE mo_sp_types,       ONLY: t_sp, t_node, sp_stat, p_addSp
  USE mo_atmo_types,     ONLY: rhol
  USE mo_velocity,       ONLY: vterm_beard
  USE mo_distr,          ONLY: random_exponential, random_normal 

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: breakup_hydro, breakup_coll

  REAL(wp), PARAMETER ::       &
    sigma = 76.1-0.155*(293.15-273.15)

CONTAINS

SUBROUTINE breakup_hydro(sp,dt)
  TYPE(t_sp), INTENT(INOUT) :: sp
  REAL(wp),   INTENT(IN)    :: dt

  REAL(wp)              :: r0,rb,p0,mb,rnd
  REAL(wp), PARAMETER   :: pi43rho = 4./3.*pi*rhol
  INTEGER(i8)           :: xi
  INTEGER               :: counter

  INTEGER,  PARAMETER :: max_counter = 10
  REAL(wp), PARAMETER :: mhydrobr = pi/6_wp * rhol * (6e-3_wp)**3  ! hydro. breakup possible for D > 6 mm

  IF( sp%m_w < mhydrobr ) RETURN

  ! drop radius in cm
  r0 =  (sp%m_w/pi43rho)**(1./3.)*100 

  ! breakup probability
  p0 = 2.94e-7 * exp(34.0_wp*r0) * dt

  call random_number(rnd)
  if (rnd.lt.p0) then     
     do counter = 1,max_counter
        ! Use Eq. (11) of Srivastava (1971)
        rb = r0/7.0_wp * random_exponential()
        if (rb < r0/4. .and. rb > 0.02_wp ) exit
        !if (rb < r0/4. .and. rb > 0.02_wp .AND. (sp%m_w/(pi43rho*rb**3*0.01**3) < 200) ) exit
     end do
     IF( counter > max_counter ) RETURN ! did not find suitable value, so skip break-up
  
     ! radius in SI
     rb = rb * 0.01
     
     mb = pi43rho * rb**3

     xi = int(sp%xi * sp%m_w/mb,i8)

     ! conserve mass, as xi is truncated to an integer
     mb = sp%m_w*sp%xi/xi
     
     sp%m_w  = mb
     sp%xi = xi
  end if

END SUBROUTINE breakup_hydro

LOGICAL FUNCTION breakup_coll(addSp, sp1, sp2, gama_t) RESULT( lbreakup )
  procedure(p_addSp)                   :: addSp
  TYPE(t_sp),            INTENT(INOUT) :: sp1, sp2
  !TYPE(t_node), POINTER, INTENT(INOUT) :: bhead,btail
  !INTEGER,               INTENT(INOUT) :: ndis
  INTEGER,               INTENT(IN)    :: gama_t

  REAL(wp), PARAMETER :: mbreakup = pi/6_wp * rhol * (1e-4_wp)**3  ! breakup possible for D > 0.1 mm
  REAL(wp)            :: r1,r2,v1,v2,rs,rl, ecoal,cke,we,w1,w2,nf,mf,rf, rnd,We_crit

  lbreakup = .TRUE.
  IF( (sp1%m_w < mbreakup) .OR. (sp2%m_w < mbreakup) ) THEN
    lbreakup = .FALSE.
    RETURN
  END IF
  
  r1 = (sp1%m_w/(z4pi_3*rhol))**z1_3
  r2 = (sp2%m_w/(z4pi_3*rhol))**z1_3
  rl = max(r1,r2)
  rs = min(r1,r2)
  v1 = vterm_beard(rl)
  v2 = vterm_beard(rs)
  CALL breakup(rl,rs,abs(v1-v2),ecoal,cke,w1,w2,we,nf,mf,rf)
  CALL random_number(rnd)
  IF (rnd.gt.ecoal**gama_t) THEN
    if (ibreakup.eq.1) then
      We_crit = 6.0*(1+(rs/rl)**3)  ! Testik et al. 2011, Eq. 2 
      if (We.gt.We_crit) then       ! to exclude rebound pairs
        CALL breakup_straub(addSp,sp1,sp2,rl,rs,ecoal,cke,w1)
      end if
    elseif (ibreakup.eq.2) then
      We_crit = 6.0*(1+(rs/rl)**3)  ! Testik et al. 2011, Eq. 2 
      if (We.gt.We_crit) then       ! to exclude rebound pairs
        CALL breakup_mcfarquhar(addSp,sp1,sp2,rl,rs,ecoal,abs(v1-v2))
      end if
    elseif (ibreakup.eq.3) then
      if (rs.gt.1.0e-3_wp.or.(rs.gt.0.2e-3_wp.and.rl.gt.4e-3)) then
        CALL breakup_simple(sp1,sp2,gama_t)
      end if
    end if
  ELSE
    lbreakup = .FALSE.
  END IF
END FUNCTION breakup_coll

SUBROUTINE breakup_simple(sp1,sp2,gama_t)
  TYPE(t_sp)            :: sp1,sp2
  INTEGER               :: gama_t

  REAL(wp)              :: r0,rb,mb!,p0,rnd
  REAL(wp), PARAMETER   :: pi43rho = 4./3.*pi*rhol
  INTEGER(i8)           :: xi
  INTEGER               :: counter
  INTEGER,  PARAMETER   :: max_counter = 10

  ! (temporary) coalescence
  sp2%m_w  = gama_t*sp1%m_w + sp2%m_w
  sp1%xi = sp1%xi - gama_t*sp2%xi

  ! drop radius in m
  r0 =  (sp2%m_w/pi43rho)**(1./3.)

  do counter = 1,max_counter
     rb = r0 * random_exponential()
     if (rb < r0 .and. rb > 0.1e-3_wp) exit
  end do
  IF( counter > max_counter ) RETURN  ! did not find suitable value, so skip break-up
  
  mb = pi43rho * rb**3
  xi = int(sp2%xi * sp2%m_w/mb,i8)
  !conserve mass as xi is truncated to an integer
  mb = sp2%m_w*sp2%xi/xi

  sp2%m_w  = mb
  sp2%xi = xi

END SUBROUTINE breakup_simple

SUBROUTINE breakup_straub(addSp,sp1,sp2,rl,rs,ecoal,cke,we)
  procedure(p_addSp)    :: addSp
  TYPE(t_sp)            :: sp1, sp2
  !INTEGER               :: gama_t, ndis
  REAL(wp)              :: rl,rs,ecoal,cke,we
  !TYPE(t_node), POINTER, INTENT(inout) :: bhead,btail

  REAL(wp)     :: Dkl, Dgr, CW, gCW, d3sum, dcoal, d3coal!, gam
  REAL(wp)     :: mean1, vari1, delD1, Nmod1, diam1, mue1, sig1
  REAL(wp)     :: mean2, vari2, delD2, Nmod2, diam2
  REAL(wp)     :: mean3, vari3, delD3, Dmod3, Nmod3, diam3, diam4
  REAL(wp)     :: rnd,resi!,mass_new,mass_old
  INTEGER      :: mult1, mult2, mult3, mult4!, ni
  INTEGER(i8)  :: xicoll

  TYPE(t_node), POINTER :: bnode1, bnode2, bnode3, bnode4

  REAL(wp), PARAMETER   :: Dmin  = 0.01    ! minimum diameter of 1st mode
  REAL(wp), PARAMETER   :: Dmod1 = 0.05    ! V2, diameter of 1st mode in cm
  !REAL(wp), PARAMETER   :: Dmod1 = 0.06    ! V1, diameter of 1st mode in cm  
  REAL(wp), PARAMETER   :: Dmod2 = 0.095   ! diameter of 2nd mode in cm

  LOGICAL, PARAMETER :: debug_breakup = .false.
  
  LOGICAL :: sp2_lt_sp1, sp2_ge_sp1

  sp2_lt_sp1 = sp2%m_w.lt.sp1%m_w
  sp2_ge_sp1 = .not. sp2_lt_sp1

  ! (temporary) coalescence with gama_t=1
  sp2%m_w  = sp1%m_w  + sp2%m_w
  sp1%xi = sp1%xi - sp2%xi

  ! multiplicity of collision
  xicoll = sp2%xi   

  Dgr = 2*rl * 100.  ! larger diameter in cm
  Dkl = 2*rs * 100.  ! smaller diameter in cm

  ! diameter of coalesced drop in cm
  dcoal  = (sp2%m_w*6./pi/rhol)**(1./3.)*100
  d3coal = dcoal**3

  !..conversion of CKE to muJ (input is CGS)
  !  (1 muJ = 1e-6 J = 10 g*cm^2/s^2)   
  CKE = CKE / 10.

  CW  = CKE*We
  gCW = Dgr/Dkl*CW

  !..first mode, log-normal distribution
  if (gCW.gt.7.0) then
     !delD1 = 0.0125 * sqrt(CW)   ! Orig
     !delD1 = 0.025 * sqrt(CW)    ! V1
     delD1 = 0.035 * sqrt(CW)     ! V2
     mean1 = Dmod1
     vari1 = delD1**2/12.
     !Nmod1 = 0.088 * (gCW-7.0) * 1.7 ! V1
     Nmod1 = 0.088 * (gCW-7.0) * 3.   ! V2
     sig1 = log(vari1/mean1**2 + 1.0)
     mue1 = log(mean1) - 0.5*sig1
     mue1 = max(mue1,log(Dmin))
  else
     Nmod1 = 0.0
  end if

  !..2nd mode, normal distribution
  if (CW.gt.21.0) then
     delD2 = 0.02 * (CW - 21.) 
     mean2 = Dmod2
     vari2 = sqrt(delD2**2/12.)
     Nmod2 = 0.22 * (CW - 21.) * 1.3
   else
     Nmod2 = 0.0
  end if

  !..3rd mode, normal distribution
  if (CW.le.46.0) then
     Dmod3 = 0.90 * Dkl
     delD3 = 0.02 * (1.0 + 0.76*sqrt(CW)) 
     mean3 = Dmod3
     vari3 = sqrt(delD3**2/12.) 
     Nmod3 = min(0.04 * (46. - CW), 1.0_wp) 
  else
     Nmod3 = 0.0
  end if

  !..first mode, integer number of samples and size sample from log-normal distr.
  if (Nmod1.gt.0) then
     call random_number(rnd)
     mult1 = INT(Nmod1)+INT(rnd+(Nmod1-INT(Nmod1)))
     diam1 = exp(mue1 + sqrt(sig1) * random_normal()) 
     d3sum = mult1*diam1**3
  else
     mult1 = 0
     diam1 = 1.
     d3sum = 0.
  end if  
  if (d3sum.gt.d3coal) then
     diam1 = (d3coal/mult1)**(1./3.)
     d3sum = d3coal
  end if

  !..2nd mode, integer number of samples and size sample from normal distr.
  if (Nmod2.gt.0.and.d3sum.lt.d3coal) then
     call random_number(rnd)
     mult2 = INT(Nmod2)+INT(rnd+(Nmod2-INT(Nmod2)))
     diam2 = MAX(mean2 + vari2 * random_normal(),Dmin)
     d3sum = mult1*diam1**3 + mult2*diam2**3 
  else
     mult2 = 0
     diam2 = 1.
  end if
  if (d3sum.gt.d3coal) then
     diam2 = ((d3coal-mult1*diam1**3)/mult2)**(1./3.)
     d3sum = d3coal
  end if

  !..3rd mode, remnant of the small drop
  if (Nmod3.gt.0.and.d3sum.lt.d3coal) then
     if (Nmod3.lt.1) then
        call random_number(rnd)
        mult3 = INT(Nmod3)+INT(rnd+(Nmod3-INT(Nmod3)))
     else
        mult3 = 1
     end if
     diam3 = MAX(mean3 + vari3 * random_normal(),Dmin)
  else
     mult3 = 0
     diam3 = 1.
  end if

  !..4th mode, remnant of the large drop
  resi = dcoal**3 - mult1*diam1**3 - mult2*diam2**3 - mult3*diam3**3
  if (resi.gt.0) then
     mult4 = 1
     diam4 = resi**(1./3.)
  elseif (resi.lt.0.and.mult3.eq.1) then
     diam3 = (dcoal**3 - mult1*diam1**3 - mult2*diam2**3)**(1./3.)
     mult4 = 0
     diam4 = 1.          
  elseif (resi.lt.0.and.mult2.ge.1) then
     diam2 = ((dcoal**3 - mult1*diam1**3)/mult2)**(1./3.)
     mult4 = 0
     diam4 = 1.          
  elseif (resi.lt.0) then
     write (*,*) 'ERROR: resi < 0 in breakup_straub', resi, dgr, dkl
     write(*,'(a,i5)') ' mult1 = ',mult1
     write(*,'(a,i5)') ' mult2 = ',mult2
     write(*,'(a,i5)') ' mult3 = ',mult3
     write(*,'(a,i5)') ' mult4 = ',mult4
     STOP
  else
     mult4 = 0
     diam4 = 1.     
  end if

  if (debug_breakup) then
     write(*,'(a,f10.3)') '-------------------------------'
     write(*,'(a,f10.3)') ' Dgr   = ',Dgr
     write(*,'(a,f10.3)') ' Dkl   = ',Dkl
     write(*,'(a,f12.5)') ' ecoal = ',ecoal
     write(*,'(a,f10.3)') ' CKE   = ',CKE
     write(*,'(a,f10.3)') ' We    = ',We
     write(*,'(a,f10.3)') ' CW    = ',CW
     write(*,'(a,f10.3)') ' gCW   = ',gCW
     write(*,'(a,f10.3)') ' Nmod1 = ',Nmod1
     write(*,'(a,f10.3)') ' delD1 = ',delD1
     write(*,'(a,f10.3)') ' sig1   = ',sig1
     write(*,'(a,f10.3)') ' mue1   = ',mue1
     write(*,'(a,f10.3)') ' Nmod2 = ',Nmod2
     write(*,'(a,f10.3)') ' delD2 = ',delD2
     write(*,'(a,f10.3)') ' mean2 = ',mean2
     write(*,'(a,f12.5)') ' vari2 = ',vari2
     write(*,'(a,f10.3)') ' Nmod3 = ',Nmod3
     write(*,'(a,f10.3)') ' delD3 = ',delD3
     write(*,'(a,f10.3)') ' mean3 = ',mean3
     write(*,'(a,f12.5)') ' vari3 = ',vari3
     write(*,'(a,f12.5)') ' diam1 = ',diam1
     write(*,'(a,f12.5)') ' diam2 = ',diam2
     write(*,'(a,f12.5)') ' diam3 = ',diam3
     write(*,'(a,f12.5)') ' diam4 = ',diam4
     write(*,'(a,i5)') ' mult1 = ',mult1
     write(*,'(a,i5)') ' mult2 = ',mult2
     write(*,'(a,i5)') ' mult3 = ',mult3
     write(*,'(a,i5)') ' mult4 = ',mult4
  end if

  !..change to SI units
  diam1 = diam1 * 0.01
  diam2 = diam2 * 0.01
  diam3 = diam3 * 0.01
  diam4 = diam4 * 0.01

  !..3rd mode corresponds to smaller existing SP
  if (mult3 > 0) then
     if (sp2_lt_sp1) then
        sp2%m_w  = pi/6_wp * rhol * diam3**3
        sp2%xi = xicoll * mult3
        sp2%mm = 1
        sp2%statusb = IBSET( 0, sp_stat%breakup )
     else
        ALLOCATE( bnode3 )
        bnode3%sp%m_w  = pi/6_wp * rhol * diam3**3
        bnode3%sp%xi = xicoll * mult3
        bnode3%sp%z  = sp2%z
        bnode3%sp%mm = 1
        bnode3%sp%T  = sp1%T
        bnode3%sp%statusb = IBSET( 0, sp_stat%breakup )
        CALL addSp( bnode3%sp )
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode3%next => bhead
        !   bhead => bnode3
        !ELSE
        !   bhead => bnode3
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        NULLIFY( bnode3 )
        !ndis = ndis - 1
     end if
  else 
     ! mult3=0
     if (sp2_lt_sp1) then
        sp2%xi = 0  ! available for fragments
     end if
  end if

  !..4th mode corresponds to large existing SP
  if (mult4 > 0) then
     if (sp2_ge_sp1) then
        sp2%m_w  = pi/6_wp * rhol * diam4**3
        sp2%xi = xicoll
        sp2%mm = 1
        sp2%statusb = IBSET( 0, sp_stat%breakup )
     else
        ALLOCATE( bnode4 )
        bnode4%sp%m_w  = pi/6_wp * rhol * diam4**3
        bnode4%sp%xi = xicoll
        bnode4%sp%z  = sp2%z
        bnode4%sp%mm = 1
        bnode4%sp%T  = sp1%T
        bnode4%sp%statusb = IBSET( 0, sp_stat%breakup )
        CALL addSp( bnode4%sp )
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode4%next => bhead
        !   bhead => bnode4
        !ELSE
        !   bhead => bnode4
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        NULLIFY( bnode4 )
        !ndis = ndis - 1
     end if
  else 
     ! mult4=0
     if (sp2_ge_sp1) then
        sp2%xi = 0 ! available for fragments
     end if
  end if

  !..add SP for 1st mode into blist
  if (mult1 > 0) then
     ALLOCATE( bnode1 )
     bnode1%sp%m_w  = pi/6_wp * rhol * diam1**3
     bnode1%sp%xi = xicoll * mult1
     bnode1%sp%z  = sp2%z
     bnode1%sp%mm = 1
     bnode1%sp%T  = sp1%T
     bnode1%sp%statusb = IBSET( 0, sp_stat%breakup )
     IF (sp2%xi == 0) THEN
        sp2%m_w  = bnode1%sp%m_w
        sp2%xi = bnode1%sp%xi
        sp2%mm = bnode1%sp%mm
        sp2%statusb = bnode1%sp%statusb
     ELSE
        CALL addSp( bnode1%sp )
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode1%next => bhead
        !   bhead => bnode1
        !ELSE
        !   bhead => bnode1
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        !ndis = ndis - 1
     END IF
     NULLIFY( bnode1 )
  END IF

  !..add SP for 2nd mode into blist
  if (mult2 > 0) then
     ALLOCATE( bnode2 )
     bnode2%sp%m_w  = pi/6_wp * rhol * diam2**3
     bnode2%sp%xi = xicoll * mult2
     bnode2%sp%z  = sp2%z
     bnode2%sp%mm = 1
     bnode2%sp%T  = sp1%T
     bnode2%sp%statusb = IBSET( 0, sp_stat%breakup )
     IF (sp2%xi == 0) THEN
        sp2%m_w  = bnode2%sp%m_w
        sp2%xi = bnode2%sp%xi
        sp2%mm = bnode2%sp%mm
        sp2%statusb = bnode2%sp%statusb
     ELSE
        CALL addSp( bnode2%sp ) 
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode2%next => bhead
        !   bhead => bnode2
        !ELSE
        !   bhead => bnode2
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        !ndis = ndis - 1
     END IF
     NULLIFY( bnode2 )
  END IF

  IF (sp2%xi == 0) THEN
     WRITE (*,*) 'ERROR: Need to delete SP2 ',mult1,mult2,mult3,mult4,sp2_lt_sp1
     STOP
  END IF

END SUBROUTINE breakup_straub

SUBROUTINE breakup_mcfarquhar(addSp,sp1,sp2,rlrg,rsml,ecoal,dv)
  procedure(p_addSp)    :: addSp
  TYPE(t_sp)            :: sp1, sp2
  !INTEGER               :: gama_t, ndis
  REAL(wp)              :: rlrg,rsml,ecoal,dv
  !TYPE(t_node), POINTER, INTENT(inout) :: bhead,btail

  REAL(wp)     :: Dkl, Dgr, dcoal, d3coal!, d3sum
  REAL(wp)     :: Rf,Rs,Rd,Ff,Fs,Fd,Ds0,CKE,St,Sc,W1,W2,Dc,delS,U
  REAL(wp)     :: muelnf, siglnf, muesf,sigsf,Hsf,Flnf
  REAL(wp)     :: mueln, sigln, Fln
  REAL(wp)     :: diam1, diam2, diam3!, diam4
  REAL(wp)     :: rnd,resi!,mass_new,mass_old
  INTEGER      :: mult1, mult2, mult3!, mult4, ni
  INTEGER(i8)  :: xicoll
  LOGICAL      :: ldisk, lsheet, lfila

  TYPE(t_node), POINTER :: bnode1, bnode2, bnode3!, bnode4

  LOGICAL, PARAMETER :: debug_breakup = .false.

  INTEGER, PARAMETER :: mass_conserv_option = 2  ! (1) draw again (2) allow coalescence

  LOGICAL :: sp2_lt_sp1, sp2_ge_sp1

  sp2_lt_sp1 = sp2%m_w.lt.sp1%m_w
  sp2_ge_sp1 = .not. sp2_lt_sp1

  ! (temporary) coalescence with gama_t=1
  sp2%m_w  = sp1%m_w  + sp2%m_w
  sp1%xi = sp1%xi - sp2%xi

  ! multiplicity of collision
  xicoll = sp2%xi   

  U   = 100. * dv      ! velocity difference in cm/s
  Dgr = 2*rlrg * 100.  ! larger diameter in cm
  Dkl = 2*rsml * 100.  ! smaller diameter in cm

  ! diameter of coalesced drop in cm
  dcoal  = (sp2%m_w*6./pi/rhol)**(1./3.)*100
  d3coal = dcoal**3
  
  CALL energy(Dgr,Dkl,U,CKE,ST,Sc,W1,W2,Dc)
  CALL Rfsd(Dgr,Dkl,CKE,W2,Rf,Rs,Rd)
  CALL Ffsd(Dgr,Dkl,CKE,ST,Ff,Fs,Fd,Ds0) 

  ! from CGS (erg) to Joule
  CKE  = 1e-7 * CKE     
  delS = 1e-7 * (ST - Sc)   

  ldisk  = .false.
  lsheet = .false.
  lfila  = .false.

  ! determine active mode for this collision
  if (Rd.gt.0) then
     call random_number(rnd)
     if (rnd.le.Rd) ldisk = .true.
  end if
  if (.not.ldisk.and.Rs.gt.0) then
     call random_number(rnd)
     if (rnd.le.Rs) lsheet = .true.
  end if
  if (.not.ldisk.and..not.lsheet.and.Rf.gt.0) then
     lfila = .true.
  end if

  if (lfila) then

     ! filament mode
     muelnf = -3.884 + 1.115 * (1.0 - exp(-6.6e6 * delS))  ! Eq. (17)
     siglnf =  0.408 - 0.105 * (W1 - 1.546)**2             ! Eq. (18)
     siglnf = max(siglnf,0.186_wp)                            ! limit by W1=3
     muesf  = Dkl                                          ! Eq. (19)
     Hsf    = 54.1 * W1**(-0.851)                          ! Eq. (20)
     sigsf  = 1.0/(Hsf * sqrt(2.*pi))                      ! normal distribution

     Flnf  = MAX(Ff-2,0.0_wp)                                  ! see Eq. (6)
     call random_number(rnd)
     mult1 = INT(Flnf)+INT(rnd+(Flnf-INT(Flnf)))           ! integer fragment number
     mult2 = 1
     mult3 = 1

     if (mass_conserv_option == 1) then
        resi = -1.                                            ! loop until mass conservation
        do while (resi.lt.0)                                  ! is fulfilled
           diam1 = exp(muelnf + sqrt(siglnf) * random_normal()) 
           diam2 = muesf + sigsf * random_normal()
           resi  = dcoal**3 - mult1*diam1**3 - mult2*diam2**3
        end do
        if (resi.gt.0) then         
           diam3 = resi**(1./3.)      
        else         
           write(*,'(a,f10.3)') '-------------------------------'
           write(*,*) 'resi = ',resi
           if (lfila)  write(*,'(a,f10.3)') ' filament mode, Ff = ',Ff
           write(*,'(a,f10.3)') ' Dgr   = ',Dgr
           write(*,'(a,f10.3)') ' Dkl   = ',Dkl
           write(*,'(a,f12.5)') ' ecoal = ',ecoal
           write(*,'(a,f10.3)') ' CKE   = ',CKE*1e7
           write(*,'(a,f10.3)') ' We    = ',W2
           write(*,'(a,f12.5)') ' diam1 = ',diam1
           write(*,'(a,f12.5)') ' diam2 = ',diam2
           write(*,'(a,f12.5)') ' diam3 = ',diam3
           write(*,'(a,i5)') ' mult1 = ',mult1
           write(*,'(a,i5)') ' mult2 = ',mult2
           write(*,'(a,i5)') ' mult3 = ',mult3
           STOP
        end if
     else
        diam1 = exp(muelnf + sqrt(siglnf) * random_normal()) 
        diam2 = muesf + sigsf * random_normal()
        resi  = dcoal**3 - mult1*diam1**3 - mult2*diam2**3
        if (resi.gt.0) then         
           diam3 = resi**(1./3.)      
        else        
           mult1 = 0
           mult2 = 0
           mult3 = 1
           diam1 = 1.
           diam2 = 1.
           diam3 = dcoal
        end if
     end if
  else

     ! sheet and disk mode
     if (lsheet) then
        mueln = -3.650 + 0.51 * W1                         ! Eq. (21)
        sigln =  0.279 + 1.91e4 * CKE                      ! Eq. (22)
        Fln   = MAX(Fs-1.,1.0_wp)                              ! see Eq. (6)
     else
        mueln = -2.905  + 1.224 * Dkl/Dgr                  ! Eq. (24)
        sigln =  0.0949 + 2.131 * Dgr - 2.986 * Dkl        ! Eq. (25)
        Fln   = MAX(Fd-1.,1.0_wp)                              ! see Eq. (6)
     end if

     call random_number(rnd)
     mult1 = INT(Fln)+INT(rnd+(Fln-INT(Fln)))
     mult2 = 0
     mult3 = 1

     if (mass_conserv_option == 1) then
        resi = -1.
        do while (resi.lt.0) 
           diam1 = exp(mueln + sqrt(sigln) * random_normal()) 
           resi  = dcoal**3 - mult1*diam1**3
        end do
        if (resi.gt.0) then         
           diam3 = resi**(1./3.)      
        else         
           write(*,'(a,f10.3)') '-------------------------------'
           write(*,*) 'resi = ',resi
           if (ldisk)  write(*,'(a,f10.3)') ' disk mode, Fd = ',Fd
           if (lsheet) write(*,'(a,f10.3)') ' sheet mode, Fs = ',Fs
           write(*,'(a,f10.3)') ' Dgr   = ',Dgr
           write(*,'(a,f10.3)') ' Dkl   = ',Dkl
           write(*,'(a,f12.5)') ' ecoal = ',ecoal
           write(*,'(a,f10.3)') ' CKE   = ',CKE*1e7
           write(*,'(a,f10.3)') ' We    = ',W2
           write(*,'(a,f12.5)') ' diam1 = ',diam1
           write(*,'(a,f12.5)') ' diam2 = ',diam2
           write(*,'(a,f12.5)') ' diam3 = ',diam3
           write(*,'(a,i5)') ' mult1 = ',mult1
           write(*,'(a,i5)') ' mult2 = ',mult2
           write(*,'(a,i5)') ' mult3 = ',mult3
           STOP
        end if
     else
        diam1 = exp(mueln + sqrt(sigln) * random_normal()) 
        resi  = dcoal**3 - mult1*diam1**3
        if (resi.gt.0) then         
           diam3 = resi**(1./3.)      
        else        
           mult1 = 0
           mult2 = 0
           mult3 = 1
           diam1 = 1.
           diam2 = 1.
           diam3 = dcoal
        end if
     end if
  end if

  if (debug_breakup) then
     write(*,'(a,f10.3)') '-------------------------------'
     if (lfila)  write(*,'(a,f10.3)') ' filament mode, Ff = ',Ff
     if (ldisk)  write(*,'(a,f10.3)') ' disk mode, Fd = ',Fd
     if (lsheet) write(*,'(a,f10.3)') ' sheet mode, Fs = ',Fs
     write(*,'(a,f10.3)') ' Dgr   = ',Dgr
     write(*,'(a,f10.3)') ' Dkl   = ',Dkl
     write(*,'(a,f12.5)') ' ecoal = ',ecoal
     write(*,'(a,f10.3)') ' CKE   = ',CKE*1e7
     write(*,'(a,f10.3)') ' We    = ',W2
     write(*,'(a,f12.5)') ' diam1 = ',diam1
     write(*,'(a,f12.5)') ' diam2 = ',diam2
     write(*,'(a,f12.5)') ' diam3 = ',diam3
     write(*,'(a,i5)') ' mult1 = ',mult1
     write(*,'(a,i5)') ' mult2 = ',mult2
     write(*,'(a,i5)') ' mult3 = ',mult3
  end if

  !..change to SI units
  diam1 = diam1 * 0.01
  diam2 = diam2 * 0.01
  diam3 = diam3 * 0.01

  !..remnant of small drop corresponds to smaller existing SP
  if (mult2 > 0) then
     if (sp2_lt_sp1) then
        sp2%m_w  = pi/6_wp * rhol * diam2**3
        sp2%xi = xicoll * mult2
        sp2%mm = 1
        sp2%statusb = IBSET( 0, sp_stat%breakup )
     else
        ALLOCATE( bnode2 )
        bnode2%sp%m_w  = pi/6_wp * rhol * diam2**3
        bnode2%sp%xi = xicoll * mult2
        bnode2%sp%z  = sp2%z
        bnode2%sp%mm = 1
        bnode2%sp%T  = sp1%T
        bnode2%sp%statusb = IBSET( 0, sp_stat%breakup )
        CALL addSp( bnode2%sp )
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode2%next => bhead
        !   bhead => bnode2
        !ELSE
        !   bhead => bnode2
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        NULLIFY( bnode2 )
        !ndis = ndis - 1
     end if
  else 
     ! mult2=0
     if (sp2_lt_sp1) then
        sp2%xi = 0  ! available for fragments
     end if
  end if

  !..big drop corresponds to large existing SP
  if (mult3 > 0) then
     if (sp2_ge_sp1.or.sp2%xi.eq.0) then
        sp2%m_w  = pi/6_wp * rhol * diam3**3
        sp2%xi = xicoll
        sp2%mm = 1
        sp2%statusb = IBSET( 0, sp_stat%breakup )
     else
        ALLOCATE( bnode3 )
        bnode3%sp%m_w  = pi/6_wp * rhol * diam3**3
        bnode3%sp%xi = xicoll
        bnode3%sp%z  = sp2%z
        bnode3%sp%mm = 1
        bnode3%sp%T  = sp1%T
        bnode3%sp%statusb = IBSET( 0, sp_stat%breakup )
        CALL addSp( bnode3%sp )
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode3%next => bhead
        !   bhead => bnode3
        !ELSE
        !   bhead => bnode3
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        NULLIFY( bnode3 )
        !ndis = ndis - 1
     end if
  else 
     ! mult3=0
     if (sp2_ge_sp1) then
        sp2%xi = 0 ! available for fragments
     end if
  end if

  !..lognormal mode
  if (mult1 > 0) then
     ALLOCATE( bnode1 )
     bnode1%sp%m_w  = pi/6_wp * rhol * diam1**3
     bnode1%sp%xi = xicoll * mult1
     bnode1%sp%z  = sp2%z
     bnode1%sp%mm = 1
     bnode1%sp%T  = sp1%T
     bnode1%sp%statusb = IBSET( 0, sp_stat%breakup )
     IF (sp2%xi == 0) THEN
        sp2%m_w  = bnode1%sp%m_w
        sp2%xi = bnode1%sp%xi
        sp2%mm = bnode1%sp%mm
        sp2%statusb = bnode1%sp%statusb
     ELSE
        CALL addSp( bnode1%sp )
        !IF (ASSOCIATED(bhead)) THEN
        !   bnode1%next => bhead
        !   bhead => bnode1
        !ELSE
        !   bhead => bnode1
        !   NULLIFY(bhead%next)
        !   btail => bhead
        !END IF
        !ndis = ndis - 1
     END IF
     NULLIFY( bnode1 )
  END IF

  IF (sp2%xi == 0) THEN
     WRITE (*,*) 'ERROR: Need to delete SP2 ',mult1,mult2,mult3,sp2_lt_sp1
     STOP
  END IF

END SUBROUTINE breakup_mcfarquhar

SUBROUTINE breakup(r_l,r_s,dv,ecoal,cke,w1,w2,we,nf,mf,rf)
  REAL(wp), INTENT(in)  :: r_s,r_l,dv
  REAL(wp), INTENT(out) :: ecoal,cke,nf,mf,w1,w2,we
  REAL(wp), INTENT(out), OPTIONAL :: rf
  REAL(wp), PARAMETER   :: FPMIN = 1.e-30 
  REAL(wp), PARAMETER   :: rhoa = 1.0

  REAL(wp) :: p,U,N_w,gg,xx,x,rs,Ds,Dl,Df,q,qmin,qmax, &
       &      ecoalOchs,ecoalLL

  LOGICAL, PARAMETER  :: use_ecoal_straub  = .false.
  LOGICAL, PARAMETER  :: use_ecoal_lowlist = .true.
  REAL(wp), PARAMETER :: CKE0 = 8.93

  ! from SI to CGS
  U  = 100_wp * dv
  rs = 100_wp * r_s
  Ds = 200_wp * r_s
  Dl = 200_wp * r_l
  p  = R_s / R_l
  if (U.lt.FPMIN) U = FPMIN

  ! below is CGS
  N_w = rs * U**2 / sigma
  gg  = 2**(3./2.)/(6.*pi) * p**4 * (1.+ p) / ((1.+p**2)*(1.+p**3))
  xx  = N_w**(0.5) * gg

  ! Eq. (1) of Seifert et al. 2005
  ecoalOchs = max(0.767 - 10.14 * xx,0.0_wp)

  call ecoalLowList(Dl,Ds,U,ecoalLL,CKE,W1,W2)

  if (use_ecoal_straub) then
     ecoal = exp(-1.15_wp*W1)
  elseif (use_ecoal_lowlist) then
     q = Ds
     qmin = 300e-4
     qmax = 600e-4
     if (q.lt.qmin) then
        ! Here we use only Eq (1) of Seifert 2005, not Eq (2) and (3)
        ecoal = ecoalOchs
     elseif (q.ge.qmin.and.q.lt.qmax) then
        x = (q - qmin) / (qmax - qmin)
        ecoal = sin(pi/2.0*x)**2 * ecoalLL &
              + sin(pi/2.0*(1 - x))**2 * ecoalOchs
     elseif (q.ge.qmax) then
        ecoal = ecoalLL
     else
        ecoal = 1.0
     endif
  else
     ecoal = ecoalOchs
  end if
  !ecoal = ecoalLL

  If (PRESENT(rf)) THEN
     !..Calculate Rf, Eq. (4.40), LL82
     IF (CKE.LT.CKE0) THEN 
        Rf = 1.0 
     ELSE
        Rf = 1.11e-4 * (CKE*1.0e-7)**(-0.654)      
     ENDIF
  END IF

  We = rhoa * rs * U**2 / sigma

  nf = 0.1551 + 0.001439 * (1.0 - ecoalOchs) * We**2    ! Eq (26a) of BO95

  ! fragment diameter in cm
  Df = -0.00293+0.294*(MIN(Dl,0.18d0)*Ds)**(0.5)

  ! fragment mass in kg  
  mf = pi/6_wp * rhol * (Df*1e-2)**3

END SUBROUTINE breakup

SUBROUTINE ecoalLowList(Dgr,Dkl,U,ecoal,CKE,W1,W2)
  ! ATTENTION: Input in CGS units
  IMPLICIT NONE 
       
  REAL(wp) :: Dgr,Dkl,U,ecoal,CKE
  REAL(wp) :: ka,kb,epsi
  REAL(wp) :: ST,Sc,ET,dSTSc,W1,W2,Dc,Ecl
  REAL(wp) :: qq0,qq1,qq2

  PARAMETER (epsi=1.e-20)   
  
  ! 1 J = 10^7 g cm^2/s^2
  ka = 0.778                   ! Empirical Constant 
  kb = 2.61e-4                 ! Empirical Constant,[b]=2.61E6 m^2/J^2
  
  CALL energy(Dgr,Dkl,U,CKE,ST,Sc,W1,W2,Dc)

  dSTSc = ST-Sc             ! Diff. of Surf. Energies   [dSTSc] = g*cm^2/s^2   
  ET = CKE+dSTSc            ! Coal. Energy,             [ET]    =     "   
         
  IF (ET .LT. 50.0) THEN    ! ET < 5 uJ (= 50 g*cm^2/s^2) 
     qq0=1.0+(Dkl/Dgr)                       
     qq1=ka/qq0**2  
     qq2=kb*sigma*(ET**2)/(Sc+epsi)                        
     Ecl=qq1*exp(-qq2)     
  ELSE
     Ecl=0.0 
  ENDIF

  ecoal = Ecl

  RETURN 
END SUBROUTINE ecoalLowList

SUBROUTINE ENERGY(Dgr,Dkl,U,CKE,ST,Sc,W1,W2,Dc) 
  IMPLICIT NONE 

  REAL(wp) :: Dgr,Dkl,U,Dc
  REAL(wp) :: k10,rho
  REAL(wp) :: CKE,W1,W2,ST,Sc
  REAL(wp) :: Dgka3,Dgkb3,Dgka2 
  REAL(wp) :: dv
  
  REAL(wp), PARAMETER :: epsf=1.e-30 
  REAL(wp), PARAMETER :: FPMIN = 1.e-30 
  
  rho   = 1.0               ! Water Density,[rho]=g/cm^3 

  k10=rho*pi/12.0d0 

  Dgr = max(Dgr,epsf)
  Dkl = max(Dkl,epsf)

  Dgka2=(Dgr**2)+(Dkl**2)

  Dgka3=(Dgr**3)+(Dkl**3)
     
  if (Dgr.ne.Dkl) then
     dv = U
     if (dv.lt.FPMIN) dv = FPMIN 
     dv = dv**2
     if (dv.lt.FPMIN) dv = FPMIN 
     Dgkb3=(Dgr**3)*(Dkl**3) 
     CKE = k10 * dv * Dgkb3/Dgka3 ! Collision Energy           [CKE]=g*cm^2/s^2
  else
     CKE = 0.0d0
  endif
  ST = PI*sigma*Dgka2            ! Surf. Energy (Parent Drop)  [ST]=   ''      
  Sc = PI*sigma*Dgka3**(2./3.)   ! Surf. Energy (coal. System) [Sc]=   ''    
  
  W1=CKE/(Sc+epsf)                ! Weber Number 1   
  W2=CKE/(ST+epsf)                ! Weber Number 2   
  
  Dc=Dgka3**(1./3.)            ! Diam. of coal. System
  
  RETURN 
END SUBROUTINE ENERGY

SUBROUTINE Rfsd(Dgr,Dkl,CKE,W2,Rf,Rs,Rd)       
  IMPLICIT NONE 
  REAL(wp) :: Dgr,Dkl
  REAL(wp) :: Rf,Rs,Rd
  REAL(wp) :: Rfn,Rsn,norm
  REAL(wp) :: CKE,CKE0,CKEJ,W2,W20!,dW
       
  !REAL(wp), PARAMETER :: epsf=1.e-20

  !..CKE und W2 as in subroutine energy     
  CKE0 = 8.93        ! [CKE0]     = g*cm2/s2 (=0.1uJ) 
  CKEJ = CKE*1.0e-7  ! [CKE*1.E-7]= J 

  !..Calculating Rf, Eq. (4.40), LL82
  IF (CKE .LT. CKE0) THEN 
     Rf = 1.0 
  ELSE
     Rf = 1.11e-4 * CKEJ**(-0.654)      
  ENDIF
 
  !..Calculating Rs, Eq. (4.43), LL82
  W20 = 0.86             
  IF (W2 .LT. W20) THEN 
     Rs = 0.0 
  ELSE
     Rs = 0.685 * (1.0 - exp(-1.63*(W2-W20)))
  ENDIF
 
  !..Calculating Rd as residual, Eq. (4.45), LL82      
  IF ((Rf+Rs) .GT. 1.0) THEN 
     norm = 1./(Rf+Rs)      ! normalization follwing List et al (1987)
     Rfn  = norm*Rf 
     Rsn  = norm*Rs 
     Rf   = Rfn                
     Rs   = Rsn
     Rd   = 0.0       
     !Rf = Rf       !Rfn  ... Aenderung 03.11.2002
     !Rs = 1.0 - Rf !Rsn      Die alte Normierung passt besser zur Tab. 2 in LL82  
     !Rd = 0.0      !0.0      ist aber unstetig, d.h. unschoene Sprungstelle
  ELSE IF ((Rf+Rs) .LE. 1.0) THEN 
     Rd = 1.0 - Rf - Rs 
  ENDIF

  RETURN 
END SUBROUTINE Rfsd

!-------------------------------------------------------------------
!     Calculating Ff,Fs,Fd (= # of Fragments for each Breakup-Mode) 
!     Axel Seifert, 18.11.2002
!-------------------------------------------------------------------  

SUBROUTINE Ffsd(Dgr,Dkl,CKE,ST,Ff,Fs,Fd,Ds0) 
  IMPLICIT NONE  
  REAL(wp) Ff,Fs,Fd,FfLowList
  REAL(wp) Dgr,Dkl,Ds0
  REAL(wp) c10,c11
  REAL(wp) d10,d11 
  REAL(wp) ST,STJ,CKE,CKE0,CKEJ
  REAL(wp) q,qmin,qmax!,x

  !REAL(wp), PARAMETER  :: PI   = 3.1415927
  REAL(wp), PARAMETER  :: epsf = 1.0D-20 
  LOGICAL,  PARAMETER  :: useEmersicConolly = .false.
  LOGICAL,  PARAMETER  :: useFfOchs = .false.

  CKE0 = 8.93               ! Limiting Value f. CKE,
                            ! [CKE0]=0.893uJ (= 8.93 g*cm^2/s^2 ) 
  CKEJ = CKE*1.0e-7 + epsf  ! [CKE*1.0E-7]=J,here CKE in Joule! 
  STJ  = ST *1.0e-7 + epsf  ! [ST *1.0E-7]=J,here ST  in Joule! 

  !..Calculation of FfLowList and Ds0 
  CALL FfLL(Dgr,Dkl,FfLowList,Ds0)
 
  !..Use FfOchs for small droplets
  IF (useFfOchs) THEN
     q    = Dgr 
     qmin = 1500e-4
     qmax = 2500e-4
     IF (q.lt.qmin) THEN
        !Ff = FfOchs(Dgr,Dkl)  ! FfOchs not implemented in McSnow
     ELSEIF (q.ge.qmin .AND. q.lt.qmax) THEN
        !x  = (q - qmin) / (qmax - qmin)
        !Ff = sin(pi/2.0*x)**2 * FfLowList &
        !   + sin(pi/2.0*(1 - x))**2 * FfOchs(Dgr,Dkl)
     ELSEIF (q.ge.qmax) THEN
        Ff = FfLowList
     ENDIF
  ELSE
     Ff = FfLowList
  ENDIF

  !..Limit Ff to 2
  IF (Ff .LT. 2.0) Ff = 2.0 
      
  !..Calculation of Fs (Sheet breakup)
  if (useEmersicConolly) then
     Fs = 118.034 * Dgr**3.1243
  else     
     !..Calculation of Fs, Eq. (3.7), LL82 
     c10 = 2.53e-6           
     c11 = 1.85e-6           
     Fs = 5.0 * erf( (STJ-c10)/c11  )  + 6.0 
     IF (Fs .LT. 2.0) Fs = 2.0 
  end if
 
  !..Calculation of Fd, Eq. (3.9). LL82 
  d10 = 297.5       
  d11 = 23.7        
  IF (CKEJ .GE. 3.65e-6)  THEN
     Fd = d10 + d11 * log(CKEJ)
  ELSE
     Fd = 2.0 
  ENDIF

  IF (Fd  .LT. 2.0) Fd=2.0
 
  RETURN 
END SUBROUTINE Ffsd
 
!-----------------------------------------------------------
!     Calculating Ff, LL82 Eq. (3.3)-(3.5)
!     Axel Seifert, 18.11.2002
!-----------------------------------------------------------

SUBROUTINE FfLL(Dgr,Dkl,Ff,Ds0)
  IMPLICIT NONE
  
  REAL(wp) Ff1,Ff2
  REAL(wp) Dgr,Dkl,Ds0,Ff
  REAL(wp) aa,bb
  REAL(wp) a10,a11,a12,a13,a14,a15,a16
  REAL(wp) b10,b11
 
  !REAL(wp), PARAMETER :: epsf = 1.e-20
  LOGICAL,  PARAMETER :: useEmersicConolly = .false.
 

  !..Calculation of Ff
  if (useEmersicConolly) then
     ! Eq. (1) of EC11
     Ff1 = 55.865 * (Dkl/Dgr)*(Dgr-Dkl) + 2.0     
  else         
     ! Original Low and List parameterization
     a10 = -2.25e4
     a11 = 0.403  
     a12 = 37.9   
     a13 = 2.5    
     aa  = (a10*((Dgr-a11)**2)-a12) * (Dkl**a13) 
     
     a14 = 9.67   
     a15 = 0.170  
     a16 = 4.95   
     bb  = a14*((Dgr+a15)**2) + a16 ! sign corrected based on List et al (1987)

     Ff1 = aa+bb  

     IF (Ff1 .LT. 2.0) Ff1 = 2.0  ! evtl. Zusatz: .OR. Dkl .EQ. Dgr) Ff1=2.,
                                  ! weil in L&L steht, dass: Dkl->Dgr, Ff1->2 
  end if

  !..Calculation of Ff2, Eq. (3.4), LL82 
  b10 = 1.02e4           
  b11 = 2.83             
  Ff2 = b10 * (Dkl**b11) + 2.0 

  !..Calculation of Ds0, Eq. (3.5), LL82, with correction by List et al (1987)
  Ds0 = ((Ff1 - 2.0)/b10)**(1.0/b11)
  
  !..Check if Ff1 or Ff2 
  IF (Dkl .GE. Ds0) THEN 
     Ff = Ff1 
  ELSE
     Ff = Ff2 
  ENDIF

END SUBROUTINE FfLL

!c----------------------------------------------------------- 
!c
!c     Calculating Hf1 from Brown (1997,JAS)
!c
!c----------------------------------------------------------- 

REAL(wp) FUNCTION HfBrown(D1,D2)
  IMPLICIT NONE
  INTEGER :: j,k
  REAL(wp)  :: D1,D2
  REAL(wp)  :: Dl,Ds
  REAL(wp)  :: Dl_l,Ds_u,Ds_l      
  REAL(wp)  :: HfLowList,b1,b2,b3,p!,q
  REAL(wp), DIMENSION(0:2,0:2) :: a 

  a(0,0) = +1.11968D4
  a(0,1) = -2.40889D5
  a(0,2) = +1.29148D6
  a(1,0) = -7.65374D4
  a(1,1) = +1.52563D6
  a(1,2) = -7.59082D6
  a(2,0) = +1.58197D5
  a(2,1) = -2.85481D6
  a(2,2) = +1.27850D7

  Dl = max(D1,D2)
  Ds = min(D1,D2)

  HfBrown = 0.0
  do j=0,2
     do k=0,2
        HfBrown = HfBrown + a(j,k) * Dl**j * Ds**k
     enddo
  enddo
  
  HfLowList = 50.8 * Dl**(-0.718)
  
  Ds_l = 0.0100
  Ds_u = 0.0715
  Dl_l = 0.1800

  b1 = 0.02
  b2 = 0.02
  b3 = 0.02
  
  IF (Dl >= Dl_l .AND. Ds <= Ds_u .AND. Ds >= Ds_l) THEN
     HfBrown = HfBrown
  ELSEIF (Dl < Dl_l-b1 .OR. Ds > Ds_u+b2 .OR. Ds < Ds_l-b3)THEN
     HfBrown = HfLowList
  ELSE
     p = MAX(MIN((Dl-(Dl_l-b1))/b1,1.d0),0.d0)
     HfBrown = HfBrown * p + HfLowList * (1-p)  
     p = MAX(MIN((((Ds_u+b2))-Ds)/b2,1.d0),0.d0)
     HfBrown = HfBrown * p + HfLowList * (1-p)
     p = MAX(MIN((Ds-(Ds_l-b3))/b3,1.d0),0.d0)
     HfBrown = HfBrown * p + HfLowList * (1-p)           
  ENDIF
  
END FUNCTION HfBrown


END MODULE mo_breakup
