!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_colleffi

  USE mo_constants,      ONLY: wp, pi, z1_3, z4pi_3, pi_4, z4_pi
  USE mo_sp_types,       ONLY: t_sp, sp_stat
  USE mo_sp_nml,         ONLY: sp_stick_effi, prolate_intp
  USE mo_atmo_types,     ONLY: grav, t_atmo, T_3, rhol, rhoii
#ifndef WDROPS
  USE mo_velocity,       ONLY: t_CE_boehm, boehmVel, hydrodynamicValuesBoehm
#endif

  IMPLICIT NONE
  PRIVATE

#ifndef WDROPS
  PUBLIC  ::  Ecbohm
  PUBLIC  ::  Ecbohm_spheroid, Ecbohm_riming, stick_effi_tempOnly, stick_effi_Phillips, impact_vel
  PUBLIC  ::  EcBeardGrover, EcRobinSlinnHolger, rho_rime_CL
#endif
  PUBLIC  ::  ecollBeheng

CONTAINS


#ifndef WDROPS


!
! Collision Efficency after Bread and Grover (two spheres)
!
REAL(wp) FUNCTION EcBeardGrover( atmo, sp, r ) RESULT( Ec )
  TYPE(t_atmo), INTENT(in) :: atmo
  TYPE(t_sp),   INTENT(in) :: sp
  REAL(wp),     INTENT(in) :: r

  REAL(wp), PARAMETER :: A_0 = -0.1007_wp
  REAL(wp), PARAMETER :: A_1 = -0.358_wp
  REAL(wp), PARAMETER :: A_2 =  0.0261_wp

  REAL(wp), PARAMETER :: B_0 =  0.1465_wp
  REAL(wp), PARAMETER :: B_1 =  1.302_wp
  REAL(wp), PARAMETER :: B_2 = -0.607_wp
  REAL(wp), PARAMETER :: B_3 =  0.293_wp

  REAL(wp) :: N_Re, F, G, K_0, p, K, z, H, y_c0
  ! attention in Beard and Grover 1974 everything is in cgs, convert or use non-dim stuff

  N_Re = sp%d*sp%v*atmo%rho*atmo%etai
  F = log(N_Re)
  G = A_0 + A_1*F+A_2*F**2
  K_0 = exp(G)

  p = r/sp%d;
  ! TODO left out C_SC
  K = p**2*(rhol/atmo%rho)*N_Re/9;

  z = log(K/K_0);
  H = B_0 + B_1*z + B_2*z**2 + B_3*z**3;
  y_c0 = (2/pi)*atan(H);
  IF( y_c0 > 0.0_wp ) THEN
    Ec = ((y_c0+p)**2)/((1+p)**2);
  ELSE
    Ec = 0.0_wp
  ENDIF
END FUNCTION EcBeardGrover

!
! Collision Efficiency after Slinn (Fit to Beard+Grover), Robin Vallee, and Holger Homann
! all spheres with s2 << s1
!
SUBROUTINE EcRobinSlinnHolger( atmo, sp, r, Ec_Robin, Ec_Slinn, Ec_Holger )
  TYPE(t_atmo), INTENT(in) :: atmo
  TYPE(t_sp),   INTENT(in) :: sp
  REAL(wp),     INTENT(in) :: r

  REAL(wp), INTENT(out) :: Ec_Robin, Ec_Slinn, Ec_Holger

  REAL(wp) :: Re, St, Stc_Slinn, Stc_HJ

  Re = sp%d*sp%v*atmo%rho*atmo%etai
  St = (2.0_wp/9.0_wp)*rhol*r**2*atmo%etai*sp%v/sp%d

  Stc_Slinn = (0.6_wp+1.0_wp/24.0_wp*log(1.0_wp+Re/2.0_wp))/(1.0_wp+log(1.0_wp+Re/2.0_wp))
  Stc_HJ    = (0.6_wp+1.0_wp/24.0_wp*log(1.0_wp+Re/6.0_wp))/(1.0_wp+log(1.0_wp+Re/6.0_wp))

  IF( St > Stc_Slinn ) THEN
    Ec_Robin = (St-Stc_Slinn)/(St+2.0_wp*Stc_Slinn)
    Ec_Slinn = ( (St-Stc_Slinn)/(St-Stc_Slinn+1.0_wp/3.0_wp) )**(3.0_wp/2.0_wp)
  ELSE
    Ec_Robin = 0.0_wp
    Ec_Slinn = 0.0_wp
  ENDIF
  IF( St > Stc_HJ ) THEN
    Ec_Holger = (St-Stc_HJ)/(St-Stc_HJ+2.0_wp/3.0_wp)
  ELSE
    Ec_Holger = 0.0_wp
  ENDIF
END SUBROUTINE EcRobinSlinnHolger

!
! Collision efficiency wrapper including sticking efficieny
!
FUNCTION Ecbohm_spheroid( atmo, sp1, sp2 ) RESULT( Ecbohm_spheroid_v )
  TYPE(t_atmo),        INTENT(in)     :: atmo
  TYPE(t_sp),          INTENT(in)     :: sp1, sp2
  REAL(wp)                            :: Ecbohm_spheroid_v

  REAL(wp) :: vt1, vt2, stickeffi
  TYPE(t_CE_boehm) :: partInfo1, partInfo2

  vt1 = boehmVel( atmo, sp1, partInfo1 )
  vt2 = boehmVel( atmo, sp2, partInfo2 )

  Ecbohm_spheroid_v = Ecbohm( atmo, partInfo1, partInfo2 )

  ! sticking effi
  IF ( BTEST(sp1%statusb,sp_stat%liquidSurf) .OR. &
       BTEST(sp2%statusb,sp_stat%liquidSurf) ) THEN
       stickeffi = 1.0_wp
  ELSE
    IF ( sp_stick_effi == 8 ) THEN
      stickeffi = stick_effi_Phillips( sp1, sp2, atmo%rho, atmo%etai, atmo%T, atmo%ssat )
    ELSE
      stickeffi = stick_effi_tempOnly( atmo%T )
    ENDIF
  ENDIF
  Ecbohm_spheroid_v = Ecbohm_spheroid_v * stickeffi
END FUNCTION Ecbohm_spheroid

!
! Collision efficiency of two spheres for riming
!
REAL(wp) FUNCTION Ecbohm_riming( atmo, sp1, d_c )
  TYPE(t_atmo), INTENT(in)    :: atmo
  TYPE(t_sp),   INTENT(in)    :: sp1
  REAL(wp),     INTENT(in)    :: d_c

  TYPE(t_sp) :: sp2

  sp2%z       = sp1%z
  sp2%m_w     = pi/6.0_wp * rhol * d_c**3
  sp2%d       = d_c
  sp2%p_area  = pi/4.0_wp * d_c**2
  sp2%statusb = IBSET( sp2%statusb, sp_stat%liquidOut)
  sp2%statusb = IBSET( sp2%statusb, sp_stat%liquidSurf)
  sp2%phi     = 1._wp

  Ecbohm_riming = Ecbohm_spheroid( atmo, sp1, sp2 ) 

END FUNCTION Ecbohm_riming

!
! Collision efficency following  
!
!   Boehm (1992a) 'A general hydrodynamic theory for mixed-phase microphysics.
!                 Part I: Drag and fall speed of hydrometeors', Atmospheric research, 27(4), 253-274.
!
!   Boehm (1992b) 'A general hydrodynamic theory for mixed_phase microphysics.
!                 Part II: Colision kernels for coalescence'
!
!   Boehm (1992c) 'A general hydrodynamic theory for mixed_phase microphysics.
!                 Part III: Riming and aggregation'
!
!   Boehm (1994). Theoretical collision efficiencies for riming and aerosol impaction. Atmospheric research, 32(1-4), 171-187.
!
!   Boehm (1999)  Revision and clarification of “A general hydrodynamic theory for mixed-phase microphysics”.
!                 Atmospheric research, 52(3), 167-176.
!
!
FUNCTION Ecbohm( atmo, partInfoX, partInfoY ) RESULT( Ecbohm_v )
  TYPE(t_atmo), INTENT(IN)           :: atmo
  TYPE(t_CE_boehm), TARGET, INTENT(IN) :: partInfoX, partInfoY
  REAL(wp)                           :: Ecbohm_v


  TYPE(t_CE_boehm), POINTER :: partInfo1, partInfo2
  REAL(wp) :: rhoa, nu
  REAL(wp) :: jstar, rr1, re1, v1, m1, alf1, qe1, & 
       &      dels1, cdi1, vi1, rr2, re2, V2, m2, &
       &      alf2, qe2, kk2, dels2, sig1, sig2
  REAL(wp) :: vmin, mstar, rstar, astar, phi1, arg1, phi2, arg2, vstar, delstar, alfstar, qstar,  &
       &      kstar, gamstar, nrestar, cdpstar, cdostar, betaexp, gammaexp, cdistar,              &
       &      gg, ff, hh, ffhh, bb, k2x, k2y, cx, cy, delx, dely, tdel, ep

  nu = atmo%eta
  rhoa = atmo%rho
  vmin = 1.D-10

  ! the collector particle falls at greater speed (or should it be the greater one?)
  IF( partInfoX%v > partInfoY%v ) THEN
    partInfo1 => partInfoX
    partInfo2 => partInfoY
  ELSE
    partInfo1 => partInfoY
    partInfo2 => partInfoX
  ENDIF

  jstar  = partInfo1%j
  vi1    = partInfo1%v*(2.D0+partInfo1%j)/4.D0 ! Boehm99 below Eq (23)
  vi1    = VI1*MIN(partInfo1%alpha,1.0_wp)     ! Boehm92c Eq (6), but not in Boehm99 ! TODO remove or not?
  cdi1   = partInfo1%CDI

  rr1    = partInfo1%r
  re1    = partInfo1%r_e
  V1     = partInfo1%v
  m1     = partInfo1%m
  alf1   = partInfo1%alpha_e
  qe1    = partInfo1%q_e
  dels1  = partInfo1%DELS
  sig1   = partInfo1%SIG

  rr2    = partInfo2%r
  re2    = partInfo2%r_e
  V2     = partInfo2%v
  m2     = partInfo2%m
  alf2   = partInfo2%alpha_e
  qe2    = partInfo2%q_e
  dels2  = partInfo2%DELS
  sig2   = partInfo2%SIG

  kk2    = partInfo2%k
  
  ! reduced mass of two-body system denoted by (*), Boehm99 eq. 13
  mstar = m1*m2/(m1+m2)         
  ! corresponding radius, Boehm99 eq. 14
  rstar = rr1*rr2/(re1+re2)
  ! area of intersection, Boehm99 eq. 15
  astar = pi*rstar**2
  
  ! Assume that terminal fall speed is a random variable and follows a log-normal distribution
  ! (Appendix of Boehm 1992c)
  IF (sig1.lt.1.d-10) THEN
     phi1 = SIGN(1.D0,LOG(V1/V2))
  ELSE
     ! Boehm92c, argument in eq. 18
     arg1 = (LOG(v1/v2) + sig1**2)/(2.d0*sig1)
     ! Boehm92c, approximation of error function as given by (20)
     phi1 = TANH( 2.d0/SQRT(pi)*arg1 + .1012d0*arg1**3 )
  ENDIF
  IF (sig2.lt.1.d-10) THEN
     phi2 = SIGN(1.d0,LOG(v2/v1))
  ELSE
     ! Boehm92c eq. 18
     arg2 = (LOG(v2/v1) + sig2**2)/(2.d0*sig2)
     ! Boehm92c eq. 20
     phi2 = TANH( 2.d0/SQRT(pi)*arg2 + .1012d0*arg2**3 )
  ENDIF

  ! expected value of difference of fall speeds, Boehm92c eq. 17
  vstar = MAX(ABS( v1*phi1 + v2*phi2 ),vmin)
  
  ! boundary layer thickness, Boehm99 eq. 17
  delstar = dels1*SQRT(v1/vstar)+dels2*SQRT(v2/vstar)

  ! Boehm99 eq. 6
  alfstar = rstar*(alf1/re1+alf2/re2)
  qstar   = rstar*( qe1/re1+ qe2/re2)
  
  CALL hydrodynamicValuesBoehm( alfstar, qstar, kstar, gamstar, cdpstar )

  ! reynolds number, Boehm99 eq. 19
  nrestar = 2.D0*rstar*vstar*rhoa/nu
  
  ! Oseen drag coefficient, Boehm92a eq. 16
  cdostar = 4.5D0*kstar**2*MAX(alfstar,1.D0)

  ! Boehm99 eq. 20
  betaexp = SQRT(nrestar*cdpstar/(6.d0*kstar))

  ! Boehm92a eq. 26
  gammaexp=(cdostar-cdpstar)/(4.d0*cdpstar)
  
  ! inertial drag coefficient, Boehm92a eq. 25
  cdistar = cdpstar*(1.d0+4.d0/betaexp*(1.d0-exp(-gammaexp*betaexp)))

  ! The G of Boehm99 below eq. 23
  gg = 6.d0*kstar*nu/(rhoa*rstar*vstar*cdistar)
  
  ! The F of Boehm99 below eq. 23
  ff = SQRT(gg**2+cdi1*vi1**2/cdistar/vstar**2)
  
  ! The H Boehm99 below eq. 23
  hh = 2.d0*mstar/(rhoa*astar*cdistar*delstar)

  ! The term F/H occurs often
  ffhh=ff/hh
  
  if (ffhh.GE.10) THEN
     ! Boehm99 eq. 27
     Ecbohm_v = hh*log((ff+gg)/(2.d0*ff))+ff-gg
  ELSE
     ! Boehm99 eq. 23
     Ecbohm_v = hh*log(cosh(ffhh)+(1.d0+gg)/ff*sinh(ffhh))-gg
  ENDIF

  ! Boehm99 eq. 23
  ecbohm_v = ecbohm_v**(2.d0/jstar)

  ! Boehm94 eq. 3
  bb = 3.d0*v1/(re1+dels1)
  IF(alf2.LT.1) THEN
     ! Boehm94 eq. 8
     k2x = 0.57d0+.43d0*alf2
  ELSE
     ! Boehm94 eq. 9
     k2x = kk2**1.15d0
  ENDIF
  k2y = kk2
  
  ! Boehm94 eq. 7 
  cx = m2/(3.d0*pi*re2*nu)/k2x
  cy = m2/(3.d0*pi*re2*nu)/k2y
  
  IF (bb*cy.ge.0.5d0) THEN
     
     ! Boehm99 eq. 25 and 26
     delx = sqrt(2.d0*bb*cx/jstar+1.d0) 
     dely = sqrt(2.d0*bb*cy-1.d0)

     ! Boehm94 eq. 14
     IF(bb*cy.ge.1.d0) THEN
        ! Boehm94 eq. 14 and Boehm99 eq. 24
        tdel = cy/dely*atan(dely/(bb*cy-1.d0))
        ep = ((cosh(delx*tdel/cx)+sinh(delx*tdel/cx)/delx)*exp(-tdel/cx))**(-jstar)
     ELSE
        ! Boehm94 eq. 14 and Boehm99 eq. 30
        tdel=cy/dely*(pi-atan(dely/(1.d0-bb*cy)))
        ep=(2.d0*delx/(1.d0+delx)*exp(-(delx-1.d0)*tdel/cx))**jstar
     ENDIF
  ELSE
     ep=0.d0
  ENDIF
  ecbohm_v=ep*ecbohm_v
  
END FUNCTION Ecbohm

! sticking efficiency based on temperature only
!
PURE FUNCTION stick_effi_tempOnly( T ) RESULT( se )
  USE mo_sp_nml, ONLY: istick=>sp_stick_effi
  REAL(wp), INTENT(IN) :: T
  REAL(wp)             :: se
  REAL(wp)             :: Tc
  !INTEGER, PARAMETER   :: istick = 3

  Tc = T - T_3
  IF     (istick.eq.0) THEN
        se = 1.0_wp
  ELSEIF (istick.eq.1) THEN
     ! after Pruppacher and Klett, p. 691, originally Mitchell 1988
     IF( Tc >= 0.0_wp ) THEN
        se = 1.0_wp
     ELSE IF ( Tc >= -4._wp ) THEN
        se = 0.1_wp
     ELSE IF ( Tc >= -6._wp ) THEN
        se = 0.6_wp
     ELSE IF ( Tc >= -9._wp ) THEN
        se = 0.1_wp
     ELSE IF ( Tc >= -12.5_wp ) THEN
        se = 0.4_wp
     ELSE IF ( Tc >= -17_wp ) THEN
        se = 1.0_wp
     ELSE IF ( Tc >= -20_wp ) THEN
        se = 0.4_wp
     ELSE
        se = 0.25_wp
     END IF
  ELSEIF (istick.eq.2) THEN
     ! piecewise linear sticking efficiency with maximum at -15 C,
     ! inspired by Figure 14 of Connolly et al. ACP 2012, doi:10.5194/acp-12-2055-2012
     ! Value at -40 C is based on Kajikawa and Heymsfield as cited by Philips et al. (2015, JAS)
     IF ( Tc >= 0_wp ) THEN
        se = 0.14
     ELSEIF ( Tc >= -10_wp ) THEN
        se = -0.01_wp*(Tc+10_wp)+0.24_wp
     ELSEIF ( Tc >= -15_wp ) THEN
        se = -0.08_wp*(Tc+15_wp)+0.64_wp
     ELSEIF ( Tc >= -20_wp ) THEN
        se =  0.10_wp*(Tc+20_wp)+0.14_wp
     ELSEIF ( Tc >= -40_wp ) THEN
        se = 0.005_wp*(Tc+40_wp)+0.04_wp
     ELSE
        se =  0.04_wp
     END IF
  ELSEIF (istick.eq.3) THEN
      ! as option 1, but with factor 0.5, i.e., the lower range of Figure 14
     IF ( Tc >= 0_wp ) THEN
        se = 0.14
     ELSEIF ( Tc >= -10_wp ) THEN
        se = -0.01_wp*(Tc+10_wp)+0.24_wp
     ELSEIF ( Tc >= -15_wp ) THEN
        se = -0.08_wp*(Tc+15_wp)+0.64_wp
     ELSEIF ( Tc >= -20_wp ) THEN
        se =  0.10_wp*(Tc+20_wp)+0.14_wp
     ELSEIF ( Tc >= -40_wp ) THEN
        se = 0.005_wp*(Tc+40_wp)+0.04_wp
     ELSE
        se =  0.04_wp
     END IF
     se = 0.5 * se
  ELSEIF (istick.eq.4) THEN
     ! inspired by PK (option 1) and Connolly (option 2)
     IF ( Tc >= -1 ) THEN
       se = 1.0
     ELSEIF (  Tc >= -6 ) THEN
       se = 0.19*(Tc+6)+0.24
     ELSEIF (  Tc >= -10 ) THEN
       se = 0.24
     ELSEIF (  Tc >= -12.5 ) THEN
       se = -0.304*(Tc+12.5)+1.0
     ELSEIF (  Tc >= -17 ) THEN
       se = 1.0
     ELSEIF (  Tc >= -20 ) THEN
       se =  0.286666*(Tc+20)+0.14
     ELSEIF (  Tc >= -40 ) THEN
       se = 0.005*(Tc+40)+0.04
     ELSE
       se =  0.04
     END IF
  END IF

END FUNCTION stick_effi_tempOnly

!
! sticking efficiency of Vaughan Phillips 
! using temperature and collision kinetic energy (CKE)
!
FUNCTION stick_effi_Phillips( sp1, sp2, rhoa, etai, T, ssat ) RESULT( stick_effi_v )
  TYPE(t_sp),    INTENT(in) :: sp1, sp2
  REAL(wp),      INTENT(in) :: rhoa, etai, T, ssat
  REAL(wp)                  :: stick_effi_v

  REAL(wp), PARAMETER ::      &
    T1_s    = T_3 - 15.0_wp,  & ! Table 2, snow
    omega_s = 0.05_wp,        &  ! Table 2, snow
    !omega_g = 0.1_wp,         &  ! Table 2, graupel
    gamma_s = 1.3_wp,         &  ! Table 2, snow
    !gamma_g = 1.0_wp,         &  ! Table 2, graupel
    kappa   = 1.5_wp,         &  ! Table 2, both
    b_1 = 16.9821_wp,         &  ! Table 2, snow
    c_1 = 1.02323_wp,         &  ! Table 2, snow
    d_1 = 3981.066_wp             ! Table 2, snow 
    !beta_coeff_gra = 20654._wp   ! = d1*c1 for graupel

  REAL(wp) :: ns_aggregate, ni_aggregate, factor_f, nre_large, &
              vel_factor, vimp, beta_coeff_snw, chi
  REAL(wp) :: D_snow, D_ice, vt_snow, vt_ice, m_snow, m_ice, delta_v, &
              sa, cke, cke_over_sa_si, rho_r, K_ns

  ! Habit dependency of sticking efficiency. Decreased due to more spherical shape while sublimation 
  ! chi = 1 for RH_i >= 100%
  IF (ssat .GE. 0._wp) THEN
    chi = 1._wp
  ELSE
    chi = 3._wp                   ! best fit w. chi = 3 for RH_i < 100%
  END IF
  ! page 4889, beta_star
  beta_coeff_snw = d_1 * (c_1 - b_1/(b_1 + (T-T1_s)**2))

  D_snow  = sp1%d
  D_ice   = sp2%d
  vt_snow = sp1%v
  vt_ice  = sp2%v
  m_snow  = sp1%m_w + sp1%m_i + sp1%m_r + sp1%m_f
  m_ice   = sp2%m_w + sp2%m_i + sp2%m_r + sp1%m_f

#if 0
! assumptions for bulk/bin schemes: increase linearly with D_max @ 2 monomers per 0.1 mm
  IF ( D_snow < 1.e-4_wp ) THEN
    ns_aggregate = 1._wp  ! for less than 0.1 mm
    sa = pi * D_snow**2
  ELSE
    ns_aggregate = MIN( 100._wp, 1._wp + 2._wp*(D_snow - 1.e-4_wp)/1.e-4_wp )
    sa = pi * D_ice**2
  ENDIF

  IF ( D_ice < 1.e-4_wp ) THEN
    ni_aggregate = 1.  ! for less than 0.1 mm
    sa = SQRT(sa * pi * D_ice**2)
  ELSE
    ni_aggregate = MIN( 100._wp, 1._wp + 2._wp*(D_ice - 1.e-4_wp)/1.e-4_wp )
    sa = SQRT(sa * pi * D_snow**2)
  ENDIF
#else 
  ns_aggregate = MIN( sp1%mm, 100._wp )
  ni_aggregate = MIN( sp2%mm, 100._wp )
  sa = pi * D_snow * D_ice
#endif  

  ! This is xi on page 4889
  factor_f = 1._wp + omega_s * ( (ns_aggregate*ni_aggregate)**gamma_s - 1._wp)

  IF ( vt_snow < vt_ice ) THEN
    rho_r      = MERGE(sp1%m_r/sp1%v_r, 0._wp, sp1%v_r > 0)
    nre_large  = vt_ice * D_ice * rhoa * etai
    K_ns       = nre_large * rho_r * (0.5_wp*D_snow)**2 / (9.*rhoa*(0.5_wp*D_ice)**2)
    vel_factor = impact_vel( nre_large, K_ns )
    delta_v    = (vt_ice - vt_snow) * vel_factor
    vimp       = MAX( vel_factor * (vt_ice - vt_snow), 0.05_wp )
  ELSE
    rho_r      = MERGE(sp2%m_r/sp2%v_r, 0._wp, sp2%v_r > 0)
    nre_large  = vt_snow * D_snow * rhoa * etai
    K_ns       = nre_large * rho_r * (0.5_wp*D_ice)**2 / (9.*rhoa*(0.5_wp*D_snow)**2)
    vel_factor = impact_vel( nre_large, K_ns )
    delta_v    = (vt_snow - vt_ice) * vel_factor
    vimp       = MAX( vel_factor * (vt_snow - vt_ice), 0.05_wp )
  ENDIF

  ! collision kinetic energy, Eq. 3
  cke = 0.5_wp * delta_v**2 * m_snow * m_ice / (m_snow + m_ice)

  cke_over_sa_si = cke / sa
  
  stick_effi_v = EXP( -(beta_coeff_snw * chi / (factor_f * (vimp**kappa)) ) * cke_over_sa_si )

END FUNCTION stick_effi_Phillips

!
! rime density of Cober & List 1992
!
PURE FUNCTION rho_rime_CL( sp, atmo, K_Ns, rm, dv) RESULT( rho_rime )
  TYPE(t_sp),   INTENT(in) :: sp
  TYPE(t_atmo), INTENT(in) :: atmo
  REAL(wp),     INTENT(in) :: K_Ns    ! Stokes number
  REAL(wp),     INTENT(in) :: rm      ! average droplet radius
  REAL(wp),     INTENT(in) :: dv      ! relative velocity
  REAL(wp)                 :: rho_rime

  REAL(wp) :: N_Re, Vi, Ri, d

  ! calculate N_Re to call impact_vel
  d    = sp%d * MIN( 1.0_wp, 1._wp / SQRT( sp%phi ) )     ! correction in case of prolate particles    
  N_Re = sp%v * d * atmo%rho * atmo%etai

  ! impact velocity (eq. 14, Cober & List '92)
  Vi = dv * impact_vel( N_Re, K_Ns )
  
  ! impact parameter of Cober and List '93
  Ri = -1e6_wp * rm * Vi / MIN((atmo%T - T_3), -1e-5_wp)  ! rm is taken in um in eq. (14)

  ! limit Ri to valid range (max of rho_rime)
  Ri = MIN(Ri, 184._wp / 30._wp)       ! bound value such that rho_r = f(Ri) is not decreasing

  ! Eq. (15) of Cober and List with lower limit from their Figure 7
  rho_rime = MAX( 150._wp, 78._wp + Ri * (184._wp - 15._wp * Ri) )

END FUNCTION rho_rime_CL

!
! Impact velocity in riming
!
PURE FUNCTION impact_vel( NRe, K_ns ) RESULT( vimpact )
  REAL(wp),          INTENT(in) :: NRe      !< Reynolds number
  REAL(wp),          INTENT(in) :: K_ns     !< Stokes number
  REAL(wp)                      :: vimpact  !> impact velocity

  REAL(wp) :: w, vimpx
  LOGICAL  :: l_K01, l_K10

  ! eq. (7)-(10), Rasmussen and Heymsfield '85
  w = LOG10( K_ns )
  l_K01 = ( K_ns >= 0.1_wp )
  l_K10 = ( K_ns <= 10._wp )

  ! no interpolation - just using a range of nre_large
  IF ( NRe <= 20. ) THEN
    vimpx = 0.57
    IF ( K_ns < 0.4 ) THEN
      vimpx = 0
    ELSE IF ( l_K10 ) THEN
      vimpx = 0.1701 + w*(0.7246 + w*(0.2257 + w*(-1.13 + w*0.5756)))
    END IF

  ELSE IF ( NRe <= 65. ) THEN
    vimpx = 0.59
    IF ( .NOT. l_K01 ) THEN
      vimpx = 0.0
    ELSE IF ( l_K10 ) THEN
      vimpx = 0.2927 + w*(0.5085 + w*(-0.03453 + w*(-0.2184 + w*0.03595)))
    END IF

  ELSE IF ( NRe <= 200. ) THEN
    vimpx = 0.61
    IF ( .NOT. l_K01 ) THEN
      vimpx = 0.0
    ELSE IF ( l_K10 ) THEN
      vimpx = 0.3272 + w*(0.4907 + w*(-0.09452 + w*(-0.1906 + w*0.07105)))
    END IF

  ELSE
    vimpx = 0.63
    IF ( .NOT. l_K01 ) THEN
      vimpx = 0.0
    ELSE IF ( l_K10 ) THEN
      vimpx = 0.356 + w*(0.4738 + w*(-0.1233 + w*(-0.1618 + w*0.08087)))
    END IF
  ENDIF

  vimpact = MAX( vimpx, 0._wp )

END FUNCTION impact_vel

! endif ndef WDROPS


#endif

!
! Collision efficiency for water droplets
! ATTENTION: rx and ry in CGS, which is cm.
!
FUNCTION ecollBeheng(rx,ry) RESULT(ecoll) 
  REAL(wp), INTENT(in) :: rx, ry
  REAL(wp) ::  ecoll

  REAL(wp) :: rj, rp 
  REAL(wp) :: xtt(12), qq(22), z(22,12), rt(12) 
  REAL(wp) :: q, hihi, haha, sj, help
  INTEGER  :: i, j, ike, ige, nz, nn 
  INTEGER  :: firstcall 

  data xtt/0.,10.,20.,30.,40.,50.,60.,70.,100.,150.,200.,300./      

  save firstcall,z,qq,rt 

  ike = 12                                                           
  ige = 22

  rj = rx
  rp = ry

  !Einlesen der Tabelle       
  if (firstcall.ne.1) then 
     do i=1,ige                                              
        do j=1,ike                                                    
           z(i,j) = 0.                                                    
        enddo 
     enddo 
     open(99,file='colleff_HallBeheng.dat', &
            status='unknown',blank='zero') 
     do i=1,ige-1                                                     
        read (99,'(11f9.5)') (z(i+1,j+1),j=1,ike-1) 
     enddo 
     close(99) 
     qq(1) = 0.                                                         
     do i=1,20                                                      
        qq(i+1) = qq(i)+.05                                              
     enddo 
     do i=1,20                                                      
        qq(22-(i-1)) = qq(22-i) 
     enddo 
     qq(2) = 0.025 
     write (8,*) 'the Beheng kernel' 
     write (8,1300) (xtt(i),i=1,ike)                                    
     do i=1,22                                                      
        write (8,1400) i,qq(i),(z(i,j),j=1,ike) 
     enddo 
     do i=1,ike                                                      
        do j=1,ige                                                    
          if (z(j,i).lt.1.e-3) z(j,i) = 1.e-3                          
!          z(j,i) = log(z(j,i))                                       
        enddo 
     enddo 
     rt = xtt*1.0e-4 
     firstcall = 1 
  endif 

  ! Vertauschen der Radien falls rj > rp 
  if (rj.gt.rp) then 
     help = rp 
     rp = rj 
     rj = help 
  endif 

  ! Extrapolation fuer rp > 300 mu 
  rp = min(rp,rt(ike))

  q  = rj/rp

  ! Lineare Interpolation
  nz = 1                                                           
  nn = 1        
  do while (rp.ge.rt(nn).and.nn.lt.12) 
     nn = nn+1                                                        
  enddo                                                        
  do while (q.ge.qq(nz).and.nz.lt.22)                              
     nz = nz+1 
  enddo
  hihi = qq(nz)-qq(nz-1)                                         
  haha = rt(nn)-rt(nn-1)                                       
  sj = z(nz-1,nn-1)*(qq(nz  )-q)*(rt(nn  )-rp) &
     + z(nz-1,nn  )*(qq(nz  )-q)*(rp-rt(nn-1)) &
     + z(nz,  nn-1)*(q-qq(nz-1))*(rt(nn  )-rp) &
     + z(nz,  nn  )*(q-qq(nz-1))*(rp-rt(nn-1))
  sj = sj/hihi/haha                                              
!  sj = exp(sj)         

  ecoll = sj

!  print *,rj,rt(nn),rp,rt(nn-1),qq(nz),q,qq(nz-1),exp(z(nz,nn)),sj

  1300 format (' ',8x,12(1x,f8.0))                                        
  1400 format (' ',i3,f6.3,12(2x,f7.5))                                   
END FUNCTION ecollBeheng

END MODULE mo_colleffi
