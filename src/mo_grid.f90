!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_grid

  USE mo_constants,     ONLY: wp
  USE mo_grid_nml,      ONLY: nz, dom_top, dom_bottom, box_area

  IMPLICIT NONE
  PRIVATE
 
  TYPE :: t_grid
    INTEGER    ::  &
      nz           
    REAL(wp), ALLOCATABLE :: &
      z_f(:)            ! grid full levels along z-axis
    REAL(wp)   ::  &
      dz,          &  ! const dz in the model domain
      dom_top,     &
      dom_bottom,  &
      dom_vol,     &
      box_area,    &
      dbox, dboxi
  END TYPE t_grid

  ! TODO move variable declaration out of module into main loop, problem with atmo
  TYPE(t_grid) :: grid            ! grid

  PUBLIC :: t_grid, grid, init_grid, delete_grid, coord2index


CONTAINS

SUBROUTINE init_grid( grid )
  TYPE(t_grid), INTENT(inout) :: grid

  INTEGER :: i

  grid%nz         = nz
  grid%dom_top    = dom_top
  grid%dom_bottom = dom_bottom
  grid%box_area   = box_area
  grid%dom_vol    = (dom_top - dom_bottom)*box_area
  grid%dz         = (dom_top - dom_bottom)/REAL(nz,wp)
  grid%dbox       = grid%dz * box_area
  grid%dboxi      = 1._wp / grid%dbox

  ALLOCATE( grid%z_f(nz) )

  DO i = 1, nz
   !grid%z_f( i ) = dom_bottom + grid%dz * (i - 0.5_wp)
   grid%z_f( i ) = dom_top    - grid%dz * (i - 0.5_wp)
  END DO

END SUBROUTINE init_grid

INTEGER PURE FUNCTION coord2index( z )
  REAL(wp), INTENT(in) :: z

  !coord2index = MAX(1,MIN(1 + FLOOR( (z - grid%dom_bottom) / grid%dz), grid%nz ) )
  coord2index = MAX(1,MIN(1 + FLOOR( (grid%dom_top - z) / grid%dz), grid%nz ) )
END FUNCTION coord2index


SUBROUTINE delete_grid( grid )
  TYPE(t_grid), INTENT(inout) :: grid

  IF(ALLOCATED(grid%z_f)) DEALLOCATE( grid%z_f )
END SUBROUTINE delete_grid
 
END MODULE mo_grid
