!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_loop_list

#ifdef HAVE_OPENMP
  USE omp_lib
#endif  
  USE mo_constants,         ONLY: wp, i8
  USE mo_grid,              ONLY: grid, coord2index
  USE mo_atmo,              ONLY: diag, t_atmo, get_atmo
  USE mo_sp_list,           ONLY: sp_sanity, alloc_sp_list, delete_sp_list, mergeSPs, list_stats
  USE mo_bnd_cond,          ONLY: t_inflow_bnd_data, bnd_condition_init, bnd_condition, bnd_condition_delete
  USE mo_sp_types,          ONLY: t_list, t_list_stats, t_node, t_sp
  USE mo_sp_nml,            ONLY: msg_level, dtc, idtgm, tstep_end, &
                                  ini_type, bnd_type, rime_type, sp_kernel_id, &
                                  nucl_type, multiplication_type, &
                                  melt_type, diffusion_type, hydrobreakup_type
  USE mo_sedi,              ONLY: sp_sedi, sp_moveToNewCell
  USE mo_coll,              ONLY: sp_coll
  USE mo_nucleation,        ONLY: nucl_meyers, nucl_simple
  USE mo_output,            ONLY: init_output, write_solution
  USE mo_breakup,           ONLY: breakup_hydro
  USE mo_sp_list,           ONLY: add_to_list
#ifdef WDROPS
  USE mo_cond,              ONLY: sp_cond
#else
  USE mo_ice_multiplication,ONLY: add_splinter
  USE mo_depo,              ONLY: sp_depo, sp_depo_iteration
  USE mo_melt,              ONLY: sp_melt, add_shedded, sp_meltTemp
  USE mo_rime_cont,         ONLY: sp_rime_cont => sp_rime
  USE mo_rime_stoc,         ONLY: sp_rime_stoc => sp_rime
#endif
  USE mo_mcrph_sb,          ONLY: two_moment_mcrph_init, two_moment_mcrph, two_moment_mcrph_init_mem, &
                                    i2m, twomom_data, two_moment_mcrph_init_values, two_moment_mcrph_end_mem

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: main_loop_list

CONTAINS
  
SUBROUTINE main_loop_list()
  TYPE(t_list), ALLOCATABLE :: sp_list_cell(:)
  INTEGER                   :: itg, itc
  TYPE(t_list_stats)        :: stats           ! statistics on list
  REAL(wp)                  :: dtg

  INTEGER               :: ic, nsp, isp
  TYPE(t_node), POINTER :: node, pre_node, pos_node
  INTEGER(i8)           :: n_splinter_tot, multiplication_n_colls
  REAL(wp)              :: m_shed_tot, q_heat
  TYPE(t_atmo)          :: atmo
  TYPE(t_sp),   POINTER :: sp
  LOGICAL               :: l_delnode, checkForHydroBreakup

  TYPE(t_inflow_bnd_data), POINTER :: bnd_data
  ! timer
  INTEGER(i8) :: cr, cm, times_c(0:16)
  REAL(wp)    :: clockrate, noThreads, walltime, t1, t2, timers(0:16), times_t(16)

  timers(:) = 0.0_wp

  ! init flux boundary condition
  CALL bnd_condition_init( grid, bnd_data )

  ! allocate sp_list
  CALL alloc_sp_list( sp_list_cell, grid )

  ! initialize output
  CALL init_output( grid )

  !
  ! distinguish between the box model and the 1d-Model
  !
  IF ( ini_type == 1 ) THEN
    ! **** BOX MODEL

    ic = coord2index(grid%dom_top)

    ! init list once via boundary condition call
    CALL bnd_condition( addSp, grid, bnd_data, dtc )

    ! print initial mass density function
    CALL write_solution( sp_list_cell, 0, grid )

    DO itc = 1, tstep_end

      CALL mergeSPs( sp_list_cell(ic) )
        
      ! do cellwise Monte-Carlo as in Shima et al. (2009)
      IF( sp_kernel_id > 0 ) CALL sp_coll( addSp, sp_list_cell(ic), grid%dboxi, dtc)

      ! iterate particle list
      nsp = sp_list_cell(ic)%length
      node => sp_list_cell(ic)%head
      DO isp = 1, nsp ! loop particles in cell
        sp => node%sp
        pos_node => node%next
        checkForHydroBreakup = (hydrobreakup_type > 0)
#ifndef WDROPS
        IF( (sp%m_i+sp%m_f+sp%m_r) > 0.0_wp ) checkForHydroBreakup = .FALSE.
#endif
        ! hydro break-up
        IF( checkForHydroBreakup ) CALL breakup_hydro(sp, dtc)
        node => pos_node
      END DO         ! loop particles in cell end

      ! print mass density function
      CALL write_solution( sp_list_cell, itc, grid )

      if (mod(itc,20).eq.0) then
         CALL list_stats( sp_list_cell, grid, stats )
         WRITE (*,"(i8,a,f9.1,a,es15.7,a,es11.3,a,es11.3,a,i12)") itc,",  t_c [s]:",dtc*itc,&
              & ",  mass [kg]: ",stats%tot_mass,",  number [1/m3]: ",stats%nrp,",  prec [mm/h]: ",stats%tot_prec*3600,&
              & ", #SP:", sp_list_cell(ic)%length
      end if

    END DO

    ! final statistics on droplets
    IF( msg_level > 2 ) THEN
      PRINT *, ""
      CALL list_stats( sp_list_cell, grid, stats )
      WRITE(*,'(a,es15.1)') "mean RP mass           [kg]: ", stats%mean_mass
      WRITE(*,'(a,f 15.1)') "tot. RP mass in sys    [kg]: ", stats%tot_mass_sys
      WRITE(*,'(a,es15.1)') "tot. RP mass        [kg/m3]: ", stats%tot_mass
      WRITE(*,'(a,f 15.1)') "num. SPs             [1/m3]: ", stats%nsp
      WRITE(*,'(a,f 15.1)') "num. RPs             [1/m3]: ", stats%nrp
    END IF

  ELSE ! ini_type ! **** 1D-MODEL
    ! print initial mass density function
    CALL write_solution( sp_list_cell, 0, grid )

    CALL two_moment_mcrph_init(igscp=4, msg_level=16)
    CALL two_moment_mcrph_init_mem(grid)

    CALL system_clock(count_rate=cr)
    CALL system_clock(count_max=cm)
    clockrate = REAL(cr,wp)

    CALL tic(times_c(0))
    CALL CPU_TIME(t1)
    DO itc = 1, tstep_end

      ! set diagnostic arrays to zero
      diag%rime_rate = 0.0_wp
      diag%dep_rate  = 0.0_wp
      diag%dep_mono  = 0.0_wp
      diag%heat_rate = 0.0_wp

      ! sedimentation in the BND zone for 1d models
      IF (bnd_type > 0) THEN
         CALL tic(times_c(12))
         !CALL bnd_condition( sp_list_cell(coord2index(grid%dom_top)), grid, bnd_data, dtc )
         CALL bnd_condition( addSp, grid, bnd_data, dtc )
         CALL toc(times_c(12),clockrate,timers(12))
      END IF

      IF( MOD( itc-1, tstep_end/100 ) == 0 ) THEN
        CALL tic(times_c(14))
        PRINT *, ""
        WRITE(*,'(i10,a,i10,a,i10)') (itc-1)/(tstep_end/100), " Timestep: ",  itc, "   #SP = ", SUM(sp_list_cell(:)%length)
        CALL list_stats( sp_list_cell, grid, stats )
        WRITE(*,'(a,es15.1)') "mean RP mass           [kg]: ", stats%mean_mass
        WRITE(*,'(a,f 15.1)') "tot. RP mass in sys    [kg]: ", stats%tot_mass_sys
        WRITE(*,'(a,es15.1)') "tot. RP mass        [kg/m3]: ", stats%tot_mass
        WRITE(*,'(a,f 15.1)') "num. SPs             [1/m3]: ", stats%nsp
        WRITE(*,'(a,f 15.1)') "num. RPs             [1/m3]: ", stats%nrp
        WRITE(*,'(a,es15.1)') "precipitation        [1/m2]: ", stats%nsedi
        WRITE(*,'(a,es15.1)') "precipitation mass  [kg/m2]: ", stats%msedi
        CALL toc(times_c(14),clockrate,timers(14))
      END IF

      CALL tic(times_c(16))
      CALL two_moment_mcrph_init_values( grid, bnd_data%number_flux, bnd_data%mass_flux )
      CALL two_moment_mcrph(                                    &
                       isize  = 1,                              &!in: array size
                       ke     = grid%nz,                        &!in: end level/array size
                       is     = 1,                              &!in: start index
                       ie     = 1,                              &!in: end index
                       ks     = 1,                              &!in: start level
                       dt     = dtc,                            &!in: time step
                       dz     = twomom_data%d(:,:,i2m%idz),     &!in: vertical layer thickness
                       rho    = twomom_data%d(:,:,i2m%irho),    &!in:  density
                       pres   = twomom_data%d(:,:,i2m%ipres),   &!in:  pressure
                       qv     = twomom_data%d(:,:,i2m%iqv),     &!inout:sp humidity
                       qc     = twomom_data%d(:,:,i2m%iqc),     &!inout:cloud water
                       qnc    = twomom_data%d(:,:,i2m%iqnc),    &!inout: cloud droplet number 
                       qr     = twomom_data%d(:,:,i2m%iqr),     &!inout:rain
                       qnr    = twomom_data%d(:,:,i2m%iqnr),    &!inout:rain droplet number 
                       qi     = twomom_data%d(:,:,i2m%iqi),     &!inout: ice
                       qni    = twomom_data%d(:,:,i2m%iqni),    &!inout: cloud ice number
                       qs     = twomom_data%d(:,:,i2m%iqs),     &!inout: snow 
                       qns    = twomom_data%d(:,:,i2m%iqns),    &!inout: snow number
                       qg     = twomom_data%d(:,:,i2m%iqg),     &!inout: graupel 
                       qng    = twomom_data%d(:,:,i2m%iqng),    &!inout: graupel number
                       qh     = twomom_data%d(:,:,i2m%iqh),     &!inout: hail 
                       qnh    = twomom_data%d(:,:,i2m%iqnh),    &!inout: hail number
                       ninact = twomom_data%d(:,:,i2m%ininact), &!inout: IN number
                       tk     = twomom_data%d(:,:,i2m%it),      &!inout: temp 
                       w      = twomom_data%d(:,:,i2m%iw),      &!inout: w
                       prec_nr = twomom_data%d(1,:,i2m%ifnr),   &!inout precp rate rain
                       prec_ni = twomom_data%d(1,:,i2m%ifni),   &!inout precp rate ice
                       prec_ns = twomom_data%d(1,:,i2m%ifns),   &!inout precp rate snow
                       prec_ng = twomom_data%d(1,:,i2m%ifng),   &!inout precp rate graupel
                       prec_nh = twomom_data%d(1,:,i2m%ifnh),   &!inout precp rate hail
                       prec_r = twomom_data%d(1,:,i2m%ifr),     &!inout precp rate rain
                       prec_i = twomom_data%d(1,:,i2m%ifi),     &!inout precp rate ice
                       prec_s = twomom_data%d(1,:,i2m%ifs),     &!inout precp rate snow
                       prec_g = twomom_data%d(1,:,i2m%ifg),     &!inout precp rate graupel
                       prec_h = twomom_data%d(1,:,i2m%ifh),     &!inout precp rate hail
                       msg_level = 16                ,    &
                       l_cv=.TRUE.          )
      CALL toc(times_c(16),clockrate,timers(16))

      ! simple nucleation
      IF (nucl_type == 1) THEN
        CALL tic(times_c(11))
        CALL nucl_simple( addSp, grid%box_area, dtc )
        CALL toc(times_c(11),clockrate,timers(11))
      END IF
     
      times_t(:) = 0.0_wp
      ! run over all cells
      !$OMP PARALLEL DO DEFAULT(none) &
      !$OMP SHARED(grid,nucl_type,sp_list_cell,idtgm,diffusion_type,diag,rime_type, &
      !$OMP        multiplication_type,melt_type,sp_kernel_id,dtc,hydrobreakup_type, &
      !$OMP        clockrate) &
      !$OMP PRIVATE(nsp,node,pre_node,n_splinter_tot,multiplication_n_colls,isp,sp,q_heat, &
      !$OMP        pos_node,atmo,dtg,itg,l_delnode,m_shed_tot,checkForHydroBreakup, &
      !$OMP        times_c) &
      !$OMP REDUCTION(+:times_t) &
      !$OMP SCHEDULE(static,1)
      DO ic = 1, grid%nz
      
        ! meyers nucleation
        IF (nucl_type == 2) THEN
          CALL tic(times_c(11))
          CALL nucl_meyers( sp_list_cell(ic), grid, ic )
          CALL toc(times_c(11),clockrate,times_t(11))
        END IF
        
        nsp = sp_list_cell(ic)%length
        node => sp_list_cell(ic)%head
        pre_node => node
        
        n_splinter_tot = 0_i8
        m_shed_tot = 0.0_wp
        multiplication_n_colls = 0
        
        partloop: DO isp = 1, nsp ! loop particles in cell
          sp => node%sp
          pos_node => node%next
          q_heat = 0.0_wp
          CALL get_atmo( sp%z, atmo )
          checkForHydroBreakup = (hydrobreakup_type > 0)
        
          dtg   = dtc / REAL(idtgm,wp)
          DO itg = 1, idtgm ! condensation loop
#ifdef WDROPS
            ! condensation/evaporation
            IF( diffusion_type /= 0 ) THEN
              CALL tic(times_c(4))
              CALL sp_cond( sp, atmo, dtg )
              CALL toc(times_c(4),clockrate,times_t(4))
            END IF
#else
            ! riming
            IF ( rime_type == 1 ) THEN
              CALL tic(times_c(5))
              CALL sp_rime_cont( sp, atmo, dtg, q_heat, multiplication_type, n_splinter_tot, &
                               & diag(ic)%rime_rate, diag(ic)%heat_rate )
              CALL toc(times_c(5),clockrate,times_t(5))
            ELSE IF ( rime_type == 2 ) THEN
              CALL tic(times_c(5))
              CALL sp_rime_stoc( sp, atmo, dtg, q_heat, multiplication_type, n_splinter_tot, multiplication_n_colls , &
                               & diag(ic)%rime_rate, diag(ic)%heat_rate )
              CALL toc(times_c(5),clockrate,times_t(5))
            END IF 

            ! deposition/sublimation
            IF( diffusion_type /= 0 ) THEN
              CALL tic(times_c(4))
              IF( diffusion_type == 1) THEN
                CALL sp_depo( sp, atmo, dtg, q_heat, l_delnode, diag(ic)%dep_rate, diag(ic)%dep_mono, diag(ic)%heat_rate )
              ELSE
                CALL sp_depo_iteration( sp, atmo, dtg, q_heat, l_delnode, &
                                      & diag(ic)%dep_rate, diag(ic)%dep_mono, diag(ic)%heat_rate )
              ENDIF
              IF ( l_delnode ) THEN
                IF ( ASSOCIATED(pre_node, TARGET=node) ) THEN
                  sp_list_cell(ic)%head => pos_node
                  pre_node => pos_node
                ELSE
                  pre_node%next => pos_node
                END IF
                DEALLOCATE( node )
                sp_list_cell(ic)%length = sp_list_cell(ic)%length - 1
                node => pos_node
                CYCLE partloop
              END IF
              CALL toc(times_c(4),clockrate,times_t(4))
            END IF

        
            ! melting
            IF( diffusion_type > 1 ) THEN
              CALL tic(times_c(6))
              CALL sp_meltTemp( sp, atmo, dtg, q_heat, m_shed_tot, diag(ic)%heat_rate )
              CALL toc(times_c(6),clockrate,times_t(6))
            ELSEIF ( melt_type /= 0 ) THEN
              CALL tic(times_c(6))
              CALL sp_melt( sp, atmo, dtg, q_heat, m_shed_tot, diag(ic)%heat_rate )
              CALL toc(times_c(6),clockrate,times_t(6))
            END IF

            IF( (sp%m_i+sp%m_f+sp%m_r) > 0.0_wp ) checkForHydroBreakup = .FALSE.
#endif
            ! hydro break-up
            IF( checkForHydroBreakup ) THEN 
              CALL tic(times_c(7))
              CALL breakup_hydro(sp, dtg)
              CALL toc(times_c(7),clockrate,times_t(7))
            END IF
          END DO ! conddepo_loop end

          ! motion of SPs
          CALL tic(times_c(8))
          CALL sp_sedi( sp, atmo, dtc )
          CALL toc(times_c(8),clockrate,times_t(8))

          pre_node => node
          node => pos_node
        END DO partloop ! loop particles in cell end

        ! do cellwise Monte-Carlo as in Shima et al. (2009)
        IF( sp_kernel_id > 0 ) THEN
          CALL tic(times_c(2))
          CALL sp_coll( addSp, sp_list_cell(ic), grid%dboxi, dtc)
          CALL toc(times_c(2),clockrate,times_t(2))
        END IF
        
        ! create new super-ice for ice splinters in the cell
#ifndef WDROPS
        IF( n_splinter_tot > 0_i8 ) THEN  
          CALL tic(times_c(9))
          CALL add_splinter( addSp, grid%z_f(ic), grid%dz, n_splinter_tot )
          CALL toc(times_c(9),clockrate,times_t(9))
        END IF

        IF( m_shed_tot > 0.0_wp ) THEN
          CALL tic(times_c(10))
          CALL add_shedded(  addSp, grid%z_f(ic), grid%dz, m_shed_tot )
          CALL toc(times_c(10),clockrate,times_t(10))
        END IF
#endif

        ! merge similiar-sized pure ice or water particles to reduce the number of super-particles
        CALL tic(times_c(3))
        CALL mergeSPs( sp_list_cell(ic) )
        CALL toc(times_c(3),clockrate,times_t(3))
        
        ! check if the list is sane
#ifndef NDEBUG
        CALL tic(times_c(1))
        IF( .NOT. sp_sanity(sp_list_cell(ic), "afterColl") ) STOP "SP list is not sane."
        CALL toc(times_c(1),clockrate,times_t(1))
#endif
      END DO ! loop over all cells
      !$OMP END PARALLEL DO
      DO ic=1,SIZE(times_t)
        timers(ic) = timers(ic) + times_t(ic)
      END DO
      
      ! move super-particles to new cells
      CALL tic(times_c(13))
      CALL sp_moveToNewCell( sp_list_cell, grid, stats )
      CALL toc(times_c(13),clockrate,timers(13))
      
      ! print mass density function
      CALL tic(times_c(15))
      CALL write_solution( sp_list_cell, itc, grid, twomom_data%d )
      CALL toc(times_c(15),clockrate,timers(15))

    END DO ! end time step loop

    ! final statistics on droplets
    IF( msg_level > 2 ) THEN
      PRINT *, ""
      CALL list_stats( sp_list_cell, grid, stats )
      WRITE(*,'(a,es15.1)') "mean RP mass           [kg]: ", stats%mean_mass
      WRITE(*,'(a,f 15.1)') "tot. RP mass in sys    [kg]: ", stats%tot_mass_sys
      WRITE(*,'(a,es15.1)') "tot. RP mass        [kg/m3]: ", stats%tot_mass
      WRITE(*,'(a,f 15.1)') "num. SPs             [1/m3]: ", stats%nsp
      WRITE(*,'(a,f 15.1)') "num. RPs             [1/m3]: ", stats%nrp
      WRITE(*,'(a,es15.1)') "precipitation         [1/m2]: ", stats%nsedi
      WRITE(*,'(a,es15.1)') "precipitation mass   [kg/m2]: ", stats%msedi
    END IF

    CALL toc( times_c(0), clockrate, timers(0) )
    CALL CPU_TIME(t2)
    walltime = t2-t1
#ifdef HAVE_OPENMP
    noThreads = omp_get_max_threads()
#else
    noThreads = 1.0_wp
#endif

    WRITE(*,*) "system_clock: ", timers(0), walltime
    WRITE(*,*) "sanity      : ", timers(1), NINT(timers(1)/timers(0) *100/noThreads), NINT(timers(1) /walltime*100)
    WRITE(*,*) "coll        : ", timers(2), NINT(timers(2)/timers(0) *100/noThreads), NINT(timers(2) /walltime*100)
    WRITE(*,*) "merge       : ", timers(3), NINT(timers(3)/timers(0) *100/noThreads), NINT(timers(3) /walltime*100)
    WRITE(*,*) "diffusion   : ", timers(4), NINT(timers(4)/timers(0) *100/noThreads), NINT(timers(4) /walltime*100)
    WRITE(*,*) "riming      : ", timers(5), NINT(timers(5)/timers(0) *100/noThreads), NINT(timers(5) /walltime*100)
    WRITE(*,*) "melting     : ", timers(6), NINT(timers(6)/timers(0) *100/noThreads), NINT(timers(6) /walltime*100)
    WRITE(*,*) "hydro_break : ", timers(7), NINT(timers(7)/timers(0) *100/noThreads), NINT(timers(7) /walltime*100)
    WRITE(*,*) "sedi        : ", timers(8), NINT(timers(8)/timers(0) *100/noThreads), NINT(timers(8) /walltime*100)
    WRITE(*,*) "add_splinter: ", timers(9), NINT(timers(9)/timers(0) *100/noThreads), NINT(timers(9) /walltime*100)
    WRITE(*,*) "add_shedded : ", timers(10), NINT(timers(10)/timers(0) *100/noThreads), NINT(timers(10) /walltime*100)
    WRITE(*,*) "nucleation  : ", timers(11), NINT(timers(11)/timers(0) *100/noThreads), NINT(timers(11) /walltime*100)
    WRITE(*,*) "bnd_cond    : ", timers(12), NINT(timers(12)/timers(0) *100), NINT(timers(12) /walltime*100)
    WRITE(*,*) "move+sort   : ", timers(13), NINT(timers(13)/timers(0) *100), NINT(timers(13) /walltime*100)
    WRITE(*,*) "stats       : ", timers(14), NINT(timers(14)/timers(0) *100), NINT(timers(14) /walltime*100)
    WRITE(*,*) "output      : ", timers(15), NINT(timers(15)/timers(0) *100), NINT(timers(15) /walltime*100)
    WRITE(*,*) "2mom        : ", timers(16), NINT(timers(16)/timers(0) *100), NINT(timers(16) /walltime*100)

    CALL two_moment_mcrph_end_mem()
  END IF ! ini_type

  CALL bnd_condition_delete( bnd_data )

  CALL delete_sp_list( sp_list_cell )

CONTAINS

  SUBROUTINE tic( t1 )
      INTEGER(i8), INTENT(out) :: t1
      CALL system_clock(t1)
  END SUBROUTINE tic

  SUBROUTINE toc( t1, clockrate, tot )
      INTEGER(i8), INTENT(in)    :: t1
      REAL(wp),    INTENT(in)    :: clockrate
      REAL(wp),    INTENT(inout) :: tot

      INTEGER(i8) :: t2
      CALL system_clock(t2)
      tot = tot + (t2 - t1)/clockrate
  END SUBROUTINE toc
  
  SUBROUTINE addSp( sp )
    TYPE(t_sp),   INTENT(in) :: sp
    TYPE(t_node) :: node
    INTEGER      :: ic
    ic = coord2index(sp%z)
    node%sp = sp
    CALL add_to_list( sp_list_cell(ic), node )
  END SUBROUTINE addSp 

END SUBROUTINE main_loop_list

END MODULE mo_loop_list
