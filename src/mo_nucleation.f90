!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_nucleation

  USE mo_constants,    ONLY: wp, i8, pi_4, z1_3, z6_pi, pi_6
  USE mo_grid,         ONLY: t_grid, coord2index
  USE mo_sp_types,     ONLY: t_list, t_node, t_sp, sp_stat, p_addSp
  USE mo_sp_list,      ONLY: add_to_list
  USE mo_atmo,         ONLY: t_atmo, get_atmo
  USE mo_atmo_nml,     ONLY: nh1, nh2
  USE mO_atmo_types,   ONLY: rhoii, rhoi
  USE mo_sp_nml,       ONLY: agg_hist, xi0, nclmass, nclrate, nu_gam
  USE mo_distr,        ONLY: rand_gamma
#ifndef WDROPS
  USE mo_mass2diam,    ONLY: m2d_particle, iceGeo_init
#endif

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: nucl_meyers, nucl_simple, nucl_meyers_a

CONTAINS


!
! a simple nucleation in a height-belt (nh1, nh2)
!
SUBROUTINE nucl_simple( addSp, box_area, dt ) 
  procedure(p_addSp)   :: addSp
  REAL(wp), INTENT(in) :: box_area, dt

  TYPE(t_sp) :: sp
  INTEGER :: i, ncl_in_dbox
  REAL(wp) :: rand
#ifndef WDROPS
  TYPE(t_atmo) :: atmo
#endif

  ncl_in_dbox = FLOOR(nclrate*dt*(nh2-nh1)*box_area)

  DO i = 1, ncl_in_dbox
    CALL random_number( rand )
    sp%z = nh1 + (nh2-nh1)*rand
#ifndef WDROPS
    CALL get_atmo( sp%z, atmo )
    sp%T = atmo%T
    IF ( agg_hist >= 0 ) THEN
        ! save temp. and supersaturation at nucleation point
        sp%init_temp = atmo%T
        sp%init_ssat = atmo%ssat
    END IF
    sp%m_i = rand_gamma( nu_gam+1.0_wp, nclmass/(nu_gam+1.0_wp) )
    call iceGeo_init(sp) ! set V_i and phi
#else
    sp%m_w = rand_gamma( nu_gam+1.0_wp, nclmass/(nu_gam+1.0_wp) )
#endif
    sp%xi = xi0
    sp%statusb = IBSET(sp%statusb, sp_stat%hom_nuc)
    CALL addSp( sp )
  END DO
END SUBROUTINE nucl_simple
  
!
! Meyers ice nucleation as a function of supersaturation over ice
!
SUBROUTINE nucl_meyers( sp_list, grid, k )
  TYPE(t_list), INTENT(inout) :: sp_list
  TYPE(t_grid), INTENT(in)    :: grid
  INTEGER,      INTENT(in)    :: k

  TYPE(t_node), POINTER :: node,isp_node
  TYPE(t_sp),   POINTER :: sp
  TYPE(t_atmo)          :: atmo

  REAL(wp)    :: ndiag, nnuc, rand1, Nd
  INTEGER     :: i, inuc

  ALLOCATE( node )

  CALL get_atmo( grid%z_f(k), atmo )

  IF (atmo%ssat > 0._wp) THEN

     ! number of snow particles in this grid box
     Nd = 0.0_wp
     IF (sp_list%length > 0) THEN
        isp_node => sp_list%head
        DO i = 1, sp_list%length
           sp => isp_node%sp
           Nd = Nd + sp%xi
           isp_node => isp_node%next
        END DO
     END IF

     ! diagnostic number of ice particles in this grid box
     ndiag = 1e3 * exp(-0.639+12.96*atmo%ssat) * grid%dbox
     !ndiag = MIN(ndiag,1e6) ! Limit maximum number density?

     ! number of particles to nucleate
     nnuc = MAX(ndiag-Nd,0.0_wp) / xi0

     IF (nnuc > 0) then
        CALL random_number( rand1 )
        inuc = int(nnuc) + int(rand1+nnuc-int(nnuc))

        DO i=1,inuc
           CALL random_number( rand1 )
#ifndef WDROPS
           node%sp%m_i = rand_gamma( nu_gam+1.0_wp, nclmass/(nu_gam+1.0_wp) )
           call iceGeo_init(node%sp) ! set V_i and phi
           node%sp%m_r = 0._wp
           node%sp%v_r = 0._wp
           node%sp%m_w = 0._wp
           node%sp%z   = grid%z_f(k) + grid%dz*(rand1-0.5_wp)
           node%sp%xi  = xi0
           node%sp%T   = atmo%T
           IF ( agg_hist >= 0 ) THEN
              ! save temp. and supersaturation at nucleation point
              node%sp%init_temp = atmo%T
              node%sp%init_ssat = atmo%ssat
           END IF
           node%sp%statusb = IBSET(node%sp%statusb, sp_stat%hom_nuc)
#else
           node%sp%m_w = rand_gamma( nu_gam+1.0_wp, nclmass/(nu_gam+1.0_wp) )
#endif
           CALL add_to_list( sp_list, node )
        END DO
     END IF
  END IF

  DEALLOCATE( node )
END SUBROUTINE nucl_meyers

!
! Meyers ice nucleation as a function of supersaturation over ice
!
SUBROUTINE nucl_meyers_a( addSp, z, dz, dbox, sps )
  procedure(p_addSp)          :: addSp
  REAL(wp),     INTENT(in)    :: z, dz, dbox
  TYPE(t_sp),   INTENT(inout) :: sps(:)

  TYPE(t_atmo)          :: atmo
  TYPE(t_sp)            :: sp
  REAL(wp)     :: ndiag, nnuc, rand1
  INTEGER      :: inuc, i
  INTEGER(i8)  :: Nd

  CALL get_atmo( z, atmo )
  IF( atmo%ssat > 0.0_wp ) THEN
    ! number of snow particles in this grid box
    Nd = 0
    IF( SIZE(sps) > 0 ) Nd = SUM( sps(:)%xi, sps(:)%cellId > 0 )
    ! diagnostic number of ice particles in this grid box
    ndiag = 1e3 * exp(-0.639+12.96*atmo%ssat)*dbox
    ! number of particles to nucleate
    nnuc = (ndiag-Nd) / xi0
    IF( nnuc > 0.0_wp ) THEN  
      CALL random_number( rand1 )
      inuc = int(nnuc) + int(rand1+nnuc-int(nnuc))
      DO i=1,inuc
           CALL random_number( rand1 )
           sp%z   = z + dz*(rand1-0.5_wp)
           sp%xi  = xi0
           sp%statusb = IBSET(sp%statusb, sp_stat%hom_nuc)
#ifndef WDROPS
           sp%m_i = rand_gamma( nu_gam+1.0_wp, nclmass/(nu_gam+1.0_wp) )
           sp%m_r = 0._wp
           sp%v_r = 0._wp
           sp%m_w = 0._wp
           sp%T   = atmo%T
           IF ( agg_hist >= 0 ) THEN
              ! save temp. and supersaturation at nucleation point
              sp%init_temp = atmo%T
              sp%init_ssat = atmo%ssat
           END IF
           call iceGeo_init(sp) ! set V_i and phi
#else
           sp%m_w = rand_gamma( nu_gam+1.0_wp, nclmass/(nu_gam+1.0_wp) )
#endif
           CALL addSp( sp )
      END DO
    END IF
  END IF
END SUBROUTINE nucl_meyers_a

END MODULE mo_nucleation
