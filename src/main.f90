!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!
PROGRAM sp_method

  USE mo_constants,      ONLY: wp
  USE mo_loop_array,     ONLY: main_loop_array
  USE mo_loop_list,      ONLY: main_loop_list
  USE mo_atmo_nml,       ONLY: read_atmo_nml
  USE mo_sp_nml,         ONLY: read_sp_nml, print_sp_nml, ini_type, maxTotalNoSps
  USE mo_grid_nml,       ONLY: read_grid_nml
  USE mo_grid,           ONLY: grid, init_grid, delete_grid
  USE mo_coll_utils,     ONLY: set_coll_kernel
  USE mo_velocity,       ONLY: init_part_geom
  USE mo_atmo,           ONLY: init_atmo, delete_atmo
#ifndef WDROPS
  USE mo_check,          ONLY: plot_colleffi, plot_colleffi_mtot, plot_colleffi_riming,           &
                               plot_colleffi_riming_drop_spectrum, plot_colleffi_drops, plot_a2d_m2d, &
                               plot_geo_vt, plot_atmo, plot_vt_f_mw, plot_vt_f_mw_vivec, plot_rain, &
                               plot_vt_boehm, plot_boehm_westbrook, plot_vt_boehm_height_depend, plot_Ec_boehm_height_depend, &
                               plot_boehm_westbrook_phis
#endif



  IMPLICIT NONE

  ! for random number
  INTEGER :: values(1:8), k
  INTEGER, DIMENSION(:), ALLOCATABLE :: seed

  ! random numbers
  CALL date_and_time(values=values)
  CALL random_seed(size=k)
  ALLOCATE(seed(1:k))
  seed(:) = values(8)
  CALL random_seed(put=seed)

  ! read namelists
  CALL read_grid_nml()
  CALL read_atmo_nml()
  CALL read_sp_nml()
  ! set function pointer
  CALL set_coll_kernel()
  ! initialize particle geometry
  CALL init_part_geom()
  ! print namelist parameters
  CALL print_sp_nml()

  ! intialize the grid 
  CALL init_grid( grid )
  ! intialize the atmosphere
  CALL init_atmo()

  IF( ini_type ==  0 ) THEN ! do various checks and exit
#ifndef WDROPS
      PRINT *, " Checking code... "
      PRINT *, "    ... plot_atmo ... "
      CALL plot_atmo( grid )
      PRINT *, "    ... plot_colleffi_riming ... "
      CALL plot_colleffi_riming(0.0_wp,600.0_wp)
      PRINT *, "    ... plot_colleffi_riming ... "
      CALL plot_colleffi_riming(1.0_wp,600.0_wp)
      PRINT *, "    ... plot_colleffi_riming_drop_spectrum ... "
      CALL plot_colleffi_riming_drop_spectrum( 1 ) !0.0_wp,600.0_wp)
      PRINT *, "    ... plot_colleffi_riming_drop_spectrum ... "
      CALL plot_colleffi_riming_drop_spectrum( 2 ) !1.0_wp,600.0_wp)
      CALL plot_colleffi_riming_drop_spectrum( 3 ) !1.0_wp,600.0_wp)
      PRINT *, "    ... plot_colleffi_drops ... "
      CALL plot_colleffi_drops

!      PRINT *, "    ... plot_colleffi ... "
!      CALL plot_colleffi()
      PRINT *, "    ... plot_colleffi_mtot ... "
!     if this check routine fails, make sure that you set "ulimit -s unlimited" and try again
      CALL plot_colleffi_mtot()
      PRINT *, "    ... plot_a2d_m2d (high density) ... "
      CALL plot_a2d_m2d(600.0_wp)
      PRINT *, "    ... plot_a2d_m2d (low density) ... "
      CALL plot_a2d_m2d(200.0_wp)
      PRINT *, "    ... plot_geo_vt ... "
      CALL plot_geo_vt()
      PRINT *, "    ... plot_vt_f_mw ... "
      CALL plot_vt_f_mw( 1 )
      CALL plot_vt_f_mw( 0 )
      CALL plot_vt_f_mw( 2 )
      CALL plot_vt_f_mw_vivec()
      CALL plot_rain()
      CALL plot_vt_boehm(.TRUE.)
      CALL plot_vt_boehm(.FALSE.)
      CALL plot_vt_boehm_height_depend()
      CALL plot_Ec_boehm_height_depend()
      CALL plot_boehm_westbrook(.TRUE.)
      CALL plot_boehm_westbrook(.FALSE.)
      CALL plot_boehm_westbrook_phis(.TRUE.)
      CALL plot_boehm_westbrook_phis(.FALSE.)
      STOP " Checking code finished! Good bye! ------------------ "
#else
      STOP "Nothing to do. Bye!"
#endif
  ELSE
    IF( maxTotalNoSps < 1 ) THEN
      PRINT *, " "
      PRINT *, " run list version"
      PRINT *, " "
      CALL main_loop_list()
    ELSE
      PRINT *, " "
      PRINT *, " run array version"
      PRINT *, " "
      CALL main_loop_array()
    END IF
  END IF

  CALL delete_atmo()
  CALL delete_grid( grid )

END PROGRAM sp_method
