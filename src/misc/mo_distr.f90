!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
! 

MODULE mo_distr
  USE mo_constants,   ONLY: wp, pi

  IMPLICIT NONE

  PUBLIC :: rand_gamma, random_exponential, random_normal

CONTAINS

!-----------------------------------------------------------
! Generate random numbers from a normal distribution
!-----------------------------------------------------------
REAL(wp) FUNCTION random_normal()
  ! The algorithm uses the ratio of uniforms method of 
  ! Kinderman and Monahan with quadratic bounding curves.
  IMPLICIT NONE
  !  Local variables
  REAL, PARAMETER :: half = 0.5
  REAL     :: s = 0.449871, t = -0.386595, a = 0.19600, b = 0.25472,           &
              r1 = 0.27597, r2 = 0.27846, u, v, x, y, q
  DO
     CALL RANDOM_NUMBER(u)
     u = 1.0-u
     CALL RANDOM_NUMBER(v)
     v = 1.7156 * (v - half)
     x = u - s
     y = ABS(v) - t
     q = x**2 + y*(a*y - b*x)
     IF (q < r1) EXIT
     IF (q > r2) CYCLE
     IF (v**2 < -4.0*LOG(u)*u**2) EXIT
  END DO
  ! return ratio as the normal deviate
  random_normal = v/u
  RETURN
END FUNCTION random_normal

REAL(wp) FUNCTION random_exponential( )
  IMPLICIT NONE
  REAL(wp) :: r
  REAL(wp), PARAMETER :: zero = 0.0
  DO
     CALL RANDOM_NUMBER(r)
     IF (r > zero) EXIT
  END DO

  random_exponential = -LOG(r)
  RETURN
END FUNCTION random_exponential
!
! Random sample from normal (Gaussian) distribution
!
FUNCTION rand_normal(mean,stdev) RESULT(c)
  REAL(wp), INTENT(in) :: mean, stdev
  REAL(wp) :: c

  REAL(wp) :: theta, r, temp(2)

  IF(stdev <= 0.0_wp) THEN
    WRITE(*,*) "Standard Deviation must be positive"
  ELSE
    CALL random_number(temp)
    r=SQRT(-2.0_wp*LOG(1.0_wp-temp(1)))
    theta = 2.0_wp*pi*temp(2)
    c= mean+stdev*r*SIN(theta)
  END IF
END FUNCTION
!
! Return a random sample from a gamma distribution
!
RECURSIVE FUNCTION rand_gamma(shape, scale) RESULT(ans)
  REAL(wp), INTENT(in)  :: scale, shape
  REAL(wp) :: ans

  REAL(wp) :: u,v,w,d,c,x,xsq,g

  IF (shape <= 0.0_wp) &
    WRITE(*,*) "Shape PARAMETER must be positive"
  IF (scale <= 0.0_wp) &
    WRITE(*,*) "Scale PARAMETER must be positive"
!
! ## Implementation based on "A Simple Method for Generating Gamma Variables"
! ## by George Marsaglia and Wai Wan Tsang.
! ## ACM Transactions on Mathematical Software
! ## Vol 26, No 3, September 2000, pages 363-372.
!
  IF (shape >= 1.0_wp) THEN
    d = shape - 1.0_wp/3.0_wp
    c = 1.0_wp/SQRT(9.0_wp*d)
    DO WHILE (.true.)
      x = rand_normal(0.0_wp, 1.0_wp)
      v = 1.0 + c*x
      DO WHILE (v <= 0.0_wp)
        x = rand_normal(0.0_wp, 1.0_wp)
        v = 1.0_wp + c*x
      END DO
      v = v**3
      CALL random_number(u)
      xsq = x*x
      IF ((u < 1.0_wp -.0331_wp*xsq*xsq) .OR.  &
        (LOG(u) < 0.5_wp*xsq + d*(1.0_wp - v + LOG(v))) )then
        ans = scale * d * v
        RETURN
      END IF
    END DO
  ELSE
    g = rand_gamma( shape+1.0_wp, 1.0_wp )
    CALL random_number(w)
    ans = scale * g * w**(1.0_wp/shape)
    RETURN
  END IF
END FUNCTION rand_gamma

END MODULE mo_distr
