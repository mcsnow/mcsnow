!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_atmo
  USE mo_constants,    ONLY: wp
  USE mo_atmo_nml,     ONLY: ssat, h0, h1, h2, atmo_type, area_factor, &
                             atmofile
  USE mo_atmo_types,   ONLY: t_atmo, t_diag, eta0, A_i, A_w, g_Rtlr, B_i, &
                             B_w, ps, rhlr, e_3, T_3, Rd, tlr, Rv, T_3i, grav
  USE mo_grid,         ONLY: grid, coord2index
  USE mo_sp_nml,       ONLY: lwc0, interpol_atmos
  
  IMPLICIT NONE
  PRIVATE

  ! TODO move variables out of module to main loop
  TYPE(t_atmo), ALLOCATABLE :: patmo(:)        ! 1D profile with atmo variables
  TYPE(t_diag), ALLOCATABLE :: diag(:)         ! 1D profile with diagnostic fields

  PUBLIC  ::            &
    diag,               &
    t_atmo,             &
    get_atmo,           &
    init_atmo,          &
    delete_atmo,        &
    dynamic_viscosity,  &
    thermalCond_dry,    &
    diffusivity_vapour, &
    latentHeat_depo,    &
    latentHeat_evap,    &
    latentHeat_melt,    &
    psat_ice,           &
    psat_water
  PRIVATE ::            &
    init_atmo_file,     &
    init_atmo_ideal,    &
    init_atmo_warmNose, &
    init_atmo_RH87,     &
    init_atmo_exp,      &
    patmo

CONTAINS

!
! get atmo conditions at given height
!
SUBROUTINE get_atmo( z, atmo )
  REAL(wp),     INTENT(in) :: z
  TYPE(t_atmo), INTENT(out) :: atmo

  INTEGER :: k

  IF ( (interpol_atmos .eqv. .TRUE.) .AND. (grid%nz > 1) ) THEN
    CALL interpolate_atmosphere(z, atmo) 
  ELSE
    k = coord2index( z )
    atmo = patmo( k )
  END IF
END SUBROUTINE get_atmo

!
! initialize 1d profile in patmo
!
SUBROUTINE init_atmo()

  INTEGER :: k

  IF( .NOT. ALLOCATED(patmo) ) THEN
    ALLOCATE(patmo(grid%nz))
  ENDIF
  ! allocate arrays for diagnostics/output
  IF( .NOT. ALLOCATED(diag) ) ALLOCATE(diag(grid%nz))

  SELECT CASE( atmo_type )
    CASE( 1 )
      DO k=1,grid%nz
        CALL init_atmo_ideal(grid%z_f(k),patmo(k))
      END DO
    CASE( 2 )
        CALL init_atmo_file()
    CASE( 3 )
        CALL init_atmo_exp()
    CASE( 4 )
        CALL init_atmo_RH87()
    CASE( 5 )
        !CALL init_atmo_warmNose()
        CALL init_atmo_sounding()
    CASE( 6 )
        CALL init_atmo_soundingKneifel()
    CASE( 7 )
        CALL init_atmo_boehm_validation()
    CASE( 11 )
      DO k=1,grid%nz
        CALL init_atmo_ideal2(grid%z_f(k),patmo(k))
      END DO
    CASE DEFAULT
        STOP "atmo_type not 1, 2, 3, 4, 5, 6, 7 or 11."
  END SELECT
END SUBROUTINE init_atmo

!
! free patmo memory
!
SUBROUTINE delete_atmo()
  IF( ALLOCATED(patmo) ) DEALLOCATE(patmo)
  IF( ALLOCATED(diag) )  DEALLOCATE(diag)
END SUBROUTINE delete_atmo

!
! Interpolation of atmospheric state to particle position
!
SUBROUTINE interpolate_atmosphere(z, atmo)
  REAL(wp),     INTENT(in)    :: z
  TYPE(t_atmo), INTENT(inout) :: atmo

  INTEGER   :: np, np1
  REAL(wp)  :: dz, qvsat, sumqvqc

  np  = coord2index( z ) ! in this cell -> development point for taylor approximation
  np1 = np+1
  IF( z > grid%z_f(np) ) np1=np-1 ! switch to backward difference for gradient
  IF( np == 1 )          np1=2    ! boundaries
  IF( np == grid%nz )    np1=np-1 ! boundaries

  dz  = ( z - grid%z_f(np) ) / ( grid%z_f(np1) - grid%z_f(np) ) 
  atmo%T  = patmo(np)%T  + ( patmo(np1)%T - patmo(np)%T ) * dz
  atmo%p  = patmo(np)%p  + ( patmo(np1)%p - patmo(np)%p ) * dz
  sumqvqc = patmo(np)%qv + patmo(np)%qc
  sumqvqc = sumqvqc      + ( patmo(np1)%qv + patmo(np1)%qc - sumqvqc ) * dz

  atmo%eta  = dynamic_viscosity( atmo%T )
  atmo%etai = 1._wp / atmo%eta
  atmo%psatw = psat_water( atmo%T ) 
  atmo%psati = psat_ice(   atmo%T ) 

  ! calculate cloud liquid water using saturation specific humidity
  qvsat = Rd/Rv * atmo%psatw / (atmo%p - (1.-Rd/Rv) * atmo%psatw)
  !IF( (atmo_type==2) .OR. (atmo_type==5) .OR. (atmo_type==6) ) THEN  ! real case, atmosphere read from file
      atmo%qc = MAX(sumqvqc - qvsat, 0.0_wp) 
      atmo%qv = MIN(sumqvqc, qvsat)  
  !ELSE  ! idealized case with liquid layer?
  !  IF( z >= h1 .AND. z <= h2 ) THEN 
  !    atmo%qc = lwc0 ! TODO / atmo%rho in the end?
  !    atmo%qv = qvsat
  !  ELSE
  !    atmo%qc = 0.0_wp
  !    atmo%qv = MIN(sumqvqc, qvsat )
  !  ENDIF
  !ENDIF
  atmo%pv = Rv/Rd * atmo%qv * atmo%p / (1. + (Rv/Rd - 1.) * atmo%qv)  ! vapor pressure

  atmo%rh   = atmo%pv / atmo%psatw * 100._wp                          ! relative humidity in %
  atmo%ssat = atmo%pv / atmo%psati - 1.                               ! ssat over ice
  atmo%rho  = (atmo%p - atmo%pv) / (Rd * atmo%T) + atmo%pv / (Rv * atmo%T)
END SUBROUTINE interpolate_atmosphere

!
! init atmosphere with warm nose
!
SUBROUTINE init_atmo_warmNose()
 REAL(wp), PARAMETER :: Tsurf = T_3 - 1.0_wp
 REAL(wp), PARAMETER :: RHsurf = 90.0_wp
 REAL(wp), PARAMETER :: psurf = ps ! 92000.0_wp
 REAL(wp), PARAMETER :: inversionBegin = 500.0_wp
 REAL(wp), PARAMETER :: inversionEnd   = 1750.0_wp
 REAL(wp), PARAMETER :: Tlapse = 0.5_wp/100_wp
 REAL(wp), PARAMETER :: Tinc   = 1.0_wp/100_wp
 REAL(wp), PARAMETER :: RHlapse = 10.0_wp/1000_wp
 REAL(wp), PARAMETER :: RHinc   = 50.0_wp/1000_wp

 INTEGER  :: k, ibottom, itop
 REAL(wp) :: pd, pv_surf, temp, t_old, pv_old, p_old

 pv_surf = RhSurf*psat_water( Tsurf )
 ibottom = coord2index( grid%dom_bottom )
 itop    = coord2index( grid%dom_top )
 DO k=ibottom, itop, SIGN(1,itop-ibottom)
  IF( grid%z_f(k) < inversionBegin ) THEN
      patmo(k)%T  = Tsurf - Tlapse*grid%z_f(k)
      patmo(k)%rh = RHsurf - RHlapse*grid%z_f(k)  
      IF( k==ibottom ) THEN
        pd = (psurf - pv_surf)
      ELSE
        pd = (p_old - pv_old)*(1.0_wp - Tlapse*grid%dz/t_old)**(grav/Rd/Tlapse)
      END IF
  ELSE
    IF( grid%z_f(k) < inversionEnd ) THEN
      patmo(k)%T = Tsurf - Tlapse*inversionBegin + Tinc*(grid%z_f(k) - inversionBegin)
      patmo(k)%rh = RHsurf - RHlapse*inversionBegin + RHinc*(grid%z_f(k) - inversionBegin)
      pd = (p_old - pv_old)*(1.0_wp + Tinc*grid%dz/t_old)**(-grav/Rd/Tinc) 
    ELSE
      patmo(k)%T = Tsurf - Tlapse*inversionBegin + Tinc*(inversionEnd - inversionBegin) - Tlapse*(grid%z_f(k)-inversionEnd)
      patmo(k)%rh = RHsurf - RHlapse*inversionBegin + RHinc*(inversionEnd - inversionBegin) - RHlapse*(grid%z_f(k)-inversionEnd)
      pd = (p_old - pv_old)*(1.0_wp - Tlapse*grid%dz/t_old)**(grav/Rd/Tlapse)
    END IF
  END IF 
  temp = patmo(k)%rh - 101.0_wp !rh excess
  IF( temp > 0) THEN
    temp = temp/Rv/patmo(k)%T  !rho_v excess
    patmo(k)%qc = 100*temp/(temp + pd/Rd/patmo(k)%T)
  ELSE
    patmo(k)%qc = 0.0_wp
  END IF
  patmo(k)%rh = MIN(patmo(k)%rh, 101.0_wp)
  patmo(k)%psatw = psat_water( patmo(k)%T )
  patmo(k)%psati = psat_ice( patmo(k)%T )
  patmo(k)%pv    = patmo(k)%rh/100.0_wp*patmo(k)%psatw
  patmo(k)%ssat  = patmo(k)%pv/patmo(k)%psati - 1
  patmo(k)%eta   = dynamic_viscosity( patmo(k)%T )
  patmo(k)%etai  = 1._wp / patmo(k)%eta
  patmo(k)%p     = pd + patmo(k)%pv
  patmo(k)%rho   = (pd/Rd + patmo(k)%pv/Rv)/patmo(k)%T
  patmo(k)%qv    = 1.0_wp/(1.0_wp + (patmo(k)%p/patmo(k)%pv-1.0_wp)*Rv/Rd)
  !patmo(k)%qc    = lwc0 ! 0.0_wp

  t_old  = patmo(k)%T
  pv_old = patmo(k)%pv
  p_old  = patmo(k)%p
 END DO
END SUBROUTINE init_atmo_warmNose

!
! init atmosphere following RH87 JAS 44, 2764
!
SUBROUTINE init_atmo_RH87()
  REAL(wp), PARAMETER :: Tsurf     = T_3 + 24.0_wp
  REAL(wp), PARAMETER :: TopHeight = 5200.0_wp - 800.0_wp
  REAL(wp), PARAMETER :: RhSurf    = 100.0_wp - 40.0_wp
  REAL(wp), PARAMETER :: psurf     = 92000.0_wp
  REAL(wp), PARAMETER :: Tgrad     = (Tsurf - T_3)     / (TopHeight - 0.0)
  REAL(wp), PARAMETER :: Rhgrad    = (RhSurf - 100.0_wp) / (TopHeight - 0.0)
 
  INTEGER  :: k
  REAL(wp) :: pd, pv_surf  
  pv_surf = RhSurf/100.0_wp*psat_water( Tsurf )
  
  DO k=1,grid%nz
    IF( grid%z_f(k) > TopHeight ) THEN
      patmo(k)%T   = T_3
      patmo(k)%rh  = 100.0_wp  
      pd = (psurf - pv_surf) * (T_3/Tsurf)**(grav/Rd/Tgrad)
      pd = pd*EXP(- grav/Rd/T_3*(grid%z_f(k)-TopHeight) )
    ELSE
      patmo(k)%T   = Tsurf  - Tgrad  * grid%z_f(k)
      patmo(k)%rh  = RhSurf - Rhgrad * grid%z_f(k) 
      pd = (psurf - pv_surf) * (patmo(k)%T/Tsurf)**(grav/Rd/Tgrad)
    END IF
    patmo(k)%psatw = psat_water( patmo(k)%T )
    patmo(k)%psati = psat_ice( patmo(k)%T )
    patmo(k)%pv    = patmo(k)%rh/100.0_wp*patmo(k)%psatw    
    patmo(k)%ssat  = patmo(k)%pv/patmo(k)%psati - 1
    patmo(k)%eta   = dynamic_viscosity( patmo(k)%T )
    patmo(k)%etai  = 1._wp / patmo(k)%eta
    patmo(k)%p     = pd + patmo(k)%pv 
    patmo(k)%rho   = (pd/Rd + patmo(k)%pv/Rv)/patmo(k)%T
    patmo(k)%qv    = 1.0_wp/(1.0_wp + (patmo(k)%p/patmo(k)%pv-1.0_wp)*Rv/Rd)
    patmo(k)%qc    = lwc0 / patmo(k)%rho ! 0.0_wp
  END DO
END SUBROUTINE init_atmo_RH87

!
! init atmosphere with wind tunnel atmosphere
!
SUBROUTINE init_atmo_exp()
  !REAL(wp), PARAMETER :: RH = 0.78_wp !2.0_wp ! 0.5_wp
  REAL(wp), PARAMETER :: Tcrit = T_3 -10.0_wp ! + 20.0_wp ! - 10.0_wp ! 263.6_wp !283.6_wp
  REAL(wp), PARAMETER :: rho_d = 1.2754_wp

  REAL(wp)       :: RH
  REAL(wp), SAVE :: Tcur = Tcrit

  INTEGER :: k

  RH = ssat
  Tcur = Tcur + area_factor; ! 1.7_wp/60.0_wp

  DO k=1,grid%nz
    patmo(k)%T     = Tcur !Tcrit
    patmo(k)%psatw = psat_water( patmo(k)%T )
    patmo(k)%psati = psat_ice( patmo(k)%T )
    patmo(k)%rh    = RH
    patmo(k)%pv    = RH/100.0_wp*patmo(k)%psatw 
    patmo(k)%ssat  = patmo(k)%pv/patmo(k)%psati - 1
    patmo(k)%eta   = dynamic_viscosity( patmo(k)%T )
    patmo(k)%etai  = 1._wp / patmo(k)%eta

    patmo(k)%rho   = rho_d + patmo(k)%pv/Rv/patmo(k)%T
    patmo(k)%p     = patmo(k)%pv + Rd*rho_d*patmo(k)%T
    patmo(k)%qv    = 1.0_wp/(1.0_wp + (patmo(k)%p/patmo(k)%pv-1.0_wp)*Rv/Rd)  
    patmo(k)%qc    = 0.0_wp 
  END DO
END SUBROUTINE

SUBROUTINE init_atmo_boehm_validation()
  ! Set up atmosphere for validation of Boehm collision efficiency based on his papers 
  REAL(wp) :: Tcur
  REAL(wp) :: rho_d
  REAL(wp)       :: RH
 
  INTEGER  :: k
  INTEGER  :: run
  
  run = 0
  
  IF (run == 0) THEN              ! Boehm 1992b: E_c for drops
    Tcur  = 273.15
    rho_d = 1.1477_wp             ! p =  900hPa
  ELSEIF (run == 1) THEN          ! Boehm 1992c: E_c for oblates 
    Tcur  = 263.15
    rho_d = 0.9269_wp             ! p = 700hPa
  ELSEIF (run == 2) THEN          ! Boehm 1992c: E_c for cylinders
    Tcur  = 265.15
    rho_d = 1.0512_wp             ! p = 800hPa
  ELSEIF (run == 3) THEN          ! Boehm 1992c: E_c for branched (Fig. 6)
    Tcur  = 273.15
    rho_d = 1.0205_wp             ! p = 800hPa
  ELSEIF (run == 4) THEN          ! Boehm 94 drops
    Tcur  = 295.15
    rho_d = 1.1798_wp             ! p = 800hPa
  ENDIF

  RH = ssat

  DO k=1,grid%nz
    patmo(k)%T     = Tcur !Tcrit
    patmo(k)%psatw = psat_water( patmo(k)%T )
    patmo(k)%psati = psat_ice( patmo(k)%T )
    patmo(k)%rh    = RH
    patmo(k)%pv    = RH/100.0_wp*patmo(k)%psatw 
    patmo(k)%ssat  = patmo(k)%pv/patmo(k)%psati - 1
    patmo(k)%eta   = dynamic_viscosity( patmo(k)%T )
    patmo(k)%etai  = 1._wp / patmo(k)%eta

    patmo(k)%rho   = rho_d + patmo(k)%pv/Rv/patmo(k)%T
    patmo(k)%p     = patmo(k)%pv + Rd*rho_d*patmo(k)%T
    patmo(k)%qv    = 1.0_wp/(1.0_wp + (patmo(k)%p/patmo(k)%pv-1.0_wp)*Rv/Rd)  
    patmo(k)%qc    = 0.0_wp 
  END DO
END SUBROUTINE

!
! set ambient atmospheric variables using acii radiosonde sounding from Stefan Kneifel
!
SUBROUTINE init_atmo_soundingKneifel()
  INTEGER  :: unit, i, io, iend, k, inext
  INTEGER  :: ndim = 13000
  REAL(wp) :: dz1, qvsat, qv
  REAL(wp), ALLOCATABLE :: zz(:),tt(:),pp(:),rh(:)
  ALLOCATE(zz(ndim))
  ALLOCATE(tt(ndim))
  ALLOCATE(pp(ndim))
  ALLOCATE(rh(ndim))
  unit = 74
  OPEN(unit,file=atmofile,status='old',action="read")
  DO i=1,ndim
    READ(unit,*,iostat=io) zz(i), tt(i), rh(i), pp(i)
    IF(io>0) THEN
      WRITE(*,*) "Error while reading ", atmofile, " as radiosonde sounding from University of Wyoming"
    ELSEIF(io<0) THEN
      EXIT
    END IF
  END DO
  iend = i-1
  CLOSE(unit)
  zz(1:iend) = zz(1:iend) - zz(1) ! move ground level to 0m

  DO k=1,grid%nz
     ! height interpolation
     i = MINLOC( ABS( zz(1:iend) - grid%z_f(k) ) , DIM=1 )
     ! test that other index is on other side
     inext = MERGE( MAX(i-1,1), MIN(i+1,iend), (zz(i)-grid%z_f(k))*(zz(MAX(i-1,1))-grid%z_f(k)) < 0 )
     dz1   = MERGE( ABS((grid%z_f(k) - zz(inext))/(zz(i) - zz(inext))), 1.0_wp, i .ne. inext )
           patmo(k)%T  = (1.-dz1) * tt(inext) + dz1 * tt(i) + T_3       ! data in Celsius
           patmo(k)%p  = (1.-dz1) * pp(inext) + dz1 * pp(i)             ! data in Pa
           patmo(k)%rh = (1.-dz1) * rh(inext) + dz1 * rh(i)             ! data already in %
           ! calculate other variables necessary for 1d model

           ! calculate saturation vapor pressures
           patmo(k)%psatw = psat_water( patmo(k)%T )
           patmo(k)%psati = psat_ice( patmo(k)%T )
           ! vapor pressure
           patmo(k)%pv    = patmo(k)%psatw * patmo(k)%rh / 100.0_wp
           ! calculate cloud liquid water using saturation specific humidity
           qvsat = Rd/Rv * patmo(k)%psatw / (patmo(k)%p - (1.-Rd/Rv)*patmo(k)%psatw)
           qv    = Rd/Rv * patmo(k)%pv    / (patmo(k)%p - (1.-Rd/Rv)*patmo(k)%pv)
           patmo(k)%qc = MAX(qv-qvsat,0.0_wp)
           patmo(k)%qv = MIN(qv,qvsat)
           IF( patmo(k)%rh > 98.0_wp ) THEN
             patmo(k)%rh = 100.0_wp
             patmo(k)%qc = MAX(patmo(k)%qc , lwc0) ! TODO lwc0/rho
             patmo(k)%qv = qvsat
             patmo(k)%pv = patmo(k)%psatw
           ENDIF
           ! supersaturation over ice (dimensionless)
           patmo(k)%ssat = patmo(k)%pv / patmo(k)%psati - 1.
           ! density of moist air
           patmo(k)%rho = (patmo(k)%p-patmo(k)%pv)/(Rd*patmo(k)%T) + patmo(k)%pv/(Rv*patmo(k)%T)
           ! dynamic viscocity
           patmo(k)%eta  = dynamic_viscosity( patmo(k)%T )
           patmo(k)%etai = 1._wp / patmo(k)%eta
  END DO

  DEALLOCATE(zz,tt,pp,rh)
END SUBROUTINE init_atmo_soundingKneifel

!
! set ambient atmospheric variables using acii radiosonde sounding from University of Wyoming 
!
SUBROUTINE init_atmo_sounding()
  INTEGER  :: unit, i, io, iend, k, inext
  INTEGER  :: ndim = 13000
  REAL(wp) :: dummy, dz1, qvsat, qv
  REAL(wp), ALLOCATABLE    :: zz(:),tt(:),pp(:),rh(:)
  ALLOCATE(zz(ndim))
  ALLOCATE(tt(ndim))
  ALLOCATE(pp(ndim))
  ALLOCATE(rh(ndim))
  unit = 74
  OPEN(unit,file=atmofile,status='old',action="read")
  DO i=1,ndim
    READ(unit,*,iostat=io) pp(i), zz(i), tt(i), dummy, rh(i)
    IF(io>0) THEN
      WRITE(*,*) "Error while reading ", atmofile, " as radiosonde sounding from University of Wyoming"
    ELSEIF(io<0) THEN
      EXIT
    END IF
  END DO
  iend = i-1
  CLOSE(unit)
  zz(1:iend) = zz(1:iend) - zz(1) ! move ground level to 0m

  DO k=1,grid%nz
     ! height interpolation
     i = MINLOC( ABS( zz(1:iend) - grid%z_f(k) ) , DIM=1 )
     ! test that other index is on other side
     inext = MERGE( MAX(i-1,1), MIN(i+1,iend), (zz(i)-grid%z_f(k))*(zz(MAX(i-1,1))-grid%z_f(k)) < 0 )
     dz1   = MERGE( ABS((grid%z_f(k) - zz(inext))/(zz(i) - zz(inext))), 1.0_wp, i .ne. inext )

           patmo(k)%T  = (1.-dz1) * tt(inext) + dz1 * tt(i) + T_3       ! data in Celsius
           patmo(k)%p  = ((1.-dz1) * pp(inext) + dz1 * pp(i))*100.0_wp  ! data in hPa
           patmo(k)%rh = (1.-dz1) * rh(inext) + dz1 * rh(i)             ! data already in %
           ! calculate other variables necessary for 1d model
           
           ! calculate saturation vapor pressures
           patmo(k)%psatw = psat_water( patmo(k)%T )
           patmo(k)%psati = psat_ice( patmo(k)%T )
           ! vapor pressure
           patmo(k)%pv    = patmo(k)%psatw * patmo(k)%rh / 100.0_wp
           ! calculate cloud liquid water using saturation specific humidity
           qvsat = Rd/Rv * patmo(k)%psatw / (patmo(k)%p - (1.-Rd/Rv)*patmo(k)%psatw)
           qv    = Rd/Rv * patmo(k)%pv    / (patmo(k)%p - (1.-Rd/Rv)*patmo(k)%pv)
           patmo(k)%qc = MAX(qv-qvsat,0.0_wp)
           patmo(k)%qv = MIN(qv,qvsat)
           IF( patmo(k)%rh > 98.0_wp ) THEN
             patmo(k)%rh = 100.0_wp
             patmo(k)%qc = MAX(patmo(k)%qc , lwc0) ! TODO lwc0 / rho
             patmo(k)%qv = qvsat                   
             patmo(k)%pv = patmo(k)%psatw
           ENDIF
           ! supersaturation over ice (dimensionless)
           patmo(k)%ssat = patmo(k)%pv / patmo(k)%psati - 1.
           ! density of moist air
           patmo(k)%rho = (patmo(k)%p-patmo(k)%pv)/(Rd*patmo(k)%T) + patmo(k)%pv/(Rv*patmo(k)%T)
           ! dynamic viscocity
           patmo(k)%eta  = dynamic_viscosity( patmo(k)%T )
           patmo(k)%etai = 1._wp / patmo(k)%eta
  END DO

  DEALLOCATE(zz,tt,pp,rh)
END SUBROUTINE init_atmo_sounding

!
! set ambient atmospheric variables using ascii file
!
SUBROUTINE init_atmo_file()
  INTEGER  :: i, k, iend, inext
  INTEGER  :: unit,io
  INTEGER  :: ndim = 13000 !200
  REAL(wp) :: dummy, dz1, qvsat
  REAL(wp), ALLOCATABLE    :: zz(:),tt(:),pp(:),qv(:)
  CHARACTER(30) :: fmt1a = '(a6,4a12)'
  CHARACTER(30) :: fmt1b = '(i6,4e12.4)'
  CHARACTER(40) :: fmt2a = '(a6,2a12,3a14,2a10,2a14)'
  CHARACTER(40) :: fmt2b = '(i6,2f12.2,3e14.4,2f10.3,2e14.4)'
 
  ALLOCATE(zz(ndim))
  ALLOCATE(tt(ndim))
  ALLOCATE(pp(ndim))
  ALLOCATE(qv(ndim))

  WRITE (*,fmt1a) 'i','zz','tt','pp','qv'
  !OPEN (newunit=unit,file=atmofile,status='old',action="read")
  unit = 74
  OPEN(unit,file=atmofile,status='old',action="read")
  DO i=1,ndim
     READ (unit,*,iostat=io) zz(i),tt(i),pp(i),qv(i),dummy
     WRITE (*,fmt1b) i,zz(i),tt(i),pp(i),qv(i)
     if (io > 0) then
        print *,"ERROR while read ecmwf_profile.txt"
     elseif (tt(i) /= tt(i)) then
        ! NaN is an indicator that we have reached the surface
        print *,"Hit the surface while reading profile file"
        exit
     elseif (io < 0 ) then
        exit
     end if
  END DO  
  iend = i-1
  print *," Found ",iend," lines in file"
  close(unit)
  zz(1:iend) = zz(1:iend) - zz(iend)
  
  !WRITE (*,fmt1a) 'i','zz','tt','pp','qv'
  !DO i=1,iend
  !   WRITE (*,fmt1b) i,zz(i),tt(i),pp(i),qv(i)
  !END DO

  qv(:) = qv(:) * area_factor

  DO k=1,grid%nz
     ! height interpolation
     i = MINLOC( ABS( zz(1:iend) - grid%z_f(k) ) , DIM=1 )
     ! test that other index is on other side
     inext = MERGE( MAX(i-1,1), MIN(i+1,iend), (zz(i)-grid%z_f(k))*(zz(MAX(i-1,1))-grid%z_f(k)) < 0 )
     dz1   = MERGE( ABS((grid%z_f(k) - zz(inext))/(zz(i) - zz(inext))), 1.0_wp, i .ne. inext )
     patmo(k)%T  = (1.-dz1) * tt(inext) + dz1 * tt(i)
     patmo(k)%p  = (1.-dz1) * pp(inext) + dz1 * pp(i)
     patmo(k)%qv = (1.-dz1) * qv(inext) + dz1 * qv(i)

           ! print *,k,i,grid%z_f(k),zz(i),zz(i+1),tt(i),tt(i+1),patmo(k)%T,dz1

           ! calculate other variables necessary for 1d model
           
           ! calculate vapor pressure using specific humidity and pressure

           patmo(k)%psatw = psat_water( patmo(k)%T ) 
           patmo(k)%psati = psat_ice( patmo(k)%T ) 

           ! calculate cloud liquid water using saturation specific humidity

           qvsat = Rd/Rv * patmo(k)%psatw / (patmo(k)%p - (1.-Rd/Rv)*patmo(k)%psatw)

           patmo(k)%qc = MAX(patmo(k)%qv-qvsat,0.0_wp) 
           patmo(k)%qv = MIN(patmo(k)%qv,qvsat) 

           ! vapor pressure
           patmo(k)%pv = Rv/Rd*patmo(k)%qv*patmo(k)%p/(1.+(Rv/Rd-1.)*patmo(k)%qv)

           ! relative humidity in %
           patmo(k)%rh = patmo(k)%pv / patmo(k)%psatw * 100._wp

           ! supersaturation over ice (dimensionless)
           patmo(k)%ssat = patmo(k)%pv / patmo(k)%psati - 1.

           ! density of moist air
           patmo(k)%rho = (patmo(k)%p-patmo(k)%pv)/(Rd*patmo(k)%T) + patmo(k)%pv/(Rv*patmo(k)%T)

           ! dynamic viscocity
           patmo(k)%eta  = dynamic_viscosity( patmo(k)%T )
           patmo(k)%etai = 1._wp / patmo(k)%eta
  END DO

  WRITE (*,fmt2a) 'k','z [m]','T [K]','p [Ps]','qv [kg/kg]','qc [kg/kg]',&
       &          'rh [%]','S_i [%]','rho [kg/m3]','eta [m2/s]'
  DO k=1,grid%nz,20
     WRITE (*,fmt2b) k,grid%z_f(k),patmo(k)%T,patmo(k)%p,patmo(k)%qv,patmo(k)%qc,&
          &            patmo(k)%rh,patmo(k)%ssat*100,patmo(k)%rho,patmo(k)%eta
  END DO

END SUBROUTINE init_atmo_file


!
! set ambient atmospheric variables for idealized 1d profile
!
SUBROUTINE init_atmo_ideal( z, atmo )
  REAL(wp),     INTENT(in)  :: z
  TYPE(t_atmo), INTENT(out) :: atmo

#ifndef WDROPS
  REAL(wp) :: Tc, pd, psati, rh, psatw, pv_srf
#else
  REAL(wp) :: Tc, pd, pv_srf
#endif

  ! temp. profile
  atmo%T = T_3 - tlr*z
  Tc = atmo%T - T_3

  ! for w. droplets the atmosphere is dry
  atmo%pv = 0._wp
  pv_srf = 0._wp

#ifndef WDROPS

  psati = psat_ice( atmo%T ) 
  psatw = psat_water( atmo%T ) 

  atmo%psatw = psatw
  atmo%psati = psati

  IF ( z > h2 ) THEN
    ! upper region: RH>0%, ssat=const

    ! vapor pressure in the upper region
    atmo%pv   = (ssat + 1.) * psati
    atmo%ssat = ssat
    atmo%rh  = atmo%pv / psatw * 100._wp

  ELSE 

    IF ( z > h1 ) THEN
      ! the 'LWC region': RH=100%, ssat>0
      rh = 100._wp
    ELSE
      ! ground region, RH<100%, ssat<0
      rh = 100._wp + rhlr*(z-h1)
    END IF


    atmo%pv   = 1e-2_wp * rh * psatw
    atmo%ssat = atmo%pv / psati - 1._wp
    atmo%rh  = atmo%pv / psatw * 100._wp
  END IF

  ! surface vapor pressure
  pv_srf = 1e-2_wp * (100._wp-h1*rhlr) * psatw
#endif

  ! dry atmosphere is in hyd. balance
  pd = (ps - pv_srf) * EXP( g_Rtlr * LOG(atmo%T * T_3i) )

  ! total pressure
  atmo%p = atmo%pv + pd

  atmo%rho = pd/(Rd*atmo%T) + atmo%pv/(Rv*atmo%T)

  atmo%eta = dynamic_viscosity( atmo%T )
  atmo%etai = 1._wp / atmo%eta

  ! cloud water
  atmo%qc =  MERGE(lwc0,0._wp,z>=h1.AND.z<=h2) / atmo%rho

  atmo%qv = MERGE(1.0_wp/(1.0_wp + (atmo%p/atmo%pv-1.0_wp)*Rv/Rd), 0.0_wp, atmo%pv > 0.0_wp)

END SUBROUTINE init_atmo_ideal


!
! set ambient atmospheric variables for idealized 1d profile
! extended to 
!
SUBROUTINE init_atmo_ideal2( z, atmo )
  REAL(wp),     INTENT(in)  :: z
  TYPE(t_atmo), INTENT(out) :: atmo

  REAL(wp) :: pd, psati, rh, psatw, pv_srf

  !WRITE(*,*) "init_atmo_ideal2 ", h0, " z ", z, " T ", T_3 - tlr*(z-h0)

  ! temp. profile
  atmo%T = T_3 - tlr*(z-h0)
  !IF( z <= h0 ) THEN
  ! atmo%T = T_3
  !END IF
  ! for w. droplets the atmosphere is dry
  atmo%pv = 0._wp
  pv_srf = 0._wp

  psati = psat_ice( atmo%T )
  psatw = psat_water( atmo%T )
  atmo%psatw = psatw
  atmo%psati = psati
  IF ( z > h2 ) THEN
    ! upper region: RH>0%, ssat=const
    ! vapor pressure in the upper region
    atmo%pv   = (ssat + 1.) * psati
    atmo%ssat = ssat
    atmo%rh  = atmo%pv / psatw * 100._wp
  ELSE IF( z > h1 ) THEN
      ! the 'LWC region': RH=100%, ssat>0
      rh = 100._wp
      atmo%pv   = 1e-2_wp * rh * psatw
      atmo%ssat = atmo%pv / psati - 1._wp
      atmo%rh  = atmo%pv / psatw * 100._wp
  ELSE IF( z > h0 ) THEN
      ! ground region, RH<100%, ssat<0
      rh = 100._wp + rhlr*(z-h1)*0.5_wp
      atmo%pv   = 1e-2_wp * rh * psatw
      atmo%ssat = atmo%pv / psati - 1._wp
      atmo%rh  = atmo%pv / psatw * 100._wp
  ELSE
      rh = 100._wp + rhlr*(h0-h1)*0.5_wp
      atmo%pv   = 1e-2_wp * rh * psatw
      atmo%ssat = atmo%pv / psati - 1._wp
      atmo%rh  = atmo%pv / psatw * 100._wp
  END IF
  ! surface vapor pressure
  pv_srf = 1e-2_wp * (100._wp-h1*rhlr) * psatw
  ! dry atmosphere is in hyd. balance
  pd = (ps - pv_srf) * EXP( g_Rtlr * LOG(atmo%T * T_3i) )
  ! total pressure
  atmo%p = atmo%pv + pd
  atmo%rho = pd/(Rd*atmo%T) + atmo%pv/(Rv*atmo%T)
  atmo%eta = dynamic_viscosity( atmo%T )
  atmo%etai = 1._wp / atmo%eta
  ! cloud water
  atmo%qc =  MERGE(lwc0,0._wp,z>=h1.AND.z<=h2) / atmo%rho
  atmo%qv = MERGE(1.0_wp/(1.0_wp + (atmo%p/atmo%pv-1.0_wp)*Rv/Rd), 0.0_wp, atmo%pv > 0.0_wp)
END SUBROUTINE init_atmo_ideal2



!
! dynamic viscosity
! alternative Sutherland law (https://www.cfd-online.com/Wiki/Sutherland%27s_law)
!
ELEMENTAL FUNCTION dynamic_viscosity( T ) result(eta)
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: eta
    ! REAL(wp) :: Tc
    REAL(wp), PARAMETER  :: A_S = 1.458e-6, T_S = 110.4
    ! Tc = T - T_3
    ! Where is this from?
    ! eta = eta0 * (1._wp + Tc*(0.00285215366705471478_wp - Tc*6.9848661233993015133e-6_wp))  

    ! Sutherland law
    eta = A_S * T**(3./2.) / ( T + T_S )
END FUNCTION dynamic_viscosity

!
! saturation vapor pressure over ice 
!
PURE ELEMENTAL FUNCTION psat_ice( T ) result( psati )
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: psati
    ! Beheng, lecture scrpt
    ! psati = 610.64_wp * EXP( 21.88_wp * Tc / (Tc + 265.5_wp) )
    psati = e_3 * EXP( A_i * (T - T_3) / (T - B_i) )
    ! alternative Goff-Gratch equation
    ! tt0 = 273.16_wp / atmo%T
    ! psati = 611.73_wp * 10**(-9.09718_wp*(tt0-1._wp) - 3.56654_wp*LOG10(tt0) + &
    !        0.876793_wp*(1._wp-1._wp/tt0))
END FUNCTION psat_ice

!
! saturation vapor pressure over water
!
PURE ELEMENTAL FUNCTION psat_water( T ) result ( psatw )
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: psatw
    ! Beheng, lecture scrpt
    ! psatw = 610.7_wp * EXP( 17.15_wp * Tc / (Tc + 234.9_wp) )
    psatw = e_3 * EXP( A_w * (T - T_3) / (T - B_w) )
    ! alternative Goff-Gratch equation
    ! tts = 373.15_wp / atmo%T
    ! psatw = 101325_wp * 10**(-7.90298_wp*(tts-1._wp) + 5.02808_wp*LOG10(tts) - &
    !  1.3816e-7_wp*(10**(11.344*(1._wp-1._wp/tts))-1._wp) + &
    !  8.1328e-3_wp*(10**(3.49149_wp*(1._wp-tts)-1._wp)))
END FUNCTION psat_water

!
! [J/msK] thermal conductivity of dry air (PK '97, p. 508)
!
PURE ELEMENTAL FUNCTION thermalCond_dry( T ) result ( Ka )
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: Ka

    Ka = 418.4e-5_wp*( 5.69_wp + 0.017_wp*(T - T_3) )
END FUNCTION thermalCond_dry

!
! [m2/s] diffusivity of water vapor (PK '97, p. 503)
! NOTE: Ghosh et al. (2007) proposed a more accurate expression
!
PURE ELEMENTAL FUNCTION diffusivity_vapour( T, p ) result( Dv )
    REAL(wp), INTENT(in) :: T, p
    REAL(wp)             :: Dv

    Dv = 0.211e-4_wp * (T * T_3i)**1.94_wp * ps / p
END FUNCTION diffusivity_vapour

!
! [J/kg] latent heat release of deposition/sublimation
!
PURE ELEMENTAL FUNCTION latentHeat_depo( T ) result ( Ld )
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: Ld
    REAL(wp) :: Tc
    Tc = T - T_3
    Ld = 2834100_wp - Tc*(290_wp + Tc*4_wp) 
END FUNCTION latentHeat_depo

!
! [J/kg] latent heat release of evaporation/condensation
!
PURE ELEMENTAL FUNCTION latentHeat_evap( T ) result ( Le )
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: Le
    REAL(wp) :: Tc
    Tc = T - T_3
    Le = 2500800_wp + Tc*(-2360_wp + Tc*(1.6_wp - 0.06_wp*Tc)) ! Slavko Wikipedia, more efficient, no power involved
    !Le = 2.5008e6_wp * (T_3/T)**(0.167_wp + 3.67e-4_wp* T) ! RH87a
END FUNCTION latentHeat_evap

!
! [J/kg] latent heat release of melting/freezing
!
PURE ELEMENTAL FUNCTION latentHeat_melt( T ) result ( Lm )
    REAL(wp), INTENT(in) :: T
    REAL(wp)             :: Lm
    REAL(wp) :: Tc
    Tc = T - T_3
    Lm = 4.1840e3_wp* ( 79.7_wp + 0.485_wp*Tc - 2.5e-3_wp*Tc**2 ) ! RH87a
END FUNCTION latentHeat_melt

END MODULE mo_atmo
