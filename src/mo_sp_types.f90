!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_sp_types

  USE mo_constants,    ONLY: wp, i4, i8, mp

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: t_traj, t_sp, t_tnode, t_node, t_node_ptr, t_list, t_sortNode, t_list_stats, p_addSp

  !>
  !! <Simple Lagrangian trajectory definition>
  !!
  TYPE t_traj
    ! -prognostic fields-
    REAL(wp) :: z             ! [m] z-position ! 1
    ! -management variable-
    INTEGER(i4) :: cellId = -1                 ! 1.5
    INTEGER(i4) :: start_pe                    ! 2
    INTEGER(i4) :: local_idx                   ! 2.5
  END TYPE t_traj

  !>
  !! <simple Superparticle definition>
  !!
  TYPE, EXTENDS(t_traj) :: t_superParticle
    ! -prognostic fields-
    INTEGER(i4) :: statusb = 0! set all bits to zero?     ! 3
    INTEGER(i8) :: xi         ! [1] droplet multiplicity  ! 4
    REAL(mp)    :: mm = 1.0_mp! [1] monomer multiplicity  ! 5
  END TYPE t_superParticle

  !>
  !! <superdrop definition>
  !!
  TYPE, EXTENDS(t_superParticle) :: t_drop
    ! -prognostic fields-
    REAL(wp) :: m_w = 0.0_wp  ! [kg] mass of water        ! 6
    REAL(wp) :: T             ! [K]                       ! 7
    ! -diagnostic fields-
    REAL(mp) :: v             ! [m/s] velocity            ! 8
  END TYPE t_drop

  !>
  !! <superice definition>
  !!
  TYPE, EXTENDS(t_drop) :: t_ice
    ! -prognostic fields-
    REAL(wp) :: m_i = 0.0_wp  ! [kg] mass of ice          ! 1
    REAL(wp) :: m_r = 0.0_wp  ! [kg] mass of rimed part   ! 2
    REAL(wp) :: v_r = 0.0_wp  ! [m3] volume of rimed part ! 3
    REAL(wp) :: m_f = 0.0_wp  ! [kg] mass of frozen part  ! 4
    ! -diagnostic fields-
    REAL(mp) :: d             ! [m]  diameter             ! 5
    REAL(mp) :: p_area        ! [m2] projected area       ! 6
    !REAL(mp) :: q = 0.0_wp    ! [J =m^2kg/s] heat (for melting or freezing)
    REAL(mp) :: init_temp     ! [K]  temp. at nucleation point              ! 7
    REAL(mp) :: init_ssat     ! [1]  supersaturation at nucleation point    ! 8
    REAL(mp) :: phi = 1._wp   ! aspect ratio
    REAL(wp) :: V_i = 0.0_wp  ! [m3] volume of ice part   
  END TYPE t_ice

#ifndef WDROPS
  TYPE, EXTENDS(t_ice)  :: t_sp
#else
  TYPE, EXTENDS(t_drop) :: t_sp
#endif
  END TYPE t_sp

!  !
!  ! a super-droplet
!  !
!  TYPE :: t_sp
!    ! -prognostic fields-
!    REAL(wp) :: z             ! [m] z-position
!    REAL(wp) :: m_w = 0.0_wp  ! [kg] mass of water
!    INTEGER(i8) :: xi         ! [1] droplet multiplicity
!    INTEGER(i8) :: statusb = 0! set all bits to zero?
!    INTEGER(i8) :: mm = 1        ! [1] monomer multiplicity
!    ! -diagnostic fields-
!    REAL(mp)    :: v          ! [m/s] velocity
!#ifndef WDROPS      
!    ! -prognostic fields-
!    REAL(wp) :: m_i = 0.0_wp  ! [kg] mass of ice
!    REAL(wp) :: m_r = 0.0_wp  ! [kg] mass of rimed part
!    REAL(wp) :: v_r = 0.0_wp  ! [m3] volume of rimed part
!    ! -diagnostic fields-
!    REAL(mp) :: d             ! [m]  diameter
!    REAL(mp) :: p_area        ! [m2] projected area
!    REAL(mp) :: q = 0.0_wp    ! [J =m^2kg/s] heat (for melting or freezing)
!    REAL(mp) :: init_temp     ! [K]  temp. at nucleation point
!    REAL(mp) :: init_ssat     ! [1]  supersaturation at nucleation point
!#endif
!  END TYPE t_sp

  !
  ! type for the particle status bytes
  !
  TYPE, PRIVATE :: t_status 
    ! status out of m2d
    INTEGER :: liquidOut   
    INTEGER :: liquidFillin
    INTEGER :: liquidSurf
    ! created by
    INTEGER :: initialized 
    INTEGER :: autoconversion
    INTEGER :: hom_nuc
    INTEGER :: het_nuc
    INTEGER :: iceMulti    
    INTEGER :: shedded
    INTEGER :: breakup
    ! merging
    INTEGER :: merged
    ! additional
    INTEGER :: topInflux
  END TYPE t_status

  TYPE (t_status), PUBLIC, PARAMETER :: sp_stat = t_status(1,2,3,4,5,6,7,8,9,10,11,12)
       
  !
  ! a node for binary tree in aggregation history
  !
  TYPE :: t_tnode
    TYPE(t_sp)            :: sp1, sp2
    REAL(wp)               :: coll_temp, coll_ssat
    TYPE(t_tnode), POINTER :: l => null()
    TYPE(t_tnode), POINTER :: r => null()
  END TYPE t_tnode


  !
  !  a node for a super-droplet
  !
  TYPE :: t_node
    TYPE(t_sp)            :: sp
    TYPE(t_node), POINTER  :: next => null()
    TYPE(t_tnode), POINTER :: hist => null()
  END TYPE t_node

  !
  !  a pointer to the node (used for array of pointers)
  !
  TYPE :: t_node_ptr
    TYPE(t_node), POINTER :: ptr
  END TYPE t_node_ptr


  !
  ! linked list of super-droplets
  !
  TYPE :: t_list
    TYPE(t_node), POINTER :: head => null()
    INTEGER               :: length = 0
  END TYPE t_list


  !
  ! some statistics from the list
  !
  TYPE :: t_list_stats
    REAL(wp) :: tot_mass_sys     ! [kg]    total liquid/ice mass in system
    REAL(wp) :: tot_mass         ! [kg/m3] total liquid/ice mass
    REAL(wp) :: tot_prec         ! [kg/m3*m/s=kg/(m2*s)] total precipitation rate
    REAL(wp) :: mean_mass        ! [kg]    mean mass of droplets
    REAL(wp) :: nsp             ! [1/m3]  number of SPs per volume
    REAL(wp) :: nrp             ! [1/m3]  number of RPs per volume
    REAL(wp) :: nsedi = 0._wp    ! [1/m2]  number of sedimented drops per surface area
    REAL(wp) :: msedi = 0._wp    ! [kg/m2] total mass of sedimented drops per surface area
  END TYPE t_list_stats

  !
  ! a node to sort super particles
  !
  TYPE :: t_sortNode
     REAL(wp)              :: val
     TYPE(t_node), POINTER :: targetNode => null()
  END TYPE t_sortNode

  INTERFACE
    SUBROUTINE p_addSp( sp )
      import t_sp
      TYPE(t_sp),   INTENT(in)    :: sp
    END SUBROUTINE p_addSp
  END INTERFACE

END MODULE mo_sp_types
