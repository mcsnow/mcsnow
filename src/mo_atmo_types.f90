!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_atmo_types
  USE mo_constants,    ONLY: wp, pi

  IMPLICIT NONE

  TYPE :: t_atmo
    REAL(wp) :: &
      p,        &  ! [Pa]    pressure
      pv,       &  ! [Pa]    vapor pressure
      qv,       &  ! [kg/kg] specific humidity of vapor
      qc,       &  ! [kg/kg] specific humidity of cloud liquid water
      rho,      &  ! [kg/m3] density
      T,        &  ! [K]     temperature
      eta,      &  ! [kg/ms] dynamic viscosity
      etai,     &  ! [ms/kg] inverse of dynamic viscosity
      ssat,     &  ! [1]     supersaturation over ice (ssat=pv/e_sat,i-1)
      rh,       &  ! [%]     rel. humidity
      psatw,    &
      psati
  END TYPE t_atmo

  TYPE :: t_diag
     REAL(wp) :: &
          rime_rate,  & ! riming growth rate (total)
          dep_rate,   & ! depositional growth rate (total)
          dep_mono,   & ! depositional growth rate of mono-crystals
          heat_rate     ! warming cooling due to latent heat
  END type t_diag

  ! constants
  !
  REAL(wp), PARAMETER ::    &
    ps    = 101325._wp,     &  ! [Pa]    surface pressure
    grav  = 9.81_wp,        &  ! [m/s2]  gravitational acceleration
    tlr   = 0.0062_wp,      &  ! [K/m]   temp. lapse rate
    eta0  = 1.718e-5_wp,    &  ! [kg/ms] dynamic viscosity of dry air @ T=273 K
    Rd    = 287.102_wp,     &  ! [J/kgK] gas constant of dry air
    Rv    = 461.401_wp,     &  ! [J/kgK] gas constant of cloud water vapor
    c_pa  = 1005_wp,        &  ! [J/kgK] specific heat capacity of dry air
    c_pi  = 2.108e3_wp,     &  ! [J/kgK] specific heat capacity of ice
    c_pw  = 4.205e3_wp,     &  ! [J/kgK] specific heat capacity of water
    rhlr  = 0.05_wp,        &  ! [%/m]   rel. humidity lapse rate
    rhoi  = 919_wp,         &  ! [kg/m3] density of ice (916.2@0°C, 918.9@-10°C,
                               !         919.4@-20°C, 920@-30°C)
    rhol  = 1e3_wp
  

  REAL(wp), PARAMETER ::    &
    A_i  = 2.18745584e1_wp, &  !..constant in saturation pressure - ice
    A_w  = 1.72693882e1_wp, &  !..constant in saturation pressure - liquid
    B_i  = 7.66000000e0_wp, &  !..constant in saturation pressure - ice
    B_w  = 3.58600000e1_wp, &  !..constant in saturation pressure - liquid
    T_3  = 273.15_wp,       &  ! [K]     melting temperature of ice/snow
    e_3  = 6.10780000e2_wp     !..saturation pressure at T = T_3


  ! derived constants
  REAL(wp), PARAMETER ::       &
    g_Rtlr = grav/(Rd*tlr),    &
    rhoii  = 1._wp/rhoi,       & 
    T_3i   = 1._wp/T_3,         &
    rholi  = 1._wp/rhol,       &
    m2r    = rholi*0.75_wp/pi, &
    r2m    = 1._wp/m2r

END MODULE mo_atmo_types
