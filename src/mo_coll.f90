!
! Copyright (C) 2004-2024, DWD
! See ./AUTHORS.txt for a list of authors
! See ./LICENSES/ for license information
! SPDX-License-Identifier: BSD-3-Clause
!

MODULE mo_coll

  USE mo_constants,       ONLY: wp, i8, pi, z1_3, z4pi_3
  USE mo_atmo,            ONLY: get_atmo, t_atmo
  USE mo_sp_types,        ONLY: t_sp, t_list, t_node, &
                                t_node_ptr, t_tnode, p_addSp, sp_stat
  USE mo_sp_nml,          ONLY: msg_level, agg_hist, collbreakup_type, diffusion_type, &
                                coll_kern_type, sp_habit, sp_stick_effi, sp_kern_sig
  USE mo_coll_utils,      ONLY: coll_kernel
#ifndef WDROPS
  USE mo_coll_utils,      ONLY: habit_aggregates
  USE mo_velocity,        ONLY: boehmVel, t_CE_boehm
  USE mo_colleffi,        ONLY: stick_effi_tempOnly,                 &
                                Ecbohm, stick_effi_Phillips
  USE mo_mass2diam,       ONLY: m2d_particle
  USE mo_atmo_types,      ONLY: c_pw, c_pi
#endif
  USE mo_breakup,         ONLY: breakup_coll
  USE mo_sp_array,        ONLY: t_cell, delParticle


  IMPLICIT NONE
  PRIVATE

  PRIVATE :: sp_coll_mc
  PUBLIC  :: sp_coll, sp_coll_a
#ifndef WDROPS
  PUBLIC  :: sp_coll_a_n2
#endif

CONTAINS

!
! aggregation or coalescence is based on
!   the SDM Monte Carlo scheme by Shima et al (2009):
!
SUBROUTINE sp_coll_a( addSp, cell, sps, factor )
  procedure(p_addSp)                :: addSp
  TYPE(t_cell),       INTENT(inout) :: cell
  TYPE(t_sp), TARGET, INTENT(inout) :: sps(:)
  REAL(wp),           INTENT(in)    :: factor     ! dtc*grid%dboxi

  REAL(wp),         ALLOCATABLE  :: rrand(:)
  INTEGER,          ALLOCATABLE  :: nperm(:)
  INTEGER :: n_active, nsp, isp, n_count, irnd

  TYPE(t_sp), POINTER :: sp1, sp2
  INTEGER     :: id1, id2, gama, gama_t !, n_minusAdd
  REAL(wp)    :: spmkernel, ce_in, prob, coll_factor, thermalIntertia1, thermalIntertia2, V_upd, phi_upd
  LOGICAL     :: checkForBreakup, breakup_happened

  !TYPE(t_node), POINTER :: bhead, btail  ! list head and tail of new hydrometeors after break-up
  !NULLIFY(bhead,btail)
 
  nsp = cell%noParts
  n_active = cell%noParts - cell%noDelParts

  IF(n_active < 2) RETURN                         ! no sp-coll pairs possible
  ALLOCATE( rrand(n_active), nperm(n_active) )
  CALL random_number( rrand )

  n_count = 0
  DO isp = 1, nsp                                 ! generate random permutation of active particles
    IF( sps(isp)%cellId < 1 ) CYCLE
    n_count = n_count + 1
    irnd = FLOOR(rrand(n_count)*n_count) + 1
    nperm(n_count) = nperm(irnd)
    nperm(irnd)    = isp
  END DO

  IF( n_count /= n_active ) THEN
    WRITE(*,*) "  n_count /= n_active ",  n_count, n_active, nsp
    n_active = n_count
    IF(n_active < 2) RETURN
  END IF

  nsp = n_active                                  ! formerly sp_coll_mc
  coll_factor = factor * nsp*(nsp-1)/REAL(2*(nsp/2),wp)
  CALL random_number( rrand(1:nsp/2) )            ! update SPs according to Shima, p. 1313
  DO isp = 1, nsp/2                               ! run over pairs of permuted SP list
    id1 = nperm(2*isp-1)
    id2 = nperm(2*isp)
    IF( sps(id1)%xi > sps(id2)%xi ) THEN
      sp1 => sps(id1)
      sp2 => sps(id2)
    ELSE
      sp1 => sps(id2)
      sp2 => sps(id1)
    END IF
    spmkernel = coll_kernel( sp1, sp2, ce_in )    ! collision kernel including sticking efficiency
    prob =  sp1%xi * spmkernel * coll_factor      ! compute probability for the pair (i_isp,i_(isp+1))
    IF( rrand(isp) < prob - FLOOR(prob)) THEN     ! compute gamma
      gama = FLOOR(prob)+1
    ELSE
      gama = FLOOR(prob)
    END IF
    IF ( gama == 0 ) CYCLE  ! no change in the pair if gamma=0
    gama_t = INT( MIN( INT(gama, i8), sp1%xi / sp2%xi ) ) ! gamma_tilde
    ! IF( REAL(sp1%xi,wp)/(gama*sp2%xi) < 1.0_wp ) THEN
    !   WRITE(*,*) "WARNING prob high", prob, gama, gama_t, sp1%xi, sp2%xi
    !   WRITE(*,*) "sp1 ", sp1
    !   WRITE(*,*) "sp2 ", sp2
    ! ENDIF
    
    checkForBreakup = (collbreakup_type > 0)
#ifndef WDROPS
    IF( ((sp1%m_i+sp1%m_r+sp1%m_f)>0.0_wp) .OR. ((sp2%m_i+sp2%m_r+sp2%m_f)>0.0_wp) ) checkForBreakup = .FALSE.
#endif
    breakup_happened = .FALSE.
    ! do breakup here
    IF( checkForBreakup ) breakup_happened = breakup_coll(addSp, sp1, sp2, gama_t)
    IF( .NOT. breakup_happened ) THEN
      ! now update
      
#ifndef WDROPS
      IF( diffusion_type > 1 ) THEN ! update temperature
        thermalIntertia1 = c_pw*sp1%m_w + c_pi*(sp1%m_i+sp1%m_f+sp1%m_r)
        thermalIntertia2 = c_pw*sp2%m_w + c_pi*(sp2%m_i+sp2%m_f+sp2%m_r)
        sp2%T   = (gama_t*thermalIntertia1*sp1%T+thermalIntertia2*sp2%T)/(thermalIntertia1+thermalIntertia2)
      ENDIF
      ! set created by flags (status has to be updated by m2d anyway after coll)
      sp2%statusb = IAND(sp2%statusb, sp1%statusb)
      IF (sp_habit > 1) THEN
        CALL habit_aggregates(sp1, sp2, gama_t, V_upd, phi_upd)
        sp2%V_i = V_upd
        sp2%phi = phi_upd
      ELSE
        sp2%V_i = gama_t*sp1%V_i + sp2%V_i
        sp2%phi = 1._wp                           ! aggregates are assumed to have aspect ratio of 1 
      ENDIF
      sp2%m_i = gama_t*sp1%m_i + sp2%m_i
      sp2%m_f = gama_t*sp1%m_f + sp2%m_f
      sp2%m_r = gama_t*sp1%m_r + sp2%m_r
      sp2%v_r = gama_t*sp1%v_r + sp2%v_r
#endif
      sp2%m_w = gama_t*sp1%m_w + sp2%m_w
      sp2%mm  = gama_t*sp1%mm  + sp2%mm

      IF (sp1%xi > gama_t*sp2%xi) THEN
        sp1%xi  = sp1%xi - gama_t*sp2%xi
      ELSE
        sp1%xi  = sp2%xi/2
        sp2%xi  = sp2%xi - sp1%xi
        
        sp1%m_w = sp2%m_w
        sp1%mm  = sp2%mm
#ifndef WDROPS
        IF( diffusion_type > 1 ) sp1%T = sp2%T
        sp1%statusb = sp2%statusb
        sp1%m_i = sp2%m_i
        sp1%m_f = sp2%m_f
        sp1%m_r = sp2%m_r
        sp1%v_r = sp2%v_r
        sp1%V_i = sp2%V_i
        IF ( ( sp_habit == 1 .AND. sp1%mm > 1.01 ) .OR. (sp_habit < 1) ) THEN
          sp1%phi = 1._wp
        ELSE
          sp1%phi = sp2%phi
        ENDIF
#endif        
      ENDIF
    ENDIF

    ! if new multiplicity is 0 => remove 
    IF ( sp1%xi == 0 )  CALL delParticle(cell, sp1 )
  END DO
  ! end formerly sp_coll_mc    

  DEALLOCATE( rrand, nperm )
  ! concatenate break-up list and array
  !DO WHILE( ASSOCIATED(bhead) ) !The cray compiler acts weird on the do while loop: warning of derefferencing disassociated pointer
  !DO
  !  IF( ASSOCIATED(bhead) ) THEN
  !    CALL addSp( bhead%sp ) 
  !    btail => bhead%next
  !    DEALLOCATE(bhead)
  !    bhead => btail
  !  ELSE
  !    EXIT
  !  END IF
  !END DO

END SUBROUTINE sp_coll_a

#ifndef WDROPS
!
! aggregation or coalescence is based on
!   the n^2 Monte Carlo scheme by Unterstrasser:
! TODO add break-up
!
SUBROUTINE sp_coll_a_n2( addSp, cell, sps, factor )
  procedure(p_addSp)                :: addSp
  TYPE(t_cell),       INTENT(inout) :: cell
  TYPE(t_sp), TARGET, INTENT(inout) :: sps(:)
  REAL(wp),           INTENT(in)    :: factor     ! dtc*grid%dboxi

  INTEGER :: nsp, i, j, i1, i2
  REAL(wp) :: kern_ce, prob, thermalIntertia1, thermalIntertia2, rand, r1, r2, stickeffi, v1, v2, vel
  REAL(wp) :: V_upd, phi_upd, stickeffi_T
  INTEGER(i8) :: gama, n_active, counter
  INTEGER     :: gama_t
  TYPE(t_atmo) :: atmo
  TYPE(t_CE_boehm), ALLOCATABLE :: partInfo(:)
  REAL(wp),         ALLOCATABLE :: rrand(:)
  LOGICAL     :: checkForBreakup, breakup_happened


  n_active = cell%noParts - cell%noDelParts
  IF(n_active < 2) RETURN                         ! no sp-coll pairs possible

  ALLOCATE( rrand( n_active*(n_active-1)/2 ) )
  CALL random_number( rrand )

  nsp = cell%noParts
  CALL get_atmo( sps(1)%z, atmo )
  ALLOCATE( partInfo(nsp) )
  stickeffi_T = stick_effi_tempOnly(atmo%T)
  DO i=1, nsp
    IF( sps(i)%cellId < 1 ) CYCLE
    CALL updatePart( atmo, sps(i), partInfo(i) )
  ENDDO

  counter = 0

  DO i=1, nsp-1
    IF( sps(i)%cellId < 1 ) CYCLE
    DO j=i+1, nsp
      IF( sps(j)%cellId < 1 ) CYCLE
      counter = counter + 1
      IF( sps(i)%xi > sps(j)%xi ) THEN
        i1 = i
        i2 = j
      ELSE
        i1 = j
        i2 = i
      END IF

      IF (sp_kern_sig .gt. 0._wp) THEN
        CALL random_number( rand )
        v1  = sps(i1)%v + ( (rand - 0.5_wp) * sp_kern_sig)
        CALL random_number( rand )
        v2  = sps(i2)%v + ( (rand - 0.5_wp) * sp_kern_sig)
        vel = ABS(v1 - v2) 
      ELSE
        vel = ABS(sps(i1)%v - sps(i2)%v)
      END IF 

      ! first test with coll_effi = 1 as computing it is expensive
      IF (coll_kern_type > 0) THEN
        kern_ce = 0.25 * pi * (sps(i1)%d + sps(i2)%d)**2 * vel
      ELSE
      ! for the collision we need the circumscribing ellipsis
        r1 = 0.5_wp * sps(i1)%d * MIN( 1.0_wp, 1._wp / SQRT( sps(i1)%phi ) )

        r2 = 0.5_wp * sps(i2)%d * MIN( 1.0_wp, 1._wp / SQRT( sps(i2)%phi ) ) ! area equivalent radius
        kern_ce = pi * (r1 + r2)**2 * vel
      END IF
      IF ( BTEST(sps(i1)%statusb,sp_stat%liquidSurf) .OR.      &
           BTEST(sps(i2)%statusb,sp_stat%liquidSurf) ) THEN
           stickeffi = 1.0_wp
      ELSE
        IF ( sp_stick_effi == 8 ) THEN
          stickeffi = stick_effi_Phillips( sps(i1), sps(i2), atmo%rho, atmo%etai, atmo%T, atmo%ssat )
        ELSE
          stickeffi = stickeffi_T
        ENDIF
      ENDIF
      prob =  sps(i1)%xi * factor * kern_ce * stickeffi
      gama = FLOOR(prob) + FLOOR(rrand(counter)+(prob-FLOOR(prob)))
      IF( gama < 1 ) CYCLE
      ! here we use only the collision efficiency without sticking efficiency
      prob = prob * Ecbohm( atmo, partInfo(i1), partInfo(i2) )
      gama = FLOOR(prob) + FLOOR(rrand(counter)+(prob-FLOOR(prob)))
      IF( gama < 1 ) CYCLE
      gama_t = MIN( gama, sps(i1)%xi/sps(i2)%xi )

      IF( REAL(sps(i1)%xi,wp)/(gama*sps(i2)%xi) < 1.0_wp ) THEN
       WRITE(*,*) "WARNING prob high", prob, gama, gama_t, sps(i1)%xi, sps(i2)%xi
       WRITE(*,*) "sp1 ", sps(i1)
       WRITE(*,*) "sp2 ", sps(i2)
      ENDIF
      IF( gama_t < 1 ) THEN
        WRITE(*,*) "Error gama_t < 1", gama_t, prob, gama, sps(i1)%xi, sps(i2)%xi
        WRITE(*,*) "sp1 ", sps(i1)
        WRITE(*,*) "sp2 ", sps(i2)
        CYCLE
      ENDIF
      
      checkForBreakup = (collbreakup_type > 0)
      IF( ((sps(i1)%m_i+sps(i1)%m_r+sps(i1)%m_f)>0.0_wp) .OR. &
        & ((sps(i2)%m_i+sps(i2)%m_r+sps(i2)%m_f)>0.0_wp) ) checkForBreakup = .FALSE.
      breakup_happened = .FALSE.
      ! do breakup here
      IF( checkForBreakup ) breakup_happened = breakup_coll(addSp,sps(i1), sps(i2), INT(gama_t))
      IF( .NOT. breakup_happened ) THEN
        ! now update
        IF( diffusion_type > 1 ) THEN ! update temperature
          thermalIntertia1 = c_pw*sps(i1)%m_w + c_pi*(sps(i1)%m_i+sps(i1)%m_f+sps(i1)%m_r)
          thermalIntertia2 = c_pw*sps(i2)%m_w + c_pi*(sps(i2)%m_i+sps(i2)%m_f+sps(i2)%m_r)
          sps(i1)%T   = (gama_t*thermalIntertia1*sps(i1)%T+thermalIntertia2*sps(i2)%T)/(thermalIntertia1+thermalIntertia2)
        ENDIF

        IF (sp_habit > 1) THEN
          CALL habit_aggregates(sps(i1), sps(i2), gama_t, V_upd, phi_upd)
          sps(i2)%V_i = V_upd
          sps(i2)%phi = phi_upd
        ELSE
          sps(i2)%V_i = gama_t*sps(i1)%V_i + sps(i2)%V_i
          sps(i2)%phi = 1._wp                           ! aggregates are assumed to have aspect ratio of 1 
        ENDIF

        sps(i2)%m_w = gama_t*sps(i1)%m_w + sps(i2)%m_w
        sps(i2)%mm  = gama_t*sps(i1)%mm  + sps(i2)%mm
        ! set created by flags (status has to be updated by m2d anyway after coll)
        sps(i2)%statusb = IAND(sps(i2)%statusb, sps(i1)%statusb)
        sps(i2)%m_i = gama_t*sps(i1)%m_i + sps(i2)%m_i
        sps(i2)%m_f = gama_t*sps(i1)%m_f + sps(i2)%m_f
        sps(i2)%m_r = gama_t*sps(i1)%m_r + sps(i2)%m_r
        sps(i2)%v_r = gama_t*sps(i1)%v_r + sps(i2)%v_r
        IF (sps(i1)%xi > gama_t*sps(i2)%xi) THEN
          sps(i1)%xi  = sps(i1)%xi  - gama_t*sps(i2)%xi
        ELSE
          sps(i1)%xi  = sps(i2)%xi/2
          sps(i2)%xi  = sps(i2)%xi - sps(i1)%xi
          
          sps(i1)%m_w = sps(i2)%m_w
          sps(i1)%mm  = sps(i2)%mm
          IF( diffusion_type > 1 ) sps(i1)%T = sps(i2)%T
          sps(i1)%statusb = sps(i2)%statusb
          sps(i1)%m_i = sps(i2)%m_i
          sps(i1)%m_f = sps(i2)%m_f
          sps(i1)%m_r = sps(i2)%m_r
          sps(i1)%v_r = sps(i2)%v_r
          sps(i1)%V_i = sps(i2)%V_i
          IF ( ( sp_habit == 1 .AND. sps(i1)%mm > 1.01 ) .OR. (sp_habit < 1) ) THEN
            sps(i1)%phi = 1._wp
          ELSE
            sps(i1)%phi = sps(i2)%phi
          ENDIF
          CALL updatePart( atmo, sps(i1), partInfo(i1) )
        ENDIF
          
      ELSE
        CALL   updatePart( atmo, sps(i1), partInfo(i1) )
      ENDIF
      CALL     updatePart( atmo, sps(i2), partInfo(i2) )

      ! if new multiplicity is 0 => remove 
      IF ( sps(i1)%xi < 1 )  CALL delParticle(cell, sps(i1) )

    ENDDO
  ENDDO

  DEALLOCATE( partInfo )
  DEALLOCATE( rrand )

CONTAINS

SUBROUTINE updatePart( atmo, sp, partInfo ) 
  TYPE(t_atmo),           INTENT(IN)    :: atmo
  TYPE(t_sp),             INTENT(INOUT) :: sp
  TYPE(t_CE_boehm),       INTENT(INOUT) :: partInfo

  CALL m2d_particle( sp )
  sp%v = boehmVel( atmo, sp, partInfo )
END SUBROUTINE updatePart

END SUBROUTINE sp_coll_a_n2

#endif

!
! aggregation or coalescence is based on
!   the SDM Monte Carlo scheme by Shima et al (2009):
!
SUBROUTINE sp_coll( addSp, sp_list, dboxi, dt )
  procedure(p_addSp)             :: addSp
  TYPE(t_list),    INTENT(inout) :: sp_list
  REAL(wp),        INTENT(in)    :: dboxi
  REAL(wp),        INTENT(in)    :: dt
  
  INTEGER :: llen
  REAL(wp),         ALLOCATABLE  :: rrand(:)
  INTEGER,          ALLOCATABLE  :: nperm(:)
  TYPE(t_node_ptr), ALLOCATABLE  :: sp_ptr(:)
  TYPE(t_node),          POINTER :: nhead!, &      ! new list head
                                    !bhead, btail  ! list head and tail of new hydrometeors after break-up
  INTEGER(i8) :: ncol
  INTEGER     :: isp, irnd, i, nlen, ndis
  REAL(wp)    :: ce 

  llen = sp_list%length
  IF ( llen < 2 ) RETURN   ! skip cell if there are no SPs in it
  ALLOCATE( nperm(llen), rrand(llen), sp_ptr(llen) )
  ! generate n-permutation
  ! and save the SPs' pointers in an array
  CALL random_number( rrand )
  nperm(1) = 1
  sp_ptr(1)%ptr => sp_list%head
  DO i = 2, llen
    irnd = FLOOR(rrand(i)*i) + 1
    nperm(i) = i
    nperm(i) = nperm(irnd) + i
    nperm(i) = nperm(i) - i
    nperm(irnd) = i
    sp_ptr(i)%ptr => sp_ptr(i-1)%ptr%next
  END DO

  ! fix the next-pointer in sp_list
  sp_list%head => sp_ptr(nperm(1))%ptr
  DO isp = 1, llen-1
    sp_ptr(nperm(isp))%ptr%next => sp_ptr(nperm(isp+1))%ptr
  END DO
  sp_ptr(nperm(llen))%ptr%next => null()

  nhead => sp_list%head
  !NULLIFY(bhead, btail)
  
  CALL sp_coll_mc( addSp, llen, sp_ptr, nperm, dboxi, dt, &
                   nhead, nlen, ncol, ndis, ce )

  ! concatenate break-up list and sp_list
  !IF (ASSOCIATED(bhead)) THEN
  !  btail%next => nhead
  !  sp_list%head => bhead
  !ELSE
  !  sp_list%head => nhead
  !END IF
  sp_list%length = nlen

  ! delete array pointers
  DO isp = 1, llen
    NULLIFY( sp_ptr(isp)%ptr )
  END DO
  DEALLOCATE( nperm, rrand, sp_ptr )
END SUBROUTINE sp_coll

!
! update SPs according to Shima, p. 1313
!
SUBROUTINE sp_coll_mc( addSp, nsp, sp_ptr, nperm, dboxi, dt, nhead, nlen, ncol, ndis, ce )
  procedure(p_addSp)              :: addSp
  INTEGER,             INTENT(in) :: nsp
  TYPE(t_node_ptr), INTENT(inout) :: sp_ptr(nsp) !< permuted SP list
  INTEGER,             INTENT(in) :: nperm(nsp)
  REAL(wp),            INTENT(in) :: dboxi
  REAL(wp),            INTENT(in) :: dt
  TYPE(t_node), POINTER, INTENT(inout) :: nhead !> new list head
  !TYPE(t_node), POINTER, INTENT(inout) :: bhead, btail !< list head and tail of new hydrometeors after break-up
  INTEGER,            INTENT(out) :: nlen          !> new array length
  INTEGER(i8),        INTENT(out) :: ncol          !> collisions per cell
  INTEGER,            INTENT(out) :: ndis          !> particle vanishing per cell
  REAL(wp),           INTENT(out) :: ce            !> coll. eff. per cell

  INTEGER               :: isp, id1, id2, ixi1, ixi2, pisp
  TYPE(t_node), POINTER :: del_node
  TYPE(t_sp),  POINTER :: sp1, sp2, sp1tmp, sp2tmp
  INTEGER               :: gama_t, gama
  REAL(wp)              :: prob, spmkernel, ce_in, V_upd, phi_upd
  REAL(wp), ALLOCATABLE :: rrand(:)
#ifndef NDEBUG      
  INTEGER               :: i
  INTEGER(i8)           :: xi1, xi2
#endif
#ifndef WDROPS
  TYPE(t_tnode), POINTER :: tnode1, tnode2
  TYPE(t_atmo)           :: atmo1, atmo2
  INTEGER                :: igama
#endif
  LOGICAL                :: checkForBreakup, breakup_happened

  ncol = 0_i8
  ndis = 0

  ALLOCATE( rrand(nsp/2), sp1tmp, sp2tmp )
  CALL random_number( rrand )

  ! element id in sp_ptr previous to the pair we consider
  pisp = 0

  ! initial coll. eff.
  ce = 0.

  ! run over pairs of permuted SP list
  ! NOTE: for permuted list (i1,i2,...)
  !       list of pairs is ((i1,i2),(i3,i4),...)
  !
  DO isp = 1, nsp/2
    id1 = nperm(2*isp-1)
    id2 = nperm(2*isp)

    IF( sp_ptr(id1)%ptr%sp%xi > sp_ptr(id2)%ptr%sp%xi ) THEN
      ixi1 = id1
      ixi2 = id2
    ELSE
      ixi1 = id2
      ixi2 = id1
    END IF
      
    sp1 => sp_ptr(ixi1)%ptr%sp
    sp2 => sp_ptr(ixi2)%ptr%sp

    ! collision kernel
    spmkernel = coll_kernel( sp1, sp2, ce_in )
    ce = MAX( ce, ce_in )

    ! compute probability for the pair (i_isp,i_(isp+1)) 
    prob =  sp1%xi * spmkernel * &
      nsp*(nsp-1)/REAL(2*(nsp/2),wp) * dt * dboxi

    ! compute gamma
    IF( rrand(isp) < prob - FLOOR(prob)) THEN
      gama = FLOOR(prob)+1
    ELSE
      gama = FLOOR(prob)
    END IF

    ! no change in the pair if gamma=0
    IF ( gama == 0 ) THEN
      pisp = id2
      CYCLE
    END IF

    ! gamma_tilde
#ifndef NDEBUG      
    xi1 = sp1%xi
    xi2 = sp2%xi
#endif
    gama_t = INT( MIN( INT(gama, i8), sp1%xi / sp2%xi ) )

    ! IF( REAL(sp1%xi,wp)/(gama*sp2%xi) < 1.0_wp ) THEN
    !   WRITE(*,*) "WARNING prob high", prob, gama, gama_t, sp1%xi, sp2%xi
    !   WRITE(*,*) "sp1 ", sp1
    !   WRITE(*,*) "sp2 ", sp2
    ! ENDIF

    IF (sp1%xi > gama_t*sp2%xi) THEN
#ifndef WDROPS
      ! record aggregation history before updating SPs
      IF ( agg_hist >= 0 ) THEN
        CALL get_atmo( sp2%z, atmo2 )
        sp1tmp = sp1  ! copy
        sp2tmp = sp2  ! copy
        DO igama = 1, gama_t
          ALLOCATE( tnode1 )
          tnode1%sp1 = sp1tmp  ! copy
          tnode1%sp2 = sp2tmp  ! copy
          tnode1%coll_temp = atmo2%T
          tnode1%coll_ssat = atmo2%ssat
          tnode1%l => sp_ptr(ixi1)%ptr%hist
          tnode1%r => sp_ptr(ixi2)%ptr%hist 
          sp_ptr(ixi2)%ptr%hist => tnode1
          sp1tmp%xi = sp1tmp%xi - sp2%xi
          IF (sp_habit > 1) THEN
            CALL habit_aggregates(sp1, sp2, 1, V_upd, phi_upd)
            sp2tmp%V_i = V_upd
            sp2tmp%phi = phi_upd
          ELSE
            sp2tmp%V_i = sp2tmp%V_i + sp1%V_i 
            sp2tmp%phi = 1._wp                           ! aggregates are assumed to have aspect ratio of 1 
          ENDIF
          sp2tmp%m_i = sp2tmp%m_i + sp1%m_i
          sp2tmp%m_f = sp2tmp%m_f + sp1%m_f
          sp2tmp%m_r = sp2tmp%m_r + sp1%m_r
          sp2tmp%v_r = sp2tmp%v_r + sp1%v_r
          sp2tmp%m_w = sp2tmp%m_w + sp1%m_w
          NULLIFY( tnode1 )
        END DO
      END IF
#endif

      checkForBreakup = (collbreakup_type > 0)
#ifndef WDROPS
      IF( ((sp1%m_i+sp1%m_f+sp1%m_r)>0.0_wp) .OR. ((sp2%m_i+sp2%m_f+sp2%m_r)>0.0_wp) ) checkForBreakup = .FALSE.
#endif
      breakup_happened = .FALSE.
      ! do breakup here
      ! collect new particles in a list with head 'bhead' and tail 'btail'
      IF( checkForBreakup ) breakup_happened = breakup_coll(addSp,sp1, sp2, gama_t)

      IF( .NOT. breakup_happened ) THEN
        ! now update
        sp1%xi  = sp1%xi  - gama_t*sp2%xi
        sp2%m_w = gama_t*sp1%m_w + sp2%m_w
        sp2%mm  = gama_t*sp1%mm  + sp2%mm
#ifndef WDROPS
        IF (sp_habit > 1) THEN
          CALL habit_aggregates(sp1, sp2, gama_t, V_upd, phi_upd)
          sp2%V_i = V_upd
          sp2%phi = phi_upd
        ELSE
          sp2%V_i = gama_t*sp1%V_i + sp2%V_i
          sp2%phi = 1._wp                           ! aggregates are assumed to have aspect ratio of 1 
        ENDIF
        ! set created by flags (status has to be updated by m2d anyway after coll)
        sp2%statusb = IAND(sp2%statusb, sp1%statusb)
        sp2%m_i = gama_t*sp1%m_i + sp2%m_i
        sp2%m_f = gama_t*sp1%m_f + sp2%m_f
        sp2%m_r = gama_t*sp1%m_r + sp2%m_r
        sp2%v_r = gama_t*sp1%v_r + sp2%v_r
#endif
      END IF
      ncol = ncol + gama_t

    ELSE IF (sp1%xi == gama_t*sp2%xi) THEN
#ifndef WDROPS
      ! record aggregation history before updating SPs
      IF ( agg_hist >= 0 ) THEN
        ALLOCATE( tnode1, tnode2 )
        CALL get_atmo( sp1%z, atmo1 )
        CALL get_atmo( sp2%z, atmo2 )
        tnode1%sp1 = sp1 ! copy
        tnode1%sp2 = sp2 ! copy
        tnode2%sp1 = sp1 ! copy
        tnode2%sp2 = sp2 ! copy
        tnode1%coll_temp = atmo1%T
        tnode1%coll_ssat = atmo1%ssat
        tnode2%coll_temp = atmo2%T
        tnode2%coll_ssat = atmo2%ssat
        tnode1%l => sp_ptr(ixi1)%ptr%hist
        tnode1%r => sp_ptr(ixi2)%ptr%hist 
        tnode2%l => sp_ptr(ixi1)%ptr%hist
        tnode2%r => sp_ptr(ixi2)%ptr%hist 
        sp_ptr(ixi1)%ptr%hist => tnode1
        sp_ptr(ixi2)%ptr%hist => tnode2
        NULLIFY( tnode1, tnode2 )
      END IF
#endif
      ! now update
      sp1%xi = sp2%xi/2
      sp2%xi = sp2%xi - sp1%xi
      sp1%m_w = gama_t*sp1%m_w + sp2%m_w
      sp2%m_w = sp1%m_w
      sp1%mm = gama_t*sp1%mm + sp2%mm
      sp2%mm = sp1%mm
#ifndef WDROPS
      IF (sp_habit > 1) THEN
        CALL habit_aggregates(sp1, sp2, gama_t, V_upd, phi_upd)
        sp1%V_i = V_upd
        sp1%phi = phi_upd
      ELSE
        sp1%V_i = gama_t*sp1%V_i + sp2%V_i
        sp1%phi = 1._wp                           ! aggregates are assumed to have aspect ratio of 1 
      ENDIF
      sp2%phi = sp1%phi
      ! set created by flags (status has to be updated by m2d anyway after coll)
      sp2%statusb = IAND(sp2%statusb, sp1%statusb)
      sp1%statusb = sp2%statusb 
      sp1%m_i = gama_t*sp1%m_i+sp2%m_i
      sp2%m_i = sp1%m_i
      sp1%m_f = gama_t*sp1%m_f+sp2%m_f
      sp2%m_f = sp1%m_f
      sp1%m_r = gama_t*sp1%m_r+sp2%m_r
      sp2%m_r = sp1%m_r
      sp1%v_r = gama_t*sp1%v_r+sp2%v_r
      sp2%v_r = sp1%v_r
#endif
      ! TODO do breakup here ... currently we neglect this, because it is a rare case
      ! in this case we can maybe use one of the two existing SPs for
      ! the fragments
      ncol = ncol + gama_t
    END IF

#ifndef NDEBUG      
    IF( msg_level >= 15 ) THEN
      WRITE(*,'(a,i7,a,i12,a,i12)') "Collision ", &
        xi1, " --> ", sp_ptr(ixi1)%ptr%sp%xi
      WRITE(*,'(a,i7,a,i12,a,i12)') "          ", &
        xi2, " --> ", sp_ptr(ixi2)%ptr%sp%xi
    END IF
#endif

    ! if new multiplicity is 0, remove from the list
    IF ( sp1%xi == 0 ) THEN

      NULLIFY(del_node)

      IF ( ixi1 == id1 ) THEN

        ! remove the first in the pair
        IF ( pisp /= 0 ) THEN
          ! previous element to the pair exists
          sp_ptr(pisp)%ptr%next => sp_ptr(id2)%ptr
        ELSE
          ! no previous element to this pair
          nhead => sp_ptr(id2)%ptr
        END IF
        del_node => sp_ptr(id1)%ptr
        pisp = id2

      ELSE

        ! remove the second in the pair
        sp_ptr(id1)%ptr%next => sp_ptr(id2)%ptr%next
        del_node => sp_ptr(id2)%ptr
        pisp = id1

      END IF

#ifndef NDEBUG        
      IF( msg_level >= 10 ) &
        PRINT *, "Remove a SP from cell ", i
#endif

      DEALLOCATE(del_node)
      ndis = ndis + 1

    ELSE ! delete node

      ! the pair does not need to be removed
      pisp = id2

    END IF ! delete node
  END DO  

  nlen = nsp - ndis

  DEALLOCATE( rrand, sp1tmp, sp2tmp )
END SUBROUTINE sp_coll_mc


END MODULE mo_coll
