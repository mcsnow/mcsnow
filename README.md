
![McSnow Logo](doc/manual/pic/mcsnow_logo.png)

**McSnow** is a Monte-Carlo super-particle cloud microphysics code for
simulating ice particles size distributions (aka snow) which evolve due to
processes like sedimentation, deposition/sublimation, aggregation, and
riming. The code can be also compiled for simulating the size distribution of
liquid drops which then undergo sedimentation, condensation/evaporation, and
coalescence.

The hydrodynamics of all hydrometeors in McSnow follows the theory of Johannes
Böhm which he developed at ETH Zürich during the 80s and 90s of the previous
century. This allows a continuous description of particle fall speeds and
collision efficiencies without having to revert to artificial and arbitrary
particle categories. The continuous description of hydrometeor properties is
a core concept of McSnow. For example, McSnow has only a single collision
kernel that applies to all binary hydrometeor collisions.

**McSnow 2.0** includes a habit prediction based on the spheroidal model of
  Chen and Lamb and additional parameterizations to take into account the
  habit dependency of microphysical processes.

The code is written in Fortran 90/95 but the open source version requires the
built-in Gamma and Bessel functions provided by Fortran 2008.  The code is
parallelized (multi-threaded) with OpenMP.

**McSnow** is part of the [ICON](https://icon-model.org/) modeling framework
  and open source following the BSD-3-Clause license. An ICON version that
  includes McSnow does exist but due to the underlying complexity this code is
  neither part of the ICON-Master nor included in the open source release of
  ICON.

Copyright (C) 2004-2024, DWD. See AUTHORS.txt for a list of authors and the
LICENSES directory for detailed license information.

SPDX-License-Identifier: BSD-3-Clause

References:

Brdar, S., & Seifert, A. (2018). McSnow: A Monte‐Carlo particle model for
riming and aggregation of ice particles in a multidimensional microphysical
phase space. Journal of Advances in Modeling Earth Systems, 10(1), 187-206.
https://doi.org/10.1002/2017MS001167

Seifert, A., Leinonen, J., Siewert, C., & Kneifel, S. (2019). The geometry of
rimed aggregate snowflakes: A modeling study. Journal of Advances in Modeling
Earth Systems, 11(3), 712-731. https://doi.org/10.1029/2018MS001519

Karrer, M., Seifert, A., Siewert, C., Ori, D., von Lerber, A., and Kneifel,
S. (2020). Ice particle properties inferred from aggregation
modelling. Journal of Advances in Modeling Earth Systems, 12(8),
https://doi.org/10.1029/2020MS002066

Bringi, V., Seifert, A., Wu, W., Thurai, M., Huang, G. J., and Siewert,
C. (2020). Hurricane Dorian outer rain band observations and 1D particle model
simulations: A case study. Atmosphere, 11(8), 879,
https://doi.org/10.3390/atmos11080879

Seifert, A. and Rasp, S. (2020). Potential and limitations of machine learning
for modeling warm‐rain cloud microphysical processes. Journal of Advances in
Modeling Earth Systems, 12, https://doi.org/10.1029/2020MS002301

Jan-Niklas Welss, Christoph Siewert and Axel Seifert (2024). Explicit
habit-prediction in the Lagrangian super-particle ice microphysics model
McSnow. Journal of Advances in Modeling Earth Systems, 16, e2023MS003805. https://doi.org/10.1029/2023MS003805 

Seifert, A. and Siewert, C. (2024). An ML-based P3-like multimodal two-moment ice microphysics in the ICON model. 
Journal of Advances in Modeling Earth Systems, 16, e2023MS004206. https://doi.org/10.1029/2023MS004206


