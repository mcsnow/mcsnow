set terminal png size 500,500
set output "perf.png"

set key left
set key spacing 1.5
set tics out
set xlabel "log_2(#droplets)"
set ylabel "Wall time [min]"

set style line 1 lt 3 lc rgb "red" lw 3
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 3
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3
set style line 8 lt 9 lc rgb "orange" lw 3
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3
set style line 11 lt 4 lc rgb "blue" lw 3
set style line 12 lt 1 lc rgb "blue" lw 3
set style line 13 lt 1 lc rgb "blue" lw 3

set logscale y
set yrange [0.018:18]

p "Rev300" u 1:($3/60) ls 1 w lp title "NoOMP", \
  "Rev300" u 1:($4/60) ls 5 w lp title "OMP-1", \
  "Rev300" u 1:($5/60) ls 8 w lp title "OMP-4", \
  "Rev300" u 1:($6/60) ls 11 w lp title "OMP-8"
