set logscale
set yrange [1e-14:1e-4]
set xrange [0.01:10]
set xlabel "diameter [mm]"
set ylabel "log_10( mass [mg] )"
set format y "%T"
set key left top
set key spacing 1.5

set style line 1 lt 3 lc rgb "red" lw 6
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 6
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3 
set style line 8 lt 9 lc rgb "orange" lw 6 
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3

set term postscript color size 7,7
set output "m2d_rhor400.eps"
set title "rho_r = 400 kg/m3"
p "m2d_rhor400_fr00.dat" u ($2*1000):1 w l title "Fr=0.0" ls 1, \
  "m2d_rhor400_fr00.dat" u ($6*1000):1 w l title "Dth" ls 2, \
  "m2d_rhor400_fr05.dat" u ($2*1000):1 w l title "Fr=0.5" ls 5, \
  "m2d_rhor400_fr05.dat" u ($7*1000):($1>1e-9?$1:1/0) w l title "Dgr,Fr05" ls 6, \
  "m2d_rhor400_fr05.dat" u ($8*1000):($1>1e-9?$1:1/0) w l title "Dcr,Fr05" ls 7, \
  "m2d_rhor400_fr08.dat" u ($2*1000):1 w l title "Fr=0.8" ls 8, \
  "m2d_rhor400_fr08.dat" u ($7*1000):($1>1e-9?$1:1/0) w l title "Dgr,Fr08" ls 9, \
  "m2d_rhor400_fr08.dat" u ($8*1000):($1>1e-9?$1:1/0) w l title "Dcr,Fr08" ls 10

set output "m2d_fr095.eps"
set title "Fr=0.95"
p "m2d_rhor200_fr095.dat" u ($2*1000):1 w l title "Fr=0.0" ls 1, \
  "m2d_rhor200_fr095.dat" u ($6*1000):1 w l title "Dth" ls 2, \
  "m2d_rhor200_fr095.dat" u ($7*1000):($1>1e-9?$1:1/0) w l title "Dgr,Fr00" ls 3, \
  "m2d_rhor200_fr095.dat" u ($8*1000):($1>1e-9?$1:1/0) w l title "Dcr,Fr00" ls 4, \
  "m2d_rhor400_fr095.dat" u ($2*1000):1 w l title "Fr=0.5" ls 5, \
  "m2d_rhor400_fr095.dat" u ($7*1000):($1>1e-9?$1:1/0) w l title "Dgr,Fr05" ls 6, \
  "m2d_rhor400_fr095.dat" u ($8*1000):($1>1e-9?$1:1/0) w l title "Dcr,Fr05" ls 7, \
  "m2d_rhor800_fr095.dat" u ($2*1000):1 w l title "Fr=0.8" ls 8, \
  "m2d_rhor800_fr095.dat" u ($7*1000):($1>1e-9?$1:1/0) w l title "Dgr,Fr08" ls 9, \
  "m2d_rhor800_fr095.dat" u ($8*1000):($1>1e-9?$1:1/0) w l title "Dcr,Fr08" ls 10
