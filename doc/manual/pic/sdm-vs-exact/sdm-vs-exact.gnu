#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#
set term postscript enhanced color size 5,5
set out "sdm-vs-exact.eps"
set xtics out
set linestyle 1 linewidth 3
set linestyle 2 linewidth 2
set xlabel "time [s]"
set ylabel "l2-error"
set logscale x
p "tab-sdm-vs-exact" u (0.5*($1+$3)):(0.5*($2+$4)) w l ls 1 title "SDM", \
  "tab-sdm-vs-exact-ndrops" u (0.5*($1+$3)):(0.5*($2+$4)) w l ls 2 title "exact"
