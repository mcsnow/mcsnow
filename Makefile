#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

all: 
	@echo
	@echo "------------------------------------------------------------------------------------"
	@echo "MCSNOW Code                        "
	@echo "Available Makefile options          "
	@echo
	@echo "  ICE PARTICLES"
	@echo "  -------------"
	@echo "  make release_omp       (optimized with OpenMP)"
	@echo "  make release           (optimized)"
	@echo "  make debug             (debugging)"
	@echo "  make prof              (profiling)"
	@echo
	@echo "  WATER DROPLETS"
	@echo "  --------------"
	@echo "  make wd_release_omp    (optimized with OpenMP)"
	@echo "  make wd_release        (optimized)"
	@echo "  make wd_debug          (debugging)"
	@echo "  make wd_prof           (profiling)"
	@echo
	@echo "  make clean             (remove all build files)"
	@echo "------------------------------------------------------------------------------------"
	@echo

release_omp: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DOPENMP:BOOL=Y -DCFLAGTYPE:STRING=release .. && make -s VERBOSE=1

release: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DOPENMP:BOOL=N -DCFLAGTYPE:STRING=release .. && make -s VERBOSE=1

debug: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DOPENMP:BOOL=N -DCFLAGTYPE:STRING=debug .. && make -s VERBOSE=1

prof: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DOPENMP:BOOL=N -DCFLAGTYPE:STRING=prof .. && make -s VERBOSE=1

wd_release_omp: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DWDROPS:BOOL=Y -DOPENMP:BOOL=Y -DCFLAGTYPE:STRING=release .. && make -s VERBOSE=1

wd_release: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DWDROPS:BOOL=Y -DOPENMP:BOOL=N -DCFLAGTYPE:STRING=release .. && make -s VERBOSE=1

wd_debug: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DWDROPS:BOOL=Y -DOPENMP:BOOL=N -DCFLAGTYPE:STRING=debug .. && make -s VERBOSE=1

wd_prof: $(shell find src -type f)
	mkdir -p build
	cd build && cmake -DWDROPS:BOOL=Y -DOPENMP:BOOL=N -DCFLAGTYPE:STRING=prof .. && make -s VERBOSE=1

clean:
	rm -rf build
