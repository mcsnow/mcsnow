#! /bin/bash

##
#  run script for
#  validating the SDM implementation on ice particles
#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set -e

THIS_DIR=$(pwd -P)
DATA_DIR="$1"

PLOT_FLUXES=false

# read values from the input file
testname_in="$(grep "test_case_name" "${DATA_DIR}/input.nml")"
IN_ARR=( ${testname_in} )
testname="${IN_ARR[2]}"
xi0_in=$(grep xi0 "${DATA_DIR}/input.nml")
IN_ARR=( ${xi0_in} )
xi0=${IN_ARR[2]}
nz_in=$(awk '!/run_/ && /nz/ && /=/' "${DATA_DIR}/input.nml")
IN_ARR=( ${nz_in} )
nz=${IN_ARR[2]}


###############################
# HEIGHT PROFILES
###############################

# super-droplets number density
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot --persist <<EOF
stats "${FN}" nooutput
set key spacing 1.1
set key outside
unset logscale x
unset logscale y
set xlabel "SIP number density [1]"
set ylabel "height [m]"
set title "${testname}, xi: ${xi0}, nz: ${nz}, SD number density"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 2:1 w l
EOF

# number density
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot --persist <<EOF
stats "${FN}" nooutput
set key spacing 1.1
set key outside
unset logscale x
unset logscale y
set xlabel "RIP number density [RIP/m³]"
set ylabel "height [m]"
set title "${testname}, xi: ${xi0}, nz: ${nz}, number density"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u (\$3*1000):1 w l
EOF

# mass density
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot --persist <<EOF
stats "${FN}" nooutput
set key spacing 1.1
set key outside
unset logscale x
unset logscale y
set xlabel "mass density [g/m³]"
set ylabel "height [m]"
set title "${testname}, xi: ${xi0}, nz: ${nz}, mass density"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u (\$4*1000):1 w l
EOF


if [ "${PLOT_FLUXES}" == "true" ]; then

# flux number density
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot --persist <<EOF
stats "${FN}" nooutput
set key spacing 1.1
set key outside
unset logscale x
unset logscale y
set xlabel "flux number density [1/m²s]"
set ylabel "height [m]"
set title "${testname}, xi: ${xi0}, nz: ${nz}, flux number density"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 5:1 w l
EOF

# flux mass density
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot --persist <<EOF
stats "${FN}" nooutput
set key spacing 1.1
set key outside
unset logscale x
unset logscale y
set xlabel "flux mass density [g/m²s]"
set ylabel "height [m]"
set title "${testname}, xi: ${xi0}, nz: ${nz}, flux mass density"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u (\$6*1000):1 w l
EOF

fi


###############################
# DISTRIBUTIONS
###############################

for FN in $(find ${DATA_DIR} -name distribution_\*); do
  HEI="${FN%.dat}"
  HEI="${HEI: -9}"

# dens. distr.
gnuplot --persist <<EOF
stats "${FN}" nooutput
set logscale x
set key spacing 1.1
set key outside
set xlabel "radius [m]"
set ylabel "dens. distr. [1/m³log(m)]"
set title "${testname}_${HEI}, xi: ${xi0}, nz: ${nz}, dens. distr."
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 1:2 w l
EOF

# mass dens. distr.
gnuplot --persist <<EOF
stats "${FN}" nooutput
set logscale x
set key spacing 1.1
set key outside
set xlabel "radius [m]"
set ylabel "mass. dens. distr. [g/m³log(m)]"
set title "${testname}_${HEI}, xi: ${xi0}, nz: ${nz}, mass dens. distr."
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 1:(\$3*1000) w l
EOF

if [ "${PLOT_FLUXES}" == "true" ]; then

# density flux distr.
gnuplot --persist <<EOF
stats "${FN}" nooutput
set logscale x
set key spacing 1.1
set key outside
set xlabel "radius [m]"
set ylabel "dens. flux [1/m²slog(m)]"
set title "${testname}_${HEI}, xi: ${xi0}, nz: ${nz}, density flux"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 1:4 w l
EOF

# mass density flux distr.
gnuplot --persist <<EOF
stats "${FN}" nooutput
set logscale x
set key spacing 1.1
set key outside
set xlabel "radius [m]"
set ylabel "mass. dens. flux [g/m²log(m)]"
set title "${testname}_${HEI}, xi: ${xi0}, nz: ${nz}, mass density flux"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 1:(\$5*1000) w l
EOF

# monomer density flux distr.
gnuplot --persist <<EOF
stats "${FN}" nooutput
set logscale x
set key spacing 1.1
set key outside
set xlabel "radius [m]"
set ylabel "mass. dens. distr. [1/m²log(m)]"
set title "${testname}_${HEI}, xi: ${xi0}, nz: ${nz}, monomer density flux"
set key autotitle columnhead
p for [i=0:int(STATS_blocks)] "<(sed 's/^.//' ${FN})" i i u 1:6 w l
EOF

fi

done

exit 0
