#!/bin/bash

# argument to this script is the name of the directory of the experiment
# i.e. a directory in ../experiments
# Then plots are produced into a subdirectory named Plots in the directory
# of that experiment. Just see below....

#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

exp=$1
edir="../experiments/"

if [ ! -d "$edir/$exp" ]; then
  echo "ERROR: ${exp} does not exist"
  exit
fi

mkdir -p $edir/${exp}/Plots

ncl "expname=\"${exp}\"" NCL/hprofiles.ncl


ncl "expname=\"${exp}\"" NCL/distributions_monomers.ncl
ncl "expname=\"${exp}\"" NCL/distributions_rime.ncl
ncl "expname=\"${exp}\"" NCL/distributions_rhorime.ncl
ncl "expname=\"${exp}\"" NCL/size_distributions.ncl
ncl "expname=\"${exp}\"" NCL/distributions_melt.ncl
ncl "expname=\"${exp}\"" NCL/distributions_temp.ncl
