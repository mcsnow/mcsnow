#! /bin/bash

##
#  run script for
#  validating the SDM approach of Shima for ice particles
#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

# we use bc to calculate which returns 2.5 for example
# which is unknown in german locale...
LC_ALL="en_US.UTF-8"
LANG="en_US.UTF-8"
LANGUAGE="en_US:en"

# this is image size from gnuplot
SIZE='600,480'

read_from_namelist () {
var_in="$1"
var_in="$(grep "${var_in}" "${DATA_DIR}/input.nml")"
IN_ARR=( ${var_in} )
echo "${IN_ARR[2]}"
}

check () {
if [ $1 -eq 0 ]; then
  echo "[ok]"
else
  echo "[!!]"
  if [ -z $2 ]; then exit 1; fi
fi
}

# allow no error when reading variables in
#set -e

THIS_DIR=$(pwd -P)
DATA_DIR="$1"

# read values from the input file
testname=$(read_from_namelist test_case_name)
xi0=$(read_from_namelist xi0)
nz=$(read_from_namelist nz)
nrip0=$(read_from_namelist nrip0)
dtc=$(read_from_namelist dtc)
sat=$(read_from_namelist sat)
domtop=$(read_from_namelist dom_top)
h1=$(read_from_namelist h1)
h2=$(read_from_namelist h2)
rim1=$(read_from_namelist dt_2dprof)
tend=$(read_from_namelist time_end)
dt_2dprof=$(read_from_namelist dt_1dprof)
ncl=$(read_from_namelist nclrate)

# TODO work in temporary directory
LDIR="xi${xi0}_nz${nz}_nsip${nrip0}_dtc${dtc}_ncl${ncl}_sat${sat}"
rm -rf "${LDIR}"
mkdir "${LDIR}"
cd "${LDIR}"
DATA_DIR="../${DATA_DIR}"


# allow errors from Gnuplot
set +e


###############################
# ATMOSPHERIC PROFILES
###############################

# temperature profile
FN="${DATA_DIR}/atmo.dat"
echo
echo -n "writing temp. profile              "
gnuplot &> /dev/null <<EOF
set xlabel "temp. [K]"
set ylabel "height [m]"
set term png size $SIZE
set output "atmo_temp.png"
set title "atmo. temp. profile"
unset key
p "$FN" u 3:(\$1/1000) title "T" w l lw 3
EOF
check $?

# LWC profile
FN="${DATA_DIR}/atmo.dat"
echo
echo -n "writing temp. profile              "
gnuplot &> /dev/null <<EOF
set xlabel "lwc [kg/m³]"
set ylabel "height [m]"
set term png size $SIZE
set output "atmo_lwc.png"
set title "atmo. LWC profile"
unset key
p "$FN" u 10:(\$1/1000) title "LWC" w l lw 3
EOF
check $?


# supersaturation profile
FN="${DATA_DIR}/atmo.dat"
echo
echo -n "writing supersaturation profile    "
gnuplot &> /dev/null <<EOF
set xlabel "supersaturation [%]"
set ylabel "height [m]"
set term png size $SIZE
set output "atmo_sat.png"
set title "supersaturation profile"
unset key
p "$FN" u (\$6*100):(\$1/1000) title "sat" w l lw 3
EOF
check $?


###############################
# HEIGHT PROFILES
###############################

# super-droplets number density
FN="${DATA_DIR}/hei2massdens.dat"
echo
echo -n "writing Ns profile                 "
gnuplot &> /dev/null <<EOF
stats "${FN}" u 2:1 nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set nologscale x
iend=int(STATS_blocks)-1
set xlabel "SP number density [SP/m³]"
set ylabel "height [m]"
set term png size $SIZE
set output "Ns-prof.png"
set key spacing 1.1
set key top left
set title "xi: ${xi0}, nz: ${nz}, SP number density"
set key autotitle columnheader
set multiplot
do for [i=0:iend] {
  set key at screen 0.15,screen 0.81-0.08*(i+1)/iend
  p "<(sed 's/^.//' \"${FN}\")" i i u 0:(tall="all_".stringcolumn(1),tmm1="mm1_".stringcolumn(1),tunr="unr_".stringcolumn(1),1/0) every ::0::0 notitle, \
    "<(sed 's/^.//' \"${FN}\")" i i u 2:1 w l ls i+1 lw 3 title columnheader(1), \
    ${h1} lt rgb "black", ${h2} lt rgb "black"
}
EOF
check $? do_not_exit_here

# number density
echo
echo -n "writing N profile                  "
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$3>1?\$3:1/0):1 nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale x
set format x "10^{%T}"
iend=int(STATS_blocks)-1
set xlabel "RP number density [RP/m³]"
set ylabel "height [m]"
set title "xi: ${xi0}, nz: ${nz}, number density"
set term png size $SIZE
set output "N-prof.png"
set key autotitle columnhead
set key spacing 1.1
set multiplot
do for [i=0:iend] {
  set key at screen 0.65,screen 0.85-0.27*i/iend
  p "<(sed 's/^.//' \"${FN}\")" i i u 0:(tall="all_".stringcolumn(1),tmm1="mm1_".stringcolumn(1),tunr="unr_".stringcolumn(1),1/0) every ::0::0 notitle, \
    "<(sed 's/^.//' \"${FN}\")" i i u (\$3>1?\$3:1/0):1 w l ls i+1 lw 3 title tall, \
    "<(sed 's/^.//' \"${FN}\")" i i u (\$8>1?\$8:1/0):1 w l ls i title tmm1, \
    "<(sed 's/^.//' \"${FN}\")" i i u (\$13>1?\$13:1/0):1 w p ls i+4 title tunr, \
    ${h1} lt rgb "black", ${h2} lt rgb "black"
}
EOF
check $?

# mass density
echo
echo -n "writing M profile                  "
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$4>1e-10?\$4:1/0):1 nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale x
set format x "10^{%T}"
iend=int(STATS_blocks)-1
set xlabel "mass density [kg/m³]"
set ylabel "height [m]"
set title "xi: ${xi0}, nz: ${nz}, mass density"
set term png size $SIZE
set output "M-prof.png"
set key autotitle columnhead
unset key
set format x "10^%T"
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$4>1e-10?\$4:1/0):1 w l ls i+1 lw 3,\
  "${FN}" i i u (\$9>1e-10?\$9:1/0):1 w l ls i, \
  "${FN}" i i u (\$14>1e-10?\$14:1/0):1 w p ls i+4, \
  ${h1} lt rgb "black", ${h2} lt rgb "black"
}
EOF
check $?

# flux number density
echo
echo -n "writing Fn profile                 "
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$5>1?\$5:1/0):1 nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale x
set format x "10^{%T}"
iend=int(STATS_blocks)-1
set xlabel "flux number density [1/m²s]"
set ylabel "height [m]"
set title "xi: ${xi0}, nz: ${nz}, flux number density"
set term png size $SIZE
set output "Fn-prof.png"
set key autotitle columnhead
unset key
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$5>1?\$5:1/0):1 w l ls i+1 lw 3,\
  "${FN}" i i u (\$10>1?\$10:1/0):1 w l ls i, \
  "${FN}" i i u (\$15>1?\$15:1/0):1 w p ls i+4, \
  ${h1} lt rgb "black", ${h2} lt rgb "black"
}
EOF
check $?

# flux mass density
echo
echo -n "writing Fm profile                 "
FN="${DATA_DIR}/hei2massdens.dat"
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$6>1e-10?\$6:1/0):1 nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale x
set format x "10^{%T}"
iend=int(STATS_blocks)-1
set xlabel "flux mass density [kg/m²s]"
set ylabel "height [m]"
set title "xi: ${xi0}, nz: ${nz}, flux mass density"
set term png size $SIZE
set output "Fm-prof.png"
set key autotitle columnhead
unset key
set format x "10^%T"
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$6>1e-10?\$6:1/0):1 w l ls i+1 lw 3,\
  "${FN}" i i u (\$11>1e-10?\$11:1/0):1 w l ls i, \
  "${FN}" i i u (\$16>1e-10?\$16:1/0):1 w p ls i+4, \
  ${h1} lt rgb "black", ${h2} lt rgb "black"
}
EOF
check $?

###############################
# ICE FRACTION
###############################
FN="${DATA_DIR}/mass2fr.dat"
echo
echo -n "writing (m,Fr) plot                "
gnuplot &> /dev/null <<EOF
stats "${FN}" nooutput
set key spacing 1.1
set logscale x
set format x "10^{%T}"
set ylabel "F_r [1]"
set xlabel "m [kg]"
set title "xi: ${xi0}, nz: ${nz}, ice fraction"
set term png size $SIZE
set xrange [1e-12:1e-2]
set yrange [0:1.1]
set key autotitle columnhead
iend=int(STATS_blocks)
set output "fr-unfilled.png"
set multiplot
do for [i=0:iend] {
  set key at screen 0.95,screen 0.85-0.27*i/iend
  p "<(sed 's/^.//' \"${FN}\")" i i u 0:(tall="all_".stringcolumn(1),tmm1="mm1_".stringcolumn(1),tunr="unr_".stringcolumn(1),1/0) every ::0::0 notitle, \
    "<(sed 's/^.//' \"${FN}\")" i i u 1:(\$2*\$1<\$9?\$2:1/0) w p ls i+1 title tall
}
unset multiplot
set output "fr-filled.png"
set multiplot
do for [i=0:iend] {
  set key at screen 0.95,screen 0.85-0.27*i/iend
  p "<(sed 's/^.//' \"${FN}\")" i i u 0:(tall="all_".stringcolumn(1),tmm1="mm1_".stringcolumn(1),tunr="unr_".stringcolumn(1),1/0) every ::0::0 notitle, \
    "<(sed 's/^.//' \"${FN}\")" i i u 1:(\$2*\$1>\$9?\$2:1/0) w p ls i+1 title tall
}
EOF
check $? do_no_exit_here


###############################
# DISTRIBUTIONS
###############################

for FN in $(find ${DATA_DIR} -name distribution_\*); do
  HEI="${FN%.dat}"
  HEI="${HEI: -9}"

# dens. distr.
echo
echo -n "writing N distr. for $HEI m        "
#gnuplot &> /dev/null <<EOF
gnuplot <<EOF
stats "${FN}" u (\$1*1000):(\$2>1e-4?\$2:1/0) nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale y
iend=int(STATS_blocks)-1
set xlabel "diameter [mm]"
set ylabel "dens. distr. [1/m^4]"
set title "${HEI}, xi: ${xi0}, nz: ${nz}, dens. distr."
set term png size $SIZE
set output "N-dist${HEI}.png"
set key autotitle columnhead
unset key
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$1*1000):(\$2>1e-4?\$2:1/0) w l ls i+1 lw 3, \
  "${FN}" i i u (\$1*1000):(\$7>1e-4?\$7:1/0) w l ls i, \
  "${FN}" i i u (\$1*1000):(\$12>1e-4?\$12:1/0) w p ls i+4
}
EOF
check $? do_no_exit_here

# mass dens. distr.
echo
echo -n "writing M distr. for $HEI m        "
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$1*1000):(\$3>1e-6?\$3:1/0) nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale y
iend=int(STATS_blocks)-1
set xlabel "diameter [mm]"
set ylabel "mass. dens. distr. [kg/m^4]"
set title "${HEI}, xi: ${xi0}, nz: ${nz}, mass dens. distr."
set term png size $SIZE
set output "M-dist${HEI}.png"
set key autotitle columnhead
unset key
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$1*1000):(\$3>1e-6?\$3:1/0) w l ls i+1 lw 3, \
  "${FN}" i i u (\$1*1000):(\$8>1e-6?\$8:1/0) w l ls i, \
  "${FN}" i i u (\$1*1000):(\$13>1e-6?\$13:1/0) w p ls i+4
}
EOF
check $? do_no_exit_here

# density flux distr.
echo
echo -n "writing Fn distr. for $HEI m       "
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$1*1000):(\$4>1e-4?\$4:1/0) nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set nologscale 
iend=int(STATS_blocks)-1
set xlabel "diameter [mm]"
set ylabel "dens. flux [1/m³s]"
set title "${HEI}, xi: ${xi0}, nz: ${nz}, density flux"
set term png size $SIZE
set output "Fn-dist${HEI}.png"
set key autotitle columnhead
unset key
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$1*1000):(\$4>1e-4?\$4:1/0) w l ls i+1 lw 3, \
  "${FN}" i i u (\$1*1000):(\$9>1e-4?\$9:1/0) w l ls i, \
  "${FN}" i i u (\$1*1000):(\$14>1e-4?\$14:1/0) w p ls i+4
}
EOF
check $? do_no_exit_here

# mass density flux distr.
echo
echo -n "writing Fm distr. for $HEI m       "
gnuplot &> /dev/null <<EOF
stats "${FN}" u (\$1*1000):(\$5>1e-6?\$5:1/0) nooutput
set yrange [STATS_min_y:STATS_max_y]
set xrange [STATS_min_x:STATS_max_x]
set logscale
iend=int(STATS_blocks)-1
set xlabel "diameter [mm]"
set ylabel "mass. dens. flux [kg/m³s]"
set title "${HEI}, xi: ${xi0}, nz: ${nz}, mass density flux"
set term png size $SIZE
set output "Fm-dist${HEI}.png"
set key autotitle columnhead
unset key
set multiplot
do for [i=0:iend] {
p "${FN}" i i u (\$1*1000):(\$5>1e-6?\$5:1/0) w l ls i+1 lw 3, \
  "${FN}" i i u (\$1*1000):(\$10>1e-6?\$10:1/0) w l ls i, \
  "${FN}" i i u (\$1*1000):(\$15>1e-6?\$15:1/0) w p ls i+4
}
EOF
check $? do_no_exit_here

done


###########
# GENERATE RIMING IMAGES

for FN in $(find ${DATA_DIR} -name riming2d-z\*); do
HEI="${FN%.dat}"
HEI="${HEI: -13}"
echo
echo -n "writing 2d distr. for $HEI m       "
gnuplot &> /dev/null <<EOF
#set term postscript enhanced color size 8,8
set term png size 600,600
set tics out
set palette model CMY rgbformulae 7,5,15
set logscale x
set logscale y
set logscale cb
set format x "10^{%T}"
set format y "10^{%T}"
set format cb "%T"
set xlabel "m_i [kg]"
set ylabel "m_r [kg]"
set pm3d map
set palette color 
set samples 200
set isosamples 200
set xrange [1e-12:1e-3]
set yrange [1e-18:1e-3]
set palette maxcolors 10
set cbrange [1e-8:1e+2]
set output "${HEI}_N.png"
set title "log_{10}(N), 1d, nz: ${nz}, ${HEI}, all particles"
sp "${FN}" u 1:2:3 title "" 
set cbrange [1e-12:1e-1]
set output "${HEI}_M.png"
set title "log_{10}(M), 1d, nz: ${nz}, ${HEI}, all particles"
sp "${FN}" u 1:2:4 title ""
set output "${HEI}_N_mm1.png"
set title "log_{10}(N), nz: ${nz}, ${HEI}, part. with mm=1"
sp "${FN}" u 1:2:6 title ""
set output "${HEI}_M_mm1.png"
set title "log_{10}(M), nz: ${nz}, ${HEI}, part. with mm=1"
sp "${FN}" u 1:2:7 title ""
set output "${HEI}_N_unr.png"
set title "log_{10}(N), nz: ${nz}, ${HEI}, unrimed particles"
sp "${FN}" u 1:2:8 title ""
set output "${HEI}_M_unr.png"
set title "log_{10}(M), nz: ${nz}, ${HEI}, unrimed particles"
sp "${FN}" u 1:2:9 title ""
EOF
check $? do_no_exit_here
done

#for feps in $(find -name \*eps); do
#  convert -rotate 90 -flatten ${feps} ${feps%.eps}.png
#done



###############
# GENERATE PDF
hm1=$(echo "scale=0; ${h2}/3" | bc -l)
hm1=$(printf "%04i" ${hm1})
hm2=$(echo "scale=0; 2*${h2}/3" | bc -l)
hm2=$(printf "%04i" ${hm2})
h0a=0
h0b=$(echo "scale=0; ${h1}/2" | bc -l)
h1a=$(echo "scale=0; ${h1}" | bc -l)
h1b=$(echo "scale=0; (${h2}+${h1})/2" | bc -l)
h2a=$(echo "scale=0; ${h2}" | bc -l)
h2b=$(echo "scale=0; (${h2}+${domtop})/2" | bc -l)
rim1=${rim1%"."}
tend=${tend%"."}
rim1=$(printf "%06i" ${rim1})
tend=$(printf "%06i" ${tend})
dt2d_1=$(echo "scale=1; ${dt_2dprof}/3600." | bc -l)
dt2d_2=$(echo "scale=1; ${dt_2dprof}/1800." | bc -l)
dt2d_1=$(printf "%5.1f" ${dt2d_1})
dt2d_2=$(printf "%5.1f" ${dt2d_2})
cat > report_1d.tex <<EOF
\documentclass[DIV14,11pt,a4paper,headheight=0mm]{scrreprt}
\usepackage{subfig}
\usepackage[pdftex]{graphicx}
\usepackage{caption}
\captionsetup[figure]{font=footnotesize}
\captionsetup[subfigure]{font=footnotesize}
\usepackage{chngcntr}
\counterwithout{figure}{section}
\topmargin=-2cm
\textheight=27cm
\def\l2size{0.45\linewidth}
\def\lgsize{0.33\linewidth}
\pagestyle{empty}
\begin{document}
\begin{figure}
  \centering
  \subfloat[\$T\$ profile]{\includegraphics[width=\lgsize]{atmo_temp.png}}
  \subfloat[\$S_i\$ profile]{\includegraphics[width=\lgsize]{atmo_sat.png}}
  \subfloat[LWC profile]{\includegraphics[width=\lgsize]{atmo_lwc.png}}
  \caption{Atmospheric profiles.}
\end{figure}
\newpage
\begin{figure}
  \centering
  \subfloat[\$N_s\$ profile]{\includegraphics[width=\lgsize]{Ns-prof.png}}
  \subfloat[\$N\$ profile]{\includegraphics[width=\lgsize]{N-prof.png}}
  \subfloat[\$M\$ profile]{\includegraphics[width=\lgsize]{M-prof.png}}
  \\
  \subfloat[\$F_N\$ profile]{\includegraphics[width=\lgsize]{Fn-prof.png}}
  \subfloat[\$F_M\$ profile]{\includegraphics[width=\lgsize]{Fm-prof.png}}
  \subfloat[\$N\$ ${h0a}-${h0b} m, distr.]{\includegraphics[width=\lgsize]{N-dist${h0a}_${h0b}.png}}
  \\
  \subfloat[\$N\$ ${h1a}-${h1b} m, distr.]{\includegraphics[width=\lgsize]{N-dist${h1a}_${h1b}.png}}
  \subfloat[\$N\$ ${h2a}-${h2b}, distr.]{\includegraphics[width=\lgsize]{N-dist${h2a}_${h2b}.png}}
  \subfloat[\$M\$ ${h0a}-${h0b} m, distr.]{\includegraphics[width=\lgsize]{M-dist${h0a}_${h0b}.png}}
  \\
  \subfloat[\$M\$ ${h1a}-${h1b} m, distr.]{\includegraphics[width=\lgsize]{M-dist${h1a}_${h1b}.png}}
  \subfloat[\$M\$ ${h2a}-${h2b} m, distr.]{\includegraphics[width=\lgsize]{M-dist${h2a}_${h2b}.png}}
  \subfloat[\$F_N\$ ${h0a}-${h0b} m, distr.]{\includegraphics[width=\lgsize]{Fn-dist${h0a}_${h0b}.png}}
  \\
  \subfloat[\$F_M\$ ${h0a}-${h0b} m, distr.]{\includegraphics[width=\lgsize]{Fm-dist${h0a}_${h0b}.png}}
  \subfloat[\$F_r\$ for not filled-in]{\includegraphics[width=\lgsize]{fr-unfilled.png}}
  \subfloat[\$F_r\$ for filled-in]{\includegraphics[width=\lgsize]{fr-filled.png}}
  \caption{\$\xi_0\$: {\bf ${xi0}}, \texttt{nz}: {\bf ${nz}}, \$N_{p,0}\$: {\bf ${nrip0}} 
    RP/m\$^3\$, \$\Delta t_c\$: {\bf ${dtc}}~s, \$S_i\$: {\bf ${sat}}}
\end{figure}
% second page on 2d riming
\begin{figure}
  \centering
  \subfloat[\$N\$, \$h=${h2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${rim1}_N.png}}
  \subfloat[\$N\$, \$h=${hm2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${rim1}_N.png}}
  \subfloat[\$N\$, \$h=${hm1}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${rim1}_N.png}}
  \\
  \subfloat[\$N\$, \$h=${h2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${tend}_N.png}}
  \subfloat[\$N\$, \$h=${hm2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${tend}_N.png}}
  \subfloat[\$N\$, \$h=${hm1}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${tend}_N.png}}
  \\
  \subfloat[\$M\$, \$h=${h2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${rim1}_M.png}}
  \subfloat[\$M\$, \$h=${hm2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${rim1}_M.png}}
  \subfloat[\$M\$, \$h=${hm1}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${rim1}_M.png}}
  \\
  \subfloat[\$M\$, \$h=${h2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${tend}_M.png}}
  \subfloat[\$M\$, \$h=${hm2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${tend}_M.png}}
  \subfloat[\$M\$, \$h=${hm1}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${tend}_M.png}}
  \caption{2d Riming plots for different heights and timestamps of {\bf all} particles.}
\end{figure}
% third page on 2d riming for mm=1
\begin{figure}
  \centering
  \subfloat[\$N\$, \$h=${h2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${rim1}_N_mm1.png}}
  \subfloat[\$N\$, \$h=${hm2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${rim1}_N_mm1.png}}
  \subfloat[\$N\$, \$h=${hm1}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${rim1}_N_mm1.png}}
  \\
  \subfloat[\$N\$, \$h=${h2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${tend}_N_mm1.png}}
  \subfloat[\$N\$, \$h=${hm2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${tend}_N_mm1.png}}
  \subfloat[\$N\$, \$h=${hm1}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${tend}_N_mm1.png}}
  \\
  \subfloat[\$M\$, \$h=${h2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${rim1}_M_mm1.png}}
  \subfloat[\$M\$, \$h=${hm2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${rim1}_M_mm1.png}}
  \subfloat[\$M\$, \$h=${hm1}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${rim1}_M_mm1.png}}
  \\
  \subfloat[\$M\$, \$h=${h2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${tend}_M_mm1.png}}
  \subfloat[\$M\$, \$h=${hm2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${tend}_M_mm1.png}}
  \subfloat[\$M\$, \$h=${hm1}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${tend}_M_mm1.png}}
  \caption{2d Riming plots for different heights and timestamps of {\bf non-aggregates}.}
\end{figure}
% fourth page on 2d riming for weakly rimed particles m_r<20%*m_tot
\begin{figure}
  \centering
  \subfloat[\$N\$, \$h=${h2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${rim1}_N_unr.png}}
  \subfloat[\$N\$, \$h=${hm2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${rim1}_N_unr.png}}
  \subfloat[\$N\$, \$h=${hm1}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${rim1}_N_unr.png}}
  \\
  \subfloat[\$N\$, \$h=${h2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${tend}_N_unr.png}}
  \subfloat[\$N\$, \$h=${hm2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${tend}_N_unr.png}}
  \subfloat[\$N\$, \$h=${hm1}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${tend}_N_unr.png}}
  \\
  \subfloat[\$M\$, \$h=${h2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${rim1}_M_unr.png}}
  \subfloat[\$M\$, \$h=${hm2}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${rim1}_M_unr.png}}
  \subfloat[\$M\$, \$h=${hm1}\$~m, \$t=${dt2d_1}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${rim1}_M_unr.png}}
  \\
  \subfloat[\$M\$, \$h=${h2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${h2}-t${tend}_M_unr.png}}
  \subfloat[\$M\$, \$h=${hm2}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm2}-t${tend}_M_unr.png}}
  \subfloat[\$M\$, \$h=${hm1}\$~m, \$t=${dt2d_2}\$~h]
    {\includegraphics[width=\lgsize]{z${hm1}-t${tend}_M_unr.png}}
  \caption{2d Riming plots for different heights and timestamps of 
    {\bf weakly-rimed aggregates} (\$n_m>1\$ and \$m_r<0.25\\cdot m_i\$).}
\end{figure}
\end{document}
EOF

set +e

#############
# CREATE REPORT
echo -n "writing pdf file                   "
pdflatex -interaction=nonstopmode report_1d.tex &> /dev/null
mv -f report_1d.pdf ${DATA_DIR}
check $?

#############
# CREATE GIF ANIMATIONS
# some z????s might be missing, execute 'set +e'
#eval "convert -delay 20 -loop 0 z2000*N*png z2000.gif"
#eval "convert -delay 20 -loop 0 z1500*N*png z1500.gif"
#eval "convert -delay 20 -loop 0 z1000*N*png z1000.gif"
#eval "convert -delay 20 -loop 0 z0500*N*png z0500.gif"
#mv -f *.gif ${DATA_DIR}


#############
# CLEAN
cd ${THIS_DIR}
rm -fr ${LDIR}

exit 0
