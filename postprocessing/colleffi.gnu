#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set term postscript enhanced color size 5,5 font ',10'
set key top center
set key spacing 1.5
set tics out
set lmargin 5
show margin
set colorbox vertical user origin .90, .25 size .02, .7 

set style line 1 lt 3 lc rgb "red" lw 6
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 6
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3
set style line 8 lt 9 lc rgb "orange" lw 6
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3

#set palette rgb 34,35,36 # black-red-yellow-white
set palette model CMY rgbformulae 7,5,15 # white-green-blue
#set palette rgb 30,31,32  # black-blue-violet-yellow-white
set logscale x
set logscale y
set format x "10^{%T}"
set format y "10^{%T}"
set format z "10^{%T}"
#set format cb "10^{%T}"


#
# 2d plots
#


#
# 3d plots on 2d plane
#

set xlabel " m_i of particle 1 [kg]"
set ylabel " m_i of particle 2 [kg]"
set pm3d map
set palette color 
set samples 200
set isosamples 200
set xrange [1e-14:1e-2]
set yrange [1e-14:1e-2]
set palette maxcolors 13
set cbrange [-0.1:1.1]

# set SE to 3 for coll. eff. of Bohm
#           4 for coll. eff. of Bohm with sticking eff. of Vaughand
SE=3

set output "ce_Fr00_rhor200.eps"
sp "../experiments/check/ce_frId1_rhorId1.dat" u 1:2:SE title ""
set output "ce_Fr03_rhor200.eps"
sp "../experiments/check/ce_frId2_rhorId1.dat" u 1:2:SE title ""
set output "ce_Fr06_rhor200.eps"
sp "../experiments/check/ce_frId3_rhorId1.dat" u 1:2:SE title ""
set output "ce_Fr09_rhor200.eps"
sp "../experiments/check/ce_frId4_rhorId1.dat" u 1:2:SE title ""

set output "ce_Fr00_rhor400.eps"
sp "../experiments/check/ce_frId1_rhorId2.dat" u 1:2:SE title ""
set output "ce_Fr03_rhor400.eps"
sp "../experiments/check/ce_frId2_rhorId2.dat" u 1:2:SE title ""
set output "ce_Fr06_rhor400.eps"
sp "../experiments/check/ce_frId3_rhorId2.dat" u 1:2:SE title ""
set output "ce_Fr09_rhor400.eps"
sp "../experiments/check/ce_frId4_rhorId2.dat" u 1:2:SE title ""

set output "ce_Fr00_rhor600.eps"
sp "../experiments/check/ce_frId1_rhorId3.dat" u 1:2:SE title ""
set output "ce_Fr03_rhor600.eps"
sp "../experiments/check/ce_frId2_rhorId3.dat" u 1:2:SE title ""
set output "ce_Fr06_rhor600.eps"
sp "../experiments/check/ce_frId3_rhorId3.dat" u 1:2:SE title ""
set output "ce_Fr09_rhor600.eps"
sp "../experiments/check/ce_frId4_rhorId3.dat" u 1:2:SE title ""

set output "ce_Fr00_rhor800.eps"
sp "../experiments/check/ce_frId1_rhorId4.dat" u 1:2:SE title ""
set output "ce_Fr03_rhor800.eps"
sp "../experiments/check/ce_frId2_rhorId4.dat" u 1:2:SE title ""
set output "ce_Fr06_rhor800.eps"
sp "../experiments/check/ce_frId3_rhorId4.dat" u 1:2:SE title ""
set output "ce_Fr09_rhor800.eps"
sp "../experiments/check/ce_frId4_rhorId4.dat" u 1:2:SE title ""
