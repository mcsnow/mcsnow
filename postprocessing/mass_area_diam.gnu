#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set term postscript enhanced color size 6,5
set key top center
set key spacing 1.5
set tics out
set lmargin 5
show margin
set colorbox vertical user origin .88, .2 size .02, .7

set style line 1 lt 3 lc rgb "red" lw 6
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 6
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3
set style line 8 lt 9 lc rgb "orange" lw 6
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3

set palette rgb 30,31,32  # black-blue-violet-yellow-white
set logscale x
set logscale y
set format x "10^{%T}"
set format y "10^{%T}"


#
# 2d plots
#

fr00=0.0
fr05=0.5
fr09=0.9
set output "rhor400_Fr_diam.eps"
set xlabel "D [mm]"
set ylabel "mass [kg]"
set xrange [1e-2:10]
set yrange [1e-14:1e-4]
p "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr00)<1e-2?($1+$2):1/0) title "Fr=0.0" w lp ls 2, "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr09)<1e-2?($1+$2):1/0) title "Fr=0.9" w lp ls 6

unset logscale
unset format
set ylabel "proj. area [cm^2]"
set output "rhor400_Fr_parea.eps"
set xlabel "D [mm]"
set xrange [0:10]
set yrange [0:0.5]
p "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr00)<1e-2?($6*10000):1/0) title "Fr=0.0" w lp ls 2, "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr09)<1e-2?($6*10000):1/0) title "Fr=0.9" w lp ls 6

set ylabel "term. vel. [m/s]"
set output "rhor400_Fr_vt.eps"
set xrange [0:4]
set yrange [1e-4:3]
p "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr00)<1e-2?$8:1/0) title "Fr=0.0" w lp ls 2, "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr05)<1e-2?$8:1/0) title "Fr=0.5" w lp ls 6, "../experiments/check/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr09)<1e-2?$8:1/0) title "Fr=0.9" w lp ls 9


set term postscript enhanced color size 6,6

set logscale x
set logscale y

#
# 3d plots on 2d plane
#

set format x "10^{%T}"
set format y "10^{%T}"
set xlabel "D [m]"
set xlabel "m_i [kg]"
set ylabel "m_r [kg]"
set pm3d map
set palette color 
set samples 200
set isosamples 200
set xrange [1e-12:1e-5]
set yrange [1e-12:1e-3]

set logscale cb
set format cb "10^{%T}"
set output "rhor400_diam.eps"
set palette maxcolors 12
set cbrange [1e-3:50]
sp "../experiments/check/mass_area_diam.dat" u 1:2:($5*10**3) title ""

set output "rhor400_parea.eps"
set palette maxcolors 12
set cbrange [1e-6:10]
sp "../experiments/check/mass_area_diam.dat" u 1:2:($6*10**3) title "" 

set format x "10^{%T}"
set format y "10^{%T}"
set output "rhor400_vt.eps"
set palette maxcolors 15
set cbrange [1e-2:10]
set contour base
set cntrparam levels discrete 1e-2, 2.5e-2, 5e-2, 7.5e-2, 1e-1, 2.5e-1, 5e-1, 7.5e-1, 1, 2.5, 5, 7.5
set cntrlabel onecolor format '%8.3g' font ',6' start 5e-2 interval 10
unset surface
unset key
sp "../experiments/check/mass_area_diam.dat" u 1:2:8 title "" 
