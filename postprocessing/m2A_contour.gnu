#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set tics out
set lmargin 9
set tmargin 2
show margin
set colorbox vertical user origin .88, .2 size .02, .7

set style line 1 lt 3 lc rgb "red" lw 6
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 6
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3
set style line 8 lt 9 lc rgb "orange" lw 6
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3

set palette rgb 30,31,32  # black-blue-violet-yellow-white

set term postscript enhanced color size 6,5

set view map
set contour base
set logscale x
set logscale y
unset surface
set table 'cont-bulk.dat'
set cntrparam levels discrete 1e-6, 5e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2, 5e-2, 0.1, 0.5, 1, 5, 10
sp "../experiments/check_c1_v1/mass_area_diam.dat" u 1:2:($6*10000)
unset table
set table 'cont-fillin.dat'
set cntrparam levels discrete 1e-6, 5e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2, 5e-2, 0.1, 0.5, 1, 5, 10
sp "../experiments/check_c2_v1/mass_area_diam.dat" u 1:2:($6*10000)
unset table
set table 'cont-dda.dat'
set cntrparam levels discrete 1e-6, 5e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2, 5e-2, 0.1, 0.5, 1, 5, 10
sp "../experiments/check_c3_v1/mass_area_diam.dat" u 1:2:($6*10000)
unset table

set logscale x
set logscale y
set format x "10^{%T}"
set format y "10^{%T}"
set output "rhor400_parea.eps"
set xrange [1e-14:1e-2]
set yrange [1e-14:1e-2]
set cbrange [1e-2:10]
set view map

set multiplot

set xlabel 'm_i [kg]'
set ylabel 'm_r [kg]'

p 'cont-bulk.dat' w l lt 1 lw 1.5 notitle
p 'cont-dda.dat' w l lt 2 lw 1.5 notitle

l '<./cont.sh cont-fillin.dat 0 10 0'
p '<./cont.sh cont-fillin.dat 1 10 0' w l lt -1 lw 1.5 notitle

unset multiplot
