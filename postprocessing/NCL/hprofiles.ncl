;================================================;
; These files are loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"   
; ================================================;
;
; Copyright (C) 2004-2024, DWD
; See ./AUTHORS.txt for a list of authors
; See ./LICENSES/ for license information
; SPDX-License-Identifier: BSD-3-Clause
;

begin

  lwrite_netcdf = True

  lplot_each_category = False  ; separate plot for ice, snow, graupel etc.
  lplot_process_rates = False

  prefix = ""
  exp0   = "1d_axel_xi100_nz250_lwc03_iwc10_dtc5_nrp100_rm10_rt1_vt2_h10-20_ba400_geo2_SDRA"

; variables from command line input

  if (.not.isvar("expname")) then
    expname = exp0
  end if
  if (.not.isvar("basedir")) then
    basedir   = "../experiments/"
  end if

  expdir = prefix+expname

;=================================================;

; file output
  
  fodir  = basedir+expdir+"/Plots/"
  foform = "pdf"
  foname = "hprofiles"

  wks  = gsn_open_wks(foform,fodir+foname)
  gsn_define_colormap(wks,"BkBlAqGrYeOrReViWh200")

;=================================================;

; open file and read in data

  filedir  = basedir+expdir+"/"

  ncol = 32   ; number of columns in hei2massdens.dat files
  
  filename = "hei2massdens.dat"
  print("Reading "+filedir+filename)
  data  = asciiread(filedir+filename,-1,"float")

  nlev  = toint(data(0))+1
  data := data(1:)
  
  nrow  = numAsciiRow(filedir+filename) ; number of rows in file
  ntime = (nrow-1)/(nlev+3)             ; number of time steps in file
  print("nlev  = "+nlev)
  print("ncol  = "+ncol)
  print("nrow  = "+nrow)
  print("ntime = "+ntime)

  time = new(ntime,float)
  z    = new( (/ntime,nlev/), float)
  Ns     = new( (/ntime,nlev/), float)
  Nd     = new( (/ntime,nlev/), float)
  Md     = new( (/ntime,nlev/), float)
  Fn     = new( (/ntime,nlev/), float)
  Fm     = new( (/ntime,nlev/), float)
  Fmono  = new( (/ntime,nlev/), float)
  Nd_mm1    = new( (/ntime,nlev/), float)
  Md_mm1    = new( (/ntime,nlev/), float)
  Fn_mm1    = new( (/ntime,nlev/), float)
  Fm_mm1    = new( (/ntime,nlev/), float)
  Fmono_mm1 = new( (/ntime,nlev/), float)
  Nd_unr    = new( (/ntime,nlev/), float)
  Md_unr    = new( (/ntime,nlev/), float)
  Fn_unr    = new( (/ntime,nlev/), float)
  Fm_unr    = new( (/ntime,nlev/), float)
  Fmono_unr = new( (/ntime,nlev/), float)
  Nd_grp    = new( (/ntime,nlev/), float)
  Md_grp    = new( (/ntime,nlev/), float)
  Fn_grp    = new( (/ntime,nlev/), float)
  Fm_grp    = new( (/ntime,nlev/), float)
  Fmono_grp = new( (/ntime,nlev/), float)
  Nd_liq    = new( (/ntime,nlev/), float)
  Md_liq    = new( (/ntime,nlev/), float)
  Fn_liq    = new( (/ntime,nlev/), float)
  Fm_liq    = new( (/ntime,nlev/), float)
  Fmono_liq = new( (/ntime,nlev/), float)
  Nd_mori   = new( (/ntime,nlev/), float)
  Md_mori   = new( (/ntime,nlev/), float)
  Fn_mori   = new( (/ntime,nlev/), float)
  Fm_mori   = new( (/ntime,nlev/), float)
  Fmono_mori= new( (/ntime,nlev/), float)

  istart = 0
  iend   = (1+nlev-1)*ncol
  do it=0,ntime-1 
    time(it) = data(istart)
    ;print("   "+time(it)+" min")
    data1 := onedtond(data(istart+1:istart+iend),(/nlev,ncol/))
    z(it,:) = data1(:,0)
    data1 := where(data1.gt.0,data1,data1@_FillValue)
    Ns(it,:)     = data1(:,1) 
    Nd(it,:)     = data1(:,2) 
    Md(it,:)     = data1(:,3)
    Fn(it,:)     = data1(:,4)
    Fm(it,:)     = data1(:,5)
    Fmono(it,:)  = data1(:,6)
    Nd_mm1(it,:)    = data1(:,7)
    Md_mm1(it,:)    = data1(:,8)
    Fn_mm1(it,:)    = data1(:,9)
    Fm_mm1(it,:)    = data1(:,10)
    Fmono_mm1(it,:) = data1(:,11) 
    Nd_unr(it,:)    = data1(:,12)
    Md_unr(it,:)    = data1(:,13)
    Fn_unr(it,:)    = data1(:,14)
    Fm_unr(it,:)    = data1(:,15)
    Fmono_unr(it,:) = data1(:,16)
    Nd_grp(it,:)    = data1(:,17)
    Md_grp(it,:)    = data1(:,18)
    Fn_grp(it,:)    = data1(:,19)
    Fm_grp(it,:)    = data1(:,20)
    Fmono_grp(it,:) = data1(:,21)
    Nd_liq(it,:)    = data1(:,22)
    Md_liq(it,:)    = data1(:,23)
    Fn_liq(it,:)    = data1(:,24)
    Fm_liq(it,:)    = data1(:,25)
    Fmono_liq(it,:) = data1(:,26)
    Nd_mori(it,:)    = data1(:,27)
    Md_mori(it,:)    = data1(:,28)
    Fn_mori(it,:)    = data1(:,29)
    Fm_mori(it,:)    = data1(:,30)
    Fmono_mori(it,:) = data1(:,31)
    istart = istart+iend+1
  end do

  Fm_rimed = Fm
  Fm_rimed = where(.not.ismissing(Fm_mm1),Fm_rimed-Fm_mm1,Fm_rimed)
  Fm_rimed = where(.not.ismissing(Fm_unr),Fm_rimed-Fm_unr,Fm_rimed)
  Fm_rimed = where(.not.ismissing(Fm_grp),Fm_rimed-Fm_grp,Fm_rimed)
  Fm_rimed = where(.not.ismissing(Fm_liq),Fm_rimed-Fm_liq,Fm_rimed)
  Fm_rimed = where(.not.ismissing(Fm_mori),Fm_rimed-Fm_mori,Fm_rimed)
  Fm_rimed = where(Fm_rimed.gt.0,Fm_rimed,Fm_rimed@_FillValue)

  Fn_rimed = Fn
  Fn_rimed = where(.not.ismissing(Fn_mm1),Fn_rimed-Fn_mm1,Fn_rimed)
  Fn_rimed = where(.not.ismissing(Fn_unr),Fn_rimed-Fn_unr,Fn_rimed)
  Fn_rimed = where(.not.ismissing(Fn_grp),Fn_rimed-Fn_grp,Fn_rimed)
  Fn_rimed = where(.not.ismissing(Fn_liq),Fn_rimed-Fn_liq,Fn_rimed)
  Fn_rimed = where(.not.ismissing(Fn_mori),Fn_rimed-Fn_mori,Fn_rimed)
  Fn_rimed = where(Fn_rimed.gt.0,Fn_rimed,Fn_rimed@_FillValue)

  Md_rimed = Md
  Md_rimed = where(.not.ismissing(Md_mm1),Md_rimed-Md_mm1,Md_rimed)
  Md_rimed = where(.not.ismissing(Md_unr),Md_rimed-Md_unr,Md_rimed)
  Md_rimed = where(.not.ismissing(Md_grp),Md_rimed-Md_grp,Md_rimed)
  Md_rimed = where(.not.ismissing(Md_liq),Md_rimed-Md_liq,Md_rimed)
  Md_rimed = where(.not.ismissing(Md_mori),Md_rimed-Md_mori,Md_rimed)
  Md_rimed = where(Md_rimed.gt.0,Md_rimed,Md_rimed@_FillValue)
    
  Nd_rimed = Nd
  Nd_rimed = where(.not.ismissing(Nd_mm1),Nd_rimed-Nd_mm1,Nd_rimed)
  Nd_rimed = where(.not.ismissing(Nd_unr),Nd_rimed-Nd_unr,Nd_rimed)
  Nd_rimed = where(.not.ismissing(Nd_grp),Nd_rimed-Nd_grp,Nd_rimed)
  Nd_rimed = where(.not.ismissing(Nd_liq),Nd_rimed-Nd_liq,Nd_rimed)
  Nd_rimed = where(.not.ismissing(Nd_mori),Nd_rimed-Nd_mori,Nd_rimed)
  Nd_rimed = where(Nd_rimed.gt.0,Nd_rimed,Nd_rimed@_FillValue)

; read diag file

  ncol2 = 5
  nlev2 = nlev-1
 
  filename = "hprofiles_diag.dat"
  print("Reading "+filedir+filename)
  data  := asciiread(filedir+filename,-1,"float")

  nrow  = numAsciiRow(filedir+filename)   ; number of rows in file
  ntime = nrow/(nlev2+2)                  ; number of time steps in file
  print("nlev2 = "+nlev2)
  print("ncol2 = "+ncol2)
  print("nrow  = "+nrow)
  print("ntime = "+ntime)

  zdiag    = new( (/nlev2/), float)
  dep_tot  = new( (/ntime,nlev2/), float)
  dep_mm1  = new( (/ntime,nlev2/), float)
  rime_tot = new( (/ntime,nlev2/), float)
  heat_tot = new( (/ntime,nlev2/), float)

  istart = 0
  iend   = nlev2*ncol2

  do it=0,ntime-1 
    time(it) = data(istart)
    print("   "+time(it)+" min")
    data1 := onedtond(data(istart+1:istart+iend),(/nlev2,ncol2/))
    dep_tot(it,:)     = data1(:,1) 
    dep_mm1(it,:)     = data1(:,2) 
    rime_tot(it,:)    = data1(:,3) 
    heat_tot(it,:)    = data1(:,4)
    istart = istart+iend+1
  end do
  zdiag = data1(:,0) 

;=================================================;

; vertical smoothing with lanczos low pass filter
  wgt = filwgts_lanczos (5,0,0.2,-999.,1.)  
  plist = [/Ns,Nd,Md,Fn,Fm,Fmono,\
            Nd_mm1,Md_mm1,Fn_mm1,Fm_mm1,Fmono_mm1, \
            Nd_unr,Md_unr,Fn_unr,Fm_unr,Fmono_unr, \
            Nd_rimed,Md_rimed,Fn_rimed,Fm_rimed,   \
            Nd_grp,Md_grp,Fn_grp,Fm_grp,Fmono_grp, \
            rime_tot,dep_tot,dep_mm1 /]
  do m=0,ListCount(plist)-1 
    plist[m] = wgt_runave_Wrap(plist[m],wgt,0)
  end do

;=================================================;

  res = True
  res@gsnDraw        = True
  res@gsnFrame       = False                    
  res@pmLegendDisplayMode    = "Always"            ; turn on legend
  res@pmLegendSide           = "Top"               ; Change location of 
  res@pmLegendParallelPosF   = 0.85                ; move units right
  res@pmLegendOrthogonalPosF = -0.55               ; move units down
  res@pmLegendWidthF         = 0.05                ; Change width and
  res@pmLegendHeightF        = 0.25                ; height of legend.
  res@lgPerimOn              = False               ; turn off box around
  res@lgLabelFontHeightF     = .016                ; label font height
  res@xyExplicitLegendLabels = "  "+(/"all","mono-crystals","unrimed agg."/)
  res@lgItemOrder            = (/9,8,7,6,5,4,3,2,1,0/)
  res@tmXTOn = False
  res@tmYROn = False

  res@xyDashPatterns         = (/0,0,0,0,0,0,0,0/)
  res@xyLineThicknesses      = (/1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1./) * 3.
  res@xyLineColors           = (/30,40,50,78,110,120,130,140,170,180,190,208,1/)

; subtitle
  txres                    = True
  txres@txJust             = "BottomLeft"
  txres@txFont             = "Helvetica"
  txres@txFontHeightF      = 0.013
  txres@txConstantSpacingF = 1.2
  txres@txFontHeightF      = 0.009
  txres@xpos               = 0.1
  subtitle = expname+", "+systemfunc("date")

;=================================================;
  
  nstart = 1
  nend   = ntime-1
  stride = 1

  res@xyExplicitLegendLabels := "  "+time(nstart:nend:stride)+" min"
  res@tiYAxisString  = "z in m" 
  res@trYLog  = False
  res@trXLog  = True

; super-particles
  res2 = res
  res2@pmLegendOrthogonalPosF = -0.99      ; move further down
  res2@gsnLeftString  = "number density of super particles"
  res2@tiXAxisString  = "number per grid point" 
  res2@trXMinF = 1e0
  res2@trXMaxF = 1e4
  plot1 = gsn_csm_xy(wks,Ns(nstart:nend:stride,:),z(nstart:nend:stride,:),res2)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

; number density
  res2@gsnLeftString  = "total number density"
  res2@tiXAxisString  = "number density in m~S~-3~N~" 
  res2@trXMinF = 1e2
  res2@trXMaxF = 1e7
  plot1 = gsn_csm_xy(wks,Nd(nstart:nend:stride,:),z(nstart:nend:stride,:),res2)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

; mass density
  res3 = res
;  res3@pmLegendOrthogonalPosF = -0.99      ; move further down
;  res3@pmLegendParallelPosF   = 0.20       ; move units right
  res3@gsnLeftString  = "total mass density"
  res3@tiXAxisString  = "mass density in kg m~S~-3~N~" 
  res3@trXMinF = 1e-5
  res3@trXMaxF = 1e-1
  plot1 = gsn_csm_xy(wks,Md(nstart:nend:stride,:),z(nstart:nend:stride,:),res3)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

; number flux Fn
  res2@gsnLeftString  = "total number flux"
  res2@tiXAxisString  = "number flux in m~S~-2~N~ s~S~-1~N~" 
  res2@trXMinF = 1e2
  res2@trXMaxF = 1e6
  plot1 = gsn_csm_xy(wks,Fn(nstart:nend:stride,:),z(nstart:nend:stride,:),res2)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

; mass flux Fm
  res3@gsnLeftString  = "total mass flux"
  res3@tiXAxisString  = "mass flux density in kg m~S~-2~N~ s~S~-1~N~" 
  res3@trXMinF = 1e-6
  res3@trXMaxF = 1e1
  plot1 = gsn_csm_xy(wks,Fm(nstart:nend:stride,:),z(nstart:nend:stride,:),res3)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

; monomer flux Fmono
  res@gsnLeftString  = "total monomer flux"
  res@tiXAxisString  = "monomer flux density in m~S~-2~N~ s~S~-1~N~" 
  res@trXMinF = 1e3
  res@trXMaxF = 1e7
  plot1 = gsn_csm_xy(wks,Fmono(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

  if (lplot_each_category) then

;   non-aggregates / mono-crystals

;   number density
    res@gsnLeftString  = "number density of unrimed mono-crystals"
    res@tiXAxisString  = "number density in m~S~-3~N~" 
    res@trXMinF = 1e2
    res@trXMaxF = 1e9
    plot1 = gsn_csm_xy(wks,Nd_mm1(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   mass density
    res@gsnLeftString  = "mass density of unrimed mono-crystals"
    res@tiXAxisString  = "mass density in kg m~S~-3~N~" 
    res@trXMinF = 1e-6
    res@trXMaxF = 1e-1
    plot1 = gsn_csm_xy(wks,Md_mm1(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   mass flux Fm
    res@gsnLeftString  = "number flux of unrimed mono-crystals"
    res@tiXAxisString  = "number flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e2
    res@trXMaxF = 1e8
    plot1 = gsn_csm_xy(wks,Fn_mm1(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   mass flux Fm
    res@gsnLeftString  = "mass flux of unrimed mono-crystals"
    res@tiXAxisString  = "mass flux density in kg m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e-6
    res@trXMaxF = 1e-2
    plot1 = gsn_csm_xy(wks,Fm_mm1(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   monomer flux Fm
    res@gsnLeftString  = "monomer flux of unrimed mono-crystals"
    res@tiXAxisString  = "monomer flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e2
    res@trXMaxF = 1e8
    plot1 = gsn_csm_xy(wks,Fmono_mm1(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   unrimed aggregates

;   number density
    res@gsnLeftString  = "number density of unrimed aggregates"
    res@tiXAxisString  = "number density in m~S~-3~N~" 
    res@trXMinF = 1e2
    res@trXMaxF = 1e8
    plot1 = gsn_csm_xy(wks,Nd_unr(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   mass density
    res@gsnLeftString  = "mass density of unrimed aggregates"
    res@tiXAxisString  = "mass density in kg m~S~-3~N~" 
    res@trXMinF = 1e-6
    res@trXMaxF = 1e-1
    plot1 = gsn_csm_xy(wks,Md_unr(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   number flux Fn
    res@gsnLeftString  = "number flux of unrimed aggregates"
    res@tiXAxisString  = "number flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e2
    res@trXMaxF = 1e6
    plot1 = gsn_csm_xy(wks,Fn_unr(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   mass flux Fm
    res@gsnLeftString  = "mass flux of unrimed aggregates"
    res@tiXAxisString  = "mass flux density in kg m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e-6
    res@trXMaxF = 1e-2
    plot1 = gsn_csm_xy(wks,Fm_unr(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   monomer flux
    res@gsnLeftString  = "monomer flux of unrimed aggregates"
    res@tiXAxisString  = "monomer flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e3
    res@trXMaxF = 1e8
    plot1 = gsn_csm_xy(wks,Fmono_unr(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)


;   mass flux Fm
    res@gsnLeftString  = "mass flux of rimed pristine crystals"
    res@tiXAxisString  = "mass flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e-5
    res@trXMaxF = 1e-1
    plot1 = gsn_csm_xy(wks,Fm_mori(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   rimed snow

;   mass flux Fm
    res@gsnLeftString  = "mass flux of rimed snow"
    res@tiXAxisString  = "mass flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e-5
    res@trXMaxF = 1e-1
    plot1 = gsn_csm_xy(wks,Fm_rimed(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   mass flux Fm
    res@gsnLeftString  = "mass flux of graupel"
    res@tiXAxisString  = "mass flux density in m~S~-2~N~ s~S~-1~N~" 
    res@trXMinF = 1e-5
    res@trXMaxF = 1e-1
    plot1 = gsn_csm_xy(wks,Fm_grp(nstart:nend:stride,:),z(nstart:nend:stride,:),res)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

  end if

; time average over last navg time steps

  navg = 3
;  navg = 10
  n1 = ntime-1-navg
  n2 = ntime-1

  print("Time averages from "+time(n1)+" to "+time(n2)+" min")

  Ns := dim_avg_n(Ns(n1:n2,:),0)
  Nd := dim_avg_n(Nd(n1:n2,:),0)
  Md := dim_avg_n(Md(n1:n2,:),0)
  Fm := dim_avg_n(Fm(n1:n2,:),0)
  Fn := dim_avg_n(Fn(n1:n2,:),0)
  Nd_mm1 := dim_avg_n(Nd_mm1(n1:n2,:),0)
  Md_mm1 := dim_avg_n(Md_mm1(n1:n2,:),0)
  Fm_mm1 := dim_avg_n(Fm_mm1(n1:n2,:),0)
  Fn_mm1 := dim_avg_n(Fn_mm1(n1:n2,:),0)
  Nd_unr := dim_avg_n(Nd_unr(n1:n2,:),0)
  Md_unr := dim_avg_n(Md_unr(n1:n2,:),0)
  Fm_unr := dim_avg_n(Fm_unr(n1:n2,:),0)
  Fn_unr := dim_avg_n(Fn_unr(n1:n2,:),0)
  Nd_grp := dim_avg_n(Nd_grp(n1:n2,:),0)
  Md_grp := dim_avg_n(Md_grp(n1:n2,:),0)
  Fm_grp := dim_avg_n(Fm_grp(n1:n2,:),0)
  Fn_grp := dim_avg_n(Fn_grp(n1:n2,:),0)
  Nd_liq := dim_avg_n(Nd_liq(n1:n2,:),0)
  Md_liq := dim_avg_n(Md_liq(n1:n2,:),0)
  Fm_liq := dim_avg_n(Fm_liq(n1:n2,:),0)
  Fn_liq := dim_avg_n(Fn_liq(n1:n2,:),0)
  Nd_mori := dim_avg_n(Nd_mori(n1:n2,:),0)
  Md_mori := dim_avg_n(Md_mori(n1:n2,:),0)
  Fm_mori := dim_avg_n(Fm_mori(n1:n2,:),0)
  Fn_mori := dim_avg_n(Fn_mori(n1:n2,:),0)
  Nd_rimed := dim_avg_n(Nd_rimed(n1:n2,:),0)
  Md_rimed := dim_avg_n(Md_rimed(n1:n2,:),0)
  Fm_rimed := dim_avg_n(Fm_rimed(n1:n2,:),0)
  Fn_rimed := dim_avg_n(Fn_rimed(n1:n2,:),0)

  rime_tot := dim_avg_n(rime_tot(n1:n2,:),0)
  dep_tot  := dim_avg_n(dep_tot(n1:n2,:),0)
  dep_mm1  := dim_avg_n(dep_mm1(n1:n2,:),0)
  heat_tot := dim_avg_n(heat_tot(n1:n2,:),0)

; snow categories at final time

  res@pmLegendWidthF          = 0.05                ; Change width and
  res@pmLegendHeightF         = 0.16                ; height of legend.
  res@pmLegendParallelPosF    = 0.80                ; move units right
  res@pmLegendOrthogonalPosF  = -0.35               ; move units down
  res@xyExplicitLegendLabels := "  "+(/"total","unrimed crystals","unrimed agg.","rimed crystals", \
                                       "rimed snow","graupel-like","liquid drops"/)
  res@xyLineColors           := (/1,50,30,78,140,170,208/)
  res@lgItemOrder            := (/5,4,3,2,1,0/)

  n = ntime-1

  res@gsnLeftString  = "mass flux of ice categories"
  res@tiXAxisString  = "mass flux density in kg m~S~-2~N~ s~S~-1~N~" 
  res@trXMinF = 1e-6
  res@trXMaxF = 1e-0
  xvar = (/Fm,Fm_mm1,Fm_unr,Fm_mori,Fm_rimed,Fm_grp,Fm_liq/)
  yvar = (/z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:)/)
  plot1 = gsn_csm_xy(wks,xvar,yvar,res)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

  res2 = res
  res2@pmLegendOrthogonalPosF  = -0.99               ; move units down
  res2@gsnLeftString  = "number flux of ice categories"
  res2@tiXAxisString  = "number flux density in m~S~-2~N~ s~S~-1~N~" 
  res2@trXMinF = 1e2
  res2@trXMaxF = 1e8
  xvar = (/Fn,Fn_mm1,Fn_unr,Fn_mori,Fn_rimed,Fn_grp,Fn_liq/)
  yvar = (/z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:)/)
  plot1 = gsn_csm_xy(wks,xvar,yvar,res2)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

  res2 = res
  res2@pmLegendOrthogonalPosF  = -0.99               ; move units down
  res2@gsnLeftString  = "number density of ice categories"
  res2@tiXAxisString  = "number density in m~S~-3~N~" 
  res2@trXMinF = 1e1
  res2@trXMaxF = 1e8
  xvar = (/Nd,Nd_mm1,Nd_unr,Nd_mori,Nd_rimed,Nd_grp,Nd_liq/)
  yvar = (/z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:)/)
  plot1 = gsn_csm_xy(wks,xvar,yvar,res2)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

  res@gsnLeftString  = "mass density of ice categories"
  res@tiXAxisString  = "mass density in kg m~S~-3~N~" 
  res@trXMinF = 1e-5
  res@trXMaxF = 1e-1
  xvar = (/Md,Md_mm1,Md_unr,Md_mori,Md_rimed,Md_grp,Md_liq/)
  yvar = (/z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:),z(n,:)/)
  plot1 = gsn_csm_xy(wks,xvar,yvar,res)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

  delete(res2@lgItemOrder)

; super-particles
  res2@pmLegendOrthogonalPosF = -0.99      ; move further down
  res2@gsnLeftString  = "number density of super particles"
  res2@tiXAxisString  = "number per grid point" 
  res2@trXMinF = 1e0
  res2@trXMaxF = 1e4
  plot1 = gsn_csm_xy(wks,Ns,z(n,:),res2)
  gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
  frame(wks)

  if (lplot_process_rates) then
  
;   deposition rates  
    xvar := (/dep_tot,dep_mm1/)
    yvar := (/zdiag,zdiag/)
    xvar := where(xvar.eq.0,xvar@_FillValue,xvar)    
    res2@pmLegendOrthogonalPosF = -0.4      ; move further down
    res2@gsnLeftString  = "deposition/condensation rate"
    res2@tiXAxisString  = "rate in kg/(s m~S~3~N~)" 
    res2@trXMinF = -0.02
    res2@trXMaxF = 0.05
    plot1 = gsn_csm_xy(wks,xvar,yvar,res2)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)

;   bulk riming rates
    res2@pmLegendOrthogonalPosF = -0.4      ; move further down
    res2@gsnLeftString  = "riming rate"
    res2@tiXAxisString  = "rate in kg/(s m~S~3~N~)" 
    res2@trXMinF = -0.02
    res2@trXMaxF = 0.1
    plot1 = gsn_csm_xy(wks,(/rime_tot/),(/zdiag/),res2)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)
    
;   bulk heating rates
    res2@pmLegendOrthogonalPosF = -0.4      ; move further down
    res2@gsnLeftString  = "heat rate"
    res2@tiXAxisString  = "rate in J"
    ;res2@trXMinF = -0.02
    ;res2@trXMaxF = 0.1
    plot1 = gsn_csm_xy(wks,(/heat_tot/),(/zdiag/),res2)
    gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
    frame(wks)  

  end if
  
;=================================================;

  z!0 = "ntime"
  z!1 = "k"
  Ns!0   = "k"
  Nd!0   = "k"
  Md!0   = "k"
  Fn!0   = "k"
  Fm!0   = "k"
  Fmono!0 = "k"

  Nd_mm1!0   = "k"
  Md_mm1!0   = "k"
  Fn_mm1!0   = "k"
  Fm_mm1!0   = "k"
  Fmono_mm1!0 = "k"
  Nd_unr!0   = "k"
  Md_unr!0   = "k"
  Fn_unr!0   = "k"
  Fm_unr!0   = "k"
  Fmono_unr!0 = "k"
  Nd_rimed!0   = "k"
  Md_rimed!0   = "k"
  Fn_rimed!0   = "k"
  Fm_rimed!0   = "k"
;  Fmono_rimed!0 = "k"
  Nd_mori!0   = "k"
  Md_mori!0   = "k"
  Fn_mori!0   = "k"
  Fm_mori!0   = "k"
  Fmono_mori!0 = "k"
  Nd_grp!0   = "k"
  Md_grp!0   = "k"
  Fn_grp!0   = "k"
  Fm_grp!0   = "k"
  Fmono_grp!0 = "k"
  Nd_liq!0   = "k"
  Md_liq!0   = "k"
  Fn_liq!0   = "k"
  Fm_liq!0   = "k"

  rime_tot!0 = "j"
  dep_tot!0 = "j"
  dep_mm1!0 = "j"
  zdiag!0 = "j"
  heat_tot!0 = "j"

;=================================================;

  if (lwrite_netcdf) then
    fname  = "hprofiles.nc"
    outcdf =  basedir+expdir+"/"+fname
    print("Writing data to netCDF file: "+outcdf)
    system("/bin/rm -f "+outcdf)
    ncdf     = addfile(outcdf,"c")  ; open output netCDF file
    ncdf@title     = "McSnow vertical profiles processed by NCL"
    ncdf@date      = systemfunc("date")    ; the netCDF file.
    ncdf@basedir   = basedir
    ncdf@expdir    = expdir
    ncdf@asciifile = filename

    ncdf->height = z(0,:)

    ncdf->Ns = Ns

    ncdf->Nd = Nd
    ncdf->Nd_mm1 = Nd_mm1
    ncdf->Nd_unr = Nd_unr
    ncdf->Nd_rimed = Nd_rimed
    ncdf->Nd_mori = Nd_mori
    ncdf->Nd_grp = Nd_grp
    ncdf->Nd_liq = Nd_liq

    ncdf->Md = Md
    ncdf->Md_mm1 = Md_mm1
    ncdf->Md_unr = Md_unr
    ncdf->Md_rimed = Md_rimed
    ncdf->Md_mori = Md_mori
    ncdf->Md_grp = Md_grp
    ncdf->Md_liq = Md_liq

    ncdf->Fn = Fn
    ncdf->Fn_mm1 = Fn_mm1
    ncdf->Fn_unr = Fn_unr
    ncdf->Fn_rimed = Fn_rimed
    ncdf->Fn_mori = Fn_mori
    ncdf->Fn_grp = Fn_grp
    ncdf->Fn_liq = Fn_liq

    ncdf->Fm = Fm
    ncdf->Fm_mm1 = Fm_mm1
    ncdf->Fm_unr = Fm_unr
    ncdf->Fm_rimed = Fm_rimed
    ncdf->Fm_mori = Fm_mori
    ncdf->Fm_grp = Fm_grp 
    ncdf->Fm_liq = Fm_liq

    ncdf->rime_tot = rime_tot
    ncdf->dep_tot  = dep_tot
    ncdf->dep_mm1  = dep_mm1
    ncdf->zdiag = zdiag
    ncdf->heat_tot = heat_tot

  end if
;=================================================;
  print("Finished "+fodir+foname+"."+foform)
;=================================================;

  end

