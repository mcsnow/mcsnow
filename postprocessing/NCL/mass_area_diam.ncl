;================================================;
; These files are loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"   
; ================================================;
;
; Copyright (C) 2004-2024, DWD
; See ./AUTHORS.txt for a list of authors
; See ./LICENSES/ for license information
; SPDX-License-Identifier: BSD-3-Clause
;

begin

;  itype_geo = 3   ; 1) bulk, 2) fill-in 3) similarity
;  rhor = 600

; open file and read in data

  basedir   = "../experiments/"
  filedir1  = basedir+"check_c"+itype_geo+"_v2/"
  filedir2  = basedir+"check_c"+itype_geo+"_v1/"
  filename  = "mass_area_diam_"+rhor+".dat"

;=================================================;

  wks  = gsn_open_wks("pdf","mass_area_diam_rho"+rhor+"_c"+itype_geo)
  gsn_define_colormap(wks,"BkBlAqGrYeOrReViWh200")

;=================================================;

  print("Reading "+filedir1+filename)
  ncol1 = numAsciiCol(filedir1+filename) 
  nrow1 = numAsciiRow(filedir1+filename) 
  print(" ncol1 = "+ncol1)
  print(" nrow1 = "+nrow1)
  data1 = asciiread(filedir1+filename,-1,"float")

  print("Reading "+filedir2+filename)
  ncol2 = numAsciiCol(filedir2+filename) 
  nrow2 = numAsciiRow(filedir2+filename) 
  print(" ncol2 = "+ncol2)
  print(" nrow2 = "+nrow2)
  data2 = asciiread(filedir2+filename,-1,"float")

  npts1 = toint(sqrt(dimsizes(data1) / 8))
  npts2 = toint(sqrt(dimsizes(data2) / 8))
  print(" npts1 = "+npts1)
  print(" npts2 = "+npts2)
  if (npts1.eq.npts2) then
    npts = npts1
  else
    print("ERROR: Files with different data layout")
    exit
  end if

  data1 := onedtond(data1, (/npts,npts,8/))
  data2 := onedtond(data2, (/npts,npts,8/))

  mice  = data1(:,:,0)
  mrime = data1(:,:,1) 
  vrime = data1(:,:,2) 
  dagg  = data1(:,:,3) 
  diam  = data1(:,:,4) 
  area  = data1(:,:,5) 
  aagg  = data1(:,:,6) 
  vtKC  = data1(:,:,7) 
  vtHW  = data2(:,:,7) 

  rho_rime = mrime(0,0)/vrime(0,0)

;=================================================;

; basic resources for contour plot

  res = True
  res@gsnDraw        = True
  res@gsnFrame       = False                        
  res@cnInfoLabelOn  = False

  res@tmXBOn         = False               ; in this case tickmarks
  res@tmXTOn         = False               ; are wrong anyway
  res@tmYLOn         = False
  res@tmYROn         = False

  res@cnLineThicknessF         = 1.5
  res@cnLineLabelInterval      = 1
  res@cnLineLabelPlacementMode = "Computed"
  res@cnLineLabelFormat        = "@f"

  res@gsnLeftString            = "~F33~r~F21B~rime~N~="+rho_rime+" kg m~S~-3"

; resources for separate log-log axis plot

  resAxis                   = True    
  resAxis@gsnDraw           = True                     
  resAxis@gsnFrame          = False                        
  resAxis@cnLineLabelsOn    = False    ; Turn off line labels.
  resAxis@cnInfoLabelOn     = False    ; Turn off informational
  resAxis@cnLinesOn         = False

  resAxis@sfYCStartV    = min(mrime)
  resAxis@sfYCEndV      = max(mrime)
  resAxis@sfXCStartV    = min(mice)
  resAxis@sfXCEndV      = max(mice)
  resAxis@tiXAxisString = "m~B~ice~N~ in kg"   ; Label for X axis.
  resAxis@tiYAxisString = "m~B~rime~N~ in kg"   ; Label for Y axis.
  resAxis@trXLog        = True    ; Use log scale.
  resAxis@trYLog        = True    ; Use log scale.

  resAxis@tmXTOn         = False    
  resAxis@tmYROn         = False

;=================================================;

; contour plot diam(mi,mr) 

  res1 = res
  res1@gsnRightString            = "D in mm"
  res1@cnLevelSelectionMode     = "ExplicitLevels"
  res1@cnLevels                 = (/0.01,0.03,0.1,0.3,1,3,10,30,100/) 
  res1@cnLineLabelDensityF      = 1.4   ; modifies number of labels

  plot1 = gsn_csm_contour(wks,diam*1e3,res1)     

  plot2 = gsn_csm_contour(wks,diam,resAxis) 

  frame(wks)

; contour plot area(mi,mr) 

  res2 = res
  res2@gsnRightString            = "A in mm~S~2"
  res2@cnLevelSelectionMode     = "ExplicitLevels"
  res2@cnLevels                 = (/0.001,0.01,0.1,1,10,100,1000/) 
  res2@cnLineLabelDensityF      = 1.4   ; modifies number of labels

  plot1 = gsn_csm_contour(wks,area*1e6,res2)     

  plot2 = gsn_csm_contour(wks,area,resAxis) 

  frame(wks)

;=================================================;
  
; contour plot vt(mi,mr) 

  res3 = res
  res3@gsnRightString            = "v~B~t~N~ in m/s"
  res3@cnLevelSelectionMode     = "ExplicitLevels"
  res3@cnLevels                 = (/0.01,0.03,0.1,0.5,1,2,5,10/) 
  res3@cnLineThicknessF         = 1.5
  res3@cnLineLabelInterval      = 1
  res3@cnLineLabelPlacementMode = "Computed"
  res3@cnLineLabelFormat        = "@f"
  res3@cnLineLabelDensityF      = 1.65   ; modifies number of labels

  plot1 = gsn_csm_contour(wks,vtKC,res3)     

  res3@cnLineLabelsOn    = False    ; Turn off line labels.
  res3@cnLineDashPattern = 1
  plot2 = gsn_csm_contour(wks,vtHW,res3)     

; create separate plot of axis tickmarks and labels

  plot3 = gsn_csm_contour(wks,vtKC,resAxis) 

; Manually create legend

  res_text                    = True                  ; text mods desired
  res_text@txFontHeightF      = 0.015                 ; change text size
  res_text@txJust             = "CenterLeft"          ; text justification

  res_lines1                   = True                  ; polyline mods desired
  res_lines1@gsLineDashPattern = 0                     ; solid line
  res_lines1@gsLineThicknessF  = 2.                    ; line thicker
  res_lines1@gsLineColor = 1 

  res_lines2 = res_lines1
  res_lines2@gsLineDashPattern = 1                    ; solid line
  y0 = 0.3
  yD = 0.035 
  x0 = 0.58
  x1 = x0+0.03
  x2 = x1+0.01
  xline = (/x0,x1/)
  yline = (/y0,y0/)
  gsn_polyline_ndc(wks,xline,yline,res_lines1)
  gsn_polyline_ndc(wks,xline,yline-yD,res_lines2)
  gsn_text_ndc(wks,"KC05",x2,y0,res_text)     
  gsn_text_ndc(wks,"HW10",x2,y0-yD,res_text)     

  frame(wks)

;=================================================;

  end

