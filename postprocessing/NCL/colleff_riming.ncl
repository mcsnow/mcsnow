;================================================;
; These files are loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"   
load "NCL/ucla_code.ncl"
; ================================================;
;
; Copyright (C) 2004-2024, DWD
; See ./AUTHORS.txt for a list of authors
; See ./LICENSES/ for license information
; SPDX-License-Identifier: BSD-3-Clause
;

begin

  itype_geo = 2   ; 1) bulk, 2) fill-in 3) similarity

  if (.not.isvar("Fr")) then
    Fr = 100
  end if
  if (.not.isvar("rhor")) then
    rhor = 600
  end if

; open file and read in data

  basedir   = "../experiments/"
  filedir1  = basedir+"check_c"+itype_geo+"_v2/"
  filedir2  = basedir+"check_c"+itype_geo+"_v1/"
  filename  = "colleff_riming_Fr"+sprinti("%3.3i",Fr)+"_rhor"+rhor+".dat"

;=================================================;

  if (fileexists(basedir+"/Plots/")) then
    fodir  =basedir+"/Plots/"
  else
    fodir = ""
  end if
  foname = "colleff_riming_Fr"+sprinti("%3.3i",Fr)+"_rhor"+rhor+"_c"+itype_geo
  foform = "pdf"

  wks  = gsn_open_wks(foform,fodir+foname)
  uclales_define_colormap(wks,"BkBlAqGrYeOrReViWh200+grey")

  pi = 3.14159265358979323846264338327

;=================================================;

  print("Reading "+filedir1+filename)
  ncol1 = numAsciiCol(filedir1+filename) 
  nrow1 = numAsciiRow(filedir1+filename) 
  print(" ncol1 = "+ncol1)
  print(" nrow1 = "+nrow1)
  data1 = asciiread(filedir1+filename,-1,"float")

  print("Reading "+filedir2+filename)
  ncol2 = numAsciiCol(filedir2+filename) 
  nrow2 = numAsciiRow(filedir2+filename) 
  print(" ncol2 = "+ncol2)
  print(" nrow2 = "+nrow2)
  data2 = asciiread(filedir2+filename,-1,"float")

  if (nrow1.ne.nrow2) then
    print("ERROR: Files with different data layout")
    exit
  end if

  fpts = 7
  npts = dimsizes(data1)/ncol1/fpts

  print(" fpts = "+fpts)
  print(" npts = "+npts)

  data1 := onedtond(data1, (/fpts,npts,ncol1/))
  data2 := onedtond(data2, (/fpts,npts,ncol1/))

  Di = data1(:,:,0)
  Dc = data1(:,:,1) 
  Eboehm  = data1(:,:,2) 
  Eboehm2 = data2(:,:,2) 
  Ebulk   = data1(:,:,3) 
  Ebulk2  = data2(:,:,3) 
  Esphere = data1(:,:,4) 

; rime density  
  rho_rime = data1(:,:,5) 

;  Eboehm  = where(Dc.ge.Di,0.0,Eboehm)

  labels_Dcloud = sprinti("%4.0i",toint(Dc(:,0)*1e6))

 ;=================================================;

; basic resources for contour plot

  res = True
  res@gsnDraw        = True
  res@gsnFrame       = False                        

  res@gsnLeftString          = "F~B~rime~N~="+Fr/100.+", ~F33~r~F21B~rime~N~="+rhor+" kg m~S~-3"

  res@pmLegendDisplayMode    = "Always"            ; turn on legend
  res@pmLegendSide           = "Top"               ; Change location of 
  res@pmLegendParallelPosF   = 0.15                ; move units right
  res@pmLegendOrthogonalPosF = -0.42               ; move units down
  res@pmLegendWidthF         = 0.05                ; Change width and
  res@pmLegendHeightF        = 0.18                ; height of legend.
  res@lgPerimOn              = False               ; turn off box around
  res@lgLabelFontHeightF     = .016                 ; label font height
  res@xyExplicitLegendLabels = "  "+labels_Dcloud+" ~F33~m~F21~m"
  res@lgItemOrder            = (/3,2,1,0/)

  res@xyLineThicknesses      = (/1.,1.,1.,1.,1.,1.,1.,1.,1./) * 3.
  res@xyLineColors           = (/1,30,50,78,120,140,170,208/)
  res@xyDashPatterns        = (/0,0,0,0,0,0,0,0/)

;=================================================;

; diameter as function of Di in log space

  res1 := res
  res1@trXLog   = True

  res1@trXMinF = 1e-3
  res1@trXMaxF = 10.

  res1@tiXAxisString = "D in mm" 

  res1@gsnRightString = "Cober&List"
  res1@tiYAxisString  = "bulk collision efficiency" 

  plot1 = gsn_csm_xy(wks,Di*1e3,Ebulk,res1)
  frame(wks)

  plot1 = gsn_csm_xy(wks,Di*1e3,Ebulk2,res1)
  frame(wks)

  res1@gsnRightString = "Bohm"
  res1@tiYAxisString  = "collision efficiency" 

  plot1 = gsn_csm_xy(wks,Di*1e3,Eboehm,res1)
  frame(wks)

  plot1 = gsn_csm_xy(wks,Di*1e3,Eboehm2,res1)
  frame(wks)

;  plot1 = gsn_csm_xy(wks,Di*1e3,Esphere,res1)
;  frame(wks)

  res1 := res
  res1@tiXAxisString = "D in mm" 
  res1@trXLog   = True

  res1@gsnLeftString  = ""
  res1@gsnRightString = "Cober&List"
  res1@tiYAxisString  = "rime density in kg m~S~-3" 
  res1@trXMinF = 5e-2
  res1@trXMaxF = 30.

  plot1 = gsn_csm_xy(wks,Di*1e3,rho_rime(0:,:),res1)
  frame(wks)

;=================================================;
  print("Finished "+fodir+foname+"."+foform)
;=================================================;

  end

