#!/bin/bash
#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

ncl rhor=600 itype_geo=2 NCL/mass_area_diam.ncl
ncl rhor=200 itype_geo=2 NCL/mass_area_diam.ncl
#ncl rhor=600 itype_geo=3 NCL/mass_area_diam.ncl
ncl itype_geo=2 NCL/mass_area_diam_rho.ncl
#ncl itype_geo=3 NCL/mass_area_diam_rho.ncl

ncl itype_geo=2 NCL/mass_geo_vt.ncl
#ncl itype_geo=3 NCL/mass_geo_vt.ncl

ncl itype_geo=2 NCL/mass_colleff.ncl
#ncl itype_geo=3 NCL/mass_colleff.ncl

ncl Fr=100 NCL/colleff_riming.ncl
ncl Fr=0 NCL/colleff_riming.ncl
