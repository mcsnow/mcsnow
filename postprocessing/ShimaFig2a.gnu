#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set term postscript enhanced color size 5,5
set out "runi-sd-shima-fig2a-2-17.eps"
set xtics out
set linestyle 1 linewidth 3
set linestyle 2 linewidth 3
set xlabel "radius [um]"
set ylabel "g(ln r) [g/m3/(ln m)]"
set logscale x
p "../experiments/run_sd_shima_fig2a_sd131072/mass_dens.dat" w l ls 1 title "SDM", \
  "../experiments/run_sd_shima_fig2a_sd131072/ana_mass_dens.dat" w l ls 2 title "analytical"
