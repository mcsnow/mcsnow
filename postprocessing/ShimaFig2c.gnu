#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set term postscript enhanced color size 5,5
#set out "run-sd-shima-fig2c-2-17.eps"
set out "run-sd-shima-fig2c-2-18-v2.eps"
#set out "run-sd-shima-fig2c-2-19.eps"
#set out "run-sd-shima-fig2c-2-20.eps"
set xtics out
set linestyle 1 linewidth 3
set linestyle 2 linewidth 3
set xlabel "radius [um]"
set ylabel "g(ln r) [g/m3/(ln m)]"
set logscale x
set style increment user
#p "../experiments/run_sd_shima_fig2c_sd131072/mass_dens.dat" w l ls 1 title "SDM"
#p "../experiments/run_sd_shima_fig2c_sd262144/mass_dens.dat" w l ls 1 title "SDM"
p "../experiments/run_sd_shima_fig2c_sd262144_v2/mass_dens.dat" w l ls 1 title "SDM"
#p "../experiments/run_sd_shima_fig2c_sd524288/mass_dens.dat" w l ls 1 title "SDM"
#p "../experiments/run_sd_shima_fig2c_sd1048576/mass_dens.dat" w l ls 1 title "SDM"
