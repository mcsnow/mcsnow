#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set term postscript enhanced color size 6,6
#set key top center
set key spacing 1.5
set tics out

set style line 1 lt 3 lc rgb "red" lw 6
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 6
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3
set style line 8 lt 9 lc rgb "orange" lw 6
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3

#set palette rgb 34,35,36 # black-red-yellow-white
set palette model CMY rgbformulae 7,5,15 # white-green-blue
#set palette rgb 30,31,32  # black-blue-violet-yellow-white
#set logscale x
#set logscale y
#set format x "10^{%T}"
#set format y "10^{%T}"
#set format z "10^{%T}"
#set format cb "10^{%T}"


#
# 2d plots
#


set ylabel "z [km]"
set yrange [0:5]

set output "atmo_rho.eps"
set xlabel "rho [kg/m3]"
p "../experiments/check/atmo.dat" u 2:($1/1000) title "rho" w l
set output "atmo_T.eps"
set xlabel "T [K]"
p "../experiments/check/atmo.dat" u 3:($1/1000) title "T" w l
set output "atmo_p.eps"
set xlabel "p [kPa]"
p "../experiments/check/atmo.dat" u ($4/1000):($1/1000) title "p" w l
set output "atmo_eta.eps"
set xlabel "eta [g/ms]"
set xrange [0.0125:0.0175]
set xtics 0.0125,0.002,0.0175
p "../experiments/check/atmo.dat" u ($5*1000):($1/1000) title "eta" w l
set output "atmo_sat.eps"
set xlabel "sat [1]"
set xrange [-1:1]
set xtics auto
p "../experiments/check/atmo.dat" u 6:($1/1000) title "sat" w l
set output "atmo_rh.eps"
set xlabel "RH [%]"
set xrange [0:110]
set xtics auto
p "../experiments/check/atmo.dat" u 7:($1/1000) title "RH" w l
set output "atmo_psat.eps"
set xlabel "[Pa]"
set xrange [0:700]
p "../experiments/check/atmo.dat" u 8:($1/1000) title "psat,w" w l, \
  "../experiments/check/atmo.dat" u 9:($1/1000) title "psat,i" w l
