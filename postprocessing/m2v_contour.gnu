#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

set tics out
set lmargin 9
set tmargin 2
show margin
set colorbox vertical user origin .88, .2 size .02, .7

set style line 1 lt 3 lc rgb "red" lw 6
set style line 2 lt 1 lc rgb "red" lw 3
set style line 3 lt 1 lc rgb "red" lw 3
set style line 4 lt 1 lc rgb "red" lw 3
set style line 5 lt 4 lc rgb "green" lw 6
set style line 6 lt 1 lc rgb "green" lw 3
set style line 7 lt 1 lc rgb "green" lw 3
set style line 8 lt 9 lc rgb "orange" lw 6
set style line 9 lt 1 lc rgb "orange" lw 3
set style line 10 lt 1 lc rgb "orange" lw 3

set palette rgb 30,31,32  # black-blue-violet-yellow-white

#
# 1d plot
#

set term postscript enhanced color size 6,5

fr00=0.0
fr05=0.5
fr09=0.9
set logscale x
set format x "10^{%T}"
set xlabel "diam [mm]"
set ylabel "term. vel. [m/s]"
set xrange [1e-2:100]
set yrange [0:3]
set key top left

set output "v_HW10_1d.eps"
set title 'HW10 term. velocity' 
p "../experiments/check_c1_v1/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr00)<1e-2?$8:1/0) title "Fr=0.0" w lp ls 2, \
  "../experiments/check_c1_v1/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr05)<1e-2?$8:1/0) title "Fr=0.5" w lp ls 6, \
  "../experiments/check_c1_v1/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr09)<1e-2?$8:1/0) title "Fr=0.9" w lp ls 10
set output "v_KC05_1d.eps"
set title 'KC05 term. velocity' 
p "../experiments/check_c1_v2/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr00)<1e-2?$8:1/0) title "Fr=0.0" w lp ls 2, \
  "../experiments/check_c1_v2/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr05)<1e-2?$8:1/0) title "Fr=0.5" w lp ls 6, \
  "../experiments/check_c1_v2/mass_area_diam.dat" u ($5*1000):(abs($2/($1+$2)-fr09)<1e-2?$8:1/0) title "Fr=0.9" w lp ls 10


reset

#
# 2d plot
#

set term postscript enhanced color size 6,5

set view map
set contour base
set logscale x
set logscale y
unset surface
set table 'cont-HW10.dat'
set cntrparam levels discrete 1e-2, 2e-2, 1e-1, 5e-1, 1, 2, 5, 10
sp "../experiments/check_c2_v1/mass_area_diam.dat" u 1:2:8
unset table
set table 'cont-KC05.dat'
set cntrparam levels discrete 1e-2, 2e-2, 1e-1, 5e-1, 1, 2, 5, 10
sp "../experiments/check_c2_v2/mass_area_diam.dat" u 1:2:8
unset table

set logscale x
set logscale y
set format x "10^{%T}"
set format y "10^{%T}"
set output "v_KC05_HW10.eps"
set xrange [1e-14:1e-2]
set yrange [1e-14:1e-2]
set cbrange [1e-2:10]
set view map

set multiplot

set xlabel 'm_i[kg]'
set ylabel 'm_r[kg]'

set key at 1.2e-3,1e-12
p 'cont-KC05.dat' w l lt 1 lw 1.5 title 'KC05'

set key at 1.2e-3,1e-13
l '<./cont.sh cont-HW10.dat 0 10 0'
p '<./cont.sh cont-HW10.dat 1 10 0' w l lt -1 lw 1.5 title 'HW10'

unset multiplot
