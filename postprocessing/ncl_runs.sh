#!/bin/bash
#
# Copyright (C) 2004-2024, DWD
# See ./AUTHORS.txt for a list of authors
# See ./LICENSES/ for license information
# SPDX-License-Identifier: BSD-3-Clause
#

mult=005         # multiplicity (10 to 200)
vt=1             # 1: KC05, 2: HW10 fall speed
          
for lwc in 07; do
    for rt in 1 2; do   # 1: cont., 2: stoch. riming  
	for geo in 2 22 3; do
	    ncl_post.sh 1d_axel_xi${mult}_nz250_lwc${lwc}_iwc100_dtc5_nrp500_rm10_rt${rt}_vt${vt}_h0-20_ba400_geo${geo}
	done
    done
done

prefix=../experiments/1d_axel_xi005_nz250_lwc07_iwc100_dtc5_nrp500_rm10_rt1_vt1_h0-20_ba400
texdir=/media/nas/uwork1/aseifert/JUSSI/TEX/GIT/18399166xwsvdqmtwnnf/FIG/new

for geo in 2 22 3; do
    cp ${prefix}_geo${geo}/Plots/hprofiles.pdf ${texdir}/hprofiles_geo${geo}.pdf
    cp ${prefix}_geo${geo}/Plots/distributions_rime.pdf ${texdir}/distributions_rime_geo${geo}.pdf
    cp ${prefix}_geo${geo}/Plots/distributions_rhorime.pdf ${texdir}/distributions_rhorime_geo${geo}.pdf
done


